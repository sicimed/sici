function calculoImcPC(){
    var peso = $('#peso').val();
    var talla = $('#talla').val();

    var pesoKg = peso/2.20;
    var tallaM = talla/100;

    var imc = pesoKg/(tallaM*tallaM);

    $('#imc').val(imc.toFixed(2));

    if(talla==0){
        pc=0;
    }else{
        var pc = ((talla/2)+9.5);
    }

    $('#pc').val(pc.toFixed(2));
}
