/*************************************** Variables *************************************************/
//Secuence line
var id;
var total = 0;
var details = [];
var formatData = {};
var temp = 0;
/****************************************   Events  *************************************************/
//Event page load
$(document).ready(function(){
	//init secuence
   id = 0;
});

$(".dbtnNewLineEntry").click(function(){
	  alert("The paragraph was clicked.");
	});


//Event new entry line
$(document).on('click', '.btnNewLineEntry', function () {
	
	var valid = validateLine();
	
	if(valid){
		id++;
		var line = 	'<tr> <th>'+ id +'</th>'+
					'<th><input id="id-prod" value ="'+$('#producto').val()+'" hidden />'+$('#producto option:selected').text()+'</th>'+
					'<th>'+$('#descripcion').val()+'</th>'+
					'<th>'+$('#cantidad').val()+'</th>'+
					'<th>'+$('#costo').val()+'</th>'+
					'<th>'+$('#lote').val()+'</th>'+
					'<th>'+$('#vence').val()+'</th>'+
					'<th> <button class = "btn btn-sm btn-danger"><span class = "fa fa-trash dropBtn"></span></button></th>'+
					'</tr>	';
		
		$('#table-entries tbody').append(line);	
		
		total += ($('#costo').val() * $('#cantidad').val());
		//asignacion de total
		$('#totalCompra').val(total);
		$('#table-entries tfoot').find('#thtotal').text(total);
	}
});

//function: to valid data
function validateLine(){
	var valid = true;
		if( $('#producto').val() == "" || $('#producto').val() == null ) {
			valid = false;
		}
		if( $('#descripcion').val() == "" || $('#descripcion').val() == null ) {
			valid = false;
		}
		if( $('#cantidad').val() == "" || $('#cantidad').val() == null || $('#cantidad').val() <= 0) {
			valid = false;
		}
		if( $('#costo').val() == "" || $('#costo').val() == null || $('#costo').val() < 0) {
			valid = false;
		}
		
		if(!valid) { alert("Verifique los datos nuevamente.");}
	return valid;
}

function pickData(){
	
    /** Picking data **/
    //Entry details
    details = []
    $('#table-entries tbody').find('tr').each(function(){
    	details.push({
    			idProd : $(this).find('th').eq(1).find('input').val(),
    			desc : $(this).find('th').eq(2).text(),
    			cant : $(this).find('th').eq(3).text(),
    			cost : $(this).find('th').eq(4).text(),
    			lote : $(this).find('th').eq(5).text(),
    			vence : $(this).find('th').eq(6).text()
    	})
    })
    
    
    // Json Format
    formatData = {
    	tipoComprobante : $("#tipoComprobante").val(),
    	fecha : $("#fecha").val(),
    	totalCompra : $("#totalCompra").val()
    }
    
    console.log("Valor de json: \n"+JSON.stringify(formatData)+"\n\n");	
}

//Event send entries 
$("#saveBtn").click(function(event){
	
	temp = 0;
	
	$('#form_register_ingreso').submit();
	
	event.preventDefault();
});

$("#tempBtn").click(function(){
	
	temp = 1;
	
	$('#form_register_ingreso').submit();
	
	event.preventDefault();
});


$('#form_register_ingreso').submit(function (evt) {
	var url = "";
	if(temp == 0){ url = "/producto/ingresos";	}
	else{	url = "/producto/ingresos/temp/list";	}
    
    //pickData
	pickData();
	
    // Send data by ajax
	$.ajax({
	  type:"GET",
	  contentType : "application/json",
	  url:"/producto/ingresar", //cambiar url
	  cache:false,
	  timeout : 600000,
	  data:formatData,
	  dataType:'json',	  
	  success:function (response) {
	   console.log(response);
	   
	   var error = 0;
	   console.log("\ Contenido de los detalles: \n"+details);
	   //Ingreso de Detalles
	   	error = ingresarDetalles(details);
	   	alert("ingreso de detalles presenta error?: "+error+"\n");
   		//save secuence
		saveIngreso(error);
		   
	    alert("Ingreso de productos exitoso.");
	    
	    $(location).attr('href',url);
	    
	  },
	  error:function () {
		  //$('.alert-danger').display('block');
	      alert("Error durante el ingreso de productos.");
	  }
	});
});

function ingresarDetalles(details){
	   var error = 0;
	   
	   $.each(details, function( index, value ) {
		   
		    // Send details data by ajax
			$.ajax({
			  type:"GET",
			  contentType : "application/json",
			  url:"/producto/ingresardet", //cambiar url
			  cache:false,
			  data:value,
			  dataType:'json',
			  success:function (response) {
				  error = response.error;
				  
				  console.log(response);
				  
				  alert("Ingreso de detalles de productos exitoso.");
			  },
			  error:function () {
				  error = 1;
			      alert("Error durante el ingreso de detalles de productos.");
			  }
			});		   		   
		 });
	   
	return error;
}

function saveIngreso(error){
	alert("Llega al save script");
	//save secuence
	$.ajax({
		type:"GET",
		contentType : "application/json",
		url: "/producto/ingresarsave", //cambiar url
		cache:false,
		data:{save: error,temp:temp},
		dataType:'json',
		success:function (response) {
			console.log(response);
			alert(response);
		},
		error:function () {
		     alert("Error durante el guardado de ingreso de productos.");
		}
	});			
}

$('.dropBtn').click(function(){
	var cant = parseFloat($(this).closest('tr').find('th').eq(3).text());
	var costo = parseFloat($(this).closest('tr').find('th').eq(4).text());
	
	total -= (cant*costo);
	//asignacion de total
	$('#totalCompra').val(total);
	$('#table-entries tfoot').find('#thtotal').text(total);
	
	
	$(this).closest('tr').remove();
	
});

$('#producto').change(function(){
	$.ajax({
		  type:"GET",
		  contentType : "application/json",
		  url:"/producto/ingresargetcost", //cambiar url
		  cache:false,
		  data:{id: $("#producto").val()},
		  dataType:'json',
		  success:function (response) {
			  if(response.result == 1){
				  $("#costo").val(response.costo);
				  console.log("El costo es "+String(response.costo));
			  }
			  else{
				  console.log("No se pudo recuperar el costo");
			  }
		  },
		  error:function () {
		      console.log("No se pudo recuperar el costo");
		  }
		});	
});

