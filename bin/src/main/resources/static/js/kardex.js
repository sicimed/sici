//Event new entry line
$(document).on('click', '#btnQuery', function () {
	var params = {
		idProd:$('#producto').val(),
		fini: $('#fini').val(),
		fend: $('#fend').val()
	};
	
	if(validateDatesKardex()){
		
		//console.log("Params : \n"+JSON.stringify(params));
		//console.log("Url: "+ $('#query_kardex').attr('action')+"/get"); 
		$.ajax({
			  type:"GET",
			  contentType : "application/json",
			  url:"/producto/kardexget", //cambiar url
			  cache:false,
			  timeout : 600000,
			  data: params,
			  dataType:'json',	  
			  success:function (data) {
				 //Limpiar tabla 
				  $("#tablakardex tbody").find('tr').remove();
				  
				  //load lines data 
				  //console.log("Data : \n"+data.list)
				  
				  if(data.list != null && data.list != ""){

					   $.each(data.list, function( index, mov ) {
						   var cant = mov.cantidad;
						   var costoUnit = mov.costoUnit;					   
						   
						   var line =  "<tr><td>"+mov.fecha+"</td><td >"+mov.concepto+"</td>";
							if(cant>=0){
								line =line+"<td>"+cant+"</td>" 
								+"<td >"+costoUnit+"</td>" 
								+"<td >"+(cant*costoUnit)+"</td>"
								+"<td ></td>" 
								+"<td ></td>" 
								+"<td ></td>";
							}else{
								line =line + "<td ></td>" 
								+"<td ></td>" 
								+"<td ></td>"
								+"<td >"+(cant*(-1))+"</td>" 
								+"<td >"+costoUnit+"</td>" 
								+"<td >"+(cant*costoUnit*(-1))+"</td>";							
							}					 
							line= line + "<td >"+mov.salCantidad+"</td>" 
									+"<td >"+mov.producto.costoProm+"</td>"
									+"<td >"+mov.salTotal+"</td>" 										
								+"</tr>";
						   
							//console.log(line);
							
							$('#tablakardex tbody').append(line);	
						 });	  
				  }else{
					  console.log("Sin resultados");
					  //limpiar filas
					  clearRegistersKardex();
				  }
				  
			  },
			  error:function () {
			      console.log("Error durante la consulta");
			     //Limpiar filas
			      clearRegistersKardex();
			  }
			});	
		
	}
	else {
		alert("Verifique las fechas : "+$('#fini').val()+ ", "+$('#fend').val());
	}
	
});

function validateDatesKardex(){
	var result = true;
	if($('#fini').val() != null && $('#fend').val() != null && $('#fini').val() != "" && $('#fend').val() != ""){
		if($('#fini').val() > $('#fend').val()){
			result = false;
		}
	}	
	return result;
}

function clearRegistersKardex(){
	$("#tablakardex tbody").find("tr").remove();
}