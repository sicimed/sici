$(document).ready(function() {
var table = $('#tablapacientes').DataTable({
	
	"lengthMenu": [[5, 10, 15, -1], [5, 10, 15, "Todos los pacientes"]],
	
    language: {
        "decimal": "",
        "emptyTable": "No hay información",
        "info": "Mostrando _START_ a _END_ de _TOTAL_ Pacientes",
        "infoEmpty": "Mostrando 0 to 0 of 0 Pacientes",
        "infoFiltered": "(Filtrando _MAX_ pacientes)",
        "infoPostFix": "",
        "thousands": ",",
        "lengthMenu": "Mostrar _MENU_ Pacientes",
        "loadingRecords": "Cargando...",
        "processing": "Procesando...",
        "search": "Buscar:",
        "zeroRecords": "Sin resultados encontrados",
        "paginate": {
            "first": "Primero",
            "last": "Ultimo",
            "next": "Siguiente",
            "previous": "Anterior"
        }
    },
    responsive: true
});
new $.fn.dataTable.FixedHeader( table );
} );

$(document).ready(function() {
	var table = $('#tablausuario').DataTable({
		
		"lengthMenu": [[5, 10, 15, -1], [5, 10, 15, "Todos los usuarios"]],
		
	    language: {
	        "decimal": "",
	        "emptyTable": "No hay información",
	        "info": "Mostrando _START_ a _END_ de _TOTAL_ Usuarios",
	        "infoEmpty": "Mostrando 0 to 0 of 0 Usuarios",
	        "infoFiltered": "(Filtrando _MAX_ usuarios)",
	        "infoPostFix": "",
	        "thousands": ",",
	        "lengthMenu": "Mostrar _MENU_ Usuarios",
	        "loadingRecords": "Cargando...",
	        "processing": "Procesando...",
	        "search": "Buscar:",
	        "zeroRecords": "Sin resultados encontrados",
	        "paginate": {
	            "first": "Primero",
	            "last": "Ultimo",
	            "next": "Siguiente",
	            "previous": "Anterior"
	        }
	    },
	    responsive: true
	});
	new $.fn.dataTable.FixedHeader( table );
	} );


$(document).ready(function() {
	var table = $('#tablaproductos').DataTable({
		
		"lengthMenu": [[5, 10, 15, -1], [5, 10, 15, "Todos los productos"]],
		
	    language: {
	        "decimal": "",
	        "emptyTable": "No hay información",
	        "info": "Mostrando _START_ a _END_ de _TOTAL_ Productos",
	        "infoEmpty": "Mostrando 0 to 0 of 0 Productos",
	        "infoFiltered": "(Filtrando _MAX_ productos)",
	        "infoPostFix": "",
	        "thousands": ",",
	        "lengthMenu": "Mostrar _MENU_ Productos",
	        "loadingRecords": "Cargando...",
	        "processing": "Procesando...",
	        "search": "Buscar:",
	        "zeroRecords": "Sin resultados encontrados",
	        "paginate": {
	            "first": "Primero",
	            "last": "Ultimo",
	            "next": "Siguiente",
	            "previous": "Anterior"
	        }
	    },
	    responsive: true
	});
	new $.fn.dataTable.FixedHeader( table );
	} );

	$(document).ready(function() {
		var table = $('#tablatipomedicamento').DataTable({
			
			"lengthMenu": [[5, 10, 15, -1], [5, 10, 15, "Todos los tipos de productos"]],
			
		    language: {
		        "decimal": "",
		        "emptyTable": "No hay información",
		        "info": "Mostrando _START_ a _END_ de _TOTAL_ Tipos",
		        "infoEmpty": "Mostrando 0 to 0 of 0 Tipos",
		        "infoFiltered": "(Filtrando _MAX_ tipos)",
		        "infoPostFix": "",
		        "thousands": ",",
		        "lengthMenu": "Mostrar _MENU_ Tipos",
		        "loadingRecords": "Cargando...",
		        "processing": "Procesando...",
		        "search": "Buscar:",
		        "zeroRecords": "Sin resultados encontrados",
		        "paginate": {
		            "first": "Primero",
		            "last": "Ultimo",
		            "next": "Siguiente",
		            "previous": "Anterior"
		        }
		    },
		    responsive: true
		});
		new $.fn.dataTable.FixedHeader( table );
		} );
	
	$(document).ready(function() {
		var table = $('#tablarecetas').DataTable({
			
			"lengthMenu": [[5, 10, 15, -1], [5, 10, 15, "Todas las recetas"]],
			
		    language: {
		        "decimal": "",
		        "emptyTable": "No hay información",
		        "info": "Mostrando _START_ a _END_ de _TOTAL_ Tipos",
		        "infoEmpty": "Mostrando 0 to 0 of 0 Recetas",
		        "infoFiltered": "(Filtrando _MAX_ recetas)",
		        "infoPostFix": "",
		        "thousands": ",",
		        "lengthMenu": "Mostrar _MENU_ Recetas",
		        "loadingRecords": "Cargando...",
		        "processing": "Procesando...",
		        "search": "Buscar:",
		        "zeroRecords": "Sin resultados encontrados",
		        "paginate": {
		            "first": "Primero",
		            "last": "Ultimo",
		            "next": "Siguiente",
		            "previous": "Anterior"
		        }
		    },
		    responsive: true
		});
		new $.fn.dataTable.FixedHeader( table );
		} );

$(document).ready(function() {
	var table = $('#tablageneral').DataTable({

		"lengthMenu": [[5, 10, 15, -1], [5, 10, 15, "Todos los registros"]],

		language: {
			"decimal": "",
			"emptyTable": "No hay información",
			"info": "Mostrando _START_ a _END_ de _TOTAL_ registros",
			"infoEmpty": "Mostrando 0 to 0 of 0 registros",
			"infoFiltered": "(Filtrando _MAX_ registros)",
			"infoPostFix": "",
			"thousands": ",",
			"lengthMenu": "Mostrar _MENU_ registros",
			"loadingRecords": "Cargando...",
			"processing": "Procesando...",
			"search": "Buscar:",
			"zeroRecords": "Sin resultados encontrados",
			"paginate": {
				"first": "Primero",
				"last": "Ultimo",
				"next": "Siguiente",
				"previous": "Anterior"
			}
		},
		responsive: true
	});
	new $.fn.dataTable.FixedHeader( table );
} );

	$(document).ready(function() {
		var table = $('#tablamarca').DataTable({
			
			"lengthMenu": [[5, 10, 15, -1], [5, 10, 15, "Todas las marcas"]],
			
		    language: {
		        "decimal": "",
		        "emptyTable": "No hay información",
		        "info": "Mostrando _START_ a _END_ de _TOTAL_ Marcas",
		        "infoEmpty": "Mostrando 0 to 0 of 0 Marcas",
		        "infoFiltered": "(Filtrando _MAX_ marcas)",
		        "infoPostFix": "",
		        "thousands": ",",
		        "lengthMenu": "Mostrar _MENU_ Marcas",
		        "loadingRecords": "Cargando...",
		        "processing": "Procesando...",
		        "search": "Buscar:",
		        "zeroRecords": "Sin resultados encontrados",
		        "paginate": {
		            "first": "Primero",
		            "last": "Ultimo",
		            "next": "Siguiente",
		            "previous": "Anterior"
		        }
		    },
		    responsive: true
		});
		new $.fn.dataTable.FixedHeader( table );
		} );
	
	$(document).ready(function() {
		var table = $('#tablaproveedores').DataTable({
			
			"lengthMenu": [[5, 10, 15, -1], [5, 10, 15, "Todos los proveedores"]],
			
		    language: {
		        "decimal": "",
		        "emptyTable": "No hay información",
		        "info": "Mostrando _START_ a _END_ de _TOTAL_ Proveedores",
		        "infoEmpty": "Mostrando 0 to 0 of 0 Proveedores",
		        "infoFiltered": "(Filtrando _MAX_ proveedores)",
		        "infoPostFix": "",
		        "thousands": ",",
		        "lengthMenu": "Mostrar _MENU_ Proveedores",
		        "loadingRecords": "Cargando...",
		        "processing": "Procesando...",
		        "search": "Buscar:",
		        "zeroRecords": "Sin resultados encontrados",
		        "paginate": {
		            "first": "Primero",
		            "last": "Ultimo",
		            "next": "Siguiente",
		            "previous": "Anterior"
		        }
		    },
		    responsive: true
		});
		new $.fn.dataTable.FixedHeader( table );
		} );

