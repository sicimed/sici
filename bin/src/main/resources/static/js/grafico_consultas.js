$(function(){
    Highcharts.chart('graficoConsultas', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'MEDICINA INTEGRADA'
        },
        subtitle: {
            text: 'DR. ELMER A. NAVARRO M.'
        },
        xAxis: {
            categories: [[${surveyMap2.keySet()}]],
            crosshair: true
        },
        yAxis: {
            min: 0,
            max:50,
            title: {
                text: 'Consultas rating'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} K</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Fechas',
            data: [[${surveyMap2.values()}]]
        }]
    });
});