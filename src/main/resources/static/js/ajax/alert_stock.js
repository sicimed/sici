$(document).ready(function() {
	if(Notification.permission!=="granted") {
		Notification.requestPermission();
	}
	ajaxGetAlertStock();
	//setInterval(ajaxGetAlertStock()	,180000);
});

function ajaxGetAlertStock(){
	$.ajax({
		type:"GET",
		url:$("#producto-alert").val(),
		success:function(res_){
			var result_ = JSON.parse(res_);				
			if(result_.list != "" && result_.list != null ){
				console.log("log: \n "+res_);
				 $.each(result_.list, function(i, prod){
					 console.log("\n iteracion de productos n "+i);
					 var notificacion1 = new Notification("Recordatorio existencias", {
						    icon: '/sicimed/images/logo-sicimed.png',
						    body: prod.id+" - "+prod.nombre+" | inv: "+prod.inventario+" | lim: "+prod.limStock
						});						 
				 });					 
			}
		},
		error: function(e){
			console.log("ERROR",e);
		}
	});
}