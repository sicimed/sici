$(document).ready(function() {
	if(Notification.permission!=="granted") {
		Notification.requestPermission();
	}
	$("#submiNotification").click(function(event){
		event.preventDefault();
		ajaxGetCitas();
	});
});

function ajaxGetCitas() {
	$.ajax({
		type: "GET",
		url: "/sicimed/consulta_rest/citas_actuales",
		success: function (result) {
			console.log(result.status)
			if (result.status == "Done") {
				$.each(result.data, function (i, cita) {
					var notificacion = new Notification("Recordatorio citas del día !", {
						icon: '/sicimed/images/logo-sicimed.png',
						body: cita.nombre_paciente + " " + "|" + cita.sexo + " |" + cita.hora + " " + cita.tipo_hora
					});
				})

			} else {
				var notificacion = new Notification("No hay citas !", {
					icon: '/sicimed/images/logo-sicimed.png',
					body: "Sin citas pendientes"
				});
			}
		},
		error: function (e) {
			console.log("ERROR", e);
		}
	});
};


