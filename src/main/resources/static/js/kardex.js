$(document).ready(function(){
	$('#btnReportKardex').attr("disabled", true);
});

//Event new entry line
$(document).on('click', '#btnQuery', function () {
	var msg = "";
	var params = {
		idProd:$('#producto').val() == null || $('#producto').val() == ""? 0 : $('#producto').val(),
		fini: $('#fini').val(),
		fend: $('#fend').val()
	};
	$('#idProd').val($('#producto').val() == null || $('#producto').val() == ""? 0 : $('#producto').val());
	$('#fini_').val($('#fini').val());
	$('#fend_').val($('#fend').val());
	
	if(validateDatesKardex()){
		console.log(params);
		$.ajax({
			  type:"GET",
			  contentType : "application/json",
			  url:"kardexget", //cambiar url
			  cache:false,
			  timeout : 600000,
			  data: params,
			  dataType:'json',	  
			  success:function (data) {
				 //Limpiar tabla 
				  $("#tablakardex tbody").find('tr').remove();
				  
				  if(data.list != null && data.list != ""){
					  
					   $.each(data.list, function( index, mov ) {
						   var cant = mov.cantidad;
						   var costoUnit = mov.costoUnit;					   
						   
						   var line =  "<tr><td>"+mov.fecha+"</td><td >"+mov.concepto+"</td>";
							if(cant>=0){
								line =line+"<td>"+cant+"</td>" 
								+"<td >"+costoUnit+"</td>" 
								+"<td >"+(cant*costoUnit)+"</td>"
								+"<td ></td>" 
								+"<td ></td>" 
								+"<td ></td>";
							}else{
								line =line + "<td ></td>" 
								+"<td ></td>" 
								+"<td ></td>"
								+"<td >"+(cant*(-1))+"</td>" 
								+"<td >"+costoUnit+"</td>" 
								+"<td >"+(cant*costoUnit*(-1))+"</td>";							
							}					 
							line= line + "<td >"+mov.salCantidad+"</td>" 
									+"<td >"+mov.producto.costoProm+"</td>"
									+"<td >"+mov.salTotal+"</td>" 										
								+"</tr>";
						   
							//console.log(line);
							
							$('#tablakardex tbody').append(line);	
						 });
						$('#btnReportKardex').attr("disabled", false);
				  }else{
					  console.log("Sin resultados");
					  msg = "Sin resultados";
					  //limpiar filas
					  clearRegistersKardex();
					$('#btnReportKardex').attr("disabled", true);
				  }				  
			  },
			  error:function () {
			      console.log("Sin resultados");
			      msg = "Sin resultados";
			      addNoti(msg);
			     //Limpiar filas
			      clearRegistersKardex();
				$('#btnReportKardex').attr("disabled", true);
			  }
			});		
	}
	else {
		msg = "Verifique las fechas : "+$('#fini').val()+ ", "+$('#fend').val();
		addNoti(msg);
	}
});

function validateDatesKardex(){
	var result = true;
	if($('#fini').val() != null && $('#fend').val() != null && $('#fini').val() != "" && $('#fend').val() != ""){
		if($('#fini').val() > $('#fend').val()){
			result = false;
		}
	}	
	return result;
}

/*function addNoti(msg){
    $("#notis").append("<div id = 'alert' class='alert alert-dark alert-dismissible fade show'>"+msg+
	        "<button type='button' class='close' data-dismiss='alert'>&times;</button></div>");		
}*/

function clearRegistersKardex(){
	$("#tablakardex tbody").find("tr").remove();
}