/*************************************** Variables *************************************************/
//Secuence line
var id;
var total = 0;
var details = [];
var formatData = {};
var temp = 0;
/****************************************   Events  *************************************************/
//Event page load
$(document).ready(function(){
	//init secuence
	//alert("Fecha "+$("#fecha").val());
   id = 0; // contar antes la cantidad de lineas
});

//Event send entries 
$("#saveBtn").click(function(event){	
	temp = 0;
	if(validateFormAdding()){ 	$('#form_register_ingreso').submit();  }	
	//$('#form_register_ingreso').submit();	
	event.preventDefault();
});
$("#tempBtn").click(function(){	
	temp = 1;
	if(validateFormAdding()){ 	$('#form_register_ingreso').submit();  }	
	event.preventDefault();
});
$("#saveTempBtn").click(function(event){	
	temp = 2;	
	if(validateFormAdding()){ 	$('#form_register_ingreso').submit();  }	
	//$('#form_register_ingreso').submit();  	
	event.preventDefault();
});
$("#tempTempBtn").click(function(){	
	temp = 3;	
	if(validateFormAdding()){ 	$('#form_register_ingreso').submit();  }	
	event.preventDefault();
});

//Event new entry line
$(document).on('click', '.btnNewLineEntry', function () {	
	var valid = validateLine();
	
	if(valid){
		id++;
		var line = 	'<tr> <th>'+ id +'</th>'+
					'<th><input id="id-prod" value ="'+$('#producto').val()+'" hidden />'+$('#producto option:selected').text()+'</th>'+
					'<th>'+$('#descripcion').val()+'</th>'+
					'<th>'+$('#cantidad').val()+'</th>'+
					'<th>'+$('#costo').val()+'</th>'+
					'<th>'+$('#lote').val()+'</th>'+
					'<th>'+$('#vence').val()+'</th>'+
					'<th> <button class = "btn btn-sm btn-danger dropBtn"><span class = "fa fa-trash "></span></button></th>'+
					'</tr>	';		
		$('#table-entries tbody').append(line);	
		
		total += ($('#costo').val() * $('#cantidad').val());
		//asignacion de total
		$('#totalCompra').val(total);
		$('#table-entries tfoot').find('#thtotal').text(total);
	}
});


$('#form_register_ingreso').submit(function () {
	var redirect = getRedirect();
    
    //pickData
	pickData();

	    // Send data by ajax
		$.ajax({
		  type:"GET",
		  contentType : "application/json",
		  url:$("#form_register_ingreso").attr('action') +"/ingresari", //cambiar url
		  cache:false,
		  timeout : 600000,
		  data:{ formatData: JSON.stringify({...formatData,details})},//formatData,
		  dataType:'json',	  
		  success:function (response) {
			  var error = 0;
			  console.log(response);
			  console.log("\ Contenido de los detalles: \n"+details);
	   		
			  //save secuence
			  saveIngreso(error,function(){
				  addNoti("Ingreso de productos exitoso.");
				  
				  $(location).attr('href',redirect);
			  });
			   
		  },
		  error:function () {
		      addNoti("Error durante el ingreso de productos.");
		  }
		});
});


$(document).on('click', '.dropBtn', function () {	
	//Si hay lineas en tbody y totall es cero entonces calculo el costo antes de borrar
	if(total <= 0){
		total = 0;
	    $('#table-entries tbody').find('tr').each(function(){	    	
	    	total += ($(this).find('th').eq(3).text() * $(this).find('th').eq(4).text());
	    })		
	}
	
	var cant = parseFloat($(this).closest('tr').find('th').eq(3).text());
	var costo = parseFloat($(this).closest('tr').find('th').eq(4).text());	
	total -= (cant*costo);
	
	//asignacion de total
	$('#totalCompra').val(total);
	$('#table-entries tfoot').find('#thtotal').text(total);
	
	//Eliminando linea
	$(this).closest('tr').remove();	
});


$('#producto').change(function(){
	$.ajax({
		  type:"GET",
		  contentType : "application/json",
		  url:$("#form_register_ingreso").attr('action')+"/ingresargetcost", //cambiar url
		  cache:false,
		  data:{id: $("#producto").val()},
		  dataType:'json',
		  success:function (response) {
			  if(response.result == 1){
				  $("#costo").val(response.costo);
				  console.log("El costo es "+String(response.costo));
			  }
			  else{
				  console.log("No se pudo recuperar el costo");
				  addNoti("No se pudo recuperar el costo");
			  }
		  },
		  error:function () {
		      console.log("No se pudo recuperar el costo");
		      addNoti("No se pudo recuperar el costo");
		  }
		});	
});

/*********************************** Funciones *******************************************/

function getRedirect(){
	if(temp == 0 || temp == 2)
		 { return $("#form_register_ingreso").attr('action')+"/ingresos"; }
	else { return $("#form_register_ingreso").attr('action')+"/ingresos/list/temp";}
}


//function: to valid data
function validateLine(){
	var valid = true;
	
		if( $('#producto').val() 	== "" 	|| $('#producto').val() 	== null){
			addNoti("Seleccione un producto para agregar una nueva l&iacute;nea.");
			valid = false;
		};
		if($('#descripcion').val() == "" 	|| $('#descripcion').val() 	== null ){
			addNoti("Completar descripci&oacute;n para agregar una nueva l&iacute;nea.");
			valid = false;			
		};
		if($('#cantidad').val() 	== "" 	|| $('#cantidad').val() 	== null || $('#cantidad').val() <= 0){
			addNoti("La cantidad debe ser mayor a cero para agregar una nueva l&iacute;nea.");
			valid = false;					
		};
		if($('#costo').val() 		== "" 	|| $('#costo').val() 		== null || $('#costo').val() < 0	){
			addNoti("El costo debe ser positivo o cero para agregar una nueva l&iacute;nea.");
			valid = false;					
		};	
		if( ( ($("#lote").val() != "" && $("#lote").val() != null) && ($("#vence").val() == "" || $("#vence").val() == null) ) || 
			(($("#lote").val() == "" || $("#lote").val() == null) && ($("#vence").val() != "" && $("#vence").val() != null))	
		){
			addNoti("Completar lote y vence respectivo de la l&iacute;nea.");
			valid = false;		
		}			
		
	return valid;
}

function validateFormAdding(){
	var valid = true;
	
	if($("#tipoComprobante").val().trim() == null || $("#tipoComprobante").val().trim() == ""){
		addNoti("Complete el campo del comprobante.");
		valid = false;			
	};
	if($("#tipoComprobante").val().trim() == null || $("#tipoComprobante").val().trim() == ""){
		addNoti("Complete el campo de fecha de ingreso.");
		valid = false;			
	};
	if($("#totalCompra").val() == null || $("#totalCompra").val() == "" || $("#totalCompra").val() < 0 ){
		addNoti("El total de la compra debe ser mayor a cero.");
		valid = false;			
	};	
	if($("#fecha").val() == null || $("#fecha").val() == "" ){
		addNoti("Debe ingresar una fecha");
		valid = false;			
	};	
	return valid;
}

function pickData(){
    /** Picking data **/
    //Entry details
    details = []
    $('#table-entries tbody').find('tr').each(function(){
    	console.log("iteracion de detalles");
    	details.push({
    			idProd : $(this).find('th').eq(1).find('input').val(),
    			desc : $(this).find('th').eq(2).text(),
    			cant : $(this).find('th').eq(3).text(),
    			cost : $(this).find('th').eq(4).text(),
    			lote : $(this).find('th').eq(5).text(),
    			vence : $(this).find('th').eq(6).text()
    	})
    })
    	  	
    formatData = { tipoComprobante : $("#tipoComprobante").val(), fecha : $("#fecha").val(), 
 			   		totalCompra : 	$("#totalCompra").val() }    	

    console.log("Valor de json: \n"+JSON.stringify(formatData)+"\n\n");	
}


function ingresarDetalles(details){
	   var error = 0;
	   //alert("Url: "+$("#form_register_ingreso").attr('action')+"/ingresardet");
	   $.each(details, function( index, value ) {		   
		    // Send details data by ajax
			$.ajax({
			  type:"GET",
			  contentType : "application/json",
			  url:$("#form_register_ingreso").attr('action')+"/ingresardet", //cambiar url
			  cache:false,
			  data:value,
			  dataType:'json',
			  success:function (response) {
				  error = response.error;				  
				  console.log(response);				  
				  addNoti("Ingreso de detalles de productos exitoso.");
			  },
			  error:function () {
				  error = 1;
				  addNoti("Error durante el ingreso de detalles de productos.");
			  }
			});		   		   
		 });	   
	return error;
}


function saveIngreso(error,callback){
	var id = (temp == 2 || temp == 3)? $("#id").val() : 0;  
	//alert("Llega al save script");
	//save secuence
	$.ajax({
		type:"GET",
		contentType : "application/json",
		url: $("#form_register_ingreso").attr('action')+"/ingresarsave", //cambiar url
		cache:false,
		data:{save: error,temp:temp,id: id},
		dataType:'json',
		success:function (response) {
			console.log(response);
			//alert(response);
		},
		error:function () {
		     alert("Error durante el guardado de ingreso de productos.");
		}
	});
	callback();
}



