/*************************************** Variables *************************************************/
//Secuence line
var id;
var existences = null;
/****************************************   Events  *************************************************/
//Event page load
$(document).ready(function(){
	//init secuence
   id = 0; // contar antes la cantidad de lineas
});

//Event send entries 
$("#saveExBtn").click(function(event){	
	pickData();
});

//Event new entry line
$(document).on('click', '.btnNewLineSetting', function () {	
	var valid = validateLine();
	if(valid){
		id++;
		var line = 	'<tr> <th>'+ id +'</th>'+
					'<th><input id="id-ex" value ="'+$('#lote').val()+'" hidden />'+$('#producto_ex option:selected').text()+'</th>'+
					'<th>'+$('#desc').val()+'</th>'+
					'<th>'+$('#cantidad').val()+'</th>'+
					'<th>'+($('#lote option:selected').val()==""?"":$('#lote option:selected').text())+'</th>'+
					'<th> <button class = "btn btn-sm btn-danger dropBtn"><span class = "fa fa-trash "></span></button></th>'+
					'</tr>	';		
		$('#table-existences-settings tbody').append(line);	
	}
});


$(document).on('click', '.dropBtn', function () {	
	//Eliminando linea
	$(this).closest('tr').remove();	
});

$('#cantidad').change(function(){
	var cant = $(this).val();
	var idEx = $("#lote").val();
	
	if(existences != null && existences != ""){
		$.each(existences.list, function( index, ex ) {
			//agrego los option
			if(ex.id == idEx && cant<0 && (cant*-1)>ex.stock){
				console.log("Unidades insuficientes para ajuste.");
				alert("Unidades insuficientes para ajuste.");
			}
		});		
	}
});
$('#lote').change(function(){
	var idEx = $(this).val();
	if(existences != null && existences != ""){
		$.each(existences.list, function( index, ex ) {
			//agrego los option
			if(ex.id == idEx){
				$('#cantidad').prop("min",(ex.stock*-1));
			}
		});		
	}	
});

$('#producto_ex').change(function(){
	
	$("#lote option").each(function (){
		$(this).remove();
	});
	
	$.ajax({
		  type:"GET",
		  contentType : "application/json",
		  url:$('#form_create_existencia').attr("action")+"/get", //cambiar url
		  cache:false,
		  data:{id: $("#producto_ex").val()},
		  dataType:'json',
		  success:function (data) {
			  console.log("Data content: \n"+data)
			  var options = "<option value=''>Seleccione el lote</option> ";
			  
			  if(data.list != null && data.list != ""){
				  existences = data;
				  $.each(data.list, function( index, ex ) {
					  //agrego los option
					  options = options + " <option value ='"+ex.id+"'> L: "+ex.lote+"    v: "+ex.vence +"    u: "+ex.stock+"</option>";
				  });
			  }
			  $('#lote').append(options);	
		  },
		  error:function () {
			  var options = "<option value=''>Seleccione el lote</option> ";
			  $('#lote').append(options);
		      console.log("Error en la recuperación de lotes.");
		  }
		});	
});

/*********************************** Funciones *******************************************/

//function: to valid data
function validateLine(){
		
		var valid = true;
		
		if( $('#producto_ex').val() 	== "" 	|| $('#producto_ex').val() 	== null){
			addNoti("Seleccione un producto para agregar una nueva l&iacute;nea.");
			valid = false;
		};
		if($('#desc').val() == "" 	|| $('#desc').val() 	== null ){
			addNoti("Completar descripci&oacute;n para agregar una nueva l&iacute;nea.");
			valid = false;			
		};
		if($('#lote option:selected').val() == "" ){
			addNoti("Debe seleccionar una existencia.");
			valid = false;			
		};
		if($('#cantidad').val() 	== "" 	|| $('#cantidad').val() 	== null || $('#cantidad').val() == 0){
			addNoti("Ingrese la cantidad para agregar una nueva l&iacute;nea.");
			valid = false;					
		};			
		
	return valid;
}

function pickData(){
    /** Picking data **/
    //Entry details
    $('#table-existences-settings tbody').find('tr').each(function(){
    	console.log("iteracion de detalles");
    	alert(
			"idEx :" +$(this).find('th').eq(1).find('input').val()+
			"desc :" +$(this).find('th').eq(2).text()+
			"cant :"+ $(this).find('th').eq(3).text()
		);
    	//save secuence
    	$.ajax({
    		type:"GET",
    		contentType : "application/json",
    		url: $('#form_create_existencia').attr("action")+"/ingresar", //cambiar url
    		cache:false,
    		data: {
    			idEx : $(this).find('th').eq(1).find('input').val(),
    			desc : $(this).find('th').eq(2).text(),
    			cant : $(this).find('th').eq(3).text()
    		},
    		dataType:'json',
    		success:function (response) {
    			console.log(response);
    		},
    		error:function () {
    		     alert("Error durante el guardado de ingreso de productos.");
    		}
    	});
    });	  	 	
	$.ajax({
		type:"GET",
		contentType : "application/json",
		url: $('#form_create_existencia').attr("action")+"/ingresar/save", //cambiar url
		cache:false,
		data: { error : 0 },
		dataType:'json',
		success:function (response) {
			console.log(response);
			alert(response);
			$(location).attr('href',$('#form_create_existencia').attr("action")+"s");
		},
		error:function () {
		     alert("Error durante el guardado de ingreso de productos.");
		}
	});
}



