package com.sici.reports;

import java.awt.Color;
import java.awt.Toolkit;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfAnnotation;
import com.lowagie.text.pdf.PdfWriter;
import com.sici.controller.DateFunction;
import com.sici.entity.Consulta;
import com.sici.entity.Persona;
import com.sici.entity.Receta;

public class ConsultaPdfView {
    private static final Logger logger = LoggerFactory.getLogger(RecetaPdfView.class);

    public static ByteArrayInputStream consultaReport(Consulta consulta) throws Exception {
    	
        Font fuente1 = new Font(Font.HELVETICA, 12, Font.BOLD, Color.BLACK);
        Font fuente2 = new Font(Font.HELVETICA, 10, Font.BOLD, Color.BLACK);
        Font fuente3 = new Font(Font.HELVETICA, 10);
        Font fuente4 = new Font(Font.HELVETICA, 10, Font.BOLD, Color.BLACK);
        Font fuente5 = new Font(Font.HELVETICA, 10, Font.ITALIC, Color.BLACK);
        
        DateFunction fecha = new DateFunction();
        Document document = new Document();
        ByteArrayOutputStream out = new ByteArrayOutputStream();


        try {
        	
            //contenido de encabezado
        	Paragraph p = new Paragraph("MEDICINA INTEGRADA",fuente1);
        	Paragraph p1 = new Paragraph(" ",fuente1);
            Paragraph p2 = new Paragraph("DR. ELMER A. NAVARRO M.",fuente2);
            Paragraph p3 = new Paragraph("J.V.P.M Nº 4656",fuente2);
            Paragraph p4 = new Paragraph("UNIVERSIDAD DE EL SALVADOR",fuente2);
            p.setAlignment(Element.ALIGN_CENTER);
            p2.setAlignment(Element.ALIGN_CENTER);
            p3.setAlignment(Element.ALIGN_CENTER);
            p4.setAlignment(Element.ALIGN_CENTER);
            
            Paragraph p5 = new Paragraph("Tel: 7010-6443",fuente2);
            Paragraph p6 = new Paragraph("PEDIATRÍA GENERAL.",fuente2);
            
            Paragraph p7 = new Paragraph("Tel: 7010-6443",fuente2);
            Paragraph p8 = new Paragraph("MEDICINA INTERNA",fuente2);
            
            p7.setAlignment(Element.ALIGN_RIGHT);
            p8.setAlignment(Element.ALIGN_RIGHT);
      
          
            Paragraph f = new Paragraph("Fecha:",fuente2);
            f.setAlignment(Element.ALIGN_LEFT);
            
            String f1= fecha.convertDateToSpanish(consulta.getCita().getFecha_consulta()).toString();
            Paragraph f2 = new Paragraph(f1,fuente3);
            f2.setAlignment(Element.ALIGN_LEFT);
            


            //TABLA 1             
            Persona persona = consulta.getCita().getExpediente().getPaciente().getPersona();


            PdfPTable tablaAP = new PdfPTable(1);
            tablaAP.getDefaultCell().setBorder(0);
            tablaAP.setSpacingAfter(20);
            tablaAP.addCell("AP:"+consulta.getAntecendentes());

            PdfPTable tablaDetallePaciente = new PdfPTable(2);
            tablaDetallePaciente.getDefaultCell().setBorder(0);
            tablaDetallePaciente.setSpacingAfter(20);
            tablaDetallePaciente.setWidthPercentage(80);
            tablaDetallePaciente.setWidths(new float[]{3.0f,4.0f});
            
            Paragraph per = new Paragraph(persona.getNombre()+" "+persona.getApellido(),fuente3);
            per.setAlignment(Element.ALIGN_LEFT);
            
            Paragraph edad = new Paragraph(persona.getEdad(),fuente3);
            edad.setAlignment(Element.ALIGN_LEFT);
            
            Paragraph colonia = new Paragraph("Dirección: "+persona.getColonia()+" Teléfono: "+persona.getTelefono(), fuente3);
            colonia.setAlignment(Element.ALIGN_LEFT);
          
            Paragraph motivoConsulta = new Paragraph("Motivo de consulta (CX): "+consulta.getMotivoConsulta(), fuente3);
            motivoConsulta.setAlignment(Element.ALIGN_LEFT);
            
            Paragraph descripcion = new Paragraph("Descripción (PE): "+consulta.getDescripcionMotivo(), fuente3);
            descripcion.setAlignment(Element.ALIGN_LEFT);
            
            
            
            
            //TABLA 2
            Paragraph peso1 = new Paragraph("Peso",fuente4);
            Paragraph peso= new Paragraph(""+ String.valueOf(consulta.getDetallePaciente().getPeso()).concat(" ").concat("lb"), fuente3);
            
            Paragraph talla1 = new Paragraph("Talla",fuente4);
            Paragraph talla= new Paragraph(""+ String.valueOf(consulta.getDetallePaciente().getTalla()).concat(" ").concat("cm"), fuente3);
            
            Paragraph pc1 = new Paragraph("PC",fuente4);
            Paragraph pc= new Paragraph(""+ String.valueOf(consulta.getDetallePaciente().getPc()).concat(" ").concat("cm"), fuente3);
            
            Paragraph temperatura1 = new Paragraph("Temperatura",fuente4);
            Paragraph temperatura= new Paragraph(""+ String.valueOf(consulta.getDetallePaciente().getTemperatura()).concat(" ").concat("°C"), fuente3);
            
            Paragraph presion1 = new Paragraph("Presión Arterial",fuente4);
            Paragraph presion= new Paragraph(""+ String.valueOf(consulta.getDetallePaciente().getPresionArterial()).concat(" ").concat("mmHg"), fuente3);
            
            Paragraph imc1 = new Paragraph("IMC",fuente4);
            Paragraph imc= new Paragraph(""+ String.valueOf(consulta.getDetallePaciente().getImc()).concat(" ").concat("Kg/m2"), fuente3);
            
            Paragraph fc1 = new Paragraph("Frecuencia Cardíaca",fuente4);
            Paragraph fc= new Paragraph(""+ String.valueOf(consulta.getDetallePaciente().getFrecuenciaCardiaca()).concat(" ").concat("lat/min"), fuente3);
           
            Paragraph fr1 = new Paragraph("Frecuencia Respiratoria",fuente4);
            Paragraph fr= new Paragraph(""+ String.valueOf(consulta.getDetallePaciente().getFrecuenciaRespiratoria()).concat(" ").concat("respiraciones/min"), fuente3);
            
            Paragraph d1 = new Paragraph("Descripción",fuente4);
            Paragraph d= new Paragraph(""+ String.valueOf(consulta.getDetallePaciente().getDescripcion()), fuente3);
            
            
            
            //TABLA 4
            Paragraph diagnostico = new Paragraph("Diagnóstico (DX): "+consulta.getDiagnostico(), fuente3);
            diagnostico.setAlignment(Element.ALIGN_LEFT);
            
            Paragraph tratamiento = new Paragraph("Tratamiento (TX): "+consulta.getTratamiento(), fuente3);
            tratamiento.setAlignment(Element.ALIGN_LEFT);
            
            Paragraph de= new Paragraph("Descripción de examenes: "+consulta.getExamenes(), fuente3);
            de.setAlignment(Element.ALIGN_LEFT);
            
  
            //IMAGENES
            Image image1 = Image.getInstance(consulta.getClass().getClassLoader().getResource("logo-sicimed.png"));
            image1.setWidthPercentage(40);
            
            Image image2 = Image.getInstance(consulta.getClass().getClassLoader().getResource("Logo_UES.jpg"));
            image2.setWidthPercentage(30);
            image2.setAlignment(Element.ALIGN_RIGHT);
            
       
       //TABLA #1
            PdfPTable tabla = new PdfPTable(3); // 3 columnas.
            tabla.getDefaultCell().setBorder(0);
            tabla.setWidthPercentage(100); //Width 100%
            tabla.setSpacingBefore(30f); //Space before table
            tabla.setSpacingAfter(0f); //Space afterr  
            
            float[] columnWidths = {1f, 1f, 1f};
            tabla.setWidths(columnWidths);
     
            //columna 1
            PdfPCell cell1 = new PdfPCell(new Paragraph("",fuente2));
            cell1.addElement(image1);
            cell1.addElement(p5);
            cell1.addElement(p6);
          
            
            cell1.setBorder(0);
            cell1.setPaddingLeft(10);
            cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
            
            
            //columna 2
            PdfPCell cell2 = new PdfPCell(new Paragraph(" " ,fuente2)); 

            cell2.addElement(p);
            cell2.addElement(p1);
            cell2.addElement(p2);
            cell2.addElement(p3);
            cell2.addElement(p4);
            
            cell2.setBorder(0);
            cell2.setPaddingLeft(10);
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);
            
            
            //columna 3
            PdfPCell cell3 = new PdfPCell(new Paragraph("Cell 3"));
            cell3.addElement(image2);
            cell3.addElement(p7);
            cell3.addElement(p8);
            
            cell3.setBorder(0);
            cell3.setPaddingLeft(10);
            cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell3.setVerticalAlignment(Element.ALIGN_MIDDLE);

            //Agregando las columnas a la tabla          
            tabla.addCell(cell1);
            tabla.addCell(cell2);
            tabla.addCell(cell3);
            
      
      //TABLA #2
         
            PdfPTable tabla2 = new PdfPTable(1); // 1 columna.
            tabla2.getDefaultCell().setBorder(0);
            tabla2.setWidthPercentage(100); //Width 100%
            tabla2.setSpacingBefore(30f); //Space before table
            tabla2.setSpacingAfter(0f); //Space afterr  
            
            float[] columnWidths1 = {1f};
            tabla2.setWidths(columnWidths1);
     
            //columna 1
            PdfPCell col1 = new PdfPCell(new Paragraph("",fuente2));
            col1.addElement(f2);
            col1.addElement(per);
            col1.addElement(colonia );
            col1.addElement(motivoConsulta);
            col1.addElement(descripcion);
            
            col1.setBorder(0);
            col1.setPaddingLeft(10);
            col1.setHorizontalAlignment(Element.ALIGN_LEFT);
            col1.setVerticalAlignment(Element.ALIGN_MIDDLE);
            
            //Agregando las columnas a la tabla          
            tabla2.addCell(col1);
            
       
            

         //TABLA #3
               
            PdfPTable tabla3 = new PdfPTable(2); // 2 columna.
            tabla3.getDefaultCell().setBorder(0);
            tabla3.setWidthPercentage(100); //Width 100%
            tabla3.setSpacingBefore(30f); //Space before table
            tabla3.setSpacingAfter(0f); //Space afterr  
            
                  
            float[] columnWidths3 = {1f,1f};
            tabla3.setWidths(columnWidths3);
            PdfPCell cell = new PdfPCell();

            cell.setPhrase(new Phrase("Descripción",fuente2));
            cell.setBackgroundColor(new Color(56, 115, 212));
            cell.setPadding(10f);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            tabla3.addCell(cell);
            cell.setPhrase(new Phrase("Medidas",fuente2));
            cell.setBackgroundColor(new Color(56, 115, 212));
            cell.setPadding(10f);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            tabla3.addCell(cell);
            System.out.println("detalle paciente PDF:"+consulta.getDetallePaciente());
            tabla3.addCell(peso1);
            tabla3.addCell(peso);
            tabla3.addCell(talla1);
            tabla3.addCell(talla);
            tabla3.addCell(pc1);
            tabla3.addCell(pc);
            tabla3.addCell(temperatura1);
            tabla3.addCell(temperatura);
            tabla3.addCell(presion1);
            tabla3.addCell(presion);
            tabla3.addCell(imc1);
            tabla3.addCell(imc);
            tabla3.addCell(fc1);
            tabla3.addCell(fc);
            tabla3.addCell(fr1);
            tabla3.addCell(fr);
            tabla3.addCell(d1);
            tabla3.addCell(d);

            
           
          //TABLA #4
            
            PdfPTable tabla4 = new PdfPTable(1); // 1 columna.
            tabla4.getDefaultCell().setBorder(0);
            tabla4.setWidthPercentage(100); //Width 100%
            tabla4.setSpacingBefore(30f); //Space before table
            tabla4.setSpacingAfter(0f); //Space afterr  
            
            float[] columnWidths4 = {1f};
            tabla4.setWidths(columnWidths4);
     
            //columna 1
            PdfPCell co = new PdfPCell(new Paragraph("",fuente2));
            co.addElement(diagnostico);
            co.addElement(tratamiento);
            co.addElement(de);
          
            
            co.setBorder(0);
            co.setPaddingLeft(10);
            co.setHorizontalAlignment(Element.ALIGN_LEFT);
            co.setVerticalAlignment(Element.ALIGN_MIDDLE);
            
            //Agregando las columnas a la tabla          
            tabla4.addCell(co);
       
           
          /*----------------------------------------------------*/  
          PdfWriter.getInstance(document, out);
          document.open();
          
          document.add(tabla);
          document.add(tabla2);
          document.add(tabla3);
          document.add(tabla4);
         

          document.close();

        } catch (DocumentException e) {
            e.printStackTrace();
            logger.error("Error occurred: {0}", e);
        }

        return new ByteArrayInputStream(out.toByteArray());
    }

    public static PdfPCell createImageCell(String path) throws DocumentException, IOException {
		 Image img = Image.getInstance(path);
	        PdfPCell cell = new PdfPCell(img, true);
	        return cell;
		
	}
}