package com.sici.reports;

import java.awt.Color;
import java.awt.Toolkit;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfAnnotation;
import com.lowagie.text.pdf.PdfWriter;
import com.sici.controller.DateFunction;
import com.sici.entity.Citas;
import com.sici.entity.Persona;
import com.sici.entity.Producto;
import com.sici.entity.Receta;

public class ProductosPdfView {
    private static final Logger logger = LoggerFactory.getLogger(ProductosPdfView.class);

    public static ByteArrayInputStream productosReport(List<Producto> productos)  throws Exception{
        
    	Producto productoReporte = new Producto();
    	
    	Font fuente1 = new Font(Font.HELVETICA, 12, Font.BOLD, Color.BLACK);
        Font fuente2 = new Font(Font.HELVETICA, 10, Font.BOLD, Color.BLACK);
       
        Document document = new Document();
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        try {

            Paragraph titulo = new Paragraph("PRODUCTOS DISPONIBLES EN INVENTARIO",fuente1);
            titulo.setAlignment(Element.ALIGN_CENTER);
            titulo.setSpacingAfter(25);
            
            
            Paragraph p = new Paragraph("MEDICINA INTEGRADA",fuente1);
        	Paragraph p1 = new Paragraph(" ",fuente1);
            Paragraph p2 = new Paragraph("DR. ELMER A. NAVARRO M.",fuente2);
            Paragraph p3 = new Paragraph("J.V.P.M Nº 4656",fuente2);
            Paragraph p4 = new Paragraph("UNIVERSIDAD DE EL SALVADOR",fuente2);
            p.setAlignment(Element.ALIGN_CENTER);
            p2.setAlignment(Element.ALIGN_CENTER);
            p3.setAlignment(Element.ALIGN_CENTER);
            p4.setAlignment(Element.ALIGN_CENTER);
            
            Paragraph p5 = new Paragraph("Tel: 7010-6443",fuente2);
            Paragraph p6 = new Paragraph("PEDIATRÍA GENERAL.",fuente2);
            
            Paragraph p7 = new Paragraph("Tel: 7010-6443",fuente2);
            Paragraph p8 = new Paragraph("MEDICINA INTERNA",fuente2);
            
            p7.setAlignment(Element.ALIGN_RIGHT);
            p8.setAlignment(Element.ALIGN_RIGHT);
      
            
            //IMAGENES
            Image image1 = Image.getInstance(productoReporte.getClass().getClassLoader().getResource("logo-sicimed.png"));
            image1.setWidthPercentage(40);
            
            Image image2 = Image.getInstance(productoReporte.getClass().getClassLoader().getResource("Logo_UES.jpg"));
            image2.setWidthPercentage(30);
            image2.setAlignment(Element.ALIGN_RIGHT);
          
            Paragraph f = new Paragraph("Fecha:",fuente2);
            f.setAlignment(Element.ALIGN_LEFT);
            
            
          //TABLA #1
            PdfPTable tabla1 = new PdfPTable(3); // 3 columnas.
            tabla1.getDefaultCell().setBorder(0);
            tabla1.setWidthPercentage(100); //Width 100%
            tabla1.setSpacingBefore(50f); //Space before table
            tabla1.setSpacingAfter(0f); //Space afterr  
            
            float[] columnWidths = {1f, 1f, 1f};
            tabla1.setWidths(columnWidths);
     
            //columna 1
            PdfPCell cell1 = new PdfPCell(new Paragraph("",fuente2));
            cell1.addElement(image1);
            cell1.addElement(p5);
            cell1.addElement(p6);
          
            
            cell1.setBorder(0);
            cell1.setPaddingLeft(10);
            cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
            
            
            //columna 2
            PdfPCell cell2 = new PdfPCell(new Paragraph(" " ,fuente2)); 

            cell2.addElement(p);
            cell2.addElement(p1);
            cell2.addElement(p2);
            cell2.addElement(p3);
            cell2.addElement(p4);
            
            cell2.setBorder(0);
            cell2.setPaddingLeft(10);
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);
            
            
            //columna 3
            PdfPCell cell3 = new PdfPCell(new Paragraph("Cell 3"));
            cell3.addElement(image2);
            cell3.addElement(p7);
            cell3.addElement(p8);
            
            cell3.setBorder(0);
            cell3.setPaddingLeft(10);
            cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell3.setVerticalAlignment(Element.ALIGN_MIDDLE);

            //Agregando las columnas a la tabla          
            tabla1.addCell(cell1);
            tabla1.addCell(cell2);
            tabla1.addCell(cell3);
            

            //TABLA DE PRODUCTOS
            PdfPTable tabla = new PdfPTable(4);
            tabla.setSpacingAfter(20);
            tabla.setWidthPercentage(100);
            tabla.setWidths(new float[]{6.0f,6.0f, 6.0f, 6.0f});

            PdfPCell cell = new PdfPCell();
            cell.setPhrase(new Phrase("Nombre",fuente2));
            cell.setBackgroundColor(new Color(56, 115, 212));
            cell.setPadding(8f);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            tabla.addCell(cell);
            
            cell.setPhrase(new Phrase("Tipo de producto",fuente2));
            cell.setBackgroundColor(new Color(56, 115, 212));
            cell.setPadding(8f);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            tabla.addCell(cell);
            
            cell.setPhrase(new Phrase("Marca",fuente2));
            cell.setBackgroundColor(new Color(56, 115, 212));
            cell.setPadding(8f);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            tabla.addCell(cell);
            
            cell.setPhrase(new Phrase("Proveedor",fuente2));
            cell.setBackgroundColor(new Color(56, 115, 212));
            cell.setPadding(8f);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            tabla.addCell(cell);
            DateFunction dateConvert= new DateFunction();
            if(!productos.isEmpty()){
                for(Producto producto:productos){

                	tabla.addCell(producto.getNombre());
                    tabla.addCell(producto.getTipos().getNombre());
                    tabla.addCell(producto.getMarcas().getNombre());
                    tabla.addCell(producto.getProveedores().getNombre());
                   
                }
            }else{
                tabla.addCell("No se encuentran productos activos");
          
              
            }

            PdfWriter.getInstance(document, out);
            document.open();
            document.add(tabla1);
            document.add(new Paragraph("  "));
            document.add(titulo);
            document.add(tabla);
            document.close();

        } catch (DocumentException e) {
            e.printStackTrace();
            logger.error("Error occurred: {0}", e);
        }

        return new ByteArrayInputStream(out.toByteArray());
    }

}

