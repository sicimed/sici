package com.sici.reports;

import com.lowagie.text.*;
import com.lowagie.text.Font;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.sici.controller.DateFunction;
import com.sici.entity.Citas;
import com.sici.entity.Movimiento;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.List;

public class KardexPdfView {

    private static final Logger logger = LoggerFactory.getLogger(KardexPdfView.class);

    public static ByteArrayInputStream kardexReport(List<Movimiento> movimientos) {
        Font font = new Font(Font.HELVETICA, 9, Font.NORMAL);
        Font font1 = new Font(Font.HELVETICA, 9, Font.BOLDITALIC);
        Font font2 = new Font(Font.HELVETICA, 14, Font.BOLDITALIC, Color.BLACK);

        Document document = new Document(PageSize.LETTER.rotate());
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        
        String[] COLUMN_NAMES = {"Mov.","Fecha","Prod.","Concepto","Cant.","Costo U.","Saldo Cant.","Saldo Total"};

        try {

            Paragraph para = new Paragraph("Movimientos de inventario",font2);
            para.setAlignment(Element.ALIGN_CENTER);
            para.setSpacingAfter(20);

            PdfPTable tablaKardex = new PdfPTable(8);
            tablaKardex.setSpacingAfter(20);
            tablaKardex.setWidthPercentage(100);
            tablaKardex.setWidths(new float[]{1.0f,1.0f,2.7f,3.3f,0.7f,1.0f,1.0f,1.3f});


            PdfPCell cell = new PdfPCell();
            for(String col_name : COLUMN_NAMES) {
                cell.setPhrase(new Phrase(col_name,font1));
                cell.setBackgroundColor(new Color(184,218,255));
                cell.setPadding(8f);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                tablaKardex.addCell(cell);
            }

            if(!movimientos.isEmpty()){
                for(Movimiento mov:movimientos){
                    tablaKardex.addCell(new Phrase(mov.getId().toString(),font));
                    tablaKardex.addCell(new Phrase(mov.getFecha().getDayOfMonth()+"/"+mov.getFecha().getMonthValue()+"/"+mov.getFecha().getYear(),font));
                    tablaKardex.addCell(new Phrase(mov.getProducto().getId()+" - "+mov.getProducto().getNombre(),font));
                    tablaKardex.addCell(new Phrase(mov.getConcepto(),font));
                    tablaKardex.addCell(new Phrase(Integer.toString(mov.getCantidad()),font));
                    tablaKardex.addCell(new Phrase(Float.toString(mov.getCostoUnit()),font));
                    tablaKardex.addCell(new Phrase(Integer.toString(mov.getSalCantidad()),font));
                    tablaKardex.addCell(new Phrase(Float.toString(mov.getSalTotal()),font));
                    
                }
            }else{
                tablaKardex.addCell("-");
                tablaKardex.addCell("-");
                tablaKardex.addCell("-");
                tablaKardex.addCell("No hay");
                tablaKardex.addCell("-");
                tablaKardex.addCell("-");
                tablaKardex.addCell("-");
                tablaKardex.addCell("-");
            }

             
            PdfPTable tablaPie = new PdfPTable(1);
            tablaPie.setWidths(new float[]{1.0f});
            tablaPie.addCell("Página "+document.getPageNumber()+1);
            
            PdfWriter.getInstance(document, out);
            document.open();
            document.add(para);
            document.add(tablaKardex);
            //document.add(tablaPie);
            document.close();

        } catch (DocumentException e) {
            e.printStackTrace();
            logger.error("Error occurred: {0}", e);
        }

        return new ByteArrayInputStream(out.toByteArray());
    }
}
