package com.sici.reports;

import com.lowagie.text.*;
import com.lowagie.text.Font;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.sici.controller.DateFunction;
import com.sici.entity.Citas;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.List;

public class DiagnosticoPdfView {

    private static final Logger logger = LoggerFactory.getLogger(DiagnosticoPdfView.class);

    public static ByteArrayInputStream diagnosticReport(List<Citas> citas)  throws Exception{
        Font font = new Font(Font.HELVETICA, 12, Font.BOLDITALIC);
        Font font2 = new Font(Font.HELVETICA, 18, Font.BOLDITALIC, Color.BLACK);

        Document document = new Document();
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        try {

            Paragraph para = new Paragraph("Diagnosticos realizados",font2);
            para.setAlignment(Element.ALIGN_CENTER);
            para.setSpacingAfter(20);

            PdfPTable tablaDiagnostico = new PdfPTable(2);
            tablaDiagnostico.setSpacingAfter(20);
            tablaDiagnostico.setWidthPercentage(100);
            tablaDiagnostico.setWidths(new float[]{6.0f,6.0f});


            PdfPCell cell = new PdfPCell();
            cell.setPhrase(new Phrase("Fecha",font));
            cell.setBackgroundColor(new Color(184,218,255));
            cell.setPadding(8f);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaDiagnostico.addCell(cell);
            cell.setPhrase(new Phrase("Diagnóstico",font));
            cell.setBackgroundColor(new Color(184,218,255));
            cell.setPadding(8f);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaDiagnostico.addCell(cell);
            DateFunction dateConvert= new DateFunction();
            if(!citas.isEmpty()){
                for(Citas cita:citas){

                    tablaDiagnostico.addCell(dateConvert.convertDateToSpanish(cita.getFecha_consulta()));
                    tablaDiagnostico.addCell(cita.getConsulta().getDiagnostico());
                }
            }else{
                tablaDiagnostico.addCell("No hay");
                tablaDiagnostico.addCell("No hay");
            }

            PdfWriter.getInstance(document, out);
            document.open();
            document.add(para);
            document.add(tablaDiagnostico);
            document.close();

        } catch (DocumentException e) {
            e.printStackTrace();
            logger.error("Error occurred: {0}", e);
        }

        return new ByteArrayInputStream(out.toByteArray());
    }
}
