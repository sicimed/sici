package com.sici.reports;

import java.awt.Color;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

import org.dom4j.DocumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.sici.model.ModelDate;

public class CantidadCitasPeriodoPdfView {

	private static final Logger logger = LoggerFactory.getLogger(CantidadCitasPeriodoPdfView.class);

	public static ByteArrayInputStream cantidadCitasReport(List<?> citas,ModelDate model) throws Exception{

		Font fuente1 = new Font(Font.HELVETICA, 12, Font.BOLD, Color.BLACK);
		Font fuente2 = new Font(Font.HELVETICA, 10, Font.BOLD, Color.BLACK);
		Font fuente3 = new Font(Font.HELVETICA, 10);
		Font fuente4 = new Font(Font.HELVETICA, 9, Font.BOLD, Color.BLACK);
		Font fuente5 = new Font(Font.HELVETICA, 10, Font.ITALIC, Color.BLACK);

		Document document = new Document();
		ByteArrayOutputStream out = new ByteArrayOutputStream();

		try {
			// contenido
			Paragraph p = new Paragraph("MEDICINA INTEGRADA", fuente1);
			Paragraph p1 = new Paragraph(" ", fuente1);
			Paragraph p2 = new Paragraph("DR. ELMER A. NAVARRO M.", fuente2);
			Paragraph p3 = new Paragraph("J.V.P.M Nº 4656", fuente2);
			Paragraph p4 = new Paragraph("UNIVERSIDAD DE EL SALVADOR", fuente2);
			p.setAlignment(Element.ALIGN_CENTER);
			p2.setAlignment(Element.ALIGN_CENTER);
			p3.setAlignment(Element.ALIGN_CENTER);
			p4.setAlignment(Element.ALIGN_CENTER);

			Paragraph p5 = new Paragraph("Tel: 7010-6443", fuente2);
			Paragraph p6 = new Paragraph("PEDIATRÍA GENERAL.", fuente2);

			Paragraph p7 = new Paragraph("Tel: 7010-6443", fuente2);
			Paragraph p8 = new Paragraph("MEDICINA INTERNA", fuente2);

			p7.setAlignment(Element.ALIGN_RIGHT);
			p8.setAlignment(Element.ALIGN_RIGHT);

			// URL path =
			// CantidadCitasPeriodoPdfView.class.getClass().getProtectionDomain().getCodeSource().getLocation();
			// Image image1
			// Image.getInstance("C:\\\\Users\\\\YESELIZ\\\\git\\\\sici\\\\src\\\\main\\\\resources\\\\logo-sicimed.png");
			// Image image1 =
			// Image.getInstance(receta.getClass().getClassLoader().getResource("logo-sicimed.png"));
			
			Image image1 = Image.getInstance(CantidadCitasPeriodoPdfView.class.getClassLoader().getResource("logo-sicimed.png"));
			image1.setWidthPercentage(40);

			// Image image2 =
			// Image.getInstance("C:\\\\Users\\\\YESELIZ\\\\git\\\\sici\\\\src\\\\main\\\\resources\\\\Logo_UES.jpg");
			// Image image2 =
			// Image.getInstance(receta.getClass().getClassLoader().getResource("Logo_UES.jpg"));
			
			Image image2 = Image.getInstance(CantidadCitasPeriodoPdfView.class.getClassLoader().getResource("Logo_UES.jpg"));
			image2.setWidthPercentage(30);
			image2.setAlignment(Element.ALIGN_RIGHT);
			
			//TABLA #1
			PdfPTable tabla = new PdfPTable(3); // 3 columnas.
			tabla.getDefaultCell().setBorder(0);
			tabla.setWidthPercentage(100); //Width 100%
			tabla.setSpacingBefore(30f); //Space before table
			tabla.setSpacingAfter(0f); //Space afterr  
			
			float[] columnWidths = {1f, 1f, 1f};
			tabla.setWidths(columnWidths);
	    
			//columna 1
			PdfPCell cell1 = new PdfPCell(new Paragraph("",fuente2));
	        cell1.addElement(image1);
			cell1.addElement(p5);
			cell1.addElement(p6);
	        
			
			cell1.setBorder(0);
			cell1.setPaddingLeft(10);
			cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
			
			
			//columna 2
			PdfPCell cell2 = new PdfPCell(new Paragraph(" " ,fuente2)); 

			cell2.addElement(p);
			cell2.addElement(p1);
			cell2.addElement(p2);
			cell2.addElement(p3);
			cell2.addElement(p4);
			
			cell2.setBorder(0);
			cell2.setPaddingLeft(10);
			cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);
			
			
			//columna 3
			PdfPCell cell3 = new PdfPCell(new Paragraph("Cell 3"));
			cell3.addElement(image2);
			cell3.addElement(p7);
			cell3.addElement(p8);
			
			cell3.setBorder(0);
			cell3.setPaddingLeft(10);
			cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell3.setVerticalAlignment(Element.ALIGN_MIDDLE);

			//Agregando las columnas a la tabla          
			tabla.addCell(cell1);
			tabla.addCell(cell2);
			tabla.addCell(cell3);
			
			Paragraph para = new Paragraph("Cantidad citas realizadas por día",fuente2);
            para.setAlignment(Element.ALIGN_CENTER);
            para.setSpacingAfter(20);
            
            SimpleDateFormat formato = new SimpleDateFormat("yyyy/MM/dd");
        	formato.applyPattern("dd/MM/yyyy");
        	
        	String fechaInicio = formato.format(Date.from(model.getDateStart().atStartOfDay(ZoneId.systemDefault()).toInstant()));
        	String fechaFin = formato.format(Date.from(model.getDatEnd().atStartOfDay(ZoneId.systemDefault()).toInstant()));
        	String periodo = "Desde:"+fechaInicio+" - "+" Hasta:"+fechaFin;
            
            PdfPTable tablaDatosGeneral = new PdfPTable(1);
            tablaDatosGeneral.getDefaultCell().setBorder(0);
            tablaDatosGeneral.setSpacingAfter(20);
            tablaDatosGeneral.addCell(periodo);
			
            PdfPTable tablaCantidadCitas = new PdfPTable(2);
            tablaCantidadCitas.getDefaultCell().setBorder(0);
            tablaCantidadCitas.setSpacingAfter(20);
            tablaCantidadCitas.setWidthPercentage(80);
            tablaCantidadCitas.setWidths(new float[]{3.0f,4.0f});
            PdfPCell cell = new PdfPCell();

            cell.setPhrase(new Phrase("Fecha",fuente2));
            cell.setBackgroundColor(new Color(56, 115, 212));
            cell.setPadding(10f);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaCantidadCitas.addCell(cell);
            cell.setPhrase(new Phrase("Cantidad",fuente2));
            cell.setBackgroundColor(new Color(56, 115, 212));
            cell.setPadding(10f);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaCantidadCitas.addCell(cell);
           // System.out.println("cantidad de citas:"+citas.size());
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-d");
            
            String[] parts=null,parts2=null,parts3=null,parts4=null;
            LocalDate fecha =null;
            String fechaS=null;
            int cantidadCitas = 0;
            for (int i = 0; i < citas.size(); i++) {
            	parts = citas.get(i).toString().split(",");
            	parts2 = parts[0].split("=");
            	parts3 = parts[1].split("=");
            	parts4 = parts3[1].split("");
           
            	fecha = LocalDate.parse(parts2[1],formatter);
            	fechaS = fecha.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
            	
				tablaCantidadCitas.addCell(fechaS);
				tablaCantidadCitas.addCell(parts4[0]);
				cantidadCitas+= Integer.parseInt(parts4[0]); 
			}
            tablaCantidadCitas.addCell("TOTAL CITAS");
            tablaCantidadCitas.addCell(String.valueOf(cantidadCitas));
            
			/*----------------------------------------------------*/  
	        PdfWriter.getInstance(document, out);
			document.open();
			document.add(tabla);
			document.add(para);
			document.add(tablaDatosGeneral);
			document.add(tablaCantidadCitas);
			document.close();
		} catch (com.lowagie.text.DocumentException e) {
			e.printStackTrace();
            logger.error("Error occurred: {0}", e);
		}

		return new ByteArrayInputStream(out.toByteArray());
	}

	/*
	 * public static PdfPCell createImageCell(String path) throws DocumentException,
	 * IOException { Image img = Image.getInstance(path); PdfPCell cell = new
	 * PdfPCell(img, true); return cell;
	 * 
	 * }
	 */

}
