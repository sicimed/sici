package com.sici.reports;

import com.sici.constant.ViewConstant;
import com.sici.entity.*;
import com.sici.serviceImp.CitaService;
import com.sici.serviceImp.CitaServiceImp;
import com.sici.serviceImp.ConsultaServiceImp;
import com.sici.serviceImp.DetallePacienteService;
import com.sici.serviceImp.ExpedienteService;
import com.sici.serviceImp.HistorialMedicoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/expediente_clinico")
public class ExpedienteClinicoController {

    @Autowired
    private ExpedienteService expedienteService;
    
    @Autowired
    private DetallePacienteService detallePacienteService;

    @Autowired
    private HistorialMedicoService historialMedicoService;
    
    @Autowired
    private ConsultaServiceImp consultaService;
    
    @Autowired
    private CitaService citaService;

    @GetMapping("/list_expedientes")
    public String listExpedientes(Model model){
        model.addAttribute("expedientes",expedienteService.findAll());
        return ViewConstant.LIST_EXPEDIENTES;
    }

    @GetMapping("/show_expediente/{id}/expediente")
    public String showExpediente(@PathVariable("id") Long id,Model model){
        Expediente expediente = expedienteService.getExpedienteById(id);
        model.addAttribute("expediente",expediente);
        DetallePaciente detallePaciente = detallePacienteService.getLastPaciente(expediente.getPaciente());
        model.addAttribute("detalle_paciente",detallePaciente);
        
        List<Citas> citas = citaService.findByExpediente(expediente);
        model.addAttribute("examenes",citas);
        
        List<Consulta> consultas = new ArrayList<Consulta>();
        for (Citas cita: citas) {
		    consultas.add(cita.getConsulta());
		}
 
        model.addAttribute("consultas",consultas);
        
        List<Citas> citasList = expediente.getCitasList();
        if (citasList.isEmpty()) {
            citasList=null;
        }
        model.addAttribute("citas_list",citasList);
        return ViewConstant.SHOW_EXPEDIENTE;
    }

    @GetMapping("/list_historiales")
    public String listCitasExpediente(Model model){
        model.addAttribute("historiales",historialMedicoService.findAll());
        return ViewConstant.LIST_CITAS_EXPEDIENTE;
    }

    @GetMapping("/show_historial/{id}")
    public String showHistorial(@PathVariable("id") Long id, Model model){
        HistorialMedico historialMedico = historialMedicoService.getHistorialById(id);
        List<Consulta> consultas =  historialMedico.getConsultaList();
       // System.out.println("consultas historial"+consultas.get(0));
        model.addAttribute("historial_consulta",consultas);

        return ViewConstant.SHOW_HISTORIAL_MEDICO;
    }



}
