package com.sici.reports;

import com.lowagie.text.*;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.sici.controller.DateFunction;
import com.sici.entity.Citas;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.document.AbstractPdfView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.util.List;
import java.util.Map;

@Component("cita/list_citas_pendientes")
class PacientePdfView extends AbstractPdfView {
    @Override
    protected void buildPdfDocument(Map<String, Object> map, Document document, PdfWriter pdfWriter, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
    	
    	Font fuente1 = new Font(Font.HELVETICA, 12, Font.BOLD, Color.BLACK);
        Font fuente2 = new Font(Font.HELVETICA, 10, Font.BOLD, Color.BLACK);
        Font fuente3 = new Font(Font.HELVETICA, 10);
        Font fuente4 = new Font(Font.HELVETICA, 10, Font.BOLD, Color.BLACK);
        Font fuente5 = new Font(Font.HELVETICA, 10, Font.ITALIC, Color.BLACK);
        
        
        Citas citasPdf = new Citas();
        
        Paragraph p = new Paragraph("MEDICINA INTEGRADA",fuente1);
    	Paragraph p1 = new Paragraph(" ",fuente1);
        Paragraph p2 = new Paragraph("DR. ELMER A. NAVARRO M.",fuente2);
        Paragraph p3 = new Paragraph("J.V.P.M Nº 4656",fuente2);
        Paragraph p4 = new Paragraph("UNIVERSIDAD DE EL SALVADOR",fuente2);
        p.setAlignment(Element.ALIGN_CENTER);
        p2.setAlignment(Element.ALIGN_CENTER);
        p3.setAlignment(Element.ALIGN_CENTER);
        p4.setAlignment(Element.ALIGN_CENTER);
        
        Paragraph p5 = new Paragraph("Tel: 7010-6443",fuente2);
        Paragraph p6 = new Paragraph("PEDIATRÍA GENERAL.",fuente2);
        
        Paragraph p7 = new Paragraph("Tel: 7010-6443",fuente2);
        Paragraph p8 = new Paragraph("MEDICINA INTERNA",fuente2);
        
        p7.setAlignment(Element.ALIGN_RIGHT);
        p8.setAlignment(Element.ALIGN_RIGHT);
  
        
        //IMAGENES
        Image image1 = Image.getInstance(citasPdf.getClass().getClassLoader().getResource("logo-sicimed.png"));
        image1.setWidthPercentage(40);
        
        Image image2 = Image.getInstance(citasPdf.getClass().getClassLoader().getResource("Logo_UES.jpg"));
        image2.setWidthPercentage(30);
        image2.setAlignment(Element.ALIGN_RIGHT);
      
        Paragraph f = new Paragraph("Fecha:",fuente2);
        f.setAlignment(Element.ALIGN_LEFT);
        
        
      //TABLA #1
        PdfPTable tabla = new PdfPTable(3); // 3 columnas.
        tabla.getDefaultCell().setBorder(0);
        tabla.setWidthPercentage(100); //Width 100%
        tabla.setSpacingBefore(50f); //Space before table
        tabla.setSpacingAfter(0f); //Space afterr  
        
        float[] columnWidths = {1f, 1f, 1f};
        tabla.setWidths(columnWidths);
 
        //columna 1
        PdfPCell cell1 = new PdfPCell(new Paragraph("",fuente2));
        cell1.addElement(image1);
        cell1.addElement(p5);
        cell1.addElement(p6);
      
        
        cell1.setBorder(0);
        cell1.setPaddingLeft(10);
        cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
        
        
        //columna 2
        PdfPCell cell2 = new PdfPCell(new Paragraph(" " ,fuente2)); 

        cell2.addElement(p);
        cell2.addElement(p1);
        cell2.addElement(p2);
        cell2.addElement(p3);
        cell2.addElement(p4);
        
        cell2.setBorder(0);
        cell2.setPaddingLeft(10);
        cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);
        
        
        //columna 3
        PdfPCell cell3 = new PdfPCell(new Paragraph("Cell 3"));
        cell3.addElement(image2);
        cell3.addElement(p7);
        cell3.addElement(p8);
        
        cell3.setBorder(0);
        cell3.setPaddingLeft(10);
        cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell3.setVerticalAlignment(Element.ALIGN_MIDDLE);

        //Agregando las columnas a la tabla          
        tabla.addCell(cell1);
        tabla.addCell(cell2);
        tabla.addCell(cell3);
        
        
 

        List<Citas> citas = (List<Citas>) map.get("citasWait");

        Paragraph para = new Paragraph("Citas programadas",fuente1);
        para.setAlignment(Element.ALIGN_CENTER);
        para.setSpacingAfter(40);

        PdfPTable tablaExpediente = new PdfPTable(2);
        tablaExpediente.setSpacingAfter(20);
        tablaExpediente.setWidthPercentage(100);
        tablaExpediente.setWidths(new float[]{6.0f,6.0f});

        PdfPCell cell = new PdfPCell();
        cell.setPhrase(new Phrase("Paciente",fuente2));
        cell.setBackgroundColor(new Color(56, 115, 212));
        cell.setPadding(8f);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        tablaExpediente.addCell(cell);
        cell.setPhrase(new Phrase("Fecha",fuente2));
        cell.setBackgroundColor(new Color(56, 115, 212));
        cell.setPadding(8f);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        tablaExpediente.addCell(cell);
        DateFunction dateConvert= new DateFunction();
        System.out.println(citas);
        if(!citas.isEmpty()){
            for(Citas cita:citas){

                tablaExpediente.addCell(cita.getExpediente().getPaciente().getPersona().getNombre()+" "+cita.getExpediente().getPaciente().getPersona().getApellido());
                tablaExpediente.addCell( dateConvert.convertDateToSpanish(cita.getFecha_consulta()));
            }
        }else{
            tablaExpediente.addCell("No hay");
            tablaExpediente.addCell("No hay");
        }
        
  
        document.add(tabla);
        document.add(para);
        document.add(tablaExpediente);
    }
}
