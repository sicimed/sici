package com.sici.reports;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.DefaultFontMapper;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfTemplate;
import com.lowagie.text.pdf.PdfWriter;
import org.jfree.chart.JFreeChart;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

public class ConsultaPdfChartView {

	private static final Logger logger = LoggerFactory.getLogger(ConsultaPdfChartView.class);

	public static ByteArrayInputStream consultaReportChart(JFreeChart chart) throws Exception {

		Font fuente1 = new Font(Font.HELVETICA, 12, Font.BOLD, Color.BLACK);
		Font fuente2 = new Font(Font.HELVETICA, 10, Font.BOLD, Color.BLACK);
		Font fuente3 = new Font(Font.HELVETICA, 10);
		Font fuente4 = new Font(Font.HELVETICA, 9, Font.BOLD, Color.BLACK);
		Font fuente5 = new Font(Font.HELVETICA, 10, Font.ITALIC, Color.BLACK);

		PdfWriter writer = null;
		Document document = new Document(PageSize.A4.rotate());
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		try {
			// contenido
			Paragraph p = new Paragraph("MEDICINA INTEGRADA", fuente1);
			Paragraph p1 = new Paragraph(" ", fuente1);
			Paragraph p2 = new Paragraph("DR. ELMER A. NAVARRO M.", fuente2);
			
			p.setAlignment(Element.ALIGN_CENTER);
			p2.setAlignment(Element.ALIGN_CENTER);
			
			Image image1 = Image.getInstance(ConsultaPdfChartView.class.getClassLoader().getResource("logo-sicimed.png"));
			image1.setWidthPercentage(20);
			
			Image image2 = Image.getInstance(ConsultaPdfChartView.class.getClassLoader().getResource("Logo_UES.jpg"));
			image2.setWidthPercentage(15);
			image2.setAlignment(Element.ALIGN_RIGHT);
			
			//TABLA #1
			PdfPTable tabla = new PdfPTable(3); // 3 columnas.
			tabla.getDefaultCell().setBorder(0);
			tabla.setWidthPercentage(100); //Width 100%
			tabla.setSpacingBefore(30f); //Space before table
			tabla.setSpacingAfter(0f); //Space afterr  
			
			float[] columnWidths = {1f, 1f, 1f};
			tabla.setWidths(columnWidths);
	    
			//columna 1
			PdfPCell cell1 = new PdfPCell(new Paragraph("",fuente2));
	        cell1.addElement(image1);
	        
			cell1.setBorder(0);
			cell1.setPaddingLeft(10);
			cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);

			//columna 2
			PdfPCell cell2 = new PdfPCell(new Paragraph(" " ,fuente2)); 

			cell2.addElement(p);
			cell2.addElement(p1);
			cell2.addElement(p2);
			
			cell2.setBorder(0);
			cell2.setPaddingLeft(10);
			cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);
			
			//columna 3
			PdfPCell cell3 = new PdfPCell(new Paragraph("Cell 3"));
			cell3.addElement(image2);;
			
			cell3.setBorder(0);
			cell3.setPaddingLeft(10);
			cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell3.setVerticalAlignment(Element.ALIGN_MIDDLE);

			//Agregando las columnas a la tabla          
			tabla.addCell(cell1);
			tabla.addCell(cell2);
			tabla.addCell(cell3);
			
			writer = PdfWriter.getInstance(document, out);
			document.open();
			document.add(tabla);
			document.setPageSize(PageSize.A4);
			PdfContentByte contentByte = writer.getDirectContent();
			PdfTemplate template = contentByte.createTemplate(750, 500);
			Graphics2D graphics2d = template.createGraphics(750, 500, new DefaultFontMapper());
			Rectangle2D rectangle2d = new Rectangle2D.Double(50, 0, 750, 450);

			chart.draw(graphics2d, rectangle2d);
			graphics2d.dispose();
			contentByte.addTemplate(template, 0, 0);
			// PdfWriter.getInstance(document, out);
		} catch (DocumentException de) {
			de.getMessage();
		} catch (Exception e) {
			e.printStackTrace();
		}
		document.close();
		return new ByteArrayInputStream(out.toByteArray());
	}
}
