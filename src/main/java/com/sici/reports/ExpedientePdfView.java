package com.sici.reports;

import com.lowagie.text.*;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.*;
import com.sici.controller.DateFunction;
import com.sici.entity.Citas;
import com.sici.entity.Consulta;
import com.sici.entity.DetallePaciente;
import com.sici.entity.Expediente;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.document.AbstractPdfView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.util.List;
import java.util.Map;

@Component("expediente_clinico/show_expediente")
public class ExpedientePdfView extends AbstractPdfView {

	@Override
	protected void buildPdfDocument(Map<String, Object> map, Document document, PdfWriter pdfWriter,
			HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
		Font font = new Font(Font.HELVETICA, 12, Font.BOLDITALIC);
		Font font2 = new Font(Font.HELVETICA, 18, Font.BOLDITALIC, Color.BLACK);

		Font fuente1 = new Font(Font.HELVETICA, 12, Font.BOLD, Color.BLACK);
		Font fuente2 = new Font(Font.HELVETICA, 10, Font.BOLD, Color.BLACK);
		Font fuente3 = new Font(Font.HELVETICA, 10);
		Font fuente4 = new Font(Font.HELVETICA, 9, Font.BOLD, Color.BLACK);
		Font fuente5 = new Font(Font.HELVETICA, 10, Font.ITALIC, Color.BLACK);

		try {
			// contenido
			Paragraph p = new Paragraph("MEDICINA INTEGRADA", fuente1);
			Paragraph p1 = new Paragraph(" ", fuente1);
			Paragraph p2 = new Paragraph("DR. ELMER A. NAVARRO M.", fuente2);
			Paragraph p3 = new Paragraph("J.V.P.M Nº 4656", fuente2);
			Paragraph p4 = new Paragraph("UNIVERSIDAD DE EL SALVADOR", fuente2);
			p.setAlignment(Element.ALIGN_CENTER);
			p2.setAlignment(Element.ALIGN_CENTER);
			p3.setAlignment(Element.ALIGN_CENTER);
			p4.setAlignment(Element.ALIGN_CENTER);

			Paragraph p5 = new Paragraph("Tel: 7010-6443", fuente2);
			Paragraph p6 = new Paragraph("PEDIATRÍA GENERAL.", fuente2);

			Paragraph p7 = new Paragraph("Tel: 7010-6443", fuente2);
			Paragraph p8 = new Paragraph("MEDICINA INTERNA", fuente2);

			p7.setAlignment(Element.ALIGN_RIGHT);
			p8.setAlignment(Element.ALIGN_RIGHT);

			Image image1 = Image.getInstance(ExpedientePdfView.class.getClassLoader().getResource("logo-sicimed.png"));
			image1.setWidthPercentage(40);

			Image image2 = Image.getInstance(ExpedientePdfView.class.getClassLoader().getResource("Logo_UES.jpg"));
			image2.setWidthPercentage(30);
			image2.setAlignment(Element.ALIGN_RIGHT);

			// TABLA #1
			PdfPTable tabla = new PdfPTable(3); // 3 columnas.
			tabla.getDefaultCell().setBorder(0);
			tabla.setWidthPercentage(100); // Width 100%
			tabla.setSpacingBefore(30f); // Space before table
			tabla.setSpacingAfter(0f); // Space afterr

			float[] columnWidths = { 1f, 1f, 1f };
			tabla.setWidths(columnWidths);

			// columna 1
			PdfPCell cell1 = new PdfPCell(new Paragraph("", fuente2));
			cell1.addElement(image1);
			cell1.addElement(p5);
			cell1.addElement(p6);

			cell1.setBorder(0);
			cell1.setPaddingLeft(10);
			cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);

			// columna 2
			PdfPCell cell2 = new PdfPCell(new Paragraph(" ", fuente2));

			cell2.addElement(p);
			cell2.addElement(p1);
			cell2.addElement(p2);
			cell2.addElement(p3);
			cell2.addElement(p4);

			cell2.setBorder(0);
			cell2.setPaddingLeft(10);
			cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);

			// columna 3
			PdfPCell cell3 = new PdfPCell(new Paragraph("Cell 3"));
			cell3.addElement(image2);
			cell3.addElement(p7);
			cell3.addElement(p8);

			cell3.setBorder(0);
			cell3.setPaddingLeft(10);
			cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell3.setVerticalAlignment(Element.ALIGN_MIDDLE);

			// Agregando las columnas a la tabla
			tabla.addCell(cell1);
			tabla.addCell(cell2);
			tabla.addCell(cell3);

			//AGREGANDO PAGINADOR 
			String page = pdfWriter.getCurrentPageNumber() + " de " + pdfWriter.getPageNumber();
			final HeaderFooter header = new HeaderFooter(new Phrase(page), false);
			header.setAlignment(Element.ALIGN_RIGHT);
			header.setBorder(Rectangle.NO_BORDER);
			header.setPageNumber(1);

			Paragraph para = new Paragraph("Expedíente Clínico", fuente2);
			para.setAlignment(Element.ALIGN_CENTER);
			para.setSpacingAfter(20);

			Expediente expediente = (Expediente) map.get("expediente");
			// DetallePaciente detallePaciente = (DetallePaciente)
			// map.get("detalle_paciente");
			// List<Citas> citas = (List<Citas>) map.get("citas_list");
			List<Consulta> consultas = (List<Consulta>) map.get("consultas");
			DateFunction dateFunction = new DateFunction();

			PdfPTable tablaExpediente = new PdfPTable(1);
			tablaExpediente.getDefaultCell().setBorder(0);
			tablaExpediente.setSpacingAfter(20);
			tablaExpediente.setWidthPercentage(100);

//			PdfPCell cellExpediente = new PdfPCell();
//			cellExpediente.setPhrase(new Phrase("Datos Generales", font));
//			cellExpediente.setBackgroundColor(new Color(0, 0, 255));
//			cellExpediente.setPaddingLeft(10);
//			cellExpediente.setHorizontalAlignment(Element.ALIGN_CENTER);
//			tablaExpediente.addCell(cellExpediente);
			tablaExpediente.addCell("Codigo expediente: " + expediente.getCodigo());
			tablaExpediente.addCell("Nombre: " + expediente.getPaciente().getPersona().getNombre() + " "
					+ expediente.getPaciente().getPersona().getApellido());
			tablaExpediente.addCell("Fecha nacimiento: "
					+ dateFunction.convertDateToSpanish(expediente.getPaciente().getPersona().getFechaNacimiento()));
			tablaExpediente.addCell("Edad: " + expediente.getPaciente().getPersona().getEdad());
			tablaExpediente.addCell("Sexo: " + expediente.getPaciente().getPersona().getSexo());
			tablaExpediente.addCell("Telefono: " + expediente.getPaciente().getPersona().getTelefono());
			tablaExpediente.addCell("Colonia: " + expediente.getPaciente().getPersona().getColonia());
			tablaExpediente.addCell("Direccion: " + expediente.getPaciente().getPersona().getDireccion());

			Paragraph para_consul = new Paragraph("CONSULTAS REALIZADAS", fuente2);
			para_consul.setAlignment(Element.ALIGN_CENTER);
			para_consul.setSpacingAfter(20);

			Paragraph motivo = new Paragraph("Motivo consulta:", fuente2);
			Paragraph descripcion = new Paragraph("Descripción:", fuente2);
			Paragraph antecedente = new Paragraph("Antecedente:", fuente2);

			Paragraph descripcion_fisica = new Paragraph("Descripción:", fuente2);
			Paragraph diagnostico = new Paragraph("Diagnóstico:", fuente2);
			Paragraph tratamiento = new Paragraph("Tratamiento:", fuente2);
			Paragraph examenes = new Paragraph("Examenes:", fuente2);

			PdfPTable tablaDetallePaciente = new PdfPTable(2);
			DateFunction dateConvert = new DateFunction();

			PdfPTable tablaFecha = new PdfPTable(1);
			tablaFecha.getDefaultCell().setBorder(0);
			tablaFecha.setSpacingAfter(20);
			tablaFecha.setWidthPercentage(100);

			PdfPTable tablaMDA = new PdfPTable(1);
			tablaMDA.getDefaultCell().setBorder(0);
			tablaMDA.setSpacingAfter(20);
			tablaMDA.setWidthPercentage(100);

			PdfPTable tablaDFG = new PdfPTable(1);
			tablaDFG.getDefaultCell().setBorder(0);
			tablaDFG.setSpacingAfter(20);
			tablaDFG.setWidthPercentage(100);

			Paragraph datos_fisicos = new Paragraph("Datos físicos", fuente2);
			datos_fisicos.setAlignment(Element.ALIGN_LEFT);
			datos_fisicos.setSpacingAfter(20);

			Paragraph descripcion_fisica_general = new Paragraph("Descripción física general", fuente2);
			descripcion_fisica_general.setAlignment(Element.ALIGN_LEFT);
			descripcion_fisica_general.setSpacingAfter(20);

			DetallePaciente detallePaciente = null;

			// AQUÍ SE DEFINE LA ESTRUCTURA DEL REPORTE

			
			document.add(tabla);
			document.add(para);// titulo expediente clinico
			document.add(tablaExpediente);// informacion general
			document.add(para_consul);// parrafo consultas realizadas

			for (Consulta consulta : consultas) {

				tablaFecha.addCell("____________________________________________________________________________");
				tablaFecha
						.addCell("Fecha: " + dateConvert.convertDateToSpanish(consulta.getCita().getFecha_consulta()));

				tablaMDA.addCell(motivo);
				tablaMDA.addCell(consulta.getMotivoConsulta());
				tablaMDA.addCell(descripcion);
				tablaMDA.addCell(consulta.getDescripcionMotivo());
				tablaMDA.addCell(antecedente);
				tablaMDA.addCell(consulta.getAntecendentes());

				tablaDetallePaciente.setSpacingAfter(20);
				tablaDetallePaciente.setWidthPercentage(100);
				tablaDetallePaciente.setWidths(new float[] { 6.0f, 6.0f });
				PdfPCell cell = new PdfPCell();

				cell.setPhrase(new Phrase("Descripción", font));
				cell.setBackgroundColor(new Color(0, 0, 255));
				cell.setPadding(8f);
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				tablaDetallePaciente.addCell(cell);
				cell.setPhrase(new Phrase("Medidas", font));
				cell.setBackgroundColor(new Color(0, 0, 255));
				cell.setPadding(8f);
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				tablaDetallePaciente.addCell(cell);

				if (consulta.getDetallePaciente() != null) {
					detallePaciente = consulta.getDetallePaciente();

					tablaDetallePaciente.addCell("Peso");
					tablaDetallePaciente.addCell(String.valueOf(detallePaciente.getPeso()).concat(" ").concat("lb"));
					tablaDetallePaciente.addCell("Talla");
					tablaDetallePaciente.addCell(String.valueOf(detallePaciente.getTalla()).concat(" ").concat("cm"));
					tablaDetallePaciente.addCell("PC");
					tablaDetallePaciente.addCell(String.valueOf(detallePaciente.getPc()).concat(" ").concat("cm"));
					tablaDetallePaciente.addCell("Temperatura");
					tablaDetallePaciente
							.addCell(String.valueOf(detallePaciente.getTemperatura()).concat(" ").concat("°C"));
					tablaDetallePaciente.addCell("Presión arterial");
					tablaDetallePaciente
							.addCell(String.valueOf(detallePaciente.getPresionArterial()).concat(" ").concat("mmHg"));
					tablaDetallePaciente.addCell("IMC");
					tablaDetallePaciente.addCell(String.valueOf(detallePaciente.getImc()).concat(" ").concat("Kg/m2"));
					tablaDetallePaciente.addCell("Frecuencia cardíaca");
					tablaDetallePaciente.addCell(
							String.valueOf(detallePaciente.getFrecuenciaCardiaca()).concat(" ").concat("lat/min"));
					tablaDetallePaciente.addCell("Frecuencia Respiratoria");
					tablaDetallePaciente.addCell(String.valueOf(detallePaciente.getFrecuenciaRespiratoria()).concat(" ")
							.concat("respiraciones/min"));
					tablaDetallePaciente.addCell("Descripción");
					tablaDetallePaciente.addCell(String.valueOf(detallePaciente.getDescripcion()));
				}

				tablaDFG.addCell(descripcion_fisica);
				tablaDFG.addCell(consulta.getDetallePaciente().getDescripcion());
				tablaDFG.addCell(diagnostico);
				tablaDFG.addCell(consulta.getDiagnostico());
				tablaDFG.addCell(tratamiento);
				tablaDFG.addCell(consulta.getTratamiento());
				tablaDFG.addCell(examenes);
				tablaDFG.addCell(consulta.getExamenes());

				document.add(tablaFecha);// fechas de consultas
				document.add(tablaMDA);
				document.add(datos_fisicos);
				document.add(tablaDetallePaciente);// detalles de consultas cuerpo persona
				document.add(descripcion_fisica_general);
				document.add(tablaDFG);

				tablaFecha.deleteBodyRows();
				tablaDetallePaciente.deleteBodyRows();
				tablaMDA.deleteBodyRows();
				tablaDFG.deleteBodyRows();
			}
			document.setFooter(header);

		} catch (DocumentException e) {
			e.printStackTrace();
			logger.error("Error occurred: {0}", e);
		}

	}
}
