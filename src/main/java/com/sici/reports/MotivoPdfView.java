package com.sici.reports;

import com.lowagie.text.*;
import com.lowagie.text.Font;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.sici.controller.DateFunction;
import com.sici.entity.Citas;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.List;

public class MotivoPdfView {
    private static final Logger logger = LoggerFactory.getLogger(DiagnosticoPdfView.class);

    public static ByteArrayInputStream motivoReport(List<Citas> citas)  throws Exception{
        Font font = new Font(Font.HELVETICA, 12, Font.BOLDITALIC);
        Font font2 = new Font(Font.HELVETICA, 18, Font.BOLDITALIC, Color.BLACK);

        Document document = new Document();
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        try {

            Paragraph para = new Paragraph("Motivo realizados",font2);
            para.setAlignment(Element.ALIGN_CENTER);
            para.setSpacingAfter(20);

            PdfPTable tablaMotivo = new PdfPTable(2);
            tablaMotivo.setSpacingAfter(20);
            tablaMotivo.setWidthPercentage(100);
            tablaMotivo.setWidths(new float[]{6.0f,6.0f});


            PdfPCell cell = new PdfPCell();
            cell.setPhrase(new Phrase("Fecha",font));
            cell.setBackgroundColor(new Color(184,218,255));
            cell.setPadding(8f);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaMotivo.addCell(cell);
            cell.setPhrase(new Phrase("Motivo",font));
            cell.setBackgroundColor(new Color(184,218,255));
            cell.setPadding(8f);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaMotivo.addCell(cell);
            DateFunction dateConvert= new DateFunction();
            if(!citas.isEmpty()){
                for(Citas cita:citas){

                    tablaMotivo.addCell(dateConvert.convertDateToSpanish(cita.getFecha_consulta()));
                    tablaMotivo.addCell(cita.getConsulta().getDiagnostico());
                }
            }else{
                tablaMotivo.addCell("No hay");
                tablaMotivo.addCell("No hay");
            }

            PdfWriter.getInstance(document, out);
            document.open();
            document.add(para);
            document.add(tablaMotivo);
            document.close();

        } catch (DocumentException e) {
            e.printStackTrace();
            logger.error("Error occurred: {0}", e);
        }

        return new ByteArrayInputStream(out.toByteArray());
    }

}

