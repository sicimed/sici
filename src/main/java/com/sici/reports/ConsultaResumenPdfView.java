package com.sici.reports;

import java.awt.Color;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.sici.controller.DateFunction;
import com.sici.entity.Citas;
import com.sici.entity.Persona;
import com.sici.model.ModelDate;

public class ConsultaResumenPdfView {
	

    private static final Logger logger = LoggerFactory.getLogger(ConsultaResumenPdfView.class);

    public static ByteArrayInputStream consultaResumenReport(List<Citas> citas,ModelDate model)  throws Exception{
       
    	Citas citaPaciente = new Citas();

        Font fuente1 = new Font(Font.HELVETICA, 12, Font.BOLD, Color.BLACK);
        Font fuente2 = new Font(Font.HELVETICA, 10, Font.BOLD, Color.BLACK);
        Font fuente3 = new Font(Font.HELVETICA, 10);


        Document document = new Document(PageSize.A4.rotate());
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        
        SimpleDateFormat formato = new SimpleDateFormat("yyyy/MM/dd");
    	formato.applyPattern("dd/MM/yyyy");
    	
    	String fechaInicio = formato.format(Date.from(model.getDateStart().atStartOfDay(ZoneId.systemDefault()).toInstant()));
    	String fechaFin = formato.format(Date.from(model.getDatEnd().atStartOfDay(ZoneId.systemDefault()).toInstant()));
    	String periodo = "Desde:"+fechaInicio+" - "+"Hasta:"+fechaFin;
    	
    	
    	
    	
        try {
        	
        	
            //contenido de encabezado
        	Paragraph p = new Paragraph("MEDICINA INTEGRADA",fuente1);
        	Paragraph p1 = new Paragraph(" ",fuente1);
            Paragraph p2 = new Paragraph("DR. ELMER A. NAVARRO M.",fuente2);
            Paragraph p3 = new Paragraph("J.V.P.M Nº 4656",fuente2);
            Paragraph p4 = new Paragraph("UNIVERSIDAD DE EL SALVADOR",fuente2);
            p.setAlignment(Element.ALIGN_CENTER);
            p2.setAlignment(Element.ALIGN_CENTER);
            p3.setAlignment(Element.ALIGN_CENTER);
            p4.setAlignment(Element.ALIGN_CENTER);
            
            Paragraph p5 = new Paragraph("Tel: 7010-6443",fuente2);
            Paragraph p6 = new Paragraph("PEDIATRÍA GENERAL.",fuente2);
            
            Paragraph p7 = new Paragraph("Tel: 7010-6443",fuente2);
            Paragraph p8 = new Paragraph("MEDICINA INTERNA",fuente2);
            
            p7.setAlignment(Element.ALIGN_RIGHT);
            p8.setAlignment(Element.ALIGN_RIGHT);
        	
        	
          //IMAGENES
            Image image1 = Image.getInstance(citaPaciente.getClass().getClassLoader().getResource("logo-sicimed.png"));
            image1.setWidthPercentage(40);
            
            Image image2 = Image.getInstance(citaPaciente.getClass().getClassLoader().getResource("Logo_UES.jpg"));
            image2.setWidthPercentage(30);
            image2.setAlignment(Element.ALIGN_RIGHT);
            
            
          //TABLA #1
            PdfPTable tabla = new PdfPTable(3); // 3 columnas.
            tabla.getDefaultCell().setBorder(0);
            tabla.setWidthPercentage(100); //Width 100%
            tabla.setSpacingBefore(30f); //Space before table
            tabla.setSpacingAfter(0f); //Space afterr  
            
            float[] columnWidths = {1f, 1f, 1f};
            tabla.setWidths(columnWidths);
     
            //columna 1
            PdfPCell cell1 = new PdfPCell(new Paragraph("",fuente2));
            cell1.addElement(image1);
            cell1.addElement(p5);
            cell1.addElement(p6);
          
            
            cell1.setBorder(0);
            cell1.setPaddingLeft(10);
            cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
            
            
            //columna 2
            PdfPCell cell2 = new PdfPCell(new Paragraph(" " ,fuente2)); 

            cell2.addElement(p);
            cell2.addElement(p1);
            cell2.addElement(p2);
            cell2.addElement(p3);
            cell2.addElement(p4);
            
            cell2.setBorder(0);
            cell2.setPaddingLeft(10);
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);
            
            
            //columna 3
            PdfPCell cell3 = new PdfPCell(new Paragraph("Cell 3"));
            cell3.addElement(image2);
            cell3.addElement(p7);
            cell3.addElement(p8);
            
            cell3.setBorder(0);
            cell3.setPaddingLeft(10);
            cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell3.setVerticalAlignment(Element.ALIGN_MIDDLE);

            //Agregando las columnas a la tabla          
            tabla.addCell(cell1);
            tabla.addCell(cell2);
            tabla.addCell(cell3);
            
            

            Paragraph para = new Paragraph("Resumen consultas realizadas detalle",fuente1);
            para.setAlignment(Element.ALIGN_CENTER);
            para.setSpacingAfter(20);
            
            Paragraph fechas = new Paragraph(periodo,fuente2);
            fechas.setAlignment(Element.ALIGN_CENTER);
            fechas.setSpacingAfter(20);

            PdfPTable tablaDiagnostico = new PdfPTable(7);
            tablaDiagnostico.setSpacingAfter(20);
            tablaDiagnostico.setWidthPercentage(100);
            tablaDiagnostico.setWidths(new float[]{6.0f,6.0f,6.0f,6.0f,6.0f,6.0f,6.0f});


            PdfPCell cell = new PdfPCell();
            cell.setPhrase(new Phrase("Fecha consulta",fuente2));
            cell.setBackgroundColor(new Color(56, 115, 212));
            cell.setPadding(8f);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaDiagnostico.addCell(cell);
            cell.setPhrase(new Phrase("Expediente",fuente2));
            cell.setBackgroundColor(new Color(56, 115, 212));
            cell.setPadding(8f);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaDiagnostico.addCell(cell);
            cell.setPhrase(new Phrase("Paciente",fuente2));
            cell.setBackgroundColor(new Color(56, 115, 212));
            cell.setPadding(8f);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaDiagnostico.addCell(cell);
            cell.setPhrase(new Phrase("Edad",fuente2));
            cell.setBackgroundColor(new Color(56, 115, 212));
            cell.setPadding(8f);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaDiagnostico.addCell(cell);
            cell.setPhrase(new Phrase("Sexo",fuente2));
            cell.setBackgroundColor(new Color(56, 115, 212));
            cell.setPadding(8f);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaDiagnostico.addCell(cell);
            cell.setPhrase(new Phrase("Motivo",fuente2));
            cell.setBackgroundColor(new Color(56, 115, 212));
            cell.setPadding(8f);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaDiagnostico.addCell(cell);
            cell.setPhrase(new Phrase("Diagnóstico",fuente2));
            cell.setBackgroundColor(new Color(56, 115, 212));
            cell.setPadding(8f);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            tablaDiagnostico.addCell(cell);
            DateFunction dateConvert= new DateFunction();
            if(!citas.isEmpty()){
            	Persona persona = null;
                for(Citas cita:citas){
                	persona = cita.getConsulta().getCita().getExpediente().getPaciente().getPersona();
                	
                    tablaDiagnostico.addCell(dateConvert.convertDateToSpanish(cita.getFecha_consulta()));
                    tablaDiagnostico.addCell(cita.getConsulta().getCita().getExpediente().getCodigo());
                    tablaDiagnostico.addCell(persona.getNombre()+" "+persona.getApellido());
                    tablaDiagnostico.addCell(persona.getEdad());
                    tablaDiagnostico.addCell(persona.getSexo());
                    tablaDiagnostico.addCell(cita.getConsulta().getMotivoConsulta());
                    tablaDiagnostico.addCell(cita.getConsulta().getDiagnostico());
                }
            }else{
                tablaDiagnostico.addCell("No hay");
                tablaDiagnostico.addCell("No hay");
                tablaDiagnostico.addCell("No hay");
                tablaDiagnostico.addCell("No hay");
                tablaDiagnostico.addCell("No hay");
                tablaDiagnostico.addCell("No hay");
                tablaDiagnostico.addCell("No hay");
            }

            PdfWriter.getInstance(document, out);
            document.open();
            document.setPageSize(PageSize.A4.rotate());
            document.add(tabla);
            document.add(new Paragraph(""));
            document.add(para);
            document.add(fechas);
            document.add(tablaDiagnostico);
            document.close();

        } catch (DocumentException e) {
            e.printStackTrace();
            logger.error("Error occurred: {0}", e);
        }

        return new ByteArrayInputStream(out.toByteArray());
    }

}
