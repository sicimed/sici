package com.sici.reports;

import java.awt.Color;
import java.awt.Toolkit;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfAnnotation;
import com.lowagie.text.pdf.PdfWriter;
import com.sici.controller.DateFunction;
import com.sici.entity.Persona;
import com.sici.entity.Receta;

public class RecetaPdfView {
    private static final Logger logger = LoggerFactory.getLogger(RecetaPdfView.class);

    public static ByteArrayInputStream recetaReport(Receta receta) throws Exception {
    	

    	
        Font fuente1 = new Font(Font.HELVETICA, 12, Font.BOLD, Color.BLACK);
        Font fuente2 = new Font(Font.HELVETICA, 10, Font.BOLD, Color.BLACK);
        Font fuente3 = new Font(Font.HELVETICA, 10);
        Font fuente4 = new Font(Font.HELVETICA, 9, Font.BOLD, Color.BLACK);
        Font fuente5 = new Font(Font.HELVETICA, 10, Font.ITALIC, Color.BLACK);
        
        DateFunction fecha = new DateFunction();
        Document document = new Document();
        ByteArrayOutputStream out = new ByteArrayOutputStream();


        try {
        	
            //contenido
        	Paragraph p = new Paragraph("MEDICINA INTEGRADA",fuente1);
        	Paragraph p1 = new Paragraph(" ",fuente1);
            Paragraph p2 = new Paragraph("DR. ELMER A. NAVARRO M.",fuente2);
            Paragraph p3 = new Paragraph("J.V.P.M Nº 4656",fuente2);
            Paragraph p4 = new Paragraph("UNIVERSIDAD DE EL SALVADOR",fuente2);
            p.setAlignment(Element.ALIGN_CENTER);
            p2.setAlignment(Element.ALIGN_CENTER);
            p3.setAlignment(Element.ALIGN_CENTER);
            p4.setAlignment(Element.ALIGN_CENTER);
            
            Paragraph p5 = new Paragraph("Tel: 7010-6443",fuente2);
            Paragraph p6 = new Paragraph("PEDIATRÍA GENERAL.",fuente2);
            
            Paragraph p7 = new Paragraph("Tel: 7010-6443",fuente2);
            Paragraph p8 = new Paragraph("MEDICINA INTERNA",fuente2);
            
            p7.setAlignment(Element.ALIGN_RIGHT);
            p8.setAlignment(Element.ALIGN_RIGHT);
      
            
            Paragraph pac = new Paragraph("Paciente:",fuente2);
            pac.setAlignment(Element.ALIGN_LEFT);
            
            Paragraph f = new Paragraph("Fecha:",fuente2);
            f.setAlignment(Element.ALIGN_LEFT);
            
          
            String f1= fecha.convertDateToSpanish(receta.getCreatedAt());
           
            Paragraph f2 = new Paragraph(f1,fuente3);
            f2.setAlignment(Element.ALIGN_LEFT);
            
            Paragraph ed = new Paragraph("Edad:",fuente2);
            ed.setAlignment(Element.ALIGN_LEFT);
                        
            Persona persona = receta.getConsulta().getCita().getExpediente().getPaciente().getPersona();
            Paragraph per = new Paragraph(persona.getNombre()+" "+persona.getApellido(),fuente3);
            Paragraph edad = new Paragraph(persona.getEdad(),fuente3);
            
            Paragraph sex = new Paragraph("Sexo:",fuente2);
            sex.setAlignment(Element.ALIGN_LEFT);
            Paragraph sexo= new Paragraph(receta.getConsulta().getCita().getExpediente().getPaciente().getPersona().getSexo(),fuente3);
            
            Paragraph det = new Paragraph("Detalle de receta:",fuente2);
            det.setAlignment(Element.ALIGN_LEFT);
            Paragraph detalle = new Paragraph(receta.getDetalle(),fuente3);
            
            Paragraph rec = new Paragraph("Recomendación:",fuente2);
            rec.setAlignment(Element.ALIGN_LEFT);
            Paragraph recomendacion = new Paragraph(receta.getRecomendacion(),fuente3);
            
            Paragraph datos = new Paragraph("Datos adicionales:",fuente2);
            datos.setAlignment(Element.ALIGN_LEFT);
            Paragraph otros = new Paragraph(receta.getOtros(),fuente3);
            
            Paragraph firma1 = new Paragraph("________________",fuente5);
            Paragraph firma = new Paragraph("Firma y sello",fuente5);
            firma.setAlignment(Element.ALIGN_CENTER);
            firma1.setAlignment(Element.ALIGN_CENTER);
            Paragraph recom1 = new Paragraph("*** Evite el polvo, humo y ventiladores",fuente4);
            Paragraph recom2 = new Paragraph("*** Evite todo tipo de comidas chatarras o artificiales ",fuente4);

            //Image image1 = Image.getInstance("C:\\\\Users\\\\YESELIZ\\\\git\\\\sici\\\\src\\\\main\\\\resources\\\\logo-sicimed.png");
            Image image1 = Image.getInstance(receta.getClass().getClassLoader().getResource("logo-sicimed.png"));
            image1.setWidthPercentage(40);
            
           // Image image2 = Image.getInstance("C:\\\\Users\\\\YESELIZ\\\\git\\\\sici\\\\src\\\\main\\\\resources\\\\Logo_UES.jpg");
            Image image2 = Image.getInstance(receta.getClass().getClassLoader().getResource("Logo_UES.jpg"));
            image2.setWidthPercentage(30);
            image2.setAlignment(Element.ALIGN_RIGHT);
            
       
       //TABLA #1
            PdfPTable tabla = new PdfPTable(3); // 3 columnas.
            tabla.getDefaultCell().setBorder(0);
            tabla.setWidthPercentage(100); //Width 100%
            tabla.setSpacingBefore(30f); //Space before table
            tabla.setSpacingAfter(0f); //Space afterr  
            
            float[] columnWidths = {1f, 1f, 1f};
            tabla.setWidths(columnWidths);
     
            //columna 1
            PdfPCell cell1 = new PdfPCell(new Paragraph("",fuente2));
            cell1.addElement(image1);
            cell1.addElement(p5);
            cell1.addElement(p6);
          
            
            cell1.setBorder(0);
            cell1.setPaddingLeft(10);
            cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
            
            
            //columna 2
            PdfPCell cell2 = new PdfPCell(new Paragraph(" " ,fuente2)); 

            cell2.addElement(p);
            cell2.addElement(p1);
            cell2.addElement(p2);
            cell2.addElement(p3);
            cell2.addElement(p4);
            
            cell2.setBorder(0);
            cell2.setPaddingLeft(10);
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);
            
            
            //columna 3
            PdfPCell cell3 = new PdfPCell(new Paragraph("Cell 3"));
            cell3.addElement(image2);
            cell3.addElement(p7);
            cell3.addElement(p8);
            
            cell3.setBorder(0);
            cell3.setPaddingLeft(10);
            cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell3.setVerticalAlignment(Element.ALIGN_MIDDLE);

            //Agregando las columnas a la tabla          
            tabla.addCell(cell1);
            tabla.addCell(cell2);
            tabla.addCell(cell3);
            
          //TABLA #2-1
            PdfPTable tabla11 = new PdfPTable(3); 
            tabla11.getDefaultCell().setBorder(0);
            tabla11.setWidthPercentage(100); //Width 100%
            tabla11.setSpacingBefore(5f); //Space before table
            tabla11.setSpacingAfter(30f); //Space after table
            
            //Set Column widths
            float[] column2= {1f, 1f, 1f};
            tabla.setWidths(column2);
            
            //Columna 1
            PdfPCell celda11= new PdfPCell(new Paragraph("",fuente2));
            celda11.addElement(ed);
            celda11.addElement(edad);
            celda11.setBorder(0);
   
            celda11.setHorizontalAlignment(Element.ALIGN_LEFT);
            celda11.setVerticalAlignment(Element.ALIGN_MIDDLE);
     
            //Columna 2
            PdfPCell celda12 = new PdfPCell(new Paragraph("" ,fuente2)); 
            
            celda12.setBorder(0);
            

            celda12.setHorizontalAlignment(Element.ALIGN_CENTER);
            celda12.setVerticalAlignment(Element.ALIGN_MIDDLE);
            
          //Columna 3
            PdfPCell celda13 = new PdfPCell(new Paragraph("" ,fuente2)); 
            celda13.addElement(sex);
            celda13.addElement(sexo);
            celda13.setBorder(0);
            

            celda13.setHorizontalAlignment(Element.ALIGN_CENTER);
            celda13.setVerticalAlignment(Element.ALIGN_MIDDLE);
   
           //Agregando las columnas a la tabla 
            tabla11.addCell(celda11);
            tabla11.addCell(celda12);
            tabla11.addCell(celda13);

 
         

      //TABLA #2
            PdfPTable table1 = new PdfPTable(3); 
            table1.getDefaultCell().setBorder(0);
            table1.setWidthPercentage(100); //Width 100%
            table1.setSpacingBefore(30f); //Space before table
            table1.setSpacingAfter(30f); //Space after table
            
            //Set Column widths
            float[] column= {1f, 1f, 1f};
            table1.setWidths(column);
            
            //Columna 1
            PdfPCell celda= new PdfPCell(new Paragraph("",fuente2));
            celda.addElement(pac);
            celda.addElement(per);
            celda.setBorder(0);
   
            celda.setHorizontalAlignment(Element.ALIGN_LEFT);
            celda.setVerticalAlignment(Element.ALIGN_MIDDLE);
     
            //Columna 2
            PdfPCell celda2 = new PdfPCell(new Paragraph("" ,fuente2)); 
            celda2.setBorder(0);
            

            celda2.setHorizontalAlignment(Element.ALIGN_CENTER);
            celda2.setVerticalAlignment(Element.ALIGN_MIDDLE);
            
          //Columna 3
            PdfPCell celda3 = new PdfPCell(new Paragraph("" ,fuente2)); 
            celda3.addElement(f);
            celda3.addElement(f2);
            celda3.setBorder(0);
            

            celda3.setHorizontalAlignment(Element.ALIGN_CENTER);
            celda3.setVerticalAlignment(Element.ALIGN_MIDDLE);
 
            
           //Agregando las columnas a la tabla 
            table1.addCell(celda);
            table1.addCell(celda2);
            table1.addCell(celda3);

           
            
            
        //TABLA #3
            PdfPTable table3 = new PdfPTable(1); 
            table3.getDefaultCell().setBorder(0);
            table3.setWidthPercentage(100); //Width 100%
            table3.setSpacingBefore(30f); //Space before table
            table3.setSpacingAfter(30f); //Space after table
            
            //Set Column widths
            float[] column3= {1f};
            table3.setWidths(column3);
            
          //Columna 1
            PdfPCell c1= new PdfPCell(new Paragraph("",fuente2));
            c1.setBorder(0);
            c1.addElement(det);
            c1.addElement(detalle);
   
            c1.setHorizontalAlignment(Element.ALIGN_LEFT);
            c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
            
          //Agregando las columnas a la tabla 
            table3.addCell(c1);
            
            
       //TABLA #4
            PdfPTable table4 = new PdfPTable(1); 
            table4.getDefaultCell().setBorder(0);
            table4.setWidthPercentage(100); //Width 100%
            table4.setSpacingBefore(30f); //Space before table
            table4.setSpacingAfter(30f); //Space after table
            
            //Set Column widths
            float[] column4= {1f};
            table3.setWidths(column4);
            
          //Columna 1
            PdfPCell col= new PdfPCell(new Paragraph("",fuente2));
            col.setBorder(0);
            col.addElement(rec);
            col.addElement(recomendacion);
   
            col.setHorizontalAlignment(Element.ALIGN_LEFT);
            col.setVerticalAlignment(Element.ALIGN_MIDDLE);
            
          //Agregando las columnas a la tabla 
            table4.addCell(col);
            
        //TABLA #5
            PdfPTable table5 = new PdfPTable(1); 
            table5.getDefaultCell().setBorder(0);
            table5.setWidthPercentage(100); //Width 100%
            table5.setSpacingBefore(30f); //Space before table
            table5.setSpacingAfter(30f); //Space after table
            
            //Set Column widths
            float[] column5= {1f};
            table3.setWidths(column5);
            
          //Columna 1
            PdfPCell colum= new PdfPCell(new Paragraph("",fuente2));
            colum.setBorder(0);
            colum.addElement(datos);
            colum.addElement(otros);
   
            colum.setHorizontalAlignment(Element.ALIGN_LEFT);
            colum.setVerticalAlignment(Element.ALIGN_MIDDLE);
            
          //Agregando las columnas a la tabla 
            table5.addCell(colum);
            
            
        //TABLA #6
            PdfPTable tabla6 = new PdfPTable(3); // 3 columnas.
            tabla6.getDefaultCell().setBorder(0);
            tabla6.setWidthPercentage(100); //Width 100%
            tabla6.setSpacingBefore(30f); //Space before table
            tabla6.setSpacingAfter(30f); //Space afterr  
            
            float[] column6 = {1f, 1f, 1f};
            tabla.setWidths(column6);
     
            //columna 1
            PdfPCell cl = new PdfPCell(new Paragraph("",fuente2));

            
            cl.setBorder(0);
            cl.setPaddingLeft(10);
            cl.setHorizontalAlignment(Element.ALIGN_LEFT);
            cl.setVerticalAlignment(Element.ALIGN_MIDDLE);
            
            
            //columna 2
            PdfPCell cl2 = new PdfPCell(new Paragraph(" " ,fuente2)); 

            
            cl2.setBorder(0);
            cl2.setPaddingLeft(10);
            cl2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cl2.setVerticalAlignment(Element.ALIGN_MIDDLE);
            
            
            //columna 3
            PdfPCell cl3 = new PdfPCell(new Paragraph("Cell 3"));
            cl3.addElement(firma1);
            cl3.addElement(firma);
            cl3.setBorder(0);
            cl3.setPaddingLeft(10);
            cl3.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cl3.setVerticalAlignment(Element.ALIGN_MIDDLE);

            //Agregando las columnas a la tabla          
            tabla6.addCell(cl);
            tabla6.addCell(cl2);
            tabla6.addCell(cl3);
            
            
            
          //TABLA #7
            PdfPTable table7 = new PdfPTable(1); 
            table7.getDefaultCell().setBorder(0);
            table7.setWidthPercentage(100); //Width 100%
            table7.setSpacingBefore(30f); //Space before table
            table7.setSpacingAfter(30f); //Space after table
            
            //Set Column widths
            float[] column7= {1f};
            table7.setWidths(column7);
            
          //Columna 1
            PdfPCell colum7= new PdfPCell(new Paragraph("",fuente2));
            colum7.setBorder(0);
            colum7.addElement(recom1);
            colum7.addElement(recom2);
   
            colum7.setHorizontalAlignment(Element.ALIGN_LEFT);
            colum7.setVerticalAlignment(Element.ALIGN_MIDDLE);
            
          //Agregando las columnas a la tabla 
            table7.addCell(colum7);
            
           
          /*----------------------------------------------------*/  
          PdfWriter.getInstance(document, out);
          document.open();
          
          document.add(tabla);
          document.add(table1);
          document.add(tabla11);
          document.add(table3);
          document.add(table4);
          document.add(table5);
          document.add(tabla6);
          document.add(table7);

          document.close();

        } catch (DocumentException e) {
            e.printStackTrace();
            logger.error("Error occurred: {0}", e);
        }

        return new ByteArrayInputStream(out.toByteArray());
    }

    public static PdfPCell createImageCell(String path) throws DocumentException, IOException {
		 Image img = Image.getInstance(path);
	        PdfPCell cell = new PdfPCell(img, true);
	        return cell;
		
	}
}
