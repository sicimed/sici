//package com.sici.scheduledTaskAndDB;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.core.io.FileSystemResource;
//import org.springframework.mail.MailException;
//import org.springframework.mail.javamail.JavaMailSender;
//import org.springframework.mail.javamail.MimeMessageHelper;
//import org.springframework.scheduling.annotation.Scheduled;
//import org.springframework.stereotype.Component;
//
//import java.io.File;
//import java.io.IOException;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//
//import javax.mail.MessagingException;
//import javax.mail.internet.MimeMessage;
//
//@Component
//public class BackupDB {
//	@Autowired
//	private JavaMailSender javaMailSender;
//
//    private Process proceso;
//    private ProcessBuilder constructor;
//
//    private final String host = "localhost";
//    private final String puerto="5432";
//    private final String usuario = "sici";
//    private final String clave = "SiCiMeDB";
//    private final String bd="sicimedb";
//    private final String formato="custom";
//
//    @Scheduled(cron = "0 5 18 * * *", zone = "America/El_Salvador")
//    @Scheduled(fixedRate = 10000)
//    public boolean db_backup() throws MessagingException,IOException{
//        boolean hecho = false;
//        Date backupDate = new Date();
//        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
//        String backupDateStr = format.format(backupDate);
//        String fileName = "sicimedb_backup";
//        String folderPath = "C:\\SICIMED";
//        File f1 = new File(folderPath);
//        f1.mkdir();
//
//        String saveFileName = fileName + "_" + backupDateStr + ".backup";
//        String savePath = folderPath+File.separator + saveFileName;
//
//        try{
//            File pgdump = new File("C:/Program Files/PostgreSQL/9.3/bin\\pg_dump.exe");
//            //File pgdump = new File("C:/Program Files/PostgreSQL/10/bin\\pg_dump.exe");
//            if(pgdump.exists()){
//                if(!savePath.equalsIgnoreCase("")) {
//                    System.out.println("EJECUTA 1");
//                    constructor = new ProcessBuilder("C:/Program Files/PostgreSQL/9.3/bin\\pg_dump.exe", "--verbose", "--format", formato,"-f", savePath);
//                  //  constructor = new ProcessBuilder("C:/Program Files/PostgreSQL/10/bin\\pg_dump.exe", "--verbose","--format", formato, "-f", savePath);
//                } else {
//                    System.out.println("EJECUTA 2");
//                    constructor = new ProcessBuilder("C:/Program Files/PostgreSQL/9.3/bin\\pg_dump.exe", "--verbose", "--inserts", "--column-inserts", "-f", savePath);
//                   // constructor = new ProcessBuilder("C:/Program Files/PostgreSQL/10/bin\\pg_dump.exe", "--verbose","--inserts", "--column-inserts", "-f", savePath);
//                    System.out.println("ERROR");
//                }
//                constructor.environment().put("PGHOST", host);
//                constructor.environment().put("PGPORT", puerto);
//                constructor.environment().put("PGUSER", usuario);
//                constructor.environment().put("PGPASSWORD", clave);
//                constructor.environment().put("PGDATABASE", bd);
//                constructor.redirectErrorStream(true);
//                proceso= constructor.start();
//                System.out.println("terminado backup " + savePath);
//                hecho=true;
//                //proceso.destroy();
//            }
//        }catch (IOException ex){
//            System.err.println(ex.getMessage()+ "Error de backup");
//            hecho=false;
//        }
//   
//        //ENVIO DE BACKUP A EMAIL CLINICA MEDICA
//        MimeMessage msg = javaMailSender.createMimeMessage();
//        MimeMessageHelper helper = new MimeMessageHelper(msg, true);
//        
//        FileSystemResource file = new FileSystemResource(new File(savePath));
//        helper.setTo("salvadoramelia2@gmail.com");
//        helper.setSubject("BACKUP BASE DE DATOS!");
//        helper.setText("Fecha:"+backupDateStr);
//        helper.addAttachment(saveFileName, file);
//        try {
//        javaMailSender.send(msg);
//        }catch (MailException ex) {
//        	 // Simple Log para errores msg
//            System.err.println(ex.getMessage());
//        }
//        
//       
//        return hecho;
//    }
//}
