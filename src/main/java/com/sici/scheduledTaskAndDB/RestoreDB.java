//package com.sici.scheduledTaskAndDB;
//
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.multipart.MultipartFile;
//import org.springframework.web.servlet.mvc.support.RedirectAttributes;
//
//import java.io.File;
//import java.io.IOException;
//import java.nio.file.Files;
//import java.nio.file.Path;
//import java.nio.file.Paths;
//import java.sql.Connection;
//import java.sql.DriverManager;
//import java.sql.SQLException;
//import java.sql.Statement;
//import java.util.concurrent.TimeUnit;
//
//@Controller
//@RequestMapping("/restore_db")
//public class RestoreDB {
//    private Process proceso;
//    private ProcessBuilder constructor;
//
//    private final String host = "localhost";
//    private final String puerto="5432";
//    private final String usuario = "sici";
//    private final String clave = "SiCiMeDB";
//    private final String bd="sicimedb";
//    private final String formato="custom";
//    private final String url = "jdbc:postgresql://localhost:"+puerto+"/"+bd+"?currentSchema=sici";
//
//    @GetMapping("/form")
//    public String formRestore(){
//
//        return "form_restore";
//    }
//
//    @PostMapping("/form/restore")
//    public String formRestore(@RequestParam("file")MultipartFile file, @RequestParam("password")String password, RedirectAttributes rm) throws IOException, SQLException {
//        if(password.equals("12345") && !file.isEmpty()){
////            Connection connection = DriverManager.getConnection(url,usuario,clave);
////            System.out.println("Connection:"+connection);
////            Statement statement = connection.createStatement();
////            System.out.println("statement-----------");
////            statement.execute("DROP SCHEMA IF EXISTS sici;");
////            statement.execute("create schema sici;");
////            System.out.println("EJECUTANDO DROP");
////            connection.close();
////            System.out.println("CERRANDO CONNECTION");
//            String folderPath = "C:\\SICIMED\\Restore";
//            File f1 = new File(folderPath);
//            f1.mkdir();
//            String folderPath2 = "C:/SICIMED/Restore/";
//            boolean hecho = false;
//            byte[] bytes = file.getBytes();
//            Path path = Paths.get(folderPath2+file.getOriginalFilename());
//            System.out.println("direccion:"+path.toString());
//            Files.write(path,bytes);
//            try{
//                File pgrestore  = new File("C:/Program Files/PostgreSQL/9.3/bin\\pg_restore.exe");
//                if(pgrestore.exists()){
//                    System.out.println("entrando---------");
//                    constructor = new ProcessBuilder("C:/Program Files/PostgreSQL/9.3/bin\\pg_restore.exe", "-i", "-h", host, "-p", puerto, "-U", usuario, "-d", bd,"-v",path.toString());
//                    constructor.environment().put("PGPASSWORD", clave);
//                    constructor.redirectErrorStream(true);
//                    proceso=constructor.start();
//                    hecho=true;
//                    //proceso.waitFor(8, TimeUnit.SECONDS);
////                    proceso.destroy();
////                    proceso.waitFor();
//                    //proceso.destroy();
//                }
//            }catch (IOException ex){
//                ex.printStackTrace();
//                hecho=false;
//            }
//            if(hecho){
//                System.out.println("---------------Restore success---------------");
//                //proceso.destroy();
//            }
//        }else{
//            rm.addFlashAttribute("mensaje","Error de password o archivo vacio");
//            return "redirect:/restore_db/form";
//        }
//
//        return "redirect:/login";
//    }
//}
