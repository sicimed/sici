package com.sici.scheduledTaskAndDB;

import com.sici.controller.DateFunction;
import com.sici.entity.Citas;
import com.sici.serviceImp.CitaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.TimeZone;

@Component
public class EmailCita {

	@Autowired
	private JavaMailSender javaMailSender;

	@Autowired
	private CitaService citaService;

	public static LocalDate toLocalDate(Calendar calendar) {
		if (calendar == null) {
			return null;
		}
		TimeZone tz = calendar.getTimeZone();
		ZoneId zid = tz == null ? ZoneId.systemDefault() : tz.toZoneId();
		return LocalDateTime.ofInstant(calendar.toInstant(), zid).toLocalDate();
	}

	// @Scheduled(fixedRate = 10000)
	@Scheduled(cron = "0 30 18 * * *", zone = "America/El_Salvador")
	public void emailCitas() throws MessagingException, IOException {
		Calendar calendar = Calendar.getInstance();
		int contador = 0;
		calendar.setTime(Date.valueOf(LocalDate.now()));
		calendar.add(Calendar.DAY_OF_YEAR, 2);
		System.out.println("fecha dos dias mas:" + calendar.getTime());
		LocalDate dateAfter = toLocalDate(calendar);
		DateFunction dateFunction = new DateFunction();
		Iterable<Citas> citas = citaService.listCitasBetweenDates(LocalDate.now(), dateAfter);
		for (Citas cita : citas) {
			System.out.println(cita);
			contador++;
		}
		System.out.println("CONTADOR:" + contador);

		String tabla = "";
		MimeMessage msg = javaMailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(msg, true);
		// helper.setTo("sargom66@gmail.com");
		helper.setTo("sargom66@gmail.com");
		helper.setSubject("CITAS PROXIMAS!");
		if (citas != null && contador > 0) {
			for (Citas cita : citas) {
				// System.out.println("expediente:"+cita.getExpediente().toString());
				System.out.println(cita.getFecha_consulta() + cita.getHora() + cita.getTipoHora());
				tabla += "  <tr>\n" + "    <td style=\"border: 1px solid black; border-collapse: collapse;\">"
						+ dateFunction.convertDateToSpanish(cita.getFecha_consulta()) + "</td>\n"
						+ "    <td style=\"border: 1px solid black; border-collapse: collapse;\">" + cita.getHora()
						+ " " + cita.getTipoHora() + "</td>\n" + "  </tr>\n";
			}

			helper.setText("<h1>Recordatorio de citas proximas!</h1>", true);
			helper.setText("<table style=\"width:100%;border: 1px solid black; border-collapse: collapse;\">\n"
					+ "  <tr>\n"
					+ "    <th style=\"border: 1px solid black; border-collapse: collapse;background-color: black;color: white;\">Fecha</th>\n"
					+ "    <th style=\"border: 1px solid black; border-collapse: collapse;background-color: black;color: white;\">Hora</th> \n"
					+ "  </tr>\n" + tabla + "</table>", true);
			// helper.addAttachment("clinica.png", new
			// File("src//main//resources//static//images//email//logo.png"));

		} else {
			helper.setText("<h1>No hay citas proximas!</h1>", true);
		}

		try {
			javaMailSender.send(msg);
		} catch (MailException ex) {
			// Simple Log para errores msg
			System.err.println(ex.getMessage());
		}

	}
}
