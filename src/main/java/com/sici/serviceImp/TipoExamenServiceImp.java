package com.sici.serviceImp;

import com.sici.entity.TipoExamen;
import com.sici.repository.TipoExamenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("tipoExamenService")
public class TipoExamenServiceImp implements TipoExamenService {

    @Autowired
    @Qualifier("tipoExamenRepository")
    private TipoExamenRepository tipoExamenRepository;

    @Override
    public TipoExamen addTipoExamen(TipoExamen tipoExamen) {
        return tipoExamenRepository.save(tipoExamen);
    }

    @Override
    public TipoExamen getTipoExamenId(Long id) {
        return tipoExamenRepository.findById(id);
    }


    @Override
    public Iterable<TipoExamen> listTipoExamen() {
        return tipoExamenRepository.findAll();
    }

    @Override
    public int deleteTipoExamen(Long id) {
        int result=0;
        TipoExamen tipoExamen = getTipoExamenId(id);
        tipoExamen.setEstado(false);
        if(null!=tipoExamenRepository.save(tipoExamen)){
            result =1;
        }
        return result;
    }
}
