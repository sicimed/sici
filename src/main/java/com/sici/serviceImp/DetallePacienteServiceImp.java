package com.sici.serviceImp;

import com.sici.entity.DetallePaciente;
import com.sici.entity.Paciente;
import com.sici.repository.DetallePacienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("detallePacienteService")
public class DetallePacienteServiceImp implements DetallePacienteService {

    @Autowired
    @Qualifier("detallePacienteRepository")
    private DetallePacienteRepository detallePacienteRepository;

    @Override
    public DetallePaciente addDetallePaciente(DetallePaciente detallePaciente) {

        return detallePacienteRepository.save(detallePaciente);

    }

    @Override
    public DetallePaciente getDetalleById(Long id) {
        return detallePacienteRepository.findById(id);
    }

    @Override
    public List<DetallePaciente> latestDetallePaciente(Paciente paciente) {

        return detallePacienteRepository.findByPacienteOrderByPacienteDesc(paciente);
    }

    @Override
    public DetallePaciente getLastPaciente(Paciente paciente) {
        return detallePacienteRepository.findLatest(paciente);
    }

}
