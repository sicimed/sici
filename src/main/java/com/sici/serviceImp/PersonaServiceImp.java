package com.sici.serviceImp;

import com.sici.entity.Expediente;
import com.sici.entity.HistorialMedico;
import com.sici.entity.Paciente;
import com.sici.entity.Persona;
import com.sici.repository.ExpedienteRepository;
import com.sici.repository.PacienteRepository;
import com.sici.repository.PersonaRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Calendar;

@Service("personaServiceImp")
public class PersonaServiceImp implements PersonaService {

    @Autowired
    @Qualifier("personaRepository")
    private PersonaRepository personaRepository;

    @Autowired
    @Qualifier("pacienteRepository")
    private PacienteRepository pacienteRepository;

    @Autowired
    @Qualifier("expedienteRepository")
    private ExpedienteRepository expedienteRepository;

    @Autowired
    @Qualifier("clinicaServiceImp")
    private ClinicaService clinicaService;

    @Autowired
    @Qualifier("historialMedicoService")
    private HistorialMedicoService historialMedicoService;

    private String codePaciente(Persona persona){
        String lastname = persona.getApellido().toUpperCase();
        String age = persona.getEdad().substring(0,2);
        lastname = lastname.replace(" ","");
        String code = lastname+age;
        return code;
    }

    private String codeExpediente(Paciente paciente){
        String name_space = paciente.getPersona().getNombre().toUpperCase().replace(" ","");
        String lastname_space = paciente.getPersona().getApellido().toUpperCase().replace(" ","");
        String edad2 = paciente.getPersona().getEdad();
        LocalDate fecha_nacimiento = paciente.getPersona().getFechaNacimiento();
        System.out.println("ANIO NACIMIENTO:"+fecha_nacimiento.getYear());
//        edad2 = edad2.substring(0,2);
//        edad2 = edad2.trim();
        System.out.println("Edad bebe:"+edad2);
//        int age_code = 0;
//        if(edad2.equals("0 ")) {
//        	System.out.println("Entro aqui age_code=0");
//        	edad2=0;
//        }
       // age_code = Integer.parseInt(edad2);
        System.out.println("parsea bien");
        char code_name = name_space.charAt(0);
        char code_lastname = lastname_space.charAt(0);

        String code_name_2 = Character.toString(code_name);
        String code_lastname_2 = Character.toString(code_lastname);

//        Calendar date = Calendar.getInstance();
//        int year = date.get(Calendar.YEAR);
//        int birth = year - age_code;
//        System.out.println("year - age_code");
        String year_birth = Integer.toString(fecha_nacimiento.getYear());
        String sub_year_birth = year_birth.substring(2,4);
        System.out.println("extraer digitos anio");
        String code_final = code_name_2+code_lastname_2+sub_year_birth;
        return code_final;
    }

    @Override
    public Persona addPersona(Persona persona){
        persona.setEstado(true);
        persona.setTipo("paciente");
        Persona persona2 = personaRepository.save(persona);

        String code = codePaciente(persona2);

        Paciente paciente = new Paciente();
        paciente.setCodigo(code);
        paciente.setPersona(persona2);
        paciente.setClinica(clinicaService.getClinicaByCodigo("C1"));

        Paciente paciente2 = pacienteRepository.save(paciente);

        String code_final = codeExpediente(paciente2);

        Expediente expediente = new Expediente();
        expediente.setCodigo(code_final);
        expediente.setEstado(true);
        expediente.setPaciente(paciente2);

        Expediente expediente1= expedienteRepository.save(expediente);
        //Variable para reducir redundancia;
        Persona persona1 = expediente1.getPaciente().getPersona();

        HistorialMedico historialMedico = new HistorialMedico();
        historialMedico.setExpediente(expediente1);
        historialMedico.setNombre("HM-"+expediente1.getCodigo()+' '+persona1.getNombre()+'_'+ persona1.getApellido());
        historialMedico.setEstado(true);
        historialMedicoService.addHistorialMedico(historialMedico);

        return persona2;
    }

    @Override
    public Persona deletePersona(Persona persona){
        persona.setEstado(false);
        Persona persona2 = personaRepository.save(persona);
        return persona2;
    }

    @Autowired
    public Iterable<Persona> listAllPacientes(){
    	return personaRepository.findByEstado(true);
    }
   
    @Override
    public Persona getPacienteById(Long id) {
        return personaRepository.findById(id).get();
    }
    
    @Override
    public Persona editPersona(Persona persona){
        Persona persona1 = getPacienteById(persona.getId());
        persona1.setNombre(persona.getNombre());
        persona1.setApellido(persona.getApellido());
        persona1.setFechaNacimiento(persona.getFechaNacimiento());
        persona1.setEdad(persona.getEdad());
        persona1.setDireccion(persona.getDireccion());
        persona1.setSexo(persona.getSexo());
        persona1.setTelefono(persona.getTelefono());
        persona1.setColonia(persona.getColonia());
        persona1.setComentario(persona.getComentario());
        persona1 = personaRepository.save(persona1);

        String code_paciente = codePaciente(persona1);

        Paciente paciente = persona.getPaciente();
        paciente.setCodigo(code_paciente);
        paciente = pacienteRepository.save(paciente);

        String code_final = codeExpediente(paciente);
        Expediente expediente = paciente.getExpediente();
        expediente.setCodigo(code_final);
        expedienteRepository.save(expediente);
        return persona1;
    }
    
    @Autowired
    public Iterable<Persona> listAllExpedientes(){
    	return personaRepository.findAll();
    }
    
    public Persona changeStatusPersona (Persona persona) {
    	Persona change;
    	if(persona.isEstado()==true) {
    		persona.setEstado(false);
    		change = personaRepository.save(persona);
    	}else{
    	persona.setEstado(true);
    	
    	change = personaRepository.save(persona);
        }
    	return change;
    }
   

}
