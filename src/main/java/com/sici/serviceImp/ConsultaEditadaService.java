package com.sici.serviceImp;

import java.util.List;

import com.sici.entity.ConsultaEditada;

public interface ConsultaEditadaService {

	public abstract ConsultaEditada save(ConsultaEditada consulta);
	public abstract ConsultaEditada findById(Long id);
	public abstract List<ConsultaEditada> listConsultas(Long idConsulta);
}
