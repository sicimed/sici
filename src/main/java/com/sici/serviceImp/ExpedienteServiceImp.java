package com.sici.serviceImp;

import com.sici.entity.Expediente;
import com.sici.repository.ExpedienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("expedienteService")
public class ExpedienteServiceImp implements ExpedienteService {

    @Autowired
    @Qualifier("expedienteRepository")
    private ExpedienteRepository expedienteRepository;


    @Override
    public List<Expediente> findAll() {
        return (List<Expediente>) expedienteRepository.findAll();
    }

    @Override
    public Expediente getExpedienteById(Long id) {
        return expedienteRepository.findById(id);
    }


}
