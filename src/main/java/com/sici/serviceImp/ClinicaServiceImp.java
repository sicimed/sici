package com.sici.serviceImp;

import com.sici.entity.Clinica;
import com.sici.repository.ClinicaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("clinicaServiceImp")
public class ClinicaServiceImp implements ClinicaService {

    @Autowired
    @Qualifier("clinicaRepository")
    private ClinicaRepository clinicaRepository;

    @Override
    public Clinica addClinica(Clinica clinica) {

        clinicaRepository.save(clinica);

        return clinica;
    }

    @Override
    public Clinica getClinicaByCodigo(String codigo) {
        return clinicaRepository.findClinicaByCodigo(codigo);
    }
}
