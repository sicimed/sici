package com.sici.serviceImp;

import com.sici.entity.Empleado;
import com.sici.repository.EmpleadoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("empleadoService")
public class EmpleadoServiceImp {

    @Autowired
    @Qualifier("empleadoRepository")
    private EmpleadoRepository empleadoRepository;

    public Empleado addEmpleado(Empleado empleado){
        return empleadoRepository.save(empleado);
    }

}
