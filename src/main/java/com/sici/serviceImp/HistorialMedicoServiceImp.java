package com.sici.serviceImp;

import com.sici.entity.Expediente;
import com.sici.entity.HistorialMedico;
import com.sici.repository.HistorialMedicoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("historialMedicoService")
public class HistorialMedicoServiceImp implements HistorialMedicoService {

    @Autowired
    @Qualifier("historialMedicoRepository")
    private HistorialMedicoRepository historialMedicoRepository;

    @Override
    public void addHistorialMedico(HistorialMedico historialMedico) {

         historialMedicoRepository.save(historialMedico);
    }

    @Override
    public HistorialMedico getHistorialById(Long id) {
        return historialMedicoRepository.findById(id).orElse(null);
    }

    @Override
    public HistorialMedico getHistorialByExpediente(Expediente expediente) {
        return historialMedicoRepository.findByExpediente(expediente);
    }

    @Override
    public List<HistorialMedico> findAll() {
        return (List<HistorialMedico>) historialMedicoRepository.findAll();
    }


}
