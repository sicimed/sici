package com.sici.serviceImp;

import com.sici.entity.*;

public interface ProveedorService {


	    Iterable<Proveedor> listAllProveedores();
	    
	    Proveedor getProveedorById(Long id);
	    
	    Proveedor saveProveedor(Proveedor proveedor);
	    
	    void deleteProveedor(Long id);
	    
	    public abstract Proveedor editProveedor(Proveedor proveedor);


}
