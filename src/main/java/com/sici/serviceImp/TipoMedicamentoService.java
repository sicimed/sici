package com.sici.serviceImp;

import com.sici.entity.*;

public interface TipoMedicamentoService {

    Iterable<TipoMedicamento> listAllTiposMedicamento();
    
    TipoMedicamento getTipoMedicamentoById(Long id);
    
    TipoMedicamento saveTipoMedicamento(TipoMedicamento tipomedicamento);
    
    void deleteTipoMedicamento(Long id);
    
    public abstract TipoMedicamento editTipomedicamento(TipoMedicamento tipomedicamento);
 
}
