package com.sici.serviceImp;

import com.sici.entity.Consulta;

public interface ConsultaService {

    public abstract Consulta addConsulta(Consulta consulta,Long id);
    public abstract Iterable<Consulta> listConsultas();
    public abstract Consulta getConsultaById(Long id);
    public abstract Consulta editConsulta(Consulta consulta, Long id_consulta,Long id_cita);
    public abstract Consulta cancelConsulta(Long id);
}
