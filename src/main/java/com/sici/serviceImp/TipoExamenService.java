package com.sici.serviceImp;

import com.sici.entity.TipoExamen;

public interface TipoExamenService {

    public abstract TipoExamen addTipoExamen(TipoExamen tipoExamen);
    public abstract TipoExamen getTipoExamenId(Long id);
    public abstract Iterable<TipoExamen> listTipoExamen();
    public abstract int deleteTipoExamen(Long id);
}
