package com.sici.serviceImp;

import com.sici.entity.*;

public interface RecetaService {

	public abstract Receta getRecetaByConsulta(Consulta consulta);

	Iterable<Receta> listAllRecetas();
	
	Iterable<Receta> listAllRecetasActivas();
    
	public abstract Receta deleteReceta(Receta receta);

	public abstract Receta addReceta(Receta receta);
		
	Receta getRecetaById(Long id);
	
	public abstract Receta editReceta(Receta receta);

  
}
