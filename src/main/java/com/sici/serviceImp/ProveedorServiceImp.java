package com.sici.serviceImp;

import com.sici.entity.*;
import com.sici.repository.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;


@Service("proveedorServiceImp")
public class ProveedorServiceImp implements ProveedorService {

    @Autowired
    @Qualifier("proveedorRepository")
    private ProveedorRepository proveedorRepository;

   

	@Override
	public  Iterable<Proveedor> listAllProveedores() {
		return proveedorRepository.findAll();
	}
	

	@Override
	public Proveedor getProveedorById(Long id){
		return proveedorRepository.findById(id).get();
	}
	

	@Override
    public void deleteProveedor(Long id) {
        proveedorRepository.deleteById(id);
    }

	@Override
	 public Proveedor saveProveedor(Proveedor proveedor){
	            return proveedorRepository.save(proveedor);
	 }

	@Override
	public Proveedor editProveedor(Proveedor proveedor) {
		return proveedorRepository.save(proveedor);
	}


}
