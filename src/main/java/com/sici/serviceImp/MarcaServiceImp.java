package com.sici.serviceImp;

import com.sici.entity.*;
import com.sici.repository.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;


/**
 * @author YESELIZ
 *
 */
@Service("marcaServiceImp")
public class MarcaServiceImp implements MarcaService {

    @Autowired
    @Qualifier("marcaRepository")
    private MarcaRepository marcaRepository;

 

	@Override
	public Iterable<Marca> listAllMarca(){
		return marcaRepository.findAll();
	
	}
	
	@Override
	public Marca getMarcaById(Long id) {
		return marcaRepository.findById(id).get();
	}	

	@Override
    public void deleteMarca(Long id) {
        marcaRepository.deleteById(id);
    }
	
	 @Override
	    public Marca saveMarca(Marca marca) {
	        return marcaRepository.save(marca);
	    }
	 
	 @Override 
	 public Marca editMarca(Marca marca){ 
		 return marcaRepository.save(marca);
			  
	 }
	 


	 }
	
	




