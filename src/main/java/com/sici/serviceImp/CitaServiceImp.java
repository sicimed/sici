package com.sici.serviceImp;

import com.sici.entity.Citas;
import com.sici.entity.Expediente;
import com.sici.model.CitaCountModel;
import com.sici.repository.CitaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service("citaServiceImp")
public class CitaServiceImp implements CitaService {

    @Autowired
    @Qualifier("citaRepository")
    private CitaRepository citaRepository;

    @Override
    public Citas addCita(Citas citas) {
       // citas.setEstado(true);
       // citas.setEstado_cita("ESPERA");
        //Citas citas1 = citaRepository.save(citas);
        Citas citas1 = citaRepository.save(new Citas(citas.getExpediente(),citas.getFecha_consulta(),citas.getHora(),citas.getTipoHora(),"ESPERA",true));

        return citas1;
    }

    @Override
    public Iterable<Citas> listAllCitasPendiente() {

        return citaRepository.findByProcesoAndEstado("ESPERA",true);
    }

    @Override
    public Iterable<Citas> listAllCitasEjecutada() {
        return citaRepository.findByProcesoAndEstado("EJECUTADA",true);
    }

    @Override
    public Citas getCitaById(Long id) {
        return citaRepository.findById(id);
    }

    @Override
    public Citas changeStatusCita(Citas citas,String proceso) {
        citas.setEstado_cita(proceso);
        return citaRepository.save(citas);
    }

    @Override
    public Citas editCitas(Citas citas, Long id) {
        Citas citas1 = getCitaById(id);
        citas1.setExpediente(citas1.getExpediente());
        citas1.setFecha_consulta(citas.getFecha_consulta());
        citas1.setHora(citas.getHora());
        citas1.setTipoHora(citas.getTipoHora());
        //citas1.setEstado_cita("ESPERA");
        citas1.setEstado(true);
        citas1 = citaRepository.save(citas1);
        return citas1;
    }

    @Override
    public Citas deleteCita(Long id) {
        Citas citas1 = getCitaById(id);
        citas1.setEstado(false);
        citas1.setEstado_cita("CANCELADA");
        return citaRepository.save(citas1);
    }

    @Override
    public List<Citas> listCitasExpediente(Expediente expediente) {
        return citaRepository.findByExpedienteOrderByExpedienteDesc(expediente);
    }

    @Override
    public List<Citas> listCitasBetweenDates(LocalDate fecha_inicio, LocalDate fecha_fin) throws NullPointerException{
        return citaRepository.getAllBetweenDates(fecha_inicio,fecha_fin,"ESPERA");
    }
    
    @Override
    public List<Citas> listCitasBetweenDates2(LocalDate fecha_inicio, LocalDate fecha_fin) throws NullPointerException{
        return citaRepository.getAllBetweenDates(fecha_inicio,fecha_fin,"EJECUTADA");
    }

    private Sort orderByDateAsc(){
        return new Sort(Sort.Direction.ASC,"fecha_consulta");
    }

    @Override
    public Iterable<Citas> getCitasFecha(LocalDate fecha_actual) {
        return citaRepository.findAllByFecha_consulta(fecha_actual);
    }

    @Override
    public List<Integer> countCitas(LocalDate fecha_inicio, LocalDate fecha_fin) {
        return citaRepository.contadorCitas(fecha_inicio,fecha_fin,"EJECUTADA");
    }

	@Override
	public List<Citas> getCitasByFechaActual(LocalDate fecha_actual) {
		
		return citaRepository.findAllByFecha_consultaActual(fecha_actual);
	}

	@Override
	public List<Citas> findByExpediente(Expediente expediente) {
		
		return citaRepository.findByExpediente(expediente);
	}

	@Override
	public List<?> countCitasDay(LocalDate fecha_inicio, LocalDate fecha_fin) {
		
		return citaRepository.contadorCitasDia(fecha_inicio, fecha_fin);
	}
}
