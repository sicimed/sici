package com.sici.serviceImp;

import com.sici.entity.*;

public interface ProductoService {


	public abstract  Producto changeStatusProducto(Producto producto);
    
	Iterable<Producto> listAllProductosActivos();
	
	Iterable<Producto> listAllProductos();
	
	Iterable<Producto> listAllProductosDesactivados();
	
	Iterable<Producto> listAllProductosExistenciaBaja();
    
	public abstract  Producto deleteProducto(Producto producto);

	public abstract Producto addProducto(Producto producto);
	
	public abstract Producto editProducto(Producto producto);
	
	Producto getProductoById(Long id);


}
