package com.sici.serviceImp;

import com.sici.entity.ServiciosOfrecidos;
import com.sici.repository.ServiciosClinicaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("servicioClinicaImp")
public class ServicioServiceImp implements ServicioService {

    @Autowired
    @Qualifier("servicioClinicaRepository")
    private ServiciosClinicaRepository serviciosClinicaRepository;

    @Override
    public ServiciosOfrecidos addServicio(ServiciosOfrecidos serviciosOfrecidos) {
        return serviciosClinicaRepository.save(serviciosOfrecidos);
    }

    @Override
    public ServiciosOfrecidos findById(Long id) {
        return serviciosClinicaRepository.findById(id).orElse(null);
    }

    @Override
    public void deleteById(Long id) {
        serviciosClinicaRepository.deleteById(id);
    }

    @Override
    public List<ServiciosOfrecidos> findAll() {
        return (List<ServiciosOfrecidos>) serviciosClinicaRepository.findAll();
    }
}
