package com.sici.serviceImp;

import com.sici.entity.*;
import com.sici.repository.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;


@Service("recetaService")
public class RecetaServiceImp implements RecetaService {
	
	   @Autowired
	    @Qualifier("recetaRepository")
	    private RecetaRepository recetaRepository;

	
	@Override
	public Iterable<Receta> listAllRecetas() {
		return recetaRepository.findAll();
	}
	
	 @Autowired
	    public Iterable<Receta> listAllRecetasActivas(){
	    	return recetaRepository.findByEstado(true);
	    }
	
	@Override
	public Receta getRecetaById(Long id) {
		return recetaRepository.findById(id).get();
	}
	
	@Override
	public Receta deleteReceta(Receta receta) {
		 receta.setEstado(false);
	     Receta recetaoff = recetaRepository.save(receta);
	     return recetaoff;
	}
	
	@Override
	 public Receta addReceta (Receta receta) {
	
		  receta.setEstado(true);
	      return recetaRepository.save(receta);
	 }

	
	@Override
	   public Receta editReceta(Receta receta){
	       return recetaRepository.save(receta);
	      
	   }

	@Override
	public Receta getRecetaByConsulta(Consulta consulta) {
		
		return recetaRepository.findByConsulta(consulta);
	}
   
	

}
