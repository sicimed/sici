package com.sici.serviceImp;

import com.sici.entity.*;

public interface MarcaService {

    Iterable<Marca> listAllMarca();
    
    Marca getMarcaById(Long id);
    
    Marca saveMarca(Marca marca);
    
    void deleteMarca(Long id);
    
    public abstract Marca editMarca(Marca marca);
 
}
