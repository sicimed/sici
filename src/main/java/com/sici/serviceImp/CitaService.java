package com.sici.serviceImp;

import com.sici.entity.Citas;
import com.sici.entity.Expediente;
import com.sici.model.CitaCountModel;

import java.time.LocalDate;
import java.util.List;

public interface CitaService {

    public abstract Citas addCita(Citas citas);
    public abstract Iterable<Citas> listAllCitasPendiente();
    public abstract Iterable<Citas> listAllCitasEjecutada();
    public abstract Citas getCitaById(Long id);
    public abstract Citas changeStatusCita(Citas citas,String proceso);
    public abstract Citas editCitas(Citas citas,Long id);
    public abstract Citas deleteCita(Long id);
    public abstract List<Citas> listCitasExpediente(Expediente expediente);
    public abstract List<Citas> listCitasBetweenDates(LocalDate fecha_inicio, LocalDate fecha_fin);
    public abstract List<Citas> listCitasBetweenDates2(LocalDate fecha_inicio, LocalDate fecha_fin);
    public abstract  Iterable<Citas> getCitasFecha(LocalDate fecha_actual);
    public abstract List<Integer> countCitas(LocalDate fecha_inicio, LocalDate fecha_fin);
    public abstract List<Citas> getCitasByFechaActual(LocalDate fecha_actual);
    public abstract List<?> countCitasDay(LocalDate fecha_inicio, LocalDate fecha_fin);
    public abstract List<Citas> findByExpediente(Expediente expediente);

}
