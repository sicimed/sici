package com.sici.serviceImp;

import com.sici.entity.ServiciosOfrecidos;

import java.util.List;

public interface ServicioService {
    public abstract ServiciosOfrecidos addServicio(ServiciosOfrecidos serviciosOfrecidos);
    public abstract ServiciosOfrecidos findById(Long id);
    public abstract void deleteById(Long id);
    public abstract List<ServiciosOfrecidos> findAll();
}
