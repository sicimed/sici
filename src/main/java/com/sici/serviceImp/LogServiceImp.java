package com.sici.serviceImp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.sici.entity.Log;
import com.sici.repository.LogRepository;

@Service("logServiceImp")
public class LogServiceImp implements LogService {

	@Autowired
	@Qualifier("logRepository")
	private LogRepository logRepository;

	@Override
	public List<Log> retrieveAllLogs() {
		return logRepository.findAll();
	}

	@Override
	public Log getById(Long id) {
		return logRepository.findById(id).orElse(null);
	}
	
	
}
