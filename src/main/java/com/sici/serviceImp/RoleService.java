package com.sici.serviceImp;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

//import com.sici.entity.User;
import com.sici.entity.Role;
import com.sici.entity.User;
import com.sici.repository.RoleRepository;
import com.sici.repository.UserRepository;

@Service("roleService")
public class RoleService {
	
	@Autowired
	@Qualifier("userRepository")
	private UserRepository userRepository;
	
	@Autowired
	@Qualifier("roleRepository")
	private RoleRepository roleRepository;
	
	public Iterable<Role> listRoles(){
		Iterable<Role> listEntity = roleRepository.findAll();
		return listEntity;
	}
	
	public void registerRole(String name, String details) {
		
		Role roleEntity = new Role(name, details);
		roleRepository.save(roleEntity);
		return;
	}
	//no lo necesito
	public void registerRole(Role role) {
		
		Role roleEntity = roleRepository.findByName(role.getName());
		
		if(roleEntity != null) {
			role.setId(roleEntity.getId());
		}
		roleRepository.save(role);
		return;
	}
	
	public boolean registerRole(Role role, int update) {
		boolean saved = false;
		if(roleRepository.findByName(role.getName())==null || update == 1) {
			//if(update == 1) {
			//	Role role_ = roleRepository.findByName(role.getName());
			//	role.setId(role_.getId());
			//}
			roleRepository.save(role);
			saved = true;
		}
		return saved;
	}
	
	public Role getRole(String name){
		
		Role roleEntity =  roleRepository.findByName(name);
		
		return roleEntity;
	}
	public Role getRole(long id){
		
		Role roleEntity =  roleRepository.findById(id);
		
		return roleEntity;
	}
	
	public Set<Role> getRoles(Set<User> users){
		
		Set<Role> roleEntity =  roleRepository.findByUsers(users);
		
		return roleEntity;
	}
	
	public boolean deleteRole(long id){
		boolean result = false;
		Role role =  roleRepository.findById(id);
		if(role != null) {
			roleRepository.deleteById(id);
			result = true;
		}
		return result;
	}
}
