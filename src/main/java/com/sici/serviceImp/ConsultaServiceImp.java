package com.sici.serviceImp;

import com.sici.entity.*;
import com.sici.repository.ConsultaEditadaRespository;
import com.sici.repository.ConsultaRepository;
import com.sici.repository.DetallePacienteEditadaRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service("consultaService")
public class ConsultaServiceImp implements ConsultaService {

    @Autowired
    @Qualifier("expedienteService")
    private ExpedienteService expedienteService;

    @Autowired
    @Qualifier("consultaRepository")
    private ConsultaRepository consultaRepository;
    
    @Autowired
    private ConsultaEditadaRespository consultaEditadaRepository;

    @Autowired
    @Qualifier("citaServiceImp")
    private CitaService citaService;

    @Autowired
    @Qualifier("historialMedicoService")
    private HistorialMedicoService historialMedicoService;

    @Autowired
    @Qualifier("detallePacienteService")
    private DetallePacienteService detallePacienteService;
    
    @Autowired
    private DetallePacienteEditadaRepository detallePacienteEditadaRepository;

    @Override
    public Consulta addConsulta(Consulta consulta,Long id) {
        Citas citas = citaService.getCitaById(id);
        Expediente expediente = expedienteService.getExpedienteById(citas.getExpediente().getId());
        HistorialMedico historialMedico = historialMedicoService.getHistorialByExpediente(expediente);
        consulta.getDetallePaciente().setPaciente(expediente.getPaciente());
        DetallePaciente detallePaciente = detallePacienteService.addDetallePaciente(consulta.getDetallePaciente());
        consulta.setCita(citas);
        consulta.setHistorialMedico(historialMedico);
        consulta.setDetallePaciente(detallePaciente);
        consulta.setEstado(true);
        consulta.setEstadoRegistro(true);
        Consulta consulta1=consultaRepository.save(consulta);
        citaService.changeStatusCita(citas,"EJECUTADA");
        return consulta1;
    }

    @Override
    public Iterable<Consulta> listConsultas() {
    	
    	//return consultaRepository.findByEstadoRegistro();
        return consultaRepository.findAll(orderByDateAsc());
    }

    private Sort orderByDateAsc(){
        return new Sort(Sort.Direction.DESC,"createdAt");
    }

    @Override
    public Consulta getConsultaById(Long id) {
        return consultaRepository.findById(id);
    }
    
//    @Override
//    public Consulta editConsulta(Consulta consulta, Long id_consulta,Long id_cita) {
//    	
//    	Consulta consultaAdd = addConsulta(consulta,id_cita);
//    	
//        Consulta consultaEdit = getConsultaById(id_consulta);
//        consultaEdit.setEstadoRegistro(false);
//        
//        return consultaRepository.save(consultaEdit);
//    }

    @Override
    public Consulta editConsulta(Consulta consulta, Long id_consulta,Long id_cita) {
        Consulta consulta1 = getConsultaById(id_consulta);
        Citas cita = citaService.getCitaById(id_cita);
        Expediente expediente = expedienteService.getExpedienteById(cita.getExpediente().getId());
        HistorialMedico historialMedico = historialMedicoService.getHistorialByExpediente(expediente);
        
        //GUARDANDO DATOS ANTERIORES A CONSULTA
        DetallePacienteEditada detallePacienteEditada = new DetallePacienteEditada();
        detallePacienteEditada.setPeso(consulta1.getDetallePaciente().getPeso());
        detallePacienteEditada.setTalla(consulta1.getDetallePaciente().getTalla());
        detallePacienteEditada.setPc(consulta1.getDetallePaciente().getPc());
        detallePacienteEditada.setTemperatura(consulta1.getDetallePaciente().getTemperatura());
        detallePacienteEditada.setPresionArterial(consulta1.getDetallePaciente().getPresionArterial());
        detallePacienteEditada.setImc(consulta1.getDetallePaciente().getImc());
        detallePacienteEditada.setFrecuenciaCardiaca(consulta1.getDetallePaciente().getFrecuenciaCardiaca());
        detallePacienteEditada.setFrecuenciaRespiratoria(consulta1.getDetallePaciente().getFrecuenciaRespiratoria());
        detallePacienteEditada.setDescripcion(consulta1.getDetallePaciente().getDescripcion());
        detallePacienteEditada.setPaciente(expediente.getPaciente());
        
        detallePacienteEditada=detallePacienteEditadaRepository.save(detallePacienteEditada);
        
        ConsultaEditada consultaEditada = new ConsultaEditada();
        consultaEditada.setCita(cita);
        consultaEditada.setHistorialMedico(historialMedico);
        consultaEditada.setDiagnostico(consulta1.getDiagnostico());
        consultaEditada.setTratamiento(consulta1.getTratamiento());
        consultaEditada.setComentario(consulta1.getComentario());
        consultaEditada.setDetallePaciente(detallePacienteEditada);
        consultaEditada.setMotivoConsulta(consulta1.getMotivoConsulta());
        consultaEditada.setDescripcionMotivo(consulta1.getDescripcionMotivo());
        consultaEditada.setAntecendentes(consulta1.getAntecendentes());
        consultaEditada.setEstado(true);
        consultaEditada.setExamenes(consulta1.getExamenes());
        consultaEditada.setIdConsulta(consulta1.getId());;
        consultaEditadaRepository.save(consultaEditada);
        
        //EDITANDO CONSULTA ACTUAL
        consulta1.getDetallePaciente().setPaciente(expediente.getPaciente());
        consulta1.getDetallePaciente().setPeso(consulta.getDetallePaciente().getPeso());
        consulta1.getDetallePaciente().setTalla(consulta.getDetallePaciente().getTalla());
        consulta1.getDetallePaciente().setPc(consulta.getDetallePaciente().getPc());
        consulta1.getDetallePaciente().setTemperatura(consulta.getDetallePaciente().getTemperatura());
        consulta1.getDetallePaciente().setPresionArterial(consulta.getDetallePaciente().getPresionArterial());
        consulta1.getDetallePaciente().setImc(consulta.getDetallePaciente().getImc());
        consulta1.getDetallePaciente().setFrecuenciaCardiaca(consulta.getDetallePaciente().getFrecuenciaCardiaca());
        consulta1.getDetallePaciente().setFrecuenciaRespiratoria(consulta.getDetallePaciente().getFrecuenciaRespiratoria());
        consulta1.getDetallePaciente().setDescripcion(consulta.getDetallePaciente().getDescripcion());
        DetallePaciente detallePaciente = detallePacienteService.addDetallePaciente(consulta1.getDetallePaciente());
        
        consulta1.setCita(cita);
        consulta1.setHistorialMedico(historialMedico);
        consulta1.setDetallePaciente(detallePaciente);
        consulta1.setMotivoConsulta(consulta.getMotivoConsulta());
        consulta1.setDiagnostico(consulta.getDiagnostico());
        consulta1.setTratamiento(consulta.getTratamiento());
        consulta1.setComentario(consulta.getComentario());
        consulta1.setDescripcionMotivo(consulta.getDescripcionMotivo());
        consulta1.setAntecendentes(consulta.getAntecendentes());
        consulta1.setExamenes(consulta.getExamenes());
        
        return consultaRepository.save(consulta1);
    }

    @Override
    public Consulta cancelConsulta(Long id) {
        Consulta consulta = getConsultaById(id);
        consulta.setEstado(false);
       return consultaRepository.save(consulta);
    }
}
