package com.sici.serviceImp;

import com.sici.entity.*;
import com.sici.repository.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author YESELIZ
 *
 */
@Service("ingresoService")
public class IngresoService  {
	
    @Autowired
    @Qualifier("ingresoRepository")
    private IngresoRepository ingresoRepository;

    @Autowired
    @Qualifier("ingresoTempRepository")
    private IngresoTempRepository ingresoTempRepository;
    
    @Autowired
    @Qualifier("detalleIngresoTempRepository")
    private DetalleIngresoTempRepository detalleIngresoTempRepository;
    
    @Autowired
    @Qualifier("detalleIngresoRepository")
    private DetalleIngresoRepository detalleIngresoRepository;
    
    @Autowired
    @Qualifier("movimientoRepository")
    private MovimientoRepository movimientoRepository;
    
    @Autowired
	@Qualifier("productoRepository")
	private ProductoRepository productoRepository;  
	   
    @Autowired
    @Qualifier("existenciaRepository")
    private ExistenciaRepository existenciaRepository;
    
    
	public Iterable<Ingreso> listAllIngreso(){
		return ingresoRepository.findAll();
	}
	
	/****  FUNCION: Registra un ingreso nuevo  ****/
	public Ingreso addIngreso(Ingreso ingreso, List<DetalleIngreso> detalles) {
		
		//El ingreso viene sin registrar
		boolean result = true;
	    //Registrar ingreso
		if(ingreso != null ) {			
			ingreso = ingresoRepository.save(ingreso);
			
			//Asignar el id de ingreso al detalle
			if( !detalles.isEmpty() ) {				
				for(DetalleIngreso detIng : detalles) {					
					if(detIng != null) {						
						detIng.setIngresos(ingreso);
						detIng = detalleIngresoRepository.save(detIng);
						
						//Actualizar kardex
						result = updateKardex(detIng);						
					}					
				}			
			}
			
			ingreso = ingresoRepository.findById(ingreso.getId());			
		} else {
			ingreso = null;
		}
		System.out.print("\n\nResultado de ingresoService.addIngreso result: "+result+"\n");
		System.out.print("\n\nResultado de ingresoService.addIngreso [ingreso]: \n"+ingreso.toString()+"\n");
		return ingreso;
	}
	
	/****  FUNCION: Actualiza el kardex  ****/
	public boolean updateKardex(DetalleIngreso detIng) {
		
		boolean result = true;
		Movimiento mov = new Movimiento();
		
		//Detalle de ingreso
		float costoU = detIng.getPrecioCompra();
		Producto prod = detIng.getProductos();
		String desc = detIng.getDescripcion();
		LocalDate vence = detIng.getVence();
		int cant = detIng.getCantidad();
		String lote = detIng.getLote();		
		
		if(cant >= 0 && costoU >= 0 && prod != null) {
			
			// Utilizar un hilo
			
			//Producto
			int invProd = prod.getInventario();
			float costoPromAct = prod.getCostoProm();
			
			//Calculo de saldo de unidades
			int salCantidad = prod.getInventario() + cant;

			//Calculo de CostoPromedio
			float costoProm = 0;
			float salTotal = 0;
			
			//Pendiente Revision de formula
			costoProm = ( (costoPromAct * invProd) + ( cant * costoU ) ) / ( cant + invProd);			
			salTotal = salCantidad * costoProm;
			
			//Registra movimiento de kardex
			mov = new Movimiento(prod,desc,LocalDate.now(),costoU,cant,salCantidad,salTotal);
			mov = movimientoRepository.save(mov);
			
			//Actualiza costo e inventario en producto
			prod.setCostoProm(costoProm);
			prod.setInventario(salCantidad);
			prod = productoRepository.save(prod);			
		   
			//Agrega existencias
			result = addExistencia(prod, cant,lote, vence);			
		}
		else {
			result = false;
		}
		System.out.print("\n\nResultado de ingresoService.updateKardex : "+result+"\n");
		return result;
	}
	
	/****  FUNCION: agrega unidades a un lote o registra unidades de uno nuevo  ****/
	private boolean addExistencia(Producto prod, int cantidad, String lote, LocalDate vence) {
		boolean result = true;
		
		Existencia ex = null;
		try {
			ex = existenciaRepository.findByProductoAndLote(prod.getId(), lote);
			//Agregar existencias a un lote existente o uno nuevo
			if(ex != null) {
				ex.setStock(ex.getStock() + cantidad);				
			}
			else {
				ex = new Existencia(prod,(float)cantidad,lote,vence);
			}
			
			existenciaRepository.save(ex);
			
		}catch(Exception e){
			e.printStackTrace();
			result = false;
		}
		
		System.out.print("\n\nResultado de ingresoService.addExistencia : "+result+"\n");
		
		return result;
	}
	
	/****  FUNCION: Agrega un ingreso de productos temporal  ****/
	@Transactional
	public IngresoTemp addIngresoTemp(IngresoTemp ingreso, List<DetalleIngresoTemp> detalles) {		
		
		if(ingreso != null ) {		
			//Borrar detalles anteriores
			if(!ingreso.getDetalleIngresoList().isEmpty()) { 
				detalleIngresoTempRepository.deleteByIngresos(ingreso.getId());
				ingreso.setDetalleIngresoList(null);
			}
			ingreso = ingresoTempRepository.save(ingreso);	
			
			//Asignar el id de ingreso al detalle
			if( !detalles.isEmpty() ) {				
				for(DetalleIngresoTemp detIng : detalles) {					
					if(detIng != null) {						
						detIng.setIngresos(ingreso);
						detalleIngresoTempRepository.save(detIng);					
					}					
				}			
			}			
			ingreso = ingresoTempRepository.findById(ingreso.getId());			
			System.out.print("\n\nResultado de ingresoService.addIngresoTemp [ingreso]: \n"+ingreso.toString()+"\n");
		} else {
			ingreso = null;
		}
		return ingreso;
	}

	/****  FUNCION: Devuelve la lista de ingresos temporales  ****/
	public List<IngresoTemp> listIngresoTemp() {
		return ingresoTempRepository.findAll();
	}
	
	/****  FUNCION: Devuelve el ingreso temporal solicitado ****/
	public IngresoTemp getIngresoTemp(long id) {
		return ingresoTempRepository.findById(id);
	}
	
	/****  FUNCION: Elimina el ingreso temporal solicitado, devuelve un boolean de resultado****/
	public void dropIngresoTemp(long id) {
		ingresoTempRepository.deleteById(id);
		return;
	}
	
	/****  FUNCION: Limpia el listado de ingresos temporales****/
	public void clearIngresoTemp() {
		ingresoTempRepository.deleteAll();
		return;
	}
	
	public Ingreso convertToIngreso(IngresoTemp temp) {
		return new Ingreso(temp.getTipoComprobante(),temp.getFecha(),temp.getTotalCompra());
	}
	
	public List<DetalleIngreso> convertToDetalleIngreso(List<DetalleIngresoTemp> details){
		List<DetalleIngreso> detIngList = new ArrayList<>();
		Ingreso ingreso = new Ingreso();
		
		for(DetalleIngresoTemp det : details) {
			Producto prod = productoRepository.findById(det.getProductos().getId());
			
			if(prod != null) {       					
				DetalleIngreso detail = new DetalleIngreso(det.getCantidad(),det.getPrecioCompra(),det.getDescripcion(),det.getLote(),det.getVence(), prod,ingreso);   					
				detIngList.add(detail);   					
			}    				
		}
		return detIngList;
	}
}
	
	




