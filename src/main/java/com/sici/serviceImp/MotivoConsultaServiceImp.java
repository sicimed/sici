package com.sici.serviceImp;

import com.sici.entity.MotivoConsulta;
import com.sici.repository.MotivoConsultaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("motivoConsultaService")
public class MotivoConsultaServiceImp implements MotivoConsultaService {

    @Autowired
    @Qualifier("motivoConsultaRepository")
    private MotivoConsultaRepository motivoConsultaRepository;

    @Override
    public MotivoConsulta addMotivo(MotivoConsulta motivoConsulta) {
            return motivoConsultaRepository.save(motivoConsulta);
    }

    @Override
    public Iterable<MotivoConsulta> listMotivoConsulta() {
       // return motivoConsultaRepository.findByEstado(true);
        return motivoConsultaRepository.findAll();
    }

    @Override
    public MotivoConsulta getMotivacionId(Long id) {
        return motivoConsultaRepository.findById(id);
    }

    @Override
    public Iterable<MotivoConsulta> listMotivoConsultaTrue() {
        return motivoConsultaRepository.findByEstado(true);
    }

}
