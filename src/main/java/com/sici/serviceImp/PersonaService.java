package com.sici.serviceImp;

import com.sici.entity.Persona;

public interface PersonaService {

   // public abstract PersonaModel addPersona(PersonaModel personaModel);
    public abstract Persona addPersona(Persona persona);
    public abstract  Persona deletePersona(Persona persona);
    public abstract  Persona changeStatusPersona(Persona persona);
    
    Iterable<Persona> listAllPacientes();
    Iterable<Persona> listAllExpedientes();
    
    Persona getPacienteById(Long id);
    
    public abstract Persona editPersona(Persona persona);

}
