package com.sici.serviceImp;

import com.sici.entity.Clinica;

public interface ClinicaService {

    public abstract Clinica addClinica(Clinica clinica);
    public abstract Clinica getClinicaByCodigo(String codigo);
}
