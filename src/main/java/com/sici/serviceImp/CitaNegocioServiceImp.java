package com.sici.serviceImp;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sici.entity.CitasNegocio;
import com.sici.repository.CitaNegocioRepository;

@Service("citaNegocioServiceImp")
public class CitaNegocioServiceImp implements CitaNegocioService{
	
	@Autowired
	private CitaNegocioRepository citaNegocio;

	@Override
	public CitasNegocio save(CitasNegocio cita) {
		
		return citaNegocio.save(cita);
	}

	@Override
	public List<CitasNegocio> list() {
		
		return (List<CitasNegocio>) citaNegocio.findByEstado(true);
	}

	@Override
	public CitasNegocio getById(Long id) {
		
		return  citaNegocio.findById(id).orElse(null);
	}
	
	

}
