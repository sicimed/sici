package com.sici.serviceImp;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.sici.entity.Permission;
//import com.sici.entity.User;
import com.sici.entity.Role;
import com.sici.repository.PermissionRepository;
import com.sici.repository.RoleRepository;
import com.sici.repository.UserRepository;

@Service("userService")
public class UserService implements UserDetailsService{
	
	@Autowired
	@Qualifier("userRepository")
	private UserRepository userRepository;
	
	@Autowired
	@Qualifier("roleRepository")
	private RoleRepository roleRepository;
	
	@Autowired
	@Qualifier("permissionRepository")
	private PermissionRepository permissionRepository;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException
	{
		com.sici.entity.User user = userRepository.findByUsername(username);
		Set<com.sici.entity.User> users = new HashSet<com.sici.entity.User>();
		users.add(user);
		Set<GrantedAuthority> authorities =  buildAuthorities(roleRepository.findByUsers(users));
		return buildUser(user,authorities);
	}
	
	private User buildUser(com.sici.entity.User user, Set<GrantedAuthority> authorities) {
		return new User(user.getUsername(), user.getPassword(), user.isEnabled(), true, true, true, authorities);
	}
	
	private HashSet<GrantedAuthority> buildAuthorities (Set<Role> roles){
		
		Set<GrantedAuthority> auths = new HashSet<GrantedAuthority>();
		
		Set<Permission> permissList = permissionRepository.findByRolesIn(roles);
		
		if(!permissList.isEmpty()) {
			for(Permission permiss: permissList) {
				auths.add(new SimpleGrantedAuthority(permiss.getName()));
			}	
		}
		
		return (HashSet<GrantedAuthority>) auths;
	}
	
	public Iterable<com.sici.entity.User> listUsers(){
		Iterable<com.sici.entity.User> listEntity = userRepository.findAll();
		return listEntity;
	}
	
	public void registerUser(String username, String email, String password, boolean enabled) {
		
		BCryptPasswordEncoder pe = new BCryptPasswordEncoder();
		password = pe.encode(password);
		com.sici.entity.User userEntity = new com.sici.entity.User(username, email, password, enabled);
		userRepository.save(userEntity);
		return;
	}
	
	public void registerUser(String username, String email, String password, boolean enabled, Set<Role> roles) {
		
		BCryptPasswordEncoder pe = new BCryptPasswordEncoder();
		password = pe.encode(password);
		com.sici.entity.User userEntity = new com.sici.entity.User(username, email, password, enabled, roles);
		userRepository.save(userEntity);
		return;
	}
	public boolean registerUser(com.sici.entity.User user, int update) {
		
		boolean saved = false;
		
		if(userRepository.exists(user.getUsername()) && update == 1) {
			//actualizar porque si existe
			
			BCryptPasswordEncoder pe = new BCryptPasswordEncoder();
			user.setPassword(pe.encode(user.getPassword()));

			try {
				
				com.sici.entity.User usr = userRepository.findByUsername(user.getUsername());
				user.setId(usr.getId());
				
				if(user.getUsername().trim().equalsIgnoreCase("admin")) {user.setEnabled(true);}
				
				userRepository.save(user);
				saved = true;
				
			}catch(Exception e) {
				saved = false;
			}
		}else if(!userRepository.exists(user.getUsername()) && update == 0){
			// Crear nuevo usuario
			
			BCryptPasswordEncoder pe = new BCryptPasswordEncoder();
			user.setPassword(pe.encode(user.getPassword()));

			try {
				
				userRepository.save(user);
				
				saved = true;
				
			}catch(Exception e) {
				saved = false;
			}			
		}else {
			saved = false;
		}
		return saved;
	}
	
	public com.sici.entity.User getUser(String username){
		
		com.sici.entity.User userEntity =  userRepository.findByUsername(username);
		
		return userEntity;
	}
	
	public boolean deleteUser(String username){
		boolean result = false;
		if(username.trim() != "admin" && username.trim() != "user") {
			com.sici.entity.User userEntity =  userRepository.findByUsername(username);
			if(userEntity != null) {
				userRepository.delete(userEntity);
				result = true;
			}
		}
		return result;
	}
}
