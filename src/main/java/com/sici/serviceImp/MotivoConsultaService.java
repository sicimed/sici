package com.sici.serviceImp;

import com.sici.entity.MotivoConsulta;

public interface MotivoConsultaService {
    public abstract MotivoConsulta addMotivo(MotivoConsulta motivoConsulta);
    public abstract Iterable<MotivoConsulta> listMotivoConsulta();
    public abstract MotivoConsulta getMotivacionId(Long id);
    public abstract Iterable<MotivoConsulta> listMotivoConsultaTrue();

}
