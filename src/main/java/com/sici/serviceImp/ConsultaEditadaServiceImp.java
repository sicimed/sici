package com.sici.serviceImp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sici.entity.ConsultaEditada;
import com.sici.repository.ConsultaEditadaRespository;

@Service
public class ConsultaEditadaServiceImp implements ConsultaEditadaService{

	
	@Autowired
	private ConsultaEditadaRespository consultaRepo;
	
	@Override
	public ConsultaEditada save(ConsultaEditada consulta) {
		
		return consultaRepo.save(consulta);
	}

	@Override
	public ConsultaEditada findById(Long id) {
		
		return consultaRepo.findById(id);
	}

	@Override
	public List<ConsultaEditada> listConsultas(Long idConsulta) {
		
		return consultaRepo.findByIdConsulta(idConsulta);
	}


}
