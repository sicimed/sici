package com.sici.serviceImp;

import com.sici.entity.Expediente;

import java.util.List;

public interface ExpedienteService {

    public List<Expediente> findAll();
    public abstract Expediente getExpedienteById(Long id);
}
