package com.sici.serviceImp;

import java.util.List;

import com.sici.entity.CitasNegocio;

public interface CitaNegocioService {

	public abstract CitasNegocio save(CitasNegocio cita);
	public abstract List<CitasNegocio> list();
	public abstract CitasNegocio getById(Long id);
	
	
}
