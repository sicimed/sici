package com.sici.serviceImp;

import com.sici.entity.*;
import com.sici.repository.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;


/**
 * @author HECTOR
 *
 */
@Service("movimientoService")
public class MovimientoService {

    @Autowired
    @Qualifier("movimientoRepository")
    private MovimientoRepository movimientoRepository;

	@Autowired
	@Qualifier("productoRepository")
	private ProductoRepository productoRepository;
	
	public Iterable<Movimiento> findAll(){
		return movimientoRepository.findAll();
	}
	
	public Movimiento findById(Long id) {
		return movimientoRepository.findById(id);
	}	

    public void deleteById(Long id) {
        movimientoRepository.deleteById(id);
    }
	
	public Movimiento save(Movimiento mov) {
	    return movimientoRepository.save(mov);
	}
	
	public List<Movimiento> filter(Long idProd,LocalDate fini, LocalDate fend){
		
		List<Movimiento> list = new ArrayList<Movimiento>();

	    try {
    	
		    if(idProd > 0 ) {
		    	Producto prod = productoRepository.findById(idProd);
		    	
		        if(fini != null) {		        			
		        	list = fend != null ? movimientoRepository.findByProductoAndFechaAfterAndFechaBefore(prod,fini,fend) : movimientoRepository.findByProductoAndFechaAfter(prod,fini);
		        }else {
		        	list = fend != null ? movimientoRepository.findByProductoAndFechaBefore(prod,fend) : movimientoRepository.findByProducto(prod);  			
		        }
		    }else {
		    	if(fini != null) {
		        	list = fend != null ? movimientoRepository.findByFechaAfterAndFechaBefore(fini,fend) : movimientoRepository.findByFechaAfter(fini);
		        }else {
		        	list = fend != null ? movimientoRepository.findByFechaBefore(fend) : null;  			
		        }    		
		    }		    	
	    	
	    }catch(Exception e) {
	    	e.printStackTrace();
	    }

		return list;
	}
}
	
	




