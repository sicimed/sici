package com.sici.serviceImp;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.sici.entity.Producto;
import com.sici.repository.ProductoRepository;

@Service("productoServiceImp")
public class ProductoServiceImp implements ProductoService {

    @Autowired
    @Qualifier("productoRepository")
    private ProductoRepository productoRepository;

 

	@Override
	public Iterable<Producto> listAllProductos() {
		return productoRepository.findAll();
	}
	
	 @Autowired
	    public Iterable<Producto> listAllProductosActivos(){
	    	return productoRepository.findByEstado(true);
	    }
	 
	 @Autowired
	    public Iterable<Producto> listAllProductosDesactivados(){
	    	return productoRepository.findByEstado(false);
	    }
	 
	 @Autowired
	    public Iterable<Producto> listAllProductosExistenciaBaja(){
		 return productoRepository.findByEstado(true);
	  
	 }

	@Override
	public Producto getProductoById(Long id) {
		return productoRepository.findById(id);
	}
	
	
	public Producto changeStatusProducto (Producto producto) { 
	Producto change;

	 if(producto.isEstado()==true) { 
		 producto.setEstado(false); 
		 
	change = productoRepository.save(producto); 
	}else{ 
		producto.setEstado(true);
	  
	  change = productoRepository.save(producto); 
	  } 
	 return change; 
	 }


	@Override
	public Producto deleteProducto(Producto producto) {
		 producto.setEstado(false);
	     Producto productoff = productoRepository.save(producto);

	     return productoff;
	}
	
	@Override
	 public Producto addProducto (Producto producto) {
		        producto.setEstado(true);
	            return productoRepository.save(producto);
	 }

	@Override
	public Producto editProducto(Producto producto) {
		return productoRepository.save(producto);
	}
	
}
