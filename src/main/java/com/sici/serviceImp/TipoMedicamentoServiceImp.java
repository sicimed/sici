package com.sici.serviceImp;

import com.sici.entity.*;
import com.sici.repository.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;


/**
 * @author YESELIZ
 *
 */
@Service("tipomedicamentoServiceImp")
public class TipoMedicamentoServiceImp implements TipoMedicamentoService {

    @Autowired
    @Qualifier("tipomedicamentoRepository")
    private TipoMedicamentoRepository tipomedicamentoRepository;

 

	@Override
	public Iterable<TipoMedicamento> listAllTiposMedicamento(){
		return tipomedicamentoRepository.findAll();
	
	}
	
	@Override
	public TipoMedicamento getTipoMedicamentoById(Long id) {
		return tipomedicamentoRepository.findById(id).get();
	}	

	@Override
    public void deleteTipoMedicamento(Long id) {
        tipomedicamentoRepository.deleteById(id);
    }
	
	 @Override
	    public TipoMedicamento saveTipoMedicamento(TipoMedicamento tipomedicamento) {
	        return tipomedicamentoRepository.save(tipomedicamento);
	    }
	 
	 @Override 
	 public TipoMedicamento editTipomedicamento(TipoMedicamento tipomedicamento){ 
		 return tipomedicamentoRepository.save(tipomedicamento);
			  
	 }
	 
	
	
	



}
