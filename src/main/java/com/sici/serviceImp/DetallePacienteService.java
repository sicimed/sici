package com.sici.serviceImp;

import com.sici.entity.DetallePaciente;
import com.sici.entity.Paciente;

import java.util.List;

public interface DetallePacienteService {

    public DetallePaciente addDetallePaciente(DetallePaciente detallePaciente);
    public DetallePaciente getDetalleById(Long id);
    public List<DetallePaciente> latestDetallePaciente(Paciente paciente);
    public DetallePaciente getLastPaciente(Paciente paciente);
}
