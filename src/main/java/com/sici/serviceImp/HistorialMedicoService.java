package com.sici.serviceImp;


import com.sici.entity.Expediente;
import com.sici.entity.HistorialMedico;

import java.util.List;

public interface HistorialMedicoService {

    public abstract void addHistorialMedico(HistorialMedico historialMedico);
    public abstract HistorialMedico getHistorialById(Long id);
    public abstract HistorialMedico getHistorialByExpediente(Expediente expediente);
    public abstract List<HistorialMedico> findAll();

}

