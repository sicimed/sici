package com.sici.serviceImp;

import java.util.List;

import com.sici.entity.Log;

public interface LogService {
	
	public List<Log> retrieveAllLogs();
	
	public Log getById(Long id);

}
