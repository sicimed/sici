package com.sici.model;

public class AjusteExistenciaModel {
	
	private long idEx;
	private int cant;
	private String desc;
	
	public AjusteExistenciaModel(long idEx, int cant, String desc) {
		super();
		this.idEx = idEx;
		this.cant = cant;
		this.desc = desc;
	}

	public long getIdEx() {
		return idEx;
	}

	public void setIdEx(long idEx) {
		this.idEx = idEx;
	}

	public int getCant() {
		return cant;
	}

	public void setCant(int cant) {
		this.cant = cant;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	@Override
	public String toString() {
		return "ExistenciaModel [idEx=" + idEx + ", cant=" + cant + ", desc=" + desc + "]";
	}
}
