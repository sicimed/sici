package com.sici.model;

import javax.persistence.*;

import java.time.LocalDate;
import java.util.Date;

public class DetalleIngresoModel{
    private Long idProd;
    private String desc;
    private int cant;
    private float cost;
    private String lote;
    private String vence;
	public DetalleIngresoModel() {
		super();
	}

	public DetalleIngresoModel(Long idProd, String desc, int cant, float cost, String lote, String vence) {
		super();
		this.idProd = idProd;
		this.desc = desc;
		this.cant = cant;
		this.cost = cost;
		this.lote = lote;
		this.vence = vence;
	}


	public Long getIdProd() {
		return idProd;
	}
	public void setIdProd(Long idProd) {
		this.idProd = idProd;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public int getCant() {
		return cant;
	}
	public void setCant(int cant) {
		this.cant = cant;
	}
	public float getCost() {
		return cost;
	}
	public void setCost(float cost) {
		this.cost = cost;
	}
	public String getLote() {
		return lote;
	}
	public void setLote(String lote) {
		this.lote = lote;
	}
	public String getVence() {
		return vence;
	}
	public void setVence(String vence) {
		this.vence = vence;
	}
	@Override
	public String toString() {
		return "DetalleIngresoModel [idProd=" + idProd + ", desc=" + desc + ", cant=" + cant + ", cost=" + cost
				+ ", lote=" + lote + ", vence=" + vence + "]";
	}
    
}


