package com.sici.model;

public class ResponseRest {
	
	private String status;
	private Object data;
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	public ResponseRest(String status, Object data) {
		super();
		this.status = status;
		this.data = data;
	}
	  
}
