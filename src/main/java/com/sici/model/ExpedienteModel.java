package com.sici.model;

import com.sici.entity.Paciente;
import java.util.Date;

public class ExpedienteModel {


    private int id;
    private Date fecha;
    private String codigo;
    private boolean estado;
    private Paciente paciente;

    public int getId() {
        return id;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public boolean getEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }

    public ExpedienteModel(int id, Date fecha, String codigo, boolean estado, Paciente paciente) {
        this.id = id;
        this.fecha = fecha;
        this.codigo = codigo;
        this.estado = estado;
        this.paciente = paciente;
    }

    public ExpedienteModel(){}
}
