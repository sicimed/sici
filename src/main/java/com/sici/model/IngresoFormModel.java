package com.sici.model;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

public class IngresoFormModel{

    private String tipoComprobante;
    private String fecha;
    private float totalCompra;
    private List<DetalleIngresoModel> details;
	public IngresoFormModel() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getTipoComprobante() {
		return tipoComprobante;
	}
	public void setTipoComprobante(String tipoComprobante) {
		this.tipoComprobante = tipoComprobante;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fechaHora) {
		this.fecha = fechaHora;
	}
	public float getTotalCompra() {
		return totalCompra;
	}
	public void setTotalCompra(float totalCompra) {
		this.totalCompra = totalCompra;
	}
	public List<DetalleIngresoModel> getDetails() {
		return details;
	}
	public void setDetails(List<DetalleIngresoModel> details) {
		this.details = details;
	}
	@Override
	public String toString() {
		return "IngresoFormModel [tipoComprobante=" + tipoComprobante + ", fechaHora=" + fecha + ", totalCompra="
				+ totalCompra + ", details=" + details + "]";
	}
    
}
