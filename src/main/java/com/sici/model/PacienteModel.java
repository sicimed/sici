package com.sici.model;

import com.sici.entity.Expediente;
import com.sici.entity.Persona;

public class PacienteModel {

    private int id;
    private String codigo;
    private Persona persona;
    private Expediente expediente;

    public int getId() {
        return id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public Expediente getExpediente() {
        return expediente;
    }

    public void setExpediente(Expediente expediente) {
        this.expediente = expediente;
    }

    public PacienteModel(int id, String codigo, Persona persona, Expediente expediente) {
        this.id = id;
        this.codigo = codigo;
        this.persona = persona;
        this.expediente = expediente;
    }

    public PacienteModel(){}
}
