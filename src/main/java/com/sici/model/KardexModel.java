package com.sici.model;

import java.time.LocalDate;
import com.sici.entity.Producto;

public class KardexModel {

    private Long id;
    private Producto producto;
    private String concepto;
    private LocalDate fecha;
    private float costoE;
    private int cantE;
    private float montoE;
    private float costoS;
    private int cantS;
    private float montoS; 
    private float costoProm;
    private int cantEx;
    private float totalEx;
    
    public KardexModel() { }
    
	public KardexModel(Long id, Producto producto, String concepto, LocalDate fecha, float costoE, int cantE,
			float montoE, float costoS, int cantS, float montoS, float costoProm, int cantEx, float totalEx) {
		super();
		this.id = id;
		this.producto = producto;
		this.concepto = concepto;
		this.fecha = fecha;
		this.costoE = costoE;
		this.cantE = cantE;
		this.montoE = montoE;
		this.costoS = costoS;
		this.cantS = cantS;
		this.montoS = montoS;
		this.costoProm = costoProm;
		this.cantEx = cantEx;
		this.totalEx = totalEx;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public String getConcepto() {
		return concepto;
	}

	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public float getCostoE() {
		return costoE;
	}

	public void setCostoE(float costoE) {
		this.costoE = costoE;
	}

	public int getCantE() {
		return cantE;
	}

	public void setCantE(int cantE) {
		this.cantE = cantE;
	}

	public float getMontoE() {
		return montoE;
	}

	public void setMontoE(float montoE) {
		this.montoE = montoE;
	}

	public float getCostoS() {
		return costoS;
	}

	public void setCostoS(float costoS) {
		this.costoS = costoS;
	}

	public int getCantS() {
		return cantS;
	}

	public void setCantS(int cantS) {
		this.cantS = cantS;
	}

	public float getMontoS() {
		return montoS;
	}

	public void setMontoS(float montoS) {
		this.montoS = montoS;
	}

	public float getCostoProm() {
		return costoProm;
	}

	public void setCostoProm(float costoProm) {
		this.costoProm = costoProm;
	}

	public int getCantEx() {
		return cantEx;
	}

	public void setCantEx(int cantEx) {
		this.cantEx = cantEx;
	}

	public float getTotalEx() {
		return totalEx;
	}

	public void setTotalEx(float totalEx) {
		this.totalEx = totalEx;
	}

	@Override
	public String toString() {
		return "KardexModel [id=" + id + ", producto=" + producto + ", concepto=" + concepto + ", fecha=" + fecha
				+ ", costoE=" + costoE + ", cantE=" + cantE + ", montoE=" + montoE + ", costoS=" + costoS + ", cantS="
				+ cantS + ", montoS=" + montoS + ", costoProm=" + costoProm + ", cantEx=" + cantEx + ", totalEx="
				+ totalEx + "]";
	}	
}
