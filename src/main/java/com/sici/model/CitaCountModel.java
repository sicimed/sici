package com.sici.model;

import java.util.Date;

public class CitaCountModel {

    Date fecha_consulta;
    Integer cantidad;

	public Date getFecha_consulta() {
		return fecha_consulta;
	}

	public void setFecha_consulta(Date fecha_consulta) {
		this.fecha_consulta = fecha_consulta;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	
}
