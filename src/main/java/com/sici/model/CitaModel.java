package com.sici.model;

import java.time.LocalDate;

public class CitaModel {
	
	private String expediente;
	private LocalDate fecha_consulta;
	private String hora;
	private String tipo_hora;
	private String nombre_paciente;
	private String sexo;
	
	public String getExpediente() {
		return expediente;
	}
	public void setExpediente(String expediente) {
		this.expediente = expediente;
	}
	public LocalDate getFecha_consulta() {
		return fecha_consulta;
	}
	public void setFecha_consulta(LocalDate fecha_consulta) {
		this.fecha_consulta = fecha_consulta;
	}
	public String getHora() {
		return hora;
	}
	public void setHora(String hora) {
		this.hora = hora;
	}
	public String getTipo_hora() {
		return tipo_hora;
	}
	public void setTipo_hora(String tipo_hora) {
		this.tipo_hora = tipo_hora;
	}
	public String getNombre_paciente() {
		return nombre_paciente;
	}
	public void setNombre_paciente(String nombre_paciente) {
		this.nombre_paciente = nombre_paciente;
	}
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	
	

}
