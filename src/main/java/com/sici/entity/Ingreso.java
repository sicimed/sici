package com.sici.entity;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name="Ingreso")
public class Ingreso extends AuditModel{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idIngreso")
    private Long id;

    @NotEmpty
    @Column(name = "tipoComprobante")
    private String tipoComprobante;
    
    
    @Column(name = "fecha")
    private LocalDate fecha;
    
    @Column(name = "totalCompra")
    private float totalCompra;
    
    @OneToMany(targetEntity = DetalleIngreso.class, mappedBy = "ingresos", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<DetalleIngreso> detalleIngresoList;

    
	public Ingreso() {
		super();
	}
	public Ingreso(String tipoComprobante, LocalDate fecha, float totalCompra) {
		super();
		this.tipoComprobante = tipoComprobante;
		this.fecha = fecha;
		this.totalCompra = totalCompra;
		this.detalleIngresoList = new ArrayList<DetalleIngreso>();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTipoComprobante() {
		return tipoComprobante;
	}

	public void setTipoComprobante(String tipoComprobante) {
		this.tipoComprobante = tipoComprobante;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fechaHora) {
		this.fecha = fechaHora;
	}

	public float getTotalCompra() {
		return totalCompra;
	}

	public void setTotalCompra(float totalCompra) {
		this.totalCompra = totalCompra;
	}
	

	public List<DetalleIngreso> getDetalleIngresoList() {
		return detalleIngresoList;
	}

	public void setDetalleIngresoList(List<DetalleIngreso> detalleIngresoList) {
		this.detalleIngresoList = detalleIngresoList;
	}


	@Override
	public String toString() {
		return "Ingreso [id=" + id + ", tipoComprobante=" + tipoComprobante + ", fecha=" + fecha + ", totalCompra="
				+ totalCompra + ", detalleIngresoList=" + detalleIngresoList + "]";
	}
    
    
}
