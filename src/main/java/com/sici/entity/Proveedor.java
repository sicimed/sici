package com.sici.entity;


import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

import java.util.List;

@Entity
@Table(name="proveedor")
public class Proveedor extends AuditModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "idProveedor")
    private Long id;

    @NotEmpty
    @Column(name = "nombre",length = 300)
    private String nombre;
    
    
    @Column(name = "email",length = 250)
    private String email;
    
    @Column(name = "telefono")
    private String telefono;
    
    @Column(name = "direccion",length = 600)
    private String direccion;
   
    
    @OneToMany(targetEntity = Producto.class, mappedBy = "proveedores",cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Producto> productoList;

	public Long getId() {
		return id;
	}
	

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public List<Producto> getProductoList() {
		return productoList;
	}

	public void setProductoList(List<Producto> productoList) {
		this.productoList = productoList;
	}

	public Proveedor() {
		
	}
		

	
	
	


    
	
}