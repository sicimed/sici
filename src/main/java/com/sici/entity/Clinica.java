package com.sici.entity;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@Entity
@Table(name="clinica")
public class Clinica extends AuditModel{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="clinica_id")
    private Long id;

    @NotEmpty
    @Column(name="nombre_clinica")
    private String nombre;

    @NotEmpty
    @Column(name="codigo_clinica")
    private String codigo;

    @Column(name="estado")
    private Boolean estado;

    @OneToMany(targetEntity = Empleado.class, mappedBy = "clinica", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Empleado> empleadoList;

    @OneToMany(targetEntity = Paciente.class, mappedBy = "clinica",cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Paciente> pacienteList;

    @OneToMany(targetEntity = CitasNegocio.class, mappedBy = "clinica",cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private List<CitasNegocio> citasNegocioList;

    @OneToMany(targetEntity = ServiciosOfrecidos.class,mappedBy = "clinica", cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private List<ServiciosOfrecidos> serviciosOfrecidosList;

    public List<Empleado> getEmpleadoList() {
        return empleadoList;
    }

    public void setEmpleadoList(List<Empleado> empleadoList) {
        this.empleadoList = empleadoList;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public Clinica(@NotEmpty String nombre, @NotEmpty String codigo, Boolean estado) {
        this.nombre = nombre;
        this.codigo = codigo;
        this.estado = estado;
    }

    public Clinica(){}
}


