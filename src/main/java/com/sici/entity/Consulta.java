package com.sici.entity;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;

@Entity
@Table(name="consulta")
public class Consulta extends AuditModel{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="consulta_id")
    private Long id;

    @NotNull
    @OneToOne(fetch = FetchType.LAZY,optional = false)
    @JoinColumn(name = "citas_id")
    private Citas cita;
    
    @JsonIgnore
    @OneToOne(mappedBy = "consulta")
    private Receta receta;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY,optional = false)
    @JoinColumn(name="hm_id")
    private HistorialMedico historialMedico;

    @NotEmpty
    @Column(name="diagnostico",length = 6000)
    private String diagnostico;

    @NotEmpty
    @Column(name = "tratamiento",length = 6000)
    private String tratamiento;

    @Column(name = "comentario",length = 6000)
    private String comentario;

    @OneToMany(targetEntity = ExamenesDetalle.class, mappedBy = "consulta", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<ExamenesDetalle> examenesDetalleList;

    @NotNull
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="dpaciente_id")
    private DetallePaciente detallePaciente;

    @NotEmpty
    @Column(name="motivo_consulta",length = 6000)
    private String motivoConsulta;

    @NotEmpty
    @Column(name = "descripcion_motivo",length = 6000)
    private String descripcionMotivo;

    @Column(name = "antecedentes",length = 6000)
    private String antecendentes;
    
    @Column(name="examenes", length = 6000)
    private String examenes;

    @Column(name="estado")
    private boolean estado;
    
    @Column(name="estado_registro")
    private boolean estadoRegistro;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Citas getCita() {
        return cita;
    }

    public void setCita(Citas cita) {
        this.cita = cita;
    }

    public HistorialMedico getHistorialMedico() {
        return historialMedico;
    }

    public void setHistorialMedico(HistorialMedico historialMedico) {
        this.historialMedico = historialMedico;
    }

    public String getDiagnostico() {
        return diagnostico;
    }

    public void setDiagnostico(String diagnostico) {
        this.diagnostico = diagnostico;
    }


    public List<ExamenesDetalle> getExamenesDetalleList() {
        return examenesDetalleList;
    }

    public void setExamenesDetalleList(List<ExamenesDetalle> examenesDetalleList) {
        this.examenesDetalleList = examenesDetalleList;
    }

    public String getMotivoConsulta() {
        return motivoConsulta;
    }

    public void setMotivoConsulta(String motivoConsulta) {
        this.motivoConsulta = motivoConsulta;
    }

    public DetallePaciente getDetallePaciente() {
        return detallePaciente;
    }

    public void setDetallePaciente(DetallePaciente detallePaciente) {
        this.detallePaciente = detallePaciente;
    }

    public String getTratamiento() {
        return tratamiento;
    }

    public void setTratamiento(String tratamiento) {
        this.tratamiento = tratamiento;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public String getDescripcionMotivo() {
        return descripcionMotivo;
    }

    public void setDescripcionMotivo(String descripcionMotivo) {
        this.descripcionMotivo = descripcionMotivo;
    }

    public String getAntecendentes() {
        return antecendentes;
    }

    public void setAntecendentes(String antecendentes) {
        this.antecendentes = antecendentes;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }
    

    public boolean isEstadoRegistro() {
		return estadoRegistro;
	}

	public void setEstadoRegistro(boolean estadoRegistro) {
		this.estadoRegistro = estadoRegistro;
	}
	

	public Receta getReceta() {
		return receta;
	}

	public void setReceta(Receta receta) {
		this.receta = receta;
	}
	
	public String getExamenes() {
		return examenes;
	}

	public void setExamenes(String examenes) {
		this.examenes = examenes;
	}

	public Consulta(@NotNull Citas cita, @NotNull HistorialMedico historialMedico, @NotEmpty String diagnostico, @NotNull DetallePaciente detallePaciente, String motivoConsulta,boolean estado,
			boolean estadoRegistro) {
        this.cita = cita;
        this.historialMedico = historialMedico;
        this.diagnostico = diagnostico;
        this.detallePaciente = detallePaciente;
        this.motivoConsulta = motivoConsulta;
        this.estado=estado;
        this.estadoRegistro = estadoRegistro;
    }

    public Consulta(){}
}
