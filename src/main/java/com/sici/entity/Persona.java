package com.sici.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Entity
@Table(name="Persona")
public class Persona extends AuditModel {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="persona_id")
    private Long id;

    @NotNull
    @Column(name="edad")
    private String edad;

    @NotEmpty
    @Column(name="nombre", length = 200)
    private String nombre;

    @NotEmpty
    @Column(name = "apellido", length = 200)
    private String apellido;

    @Column(name="fecha_nacimiento")
    @DateTimeFormat(pattern = "MM/dd/yyyy")
    private LocalDate fechaNacimiento;

    @NotEmpty
    @Column(name="sexo", length = 10)
    private String sexo;

    @Column(name="telefono", length = 15)
    private String telefono;

    @NotEmpty
    @Column(name="direccion", length = 600)
    private String direccion;

    @NotEmpty
    @Column(name="colonia", length = 500)
    private String colonia;

    @Column(name = "comentario",length = 600)
    private String comentario;

    @NotNull
    @Column(name="estado")
    private boolean estado;
    
    @Column(name = "tipo")
    private String tipo;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public String getEdad() {
        return edad;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }

    public String getColonia() {
        return colonia;
    }

    public void setColonia(String colonia) {
        this.colonia = colonia;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy/MM/dd", timezone = "UTC")
    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(LocalDate fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    @OneToOne(mappedBy = "persona")
    private Paciente paciente;

    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }
    

    public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Persona(@NotEmpty String nombre, @NotEmpty String apellido, LocalDate fechaNacimiento, @NotEmpty String sexo, @NotNull String edad, String telefono, @NotEmpty String direccion, @NotEmpty String colonia, String comentario, @NotNull boolean estado,String tipo) {
        this.edad = edad;
        this.nombre = nombre;
        this.apellido = apellido;
        this.fechaNacimiento = fechaNacimiento;
        this.sexo = sexo;
        this.telefono = telefono;
        this.direccion = direccion;
        this.colonia = colonia;
        this.comentario = comentario;
        this.estado = estado;
        this.tipo = tipo;
    }

    public Persona(){}
}
