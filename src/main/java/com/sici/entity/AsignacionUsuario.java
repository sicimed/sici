package com.sici.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="asignacion_usuario")
public class AsignacionUsuario extends AuditModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "asignacion_usuario_id")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    public User usuario;
   
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "empleado_id")
    public Empleado empleado;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUsuario() {
        return usuario;
    }

    public void setUsuario(User usuario) {
        this.usuario = usuario;
    }

    public Empleado getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }

    public AsignacionUsuario(@NotNull User usuario) {
        this.usuario = usuario;
    }

    public AsignacionUsuario(){

    }
}
