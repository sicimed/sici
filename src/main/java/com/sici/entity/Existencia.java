package com.sici.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import java.time.LocalDate;

@Entity
@Table(name="existencia")
public class Existencia extends AuditModel{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="existencia_id")
    private Long id;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY,optional = false)
    @JoinColumn(name="producto_id")
    private Producto producto;

    @Column(name="stock")
    private float stock;

    @Column(name = "lote")
    private String lote;

    @Column(name = "vence")
    private LocalDate vence;

	public Existencia(@NotNull Producto producto, float stock, String lote, LocalDate vence) {
		super();
		this.producto = producto;
		this.stock = stock;
		this.lote = lote;
		this.vence = vence;
	}

	public Existencia() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public float getStock() {
		return stock;
	}

	public void setStock(float stock) {
		this.stock = stock;
	}

	public String getLote() {
		return lote;
	}

	public void setLote(String lote) {
		this.lote = lote;
	}

	public LocalDate getVence() {
		return vence;
	}

	public void setVence(LocalDate vence) {
		this.vence = vence;
	}

	@Override
	public String toString() {
		return "Existencia [id=" + id + ", producto=" + producto + ", stock=" + stock + ", lote=" + lote + ", vence="
				+ vence + "]";
	}
    
}
