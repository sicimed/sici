package com.sici.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Entity
@Table(name="citas")
public class Citas extends AuditModel{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="citas_id")
    private Long id;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY,optional = false)
    @JoinColumn(name="expediente_id")
    private Expediente expediente;

    @OneToOne(mappedBy = "cita")
    private Consulta consulta;

    @NotNull
    @Column(name="fecha_consulta")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate fecha_consulta;

    @Column(name = "hora")
    private String hora;

    @Column(name = "tipo_hora")
    private String tipoHora;

    @NotNull
    @Column(name = "estado_cita")
    private String proceso;

    @Column(name = "estado")
    private Boolean estado;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Expediente getExpediente() {
        return expediente;
    }

    public void setExpediente(Expediente expediente) {
        this.expediente = expediente;
    }

    public Consulta getConsulta() {
        return consulta;
    }

    public void setConsulta(Consulta consulta) {
        this.consulta = consulta;
    }


    public LocalDate getFecha_consulta() {
        return fecha_consulta;
    }

    public void setFecha_consulta(LocalDate fecha_consulta) {
        this.fecha_consulta = fecha_consulta;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getTipoHora() {
        return tipoHora;
    }

    public void setTipoHora(String tipoHora) {
        this.tipoHora = tipoHora;
    }

    public String getEstado_cita() {
        return proceso;
    }

    public void setEstado_cita(String estado_cita) {
        this.proceso = estado_cita;
    }

    public Citas(Expediente expediente, @NotNull LocalDate fecha_consulta, String hora, String tipoHora, @NotNull String estado_cita, Boolean estado) {
        this.expediente = expediente;
        this.fecha_consulta = fecha_consulta;
        this.hora = hora;
        this.tipoHora = tipoHora;
        this.proceso = estado_cita;
        this.estado = estado;
    }

    public Citas(){}
}
