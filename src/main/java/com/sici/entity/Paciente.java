package com.sici.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Table(name="Paciente")
public class Paciente extends AuditModel{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="paciente_id")
    private Long id;

    @NotEmpty
    @Column(name="codigo", length = 100)
    private String codigo;

    @JsonIgnore
    @NotNull
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="persona_id")
    private Persona persona;

    @JsonIgnore
    @OneToOne(mappedBy = "paciente")
    private Expediente expediente;

    @ManyToOne(fetch = FetchType.LAZY,optional = false)
    @JoinColumn(name="clinica_id")
    private Clinica clinica;

    @OneToMany(targetEntity = DetallePaciente.class,mappedBy = "paciente", cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private List<DetallePaciente> detallePacienteList;

    public Expediente getExpediente() {
        return expediente;
    }

    public void setExpediente(Expediente expediente) {
        this.expediente = expediente;
    }

    public Long getId() {
        return id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public Clinica getClinica() {
        return clinica;
    }

    public void setClinica(Clinica clinica) {
        this.clinica = clinica;
    }

    public List<DetallePaciente> getDetallePacienteList() {
        return detallePacienteList;
    }

    public void setDetallePacienteList(List<DetallePaciente> detallePacienteList) {
        this.detallePacienteList = detallePacienteList;
    }

    public Paciente(@NotNull String codigo, @NotNull Persona persona, Clinica clinica) {
        this.codigo = codigo;
        this.persona = persona;
        this.clinica = clinica;
    }

    public Paciente(){}
}
