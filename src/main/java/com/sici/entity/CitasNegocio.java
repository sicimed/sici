package com.sici.entity;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import java.sql.Time;
import java.time.LocalDate;
import java.util.Date;

@Entity
@Table(name="citas_negocio")
public class CitasNegocio extends AuditModel{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="cnegocio_id")
    private Long id;

    @NotNull
    @Column(name="fecha_cita")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate fecha;

    @NotNull
    @Column(name="hora_cita")
    private String hora;

    @NotEmpty
    @Column(name="tipo_hora")
    private String tipoHora;

    @NotEmpty
    @Column(name="nombre")
    private String nombre;

    @NotEmpty
    @Column(name="descripcion")
    private String descripcion;

    @ManyToOne(fetch = FetchType.LAZY,optional = false)
    @JoinColumn(name="clinica_id")
    private Clinica clinica;

    @Column(name = "estado_cita")
    private String proceso;
    
    @Column(name = "estado")
    private boolean estado = true;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public String getTipoHora() {
        return tipoHora;
    }

    public void setTipoHora(String tipoHora) {
        this.tipoHora = tipoHora;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }
    
//    public CitasNegocio(@NotNull Date fecha, @NotNull Time hora, @NotEmpty String tipoHora, @NotEmpty String nombre, @NotEmpty String descripcion, boolean estado) {
//        this.fecha = fecha;
//        this.hora = hora;
//        this.tipoHora = tipoHora;
//        this.nombre = nombre;
//        this.descripcion = descripcion;
//        this.estado = estado;
//    }

    public String getProceso() {
		return proceso;
	}

	public void setProceso(String proceso) {
		this.proceso = proceso;
	}
	

	public Clinica getClinica() {
		return clinica;
	}

	public void setClinica(Clinica clinica) {
		this.clinica = clinica;
	}

	public CitasNegocio(){}
}
