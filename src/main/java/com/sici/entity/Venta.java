package com.sici.entity;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

import java.time.LocalDateTime;
import java.util.List;


@Entity
@Table(name="venta")
public class Venta extends AuditModel  {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "idVenta")
    private Long id;
	
    @NotEmpty
    @Column(name = "tipoComprobante")
	private String tipoComprobante;

	@Column(name="fechaHora")
	private LocalDateTime fechaHora;
   
    @NotEmpty
    @Column(name = "totalVenta")
    private Double precioVenta;
    
    @OneToMany(targetEntity = DetalleVenta.class, mappedBy = "ventas",cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<DetalleVenta> detalleVentaList;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTipoComprobante() {
		return tipoComprobante;
	}

	public void setTipoComprobante(String tipoComprobante) {
		this.tipoComprobante = tipoComprobante;
	}

	public LocalDateTime getFechaHora() {
		return fechaHora;
	}

	public void setFechaHora(LocalDateTime fechaHora) {
		this.fechaHora = fechaHora;
	}

	public Double getPrecioVenta() {
		return precioVenta;
	}

	public void setPrecioVenta(Double precioVenta) {
		this.precioVenta = precioVenta;
	}

	public List<DetalleVenta> getDetalleVentaList() {
		return detalleVentaList;
	}

	public void setDetalleVentaList(List<DetalleVenta> detalleVentaList) {
		this.detalleVentaList = detalleVentaList;
	}

	public Venta() {
		
		
	}

	

    

    
}


