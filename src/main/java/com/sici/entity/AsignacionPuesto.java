package com.sici.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="asignacion_puesto")
public class AsignacionPuesto extends AuditModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "asignacion_puesto_id")
    private Long id;

  
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "puesto_id")
    public Puesto puesto;

   
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "empleado_id")
    public Empleado empleado;

    @Column(name="estado")
    private Boolean estado;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Puesto getPuesto() {
        return puesto;
    }

    public void setPuesto(Puesto puesto) {
        this.puesto = puesto;
    }

    public Empleado getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }
    
    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public AsignacionPuesto(@NotNull Puesto puesto, Boolean estado) {
        this.puesto = puesto;
        this.estado = estado;
    }

    public AsignacionPuesto(){

    }
}
