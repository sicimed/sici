package com.sici.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.sici.entity.User;


@Entity
@Table(name = "roles")
public class Role extends AuditModel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@ManyToMany(mappedBy = "roles",fetch = FetchType.LAZY)
	private Set<User> users;
	
	@Column(name = "name", unique = true, length = 45, nullable = false)
	private String name;

	@Column(name = "details", length = 100, nullable = true)
	private String details;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name="role_permissions")
	private Set<Permission> permissions = new HashSet<Permission>();
	
	public Role(Set<User> users, String name, String details) {
		super();
		this.users = users;
		this.name = name;
		this.details = details;
	}
	
	public Role(String name, String details) {
		super();
		this.name = name;
		this.details = details;
	}

	public Role(Set<User> users, String name) {
		super();
		this.users = users;
		this.name = name;
	}

	public Role() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Set<User> getUsers() {
		return users;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}
	
	public Set<Permission> getPermissions() {
		return permissions;
	}

	public void setPermissions(Set<Permission> permissions) {
		this.permissions = permissions;
	}
	
	public void addPermission(Permission p) {
		this.permissions.add(p);
	}

	@Override
	public String toString() {
		return "Role [id=" + id + ", name=" + name + ", details=" + details + ", permissions="
				+ permissions + "]";
	}
}