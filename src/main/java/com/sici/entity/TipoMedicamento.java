package com.sici.entity;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@Entity
@Table(name="tipo_medicamento")
public class TipoMedicamento extends AuditModel{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="idTipoMedicamento")
    private Long id;

    @NotEmpty
    @Column(name="nombre",length = 300)
    private String nombre;


    @OneToMany(targetEntity = Producto.class, mappedBy = "tipos",cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Producto> productoList;
   


	private List<Producto> getProductoList() {
		return productoList;
	}

	private void setProductoList(List<Producto> productoList) {
		this.productoList = productoList;
	}



    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public TipoMedicamento(@NotEmpty String nombre) {
        this.nombre = nombre;
    }

    public TipoMedicamento(){}

	public TipoMedicamento(Long id, @NotEmpty String nombre, List<Producto> productoList) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.productoList = productoList;
	}


	
    
}