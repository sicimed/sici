package com.sici.entity;

import java.util.Date;

public class FechaEstadisticas {

    private Date fecha_inicio;
    private Date fecha_fin;

    public Date getFecha_inicio() {
        return fecha_inicio;
    }

    public Date getFecha_fin() {
        return fecha_fin;
    }
}
