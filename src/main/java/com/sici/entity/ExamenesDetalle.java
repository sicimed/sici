package com.sici.entity;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name = "examen_detalle")
public class ExamenesDetalle extends AuditModel{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "examenes_id")
    private Long id;

    @Column(name = "url_foto")
    private String foto;

    @NotEmpty
    @Column(name="descripcion",length = 300)
    private String descripcion;

    @Column(name="comentario",length = 300)
    private String comentario;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name="consulta_id")
    private Consulta consulta;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name="texamen_id")
    private TipoExamen tipoExamen;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public Consulta getConsulta() {
        return consulta;
    }

    public void setConsulta(Consulta consulta) {
        this.consulta = consulta;
    }

    public ExamenesDetalle(String foto, @NotEmpty String descripcion, String comentario, Consulta consulta) {
        this.foto = foto;
        this.descripcion = descripcion;
        this.comentario = comentario;
        this.consulta = consulta;
    }
}
