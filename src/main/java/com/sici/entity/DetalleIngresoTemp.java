package com.sici.entity;

import javax.persistence.*;

import java.time.LocalDate;


@Entity
@Table(name="detalleIngresoTemp")
public class DetalleIngresoTemp extends AuditModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idDetalleIngreso")
    private Long id;

    @Column(name = "cantidad")
    private int cantidad;
    
    @Column(name = "precioCompra")
    private float precioCompra;
    
    @Column(name = "descripcion",length = 300)
    private String descripcion;

    @Column(name= "lote")
    private String lote;
    
    @Column(name="vence")
    private LocalDate vence;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idProducto")
    private Producto productos;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idIngreso")
    private IngresoTemp ingresos;

	public DetalleIngresoTemp() {
		super();
	}
    
    
	public DetalleIngresoTemp(int cantidad, float precioCompra, String descripcion, String lote, LocalDate vence, 
			Producto productos, IngresoTemp ingresos) {
		super();
		this.cantidad = cantidad;
		this.precioCompra = precioCompra;
		this.descripcion = descripcion;
		this.lote = lote;
		this.vence = vence;
		this.productos = productos;
		this.ingresos = ingresos;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public float getPrecioCompra() {
		return precioCompra;
	}

	public void setPrecioCompra(float precioCompra) {
		this.precioCompra = precioCompra;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Producto getProductos() {
		return productos;
	}

	public void setProductos(Producto productos) {
		this.productos = productos;
	}

	public IngresoTemp getIngresos() {
		return ingresos;
	}

	public void setIngresos(IngresoTemp ingresos) {
		this.ingresos = ingresos;
	}
	public String getLote() {
		return lote;
	}

	public void setLote(String lote) {
		this.lote = lote;
	}

	public LocalDate getVence() {
		return vence;
	}

	public void setVence(LocalDate vence) {
		this.vence = vence;
	}


	@Override
	public String toString() {
		return "DetalleIngresoTemp [id=" + id + ", cantidad=" + cantidad + ", precioCompra=" + precioCompra
				+ ", descripcion=" + descripcion + ", lote=" + lote + ", vence=" + vence + ", productos=" + productos
				 +"]";
	}
 
    
}


