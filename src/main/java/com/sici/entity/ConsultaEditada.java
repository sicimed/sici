package com.sici.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="consulta_edit")
public class ConsultaEditada extends AuditModel{
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="consulta_id")
    private Long id;

    @NotNull
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "citas_id")
    private Citas cita;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY,optional = false)
    @JoinColumn(name="hm_id")
    private HistorialMedico historialMedico;

    @NotEmpty
    @Column(name="diagnostico",length = 6000)
    private String diagnostico;

    @NotEmpty
    @Column(name = "tratamiento",length = 6000)
    private String tratamiento;

    @Column(name = "comentario",length = 6000)
    private String comentario;

    @OneToMany(targetEntity = ExamenesDetalle.class, mappedBy = "consulta", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<ExamenesDetalle> examenesDetalleList;

    @NotNull
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="dpaciente_id")
    private DetallePacienteEditada detallePaciente;

    @NotEmpty
    @Column(name="motivo_consulta",length = 6000)
    private String motivoConsulta;

    @NotEmpty
    @Column(name = "descripcion_motivo",length = 6000)
    private String descripcionMotivo;

    @Column(name = "antecedentes",length = 6000)
    private String antecendentes;
    
    @Column(name="examenes", length = 7000)
    private String examenes;

    @Column(name="estado")
    private boolean estado;
    
    @Column(name="id_consulta")
    private Long idConsulta;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Citas getCita() {
        return cita;
    }

    public void setCita(Citas cita) {
        this.cita = cita;
    }

    public HistorialMedico getHistorialMedico() {
        return historialMedico;
    }

    public void setHistorialMedico(HistorialMedico historialMedico) {
        this.historialMedico = historialMedico;
    }

    public String getDiagnostico() {
        return diagnostico;
    }

    public void setDiagnostico(String diagnostico) {
        this.diagnostico = diagnostico;
    }


    public List<ExamenesDetalle> getExamenesDetalleList() {
        return examenesDetalleList;
    }

    public void setExamenesDetalleList(List<ExamenesDetalle> examenesDetalleList) {
        this.examenesDetalleList = examenesDetalleList;
    }

    public String getMotivoConsulta() {
        return motivoConsulta;
    }

    public void setMotivoConsulta(String motivoConsulta) {
        this.motivoConsulta = motivoConsulta;
    }

    public DetallePacienteEditada getDetallePaciente() {
		return detallePaciente;
	}

	public void setDetallePaciente(DetallePacienteEditada detallePaciente) {
		this.detallePaciente = detallePaciente;
	}

	public String getTratamiento() {
        return tratamiento;
    }

    public void setTratamiento(String tratamiento) {
        this.tratamiento = tratamiento;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public String getDescripcionMotivo() {
        return descripcionMotivo;
    }

    public void setDescripcionMotivo(String descripcionMotivo) {
        this.descripcionMotivo = descripcionMotivo;
    }

    public String getAntecendentes() {
        return antecendentes;
    }

    public void setAntecendentes(String antecendentes) {
        this.antecendentes = antecendentes;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

	public Long getIdConsulta() {
		return idConsulta;
	}

	public void setIdConsulta(Long idConsulta) {
		this.idConsulta = idConsulta;
	}
	
	public String getExamenes() {
		return examenes;
	}

	public void setExamenes(String examenes) {
		this.examenes = examenes;
	}
	
	public ConsultaEditada() {}
    
}
