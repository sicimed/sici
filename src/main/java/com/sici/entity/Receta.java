package com.sici.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Entity
@Table(name="receta")
public class Receta extends AuditModel{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="receta_id")
    private Long id;

    @NotNull
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name ="consulta_id")
    private Consulta consulta;
    
    @NotNull
    @Column(name="detalle_receta",length = 6000)
    private String detalle;

    
    @Column(name="recomendacion",length = 3000)
    private String recomendacion;

    
    @Column(name="otros_datos",length = 3000)
    private String otros;

   
    @Column(name="proxima_cita")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date cita;

    
    
    @Column(name = "estado")
    private Boolean estado;
    
    public boolean isEstado() {
        return estado;
    }
/*
    @OneToMany(targetEntity = DetalleReceta.class, mappedBy = "receta", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<DetalleReceta> detalleRecetaList;*/



	public Long getId() {
		return id;
	}



	public void setId(Long id) {
		this.id = id;
	}



	public Consulta getConsulta() {
		return consulta;
	}



	public void setConsulta(Consulta consulta) {
		this.consulta = consulta;
	}



	public String getDetalle() {
		return detalle;
	}



	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}



	public String getRecomendacion() {
		return recomendacion;
	}



	public void setRecomendacion(String recomendacion) {
		this.recomendacion = recomendacion;
	}



	public String getOtros() {
		return otros;
	}



	public void setOtros(String otros) {
		this.otros = otros;
	}



	public Date getCita() {
		return cita;
	}



	public void setCita(Date cita) {
		this.cita = cita;
	}



	public Boolean getEstado() {
		return estado;
	}



	public void setEstado(Boolean estado) {
		this.estado = estado;
	}



	

	public Receta(Long id, @NotNull Consulta consulta, @NotNull String detalle, String recomendacion, String otros,
			Date cita, Boolean estado) {
		super();
		this.id = id;
		this.consulta = consulta;
		this.detalle = detalle;
		this.recomendacion = recomendacion;
		this.otros = otros;
		this.cita = cita;
		this.estado = estado;
	}



	public Receta() {
		
	}
    
}
