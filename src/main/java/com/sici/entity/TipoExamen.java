package com.sici.entity;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@Entity
@Table(name="tipo_examen")
public class TipoExamen extends AuditModel{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="texamen_id")
    private Long id;

    @NotEmpty
    @Column(name="nombre",length = 600)
    private String nombre;

    @Column(name = "estado")
    private boolean estado;

    @OneToMany(targetEntity = ExamenesDetalle.class, mappedBy = "tipoExamen",cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private List<ExamenesDetalle> examenesDetalleList;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public List<ExamenesDetalle> getExamenesDetalleList() {
        return examenesDetalleList;
    }

    public void setExamenesDetalleList(List<ExamenesDetalle> examenesDetalleList) {
        this.examenesDetalleList = examenesDetalleList;
    }

    public TipoExamen(){}
    public TipoExamen(@NotEmpty String nombre, boolean estado) {
        this.estado = estado;
        this.nombre = nombre;
    }
}
