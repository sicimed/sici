package com.sici.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import java.time.LocalDate;

@Entity
@Table(name="movimiento")
public class Movimiento extends AuditModel{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="movimiento_id")
    private Long id;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY,optional = false)
    @JoinColumn(name="producto_id")
    private Producto producto;

    @Column(name="concepto",length = 400)
    private String concepto;

    @Column(name = "fecha")
    private LocalDate fecha;

    @Column(name = "costo_u")
    private float costoUnit;

    @Column(name = "cantidad")
    private int cantidad;
    
    @Column(name = "sal_cantidad")
    private int salCantidad;
    
    @Column(name = "sal_total")
    private float salTotal;

	public Movimiento(@NotNull Producto producto, String concepto, LocalDate fecha, float costoUnit, int cantidad,
			int salCantidad, float salTotal) {
		super();
		this.producto = producto;
		this.concepto = concepto;
		this.fecha = fecha;
		this.costoUnit = costoUnit;
		this.cantidad = cantidad;
		this.salCantidad = salCantidad;
		this.salTotal = salTotal;
	}

	public Movimiento() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public String getConcepto() {
		return concepto;
	}

	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public float getCostoUnit() {
		return costoUnit;
	}

	public void setCostoUnit(float costoUnit) {
		this.costoUnit = costoUnit;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public int getSalCantidad() {
		return salCantidad;
	}

	public void setSalCantidad(int salCantidad) {
		this.salCantidad = salCantidad;
	}

	public float getSalTotal() {
		return salTotal;
	}

	public void setSalTotal(float salTotal) {
		this.salTotal = salTotal;
	}

	@Override
	public String toString() {
		return "Movimiento [id=" + id + ", producto=" + producto + ", concepto=" + concepto + ", fecha=" + fecha
				+ ", costoUnit=" + costoUnit + ", cantidad=" + cantidad + ", salCantidad=" + salCantidad + ", salTotal="
				+ salTotal + "]";
	}
    
}
