package com.sici.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="empleado")
public class Empleado extends AuditModel{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "empleado_id")
    private Long id;

    @NotNull
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="persona_id")
    private Persona persona;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "clinica_id")
    private Clinica clinica;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public Clinica getClinica() {
        return clinica;
    }

    public void setClinica(Clinica clinica) {
        this.clinica = clinica;
    }

   public Empleado( Persona persona, AsignacionPuesto asignacionPuesto, Clinica clinica) {
        this.persona = persona;
        this.clinica = clinica;
    }

    public Empleado( Persona persona,  Clinica clinica) { 
        this.persona = persona;
        this.clinica = clinica;
    }
    
    public Empleado() {
    }
}
