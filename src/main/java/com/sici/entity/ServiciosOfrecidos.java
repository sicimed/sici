package com.sici.entity;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name="servicios_ofrecidos")
public class ServiciosOfrecidos {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="servicios_id")
    private Long id;

    @NotEmpty
    @Column(name="nombre",length = 300)
    private String nombre;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="clinica_id")
    private Clinica clinica;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Clinica getClinica() {
        return clinica;
    }

    public void setClinica(Clinica clinica) {
        this.clinica = clinica;
    }

    public ServiciosOfrecidos(@NotEmpty String nombre, Clinica clinica) {
        this.nombre = nombre;
        this.clinica = clinica;
    }

    public ServiciosOfrecidos(){}
}
