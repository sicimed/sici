package com.sici.entity;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name = "motivo_consulta")
public class MotivoConsulta extends AuditModel {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="mc_id")
    private Long id;

    @NotEmpty
    @Column(name = "nombre")
    private String nombre;

    @Column(name = "estado")
    private boolean estado;

//    @OneToMany(targetEntity = Consulta.class,mappedBy = "motivoConsulta",cascade = CascadeType.ALL,fetch = FetchType.LAZY)
//    private List<Consulta> consulta;

    public MotivoConsulta(@NotEmpty String nombre, boolean estado) {
        this.nombre = nombre;
        this.estado = estado;
    }

    public MotivoConsulta(){}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }
}
