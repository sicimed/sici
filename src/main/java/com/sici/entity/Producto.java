package com.sici.entity;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

import java.util.List;

@Entity
@Table(name="producto")
public class Producto extends AuditModel{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idProducto")
    private Long id;

    @NotEmpty
    @Column(name = "nombre",length = 300)
    private String nombre;
    
    @Column(name = "precio")
    private float precio;

    @Column(name = "costo_prom")
    private float costoProm;
    
    @Column(name = "inventario")
    private int inventario;
    
    @Column(name = "limStock")
    private int limStock;
    /*
    @OneToMany(targetEntity = DetalleReceta.class, mappedBy = "producto", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<DetalleReceta> detalleRecetaList;*/

	private Boolean estado;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idTipoMedicamento")
    private TipoMedicamento tipos;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idProveedor")
    private Proveedor proveedores;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idMarca")
    private Marca marcas;
    
    //@OneToMany(targetEntity = DetalleVenta.class, mappedBy = "productos", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    //private List<DetalleVenta> detalleVentaList;
    
    @OneToMany(targetEntity = DetalleIngreso.class, mappedBy = "productos", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<DetalleIngreso> detalleIngresoList;
   

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

	public float getPrecio() {
		return precio;
	}

	public void setPrecio(float precio) {
		this.precio = precio;
	}
	
	public float getCostoProm() {
		return costoProm;
	}

	public void setCostoProm(float costoProm) {
		this.costoProm = costoProm;
	}
	public int getLimStock() {
		return limStock;
	}

	public void setLimStock(int limStock) {
		this.limStock = limStock;
	}
	public void setNombre(String nombre) {
        this.nombre = nombre;
    }
/*
    public List<DetalleReceta> getDetalleRecetaList() {
        return detalleRecetaList;
    }

    public void setDetalleRecetaList(List<DetalleReceta> detalleRecetaList) {
        this.detalleRecetaList = detalleRecetaList;
    }*/
 
    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }
    
    public boolean isEstado() {
        return estado;
    }

	public TipoMedicamento getTipos() {
		return tipos;
	}

	public void setTipos(TipoMedicamento tipos) {
		this.tipos = tipos;
	}
	
	public Proveedor getProveedores() {
		return proveedores;
	}

	public void setProveedores(Proveedor proveedores) {
		this.proveedores = proveedores;
	}

	public Marca getMarcas() {
		return marcas;
	}

	public void setMarcas(Marca marcas) {
		this.marcas = marcas;
	}

	//public List<DetalleVenta> getDetalleVentaList() {
	//	return detalleVentaList;
	//}

	//public void setDetalleVentaList(List<DetalleVenta> detalleVentaList) {
	//	this.detalleVentaList = detalleVentaList;
	//}
	

	public List<DetalleIngreso> getDetalleIngresoLista() {
		return detalleIngresoList;
	}

	public void setDetalleIngresoLista(List<DetalleIngreso> detalleIngresoLista) {
		this.detalleIngresoList = detalleIngresoLista;
	}

	public Producto() {
		
	}
	
	public int getInventario() {
		return inventario;
	}

	public void setInventario(int inv) {
		this.inventario = inv;
	}
	
	
}