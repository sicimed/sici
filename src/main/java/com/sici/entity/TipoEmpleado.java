package com.sici.entity;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@Entity
@Table(name="tipo_empleado")
public class TipoEmpleado extends AuditModel{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="tipoe_id")
    private Long id;

    @NotEmpty
    @Column(name="nombre_tp",length = 300)
    private String nombre;

    @Column(name="descripcion_tp",length = 600)
    private String descripcion;

    //@OneToMany(targetEntity = Empleado.class, mappedBy = "tipoEmpleado", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    //private List<Empleado> empleadoList;

    /*public List<Empleado> getEmpleadoList() {
        return empleadoList;
    }

    public void setEmpleadoList(List<Empleado> empleadoList) {
        this.empleadoList = empleadoList;
    }*/

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public TipoEmpleado(@NotEmpty String nombre, String descripcion, List<Empleado> empleadoList) {
        this.nombre = nombre;
        this.descripcion = descripcion;
        //this.empleadoList = empleadoList;
    }
    public TipoEmpleado(@NotEmpty String nombre, String descripcion) {
        this.nombre = nombre;
        this.descripcion = descripcion;
        //this.empleadoList = empleadoList;
    }

    public TipoEmpleado() {
    }
}
