package com.sici.entity;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name="detalleVenta")
public class DetalleVenta extends AuditModel  {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "idDetalleVenta")
    private Long id;

    @NotEmpty
    @Column(name = "cantidad")
    private Long cantidad;
    
    @NotEmpty
    @Column(name = "precioVenta")
    private Double precioVenta;
    
    @NotEmpty
    @Column(name = "descuento")
    private Double descuento;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idProducto")
    private Producto productos;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idVenta")
    private Venta ventas;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCantidad() {
		return cantidad;
	}

	public void setCantidad(Long cantidad) {
		this.cantidad = cantidad;
	}

	public Double getPrecioVenta() {
		return precioVenta;
	}

	public void setPrecioVenta(Double precioVenta) {
		this.precioVenta = precioVenta;
	}

	public Double getDescuento() {
		return descuento;
	}

	public void setDescuento(Double descuento) {
		this.descuento = descuento;
	}

	public Producto getProductos() {
		return productos;
	}

	public void setProductos(Producto productos) {
		this.productos = productos;
	}

	public Venta getVentas() {
		return ventas;
	}

	public void setVentas(Venta ventas) {
		this.ventas = ventas;
	}

	public DetalleVenta() {
		
		;
	}
	
	

	
    
    

	
    
    
    
    
    
}


