package com.sici.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name="evento")
public class Evento {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    private String text;

    private LocalDateTime start;

    private LocalDateTime fin;

    @ManyToOne
    @JsonIgnore
    private Resource resource;

    private String color;

    @JsonProperty("resource")
    public Long getResourceId() {
        return resource != null ? resource.getId() : null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTexto() {
        return text;
    }

    public void setTexto(String texto) {
        this.text = texto;
    }

    public LocalDateTime getInicio() {
        return start;
    }

    public void setInicio(LocalDateTime inicio) {
        this.start = inicio;
    }

    public LocalDateTime getFin() {
        return fin;
    }

    public void setFin(LocalDateTime fin) {
        this.fin = fin;
    }

    public Resource getResource() {
        return resource;
    }

    public void setResource(Resource resource) {
        this.resource = resource;
    }

    public String getColor() { return color; }

    public void setColor(String color) { this.color = color; }
}
