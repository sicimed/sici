package com.sici.entity;



import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name="puesto")
public class Puesto extends AuditModel{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="puesto_id")
    private Long id;

    @NotEmpty
    @Column(name="nombre_p",length = 300)
    private String nombre;

    @Column(name="descripcion_p",length = 600)
    private String descripcion;

    //@OneToMany(targetEntity = AsignacionPuesto.class, mappedBy = "puesto",cascade =  CascadeType.ALL, fetch = FetchType.LAZY)
    //private List<AsignacionPuesto> asignacionPuestoList;

    //public List<AsignacionPuesto> getAsignacionPuestoList() {
    //    return asignacionPuestoList;
    //}

    //public void setAsignacionPuestoList(List<AsignacionPuesto> asignacionPuestoList) {
    //    this.asignacionPuestoList = asignacionPuestoList;
    //}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /*public Puesto(@NotEmpty String nombre, String descripcion, List<AsignacionPuesto> asignacionPuestoList) {
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.asignacionPuestoList = asignacionPuestoList;
    }*/

    public Puesto() {
    }
}
