package com.sici.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="detalle_paciente_editada")
public class DetallePacienteEditada {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="dpaciente_id")
    private Long id;

    @NotNull
    @Column(name="peso",precision = 6,scale = 2)
    private double peso;

    @NotNull
    @Column(name="talla", precision = 6, scale = 2)
    private double talla;

    @Column(name="pc", precision = 6,scale = 2)
    private double pc;

    @NotNull
    @Column(name="temperatura", precision = 6,scale = 2)
    private double temperatura;

    @NotEmpty
    @Column(name="presion_arterial")
    private String presionArterial;

    @NotNull
    @Column(name="imc", precision = 6,scale = 2)
    private double imc;

    @NotEmpty
    @Column(name="frecuencia_cardiaca")
    private String frecuenciaCardiaca;

    @NotEmpty
    @Column(name="frecuencia_respiratoria")
    private String frecuenciaRespiratoria;

    @Column(name = "descripcion",length = 6000)
    private String descripcion;

    @OneToOne(mappedBy = "detallePaciente")
    private ConsultaEditada consulta;

    @ManyToOne(fetch = FetchType.LAZY,optional = false)
    @JoinColumn(name = "paciente_id")
    private Paciente paciente;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public double getPeso() {
        return peso;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }

    public double getTalla() {
        return talla;
    }

    public void setTalla(double talla) {
        this.talla = talla;
    }


    public double getPc() {
        return pc;
    }

    public void setPc(double pc) {
        this.pc = pc;
    }

    public double getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(double temperatura) {
        this.temperatura = temperatura;
    }

    public String getPresionArterial() {
        return presionArterial;
    }

    public void setPresionArterial(String presionArterial) {
        this.presionArterial = presionArterial;
    }

    public String getFrecuenciaCardiaca() {
        return frecuenciaCardiaca;
    }

    public void setFrecuenciaCardiaca(String frecuenciaCardiaca) {
        this.frecuenciaCardiaca = frecuenciaCardiaca;
    }

    public String getFrecuenciaRespiratoria() {
        return frecuenciaRespiratoria;
    }

    public void setFrecuenciaRespiratoria(String frecuenciaRespiratoria) {
        this.frecuenciaRespiratoria = frecuenciaRespiratoria;
    }

    public double getImc() {
        return imc;
    }

    public void setImc(double imc) {
        this.imc = imc;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }
    
    public DetallePacienteEditada() {}


}
