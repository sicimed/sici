package com.sici.entity;


import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

import java.util.List;

@Entity
@Table(name="marca")
public class Marca extends AuditModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idMarca")
    private Long id;

    @NotEmpty
    @Column(name = "nombre",length = 500)
    private String nombre;
    
    @OneToMany(targetEntity = Producto.class, mappedBy = "marcas",cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Producto> productoList;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<Producto> getProductoList() {
		return productoList;
	}

	public void setProductoList(List<Producto> productoList) {
		this.productoList = productoList;
	}
	
	public Marca() {
		
	}

	public Marca(Long id, @NotEmpty String nombre, List<Producto> productoList) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.productoList = productoList;
	}
    
    


    
    
    
	
}
