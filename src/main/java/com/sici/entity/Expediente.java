package com.sici.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Table(name="expediente")
public class Expediente extends AuditModel {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="expediente_id")
    private Long id;

    @NotNull
    @Column(name="codigo", length = 10)
    private String codigo;

    @Column(name="estado",nullable = true)
    private boolean estado;

    @JsonIgnore
    @NotNull
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="paciente_id")
    private Paciente paciente;

    @JsonIgnore
    @OneToMany(targetEntity = Citas.class, mappedBy = "expediente",cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Citas> citasList;



    public List<Citas> getCitasList() {
        return citasList;
    }

    public void setCitasList(List<Citas> citasList) {
        this.citasList = citasList;
    }

    public Long getId() {
        return id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }

    public Expediente( @NotNull String codigo, boolean estado, @NotNull Paciente paciente) {
        this.codigo = codigo;
        this.estado = estado;
        this.paciente = paciente;
    }

    public Expediente(){}
}
