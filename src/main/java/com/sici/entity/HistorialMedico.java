package com.sici.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Table(name="historial_medico")
public class HistorialMedico extends AuditModel{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="hm_id")
    private Long id;

    @NotNull
    @Column(name = "nombre_hm",length = 100)
    private String nombre;

    @NotNull
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "expediente_id")
    private Expediente expediente;

    @OneToMany(targetEntity = Consulta.class, mappedBy = "historialMedico",cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Consulta> consultaList;

    @Column(name = "estado")
    private Boolean estado;

    public List<Consulta> getConsultaList() {
        return consultaList;
    }

    public void setConsultaList(List<Consulta> consultaList) {
        this.consultaList = consultaList;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public Expediente getExpediente() {
        return expediente;
    }

    public void setExpediente(Expediente expediente) {
        this.expediente = expediente;
    }

    public HistorialMedico(@NotNull String nombre, @NotNull Expediente expediente, Boolean estado) {
        this.nombre = nombre;
        this.expediente = expediente;
        this.estado = estado;
    }

    public HistorialMedico(){}
}
