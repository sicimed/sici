package com.sici.restcontroller;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;
import com.google.gson.Gson;
import com.sici.entity.DetalleIngreso;
import com.sici.entity.DetalleIngresoTemp;
import com.sici.entity.Ingreso;
import com.sici.entity.IngresoTemp;
import com.sici.entity.Movimiento;
import com.sici.entity.Producto;
import com.sici.model.DetalleIngresoModel;
import com.sici.model.IngresoFormModel;
import com.sici.repository.DetalleIngresoRepository;
import com.sici.repository.ExistenciaRepository;
import com.sici.repository.IngresoRepository;
import com.sici.repository.MovimientoRepository;
import com.sici.repository.ProductoRepository;
import com.sici.serviceImp.IngresoService;
import com.sici.serviceImp.MovimientoService;

@RestController
public class ProductoRestController {
	
	@Autowired
	@Qualifier("ingresoRepository")
	private IngresoRepository ingresoRepository;

	@Autowired
	@Qualifier("ingresoService")
	private IngresoService ingresoService;
	
	@Autowired
	@Qualifier("movimientoService")
	private MovimientoService movimientoService;
	   
	@Autowired
	@Qualifier("movimientoRepository")
	private MovimientoRepository movimientoRepository;
	   
	@Autowired
	@Qualifier("detalleIngresoRepository")
	private DetalleIngresoRepository detalleIngresoRepository;
	   
	@Autowired
	@Qualifier("productoRepository")
	private ProductoRepository productoRepository;
	   
	@Autowired
	@Qualifier("existenciaRepository")
	private ExistenciaRepository existenciaRepository;
	
	private Gson g = new Gson();
	private IngresoFormModel ing;
	private List<DetalleIngresoModel> details;

	@RequestMapping("/producto/kardexget")
	@JsonView()
	public  String getMovimientos(@RequestParam("idProd") String idProd_,@RequestParam("fini") String fini_,@RequestParam("fend") String fend_) {
	    	 
	 	// Validar datos de entrada en ProductFilter
	    List<Movimiento> list = new ArrayList<Movimiento>();
	    LocalDate fini;
	    LocalDate fend;
	    long idProd;
	    try{idProd = Long.parseLong(idProd_);}catch(NumberFormatException e) {e.printStackTrace();idProd = 0;};
	    System.out.print("\n\n Contenido de parametros: \n idProd: "+idProd+"\n fini: "+fini_+"\n fend: "+fend_+"\n");
	    	
		try {
			fini = fini_ != ""? LocalDate.parse(fini_): null;  
			fend = fend_ != ""? LocalDate.parse(fend_): null;   
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			fini = null;
			fend = null;
		}
	    	
	    String json = "{\"list\": ";
	    	
	    try {   
	    	
	    	list = movimientoService.filter(idProd,fini,fend);
	    			    				    		    	
		    if(list != null) { 
		    		
		    	//System.out.print("\n\n Contenido de list: \n"+list.toString()+"\n");
		    	json = json+"[";
		    		
		        for(Movimiento mov :list) {
		        	//System.out.print("\n\n Contenido de item mov: \n"+mov.toString()+"\n");
		        		
		        	json += "{ \"producto\":{\"id\":\""+mov.getProducto().getId()+"\",\"nombre\":\""+mov.getProducto().getNombre()+"\",\"costoProm\":\""+mov.getProducto().getCostoProm()+"\"},"
		        				+ "\"concepto\": \""+ mov.getConcepto()+"\",\"fecha\" : \""+ mov.getFecha()+"\","
		        				+ "\"costoUnit\": \""+ mov.getCostoUnit()+"\",\"cantidad\": \""+ mov.getCantidad()+"\" ,"
		        				+ "\"salCantidad\": \""+ mov.getSalCantidad()+"\" ,\"salTotal\": \""+ mov.getSalTotal()+"\""
		        				+ "},";
		        }		        	

		        json = json.substring(0, json.length() - 1);
		        json = json + "]";
		    }
		    	
		    //System.out.print("\n\n Contenido de json: \n"+json+"\n");
		    //cierre json
		    json = json + "}";		    		    	
	    }
	    catch(Exception e) {
	    	e.printStackTrace();
	    	json = "{\"list\":\"\"}";
	    }    	
	    return json;
	}	   
	   
	@RequestMapping("/producto/ingresar")
	@JsonView()
	public  String registrarIngresoAjax(
			@RequestParam("tipoComprobante") String comp,
			@RequestParam("fecha") String fecha,
			@RequestParam("totalCompra") float total)
	{
		
		String msg = "";
		ing = new IngresoFormModel();
		
		try{
			if(comp != null) {	
				msg = "Recuperacion exitosa";
				
				ing.setTipoComprobante(comp);
				ing.setFecha(fecha);
				ing.setTotalCompra(total);
				
				details = new ArrayList<>();
			}
			else {
				msg = "Recuperacion fallida";
			}	
		}
		catch(Exception e) {
			e.printStackTrace();
			msg = "Ha ocurrido una excepción"+e.getMessage();			
		}
		
		System.out.print("\n\n Contenido del ingreso: \n\n");
		
		return g.toJson(msg);
	}
	
	@RequestMapping("/producto/ingresari")
	@JsonView()
	public  String registrarIngresoAjax_(@RequestParam("formatData") String data){	
		String msg = "";
		
		try{
			if(data != null) {	
				msg = "Recuperacion exitosa";	
				ing = g.fromJson(data, IngresoFormModel.class);
				System.out.print("\n\n Contenido del ingreso: \n\n"+ing.toString());
			}
			else {
				msg = "Recuperacion fallida";
			}	
		}
		catch(Exception e) {
			e.printStackTrace();
			msg = "Ha ocurrido una excepción"+e.getMessage();			
		}
		
		return g.toJson(msg);
	}
	
	@RequestMapping(value = "/producto/ingresardet")
	@JsonView()
	public  String registrarDetalleAjax(@RequestParam("idProd") long id,@RequestParam("desc") String desc,@RequestParam("cant") int cant,
										@RequestParam("cost") float cost,@RequestParam("lote") Optional<String> lote,@RequestParam("vence") Optional<String> vence) {		
		String msg = "";
		
		if(id > 0) {
			msg = "1";
			try {				
				details.add(
						new DetalleIngresoModel(id,desc,cant,cost,
								(lote.isPresent()?lote.get():null),
								((vence.get()!=null && vence.get()!="")?vence.get():null)
								)
						); 
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		else {
			msg = "0";
		}
		System.out.print("\n\n\n Contenido Json en detalle: msg: "+msg+"\n");
		System.out.print(g.toJson(id + "\n" + desc + "\n" + cant+"\n"));	
		return g.toJson(msg);
	}
	
	@RequestMapping("/producto/ingresarsave")
	@JsonView()
	public  String saveIngresoAjax( @RequestParam("save") int error , @RequestParam("temp") int temp, @RequestParam("id") long id) {
		
		String msg = "";
		
		try {
            if(error == 0) { // cero significa que no hay error
    			msg = "Registro de ingreso de compra de productos exitoso";
    			
    			if(temp == 0 || temp == 2) { // Guardado permamente        			
    				Ingreso ingreso = new Ingreso(ing.getTipoComprobante(),LocalDate.parse(ing.getFecha()),ing.getTotalCompra());
        			List<DetalleIngreso> detIngList = new ArrayList<>();
        			
        			for(DetalleIngresoModel det : ing.getDetails()) {				
        				Producto prod = productoRepository.findById(det.getIdProd());
        				
        				if(prod != null) {       					
        					DetalleIngreso detail = new DetalleIngreso(det.getCant(),det.getCost(),det.getDesc(),det.getLote(),LocalDate.parse(det.getVence()), prod,ingreso);   					
        					detIngList.add(detail);   					
        				}    				
        			}
        			
        			ingreso = ingresoService.addIngreso(ingreso,detIngList);
        			if(temp == 2) {	ingresoService.dropIngresoTemp(id);	} //eliminacion de registro temporal
    			}else { // Guardado temporal
    				IngresoTemp ingreso;
        			if(temp == 3 ) {
        				ingreso = ingresoService.getIngresoTemp(id);   
        				ingreso.setTotalCompra(ing.getTotalCompra());
        				ingreso.setFecha(LocalDate.parse(ing.getFecha()));
        				ingreso.setTipoComprobante(ing.getTipoComprobante());
        			}else {
        				ingreso = new IngresoTemp(ing.getTipoComprobante(),LocalDate.parse(ing.getFecha()),ing.getTotalCompra());        				
        			}
        			
        			List<DetalleIngresoTemp> detIngList = new ArrayList<DetalleIngresoTemp>();       			
        			for(DetalleIngresoModel det : ing.getDetails()) { //posible error de 3 veces    				
        				Producto prod = productoRepository.findById(det.getIdProd());
        				
        				if(prod != null) {        					
        					DetalleIngresoTemp detail = new DetalleIngresoTemp(det.getCant(),det.getCost(),det.getDesc(),det.getLote(),LocalDate.parse(det.getVence()), prod,ingreso);   					
        					detIngList.add(detail);
        				}    				
        			}
        			
    				ingreso = ingresoService.addIngresoTemp(ingreso,detIngList);
    			}								  			
    		}
    		else {
    			msg = "0";
    		}
            
			ing = new IngresoFormModel();
			details = new ArrayList<>();    	
        }
        catch(Exception e) {
        	e.printStackTrace();
        	msg = "0";
        }
		
		return g.toJson(msg);
	}

	@RequestMapping(value = "/producto/ingresargetcost")
	@JsonView()
	public  String getCostoAjax(@RequestParam("id") long id) {
		
		String msg = "1";
		float costo = 0;
		
		if(id>0) {
			Producto prod = productoRepository.findById(id);
			costo = prod.getCostoProm();					
		}
		else {
			msg = "0";
		}
		//System.out.print("\n\n\n Contenido Json: ");
		//System.out.print(g.toJson("Id prod: "+id));
		//System.out.print("\n"+g.toJson(msg)+"\n");
		//System.out.print("\n\n\n");
		
		return "{\"result\":\""+msg+"\",\"costo\":\""+costo+"\"}";
	}
	
    @GetMapping("/producto/alerta_stock")
    public String getAlertStock() {
    	List<Producto> list=null;
	    String json = "{\"list\": ";
    	//System.out.println("Llega al alert Stock");
    	try {
    		list = productoRepository.findByLimStockAndInventario(); //filtro por no bloqueado y pasado de stock lim
    		//System.out.print("\n\n Contenido de list: \n"+list.toString()+"\n");
		    if(!list.isEmpty()) { 
	    		
		    	//System.out.print("\n\n Contenido de list: \n"+list.toString()+"\n");
		    	json = json+"[";
		    		
		        for(Producto prod :list) {
		        	//System.out.print("\n\n Contenido de item prod: \n"+prod.toString()+"\n");
		        		
		        	json += "{\"id\": \""+prod.getId()+"\","
		        				+ "\"nombre\": \""+ prod.getNombre()+"\",\"inventario\" : \""+ prod.getInventario()+"\" ,"
		        				+ "\"limStock\": \""+ prod.getLimStock()+"\",\"estado\": \""+ prod.getEstado()+"\""
		        				+ "},";
		        }		        	
		        json = json.substring(0, json.length() - 1);
		        json = json + "]";
		    }
		    	
		    //System.out.print("\n\n Contenido de json: \n"+json+"\n");
		    //cierre json
		    json = json + "}";	    		
    		
		} catch (DataAccessException e) {
			System.err.println(e.getMessage().concat(":").concat(e.getMostSpecificCause().getMessage()));
	    	json = "{\"list\":\"\"}";
		}
    	//System.out.println("aqui:"+json);
    	return json;
    }


}
