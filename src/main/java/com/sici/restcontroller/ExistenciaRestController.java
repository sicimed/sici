package com.sici.restcontroller;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;
import com.google.gson.Gson;
import com.sici.entity.Existencia;
import com.sici.entity.Movimiento;
import com.sici.entity.Producto;
import com.sici.model.AjusteExistenciaModel;
import com.sici.repository.DetalleIngresoRepository;
import com.sici.repository.ExistenciaRepository;
import com.sici.repository.IngresoRepository;
import com.sici.repository.MovimientoRepository;
import com.sici.repository.ProductoRepository;
import com.sici.serviceImp.IngresoService;

@RestController
public class ExistenciaRestController {
	
	@Autowired
	@Qualifier("ingresoRepository")
	private IngresoRepository ingresoRepository;

	@Autowired
	@Qualifier("ingresoService")
	private IngresoService ingresoService;
	   
	@Autowired
	@Qualifier("movimientoRepository")
	private MovimientoRepository movimientoRepository;
	   
	@Autowired
	@Qualifier("detalleIngresoRepository")
	private DetalleIngresoRepository detalleIngresoRepository;
	   
	@Autowired
	@Qualifier("productoRepository")
	private ProductoRepository productoRepository;
	   
	@Autowired
	@Qualifier("existenciaRepository")
	private ExistenciaRepository existenciaRepository;
	
	private Gson g = new Gson();
	private List<AjusteExistenciaModel> ajustes_;

	@RequestMapping("/existencia/get")
	@JsonView()
	public  String getExistencias(@RequestParam("id") long idProd) {
	    	 
	 	// Validar datos de entrada en ProductFilter
		Producto prod = productoRepository.findById(idProd);
	    List<Existencia> list = existenciaRepository.findByProducto(prod);
	    String json = "{\"list\": ";
	    	
	    try {       	
	    	
		    if(list != null) { 
		    	
		    	json = json+"[";
		    		
		        for(Existencia mov :list) {
		        		
		        	json += "{ \"producto\":{\"id\":\""+mov.getProducto().getId()+"\",\"nombre\":\""+mov.getProducto().getNombre()+"\",\"costoProm\":\""+mov.getProducto().getCostoProm()+"\"},"
		        				+ "\"stock\": \""+ mov.getStock()+"\",\"lote\" : \""+ mov.getLote()+"\","
		        				+ "\"vence\": \""+ mov.getVence().toString()+"\",\"id\": \""+ mov.getId()+"\""
		        				+ "},";
		        }		        	

		        json = json.substring(0, json.length() - 1);
		        json = json + "]";
		    }
		    
		    //cierre json
		    json = json + "}";		    		    	
	    }
	    catch(Exception e) {
	    	e.printStackTrace();
	    	json = "{\"list\":\"\"}";
	    }
		ajustes_ = new ArrayList<AjusteExistenciaModel>();
	    return json;
	}	   
	   
	@RequestMapping("/existencia/ingresar")
	@JsonView()
	public  String registrarIngresoAjax(@RequestParam("idEx") long idEx,@RequestParam("desc") String desc,@RequestParam("cant") int cant){
		
		String msg = "";
		Existencia ex;	

		try{
			if(idEx > 0) {	
				msg = "Recuperacion exitosa";
				ex = existenciaRepository.findById(idEx);
				if(ex != null){
					ajustes_.add(new AjusteExistenciaModel(idEx,cant,desc));
				}
			}
			else {
				msg = "Recuperacion fallida";
			}	
		}
		catch(Exception e) {
			e.printStackTrace();
			msg = "Ha ocurrido una excepción: "+e.getMessage();
			ajustes_.clear();
			System.out.println(msg);
		}
		return g.toJson(msg);
	}
	
	@RequestMapping("/existencia/ingresar/save")
	@JsonView()
	public  String saveAjusteAjax( @RequestParam("error") int error) {
        String msg = "";
		try {
			if(error == 0) {
				for(AjusteExistenciaModel ajuste : ajustes_) {
					 Existencia ex = new Existencia();
					 Movimiento mov;
					 Producto prod;
					 
					 String desc = ajuste.getDesc();
					 long idEx = ajuste.getIdEx();
					 int cant = ajuste.getCant();

					 
					if(idEx > 0) {
						
						ex = existenciaRepository.findById(idEx);
						
						if( (cant < 0 && (cant * -1) <= ex.getStock() ) || cant >= 0) {
							
							prod = ex.getProducto();
							int salCantidad =  prod.getInventario() + cant;
							prod.setInventario(salCantidad);
							ex.setStock(ex.getStock()+cant);
							
							float salTotal = salCantidad * prod.getCostoProm();
							mov = new Movimiento(ex.getProducto(),desc,LocalDate.now(),ex.getProducto().getCostoProm(),cant,salCantidad,salTotal);
							
							productoRepository.save(prod);
							existenciaRepository.save(ex);
							movimientoRepository.save(mov);							
						}
					}
				}
				//ajustes_.clear();
				msg = "1";
			}else {
				//Ha sucedido un error
				ajustes_.clear();
			}
        }
        catch(Exception e) {
        	e.printStackTrace();
        	msg = "0";
        }
		return g.toJson(msg);
	}	

}
