package com.sici.restcontroller;

import org.springframework.format.annotation.DateTimeFormat;

import com.sici.entity.Producto;

import java.time.LocalDate;

/**
 * @author hector.martinez
 *
 */
public class ProductFilter {
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate dateStart;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate dateEnd;
    private Producto producto;
    
    public LocalDate getDateStart() {
        return dateStart;
    }

    public void setDateStart(LocalDate dateStart) {
        this.dateStart = dateStart;
    }

    public LocalDate getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(LocalDate datEnd) {
        this.dateEnd = datEnd;
    }

	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}
    
}
