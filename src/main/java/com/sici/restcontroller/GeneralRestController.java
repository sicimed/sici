package com.sici.restcontroller;

import com.google.gson.Gson;
import com.sici.entity.Citas;
import com.sici.model.CitaModel;
import com.sici.model.ResponseRest;
import com.sici.serviceImp.CitaService;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/consulta_rest")
public class GeneralRestController {
    @Autowired
    @Qualifier("citaServiceImp")
    private CitaService citaService;
    
    @GetMapping("/citas_actuales")
    public ResponseRest getCitas() {
    	List<Citas> citas=null;
    	int contador=0;
    	String status="Done";
    	List<CitaModel> citaModelList = new ArrayList<CitaModel>();
    	ResponseRest response =null;
    	try {
    	citas = citaService.getCitasByFechaActual(LocalDate.now());
    	for(Citas cita:citas) {
    		CitaModel citaModel = new CitaModel();
    		citaModel.setExpediente(cita.getExpediente().getCodigo());
    		citaModel.setFecha_consulta(cita.getFecha_consulta());
    		citaModel.setHora(cita.getHora());
    		citaModel.setTipo_hora(cita.getTipoHora());
    		citaModel.setNombre_paciente(cita.getExpediente().getPaciente().getPersona().getNombre()+" "+cita.getExpediente().getPaciente().getPersona().getApellido());
    		citaModel.setSexo(cita.getExpediente().getPaciente().getPersona().getSexo());
    		citaModelList.add(citaModel);
    		contador++;
    	}
    	if(contador==0) {
    		status="vacio";
    	}
    	 response = new ResponseRest(status,citaModelList);
		} catch (DataAccessException e) {
			System.err.println(e.getMessage().concat(":").concat(e.getMostSpecificCause().getMessage()));
		}
    	System.out.println("Status:"+response.getStatus());
    	System.out.println("Response:"+response.getData());
    	String json = new Gson().toJson(citaModelList);
    	System.out.println("aqui:"+json);
    	return response;
    }

    
}
