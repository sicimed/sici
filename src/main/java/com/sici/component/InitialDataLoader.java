package com.sici.component;

import java.util.HashSet;
import java.util.Set;

import javax.transaction.Transactional;

import java.util.*;

import com.sici.entity.*;
import com.sici.serviceImp.ClinicaService;
import com.sici.serviceImp.ServicioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import com.sici.constant.ConstantValues;
import com.sici.repository.PermissionRepository;
import com.sici.repository.RoleRepository;
import com.sici.repository.UserRepository;

@Component
public class InitialDataLoader implements ApplicationListener<ContextRefreshedEvent> {
 
    boolean alreadySetup = false;
 
    @Autowired
    private UserRepository userRepository;
  
    @Autowired
    private RoleRepository roleRepository;
  
    @Autowired
    private PermissionRepository permissionRepository;

    @Autowired
    private ClinicaService clinicaService;

    @Autowired
    private ServicioService servicioService;
  
    private BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
  
    @Override
    @Transactional
    public void onApplicationEvent(ContextRefreshedEvent event) {
    	/* PARA QUE SE EJECUTE SOLO UNA VEZ */
        if (alreadySetup) return;
        
        long count = permissionRepository.count();
        Role adminRole, guessRole;
        
        /* CREACION DE PERMISOS */ 
        if(count < (long)(ConstantValues.ENTITIES.length*3)) {
        	for(int i=0; i<ConstantValues.ENTITIES.length;i++) {
        		for(int j=0; j<ConstantValues.TYPE_PERMISSIONS.length;j++) {
        			createPermissionIfNotFound(ConstantValues.PERMISS_PREFIX+ConstantValues.TYPE_PERMISSIONS[j]+ConstantValues.ENTITIES[i]);
        		}
        	}
        }
        
        /* ROL DE ADMINISTRADOR	 */
        if(roleRepository.findByName(ConstantValues.ROLE_PREFIX+ConstantValues.ADMIN_ROLE[0]) == null) {
            User user = new User();       	
        	adminRole = createRoleIfNotFound(ConstantValues.ROLE_PREFIX+ConstantValues.ADMIN_ROLE[0],ConstantValues.ADMIN_ROLE[1]);
            user.setUsername(ConstantValues.DEFAULT_USERS[0]);
            user.setPassword(passwordEncoder.encode(ConstantValues.DEFAULT_USERS[0]));
            user.setEnabled(true);
            user.addRole(adminRole);
            createUserIfNotFound(user);      
        }else {
            adminRole = roleRepository.findByName(ConstantValues.ROLE_PREFIX+ConstantValues.ADMIN_ROLE[0]);
        }
        
        /* TODOS LOS PERMISOS DE ADMINISTRADOR	 */
        for(Permission p: permissionRepository.findAll()) {
            adminRole.addPermission(p);
            adminRole = roleRepository.save(adminRole);
        }
        
        /* ROL DE INVITADO */
        if(roleRepository.findByName(ConstantValues.ROLE_PREFIX+ConstantValues.GUESS_ROLE[0]) == null) {
        	User user = new User();
        	guessRole = createRoleIfNotFound(ConstantValues.ROLE_PREFIX+ConstantValues.GUESS_ROLE[0],ConstantValues.GUESS_ROLE[1]);
            user.setUsername(ConstantValues.DEFAULT_USERS[1]);
            user.setPassword(passwordEncoder.encode(ConstantValues.DEFAULT_USERS[1]));
            user.addRole(guessRole);
            user.setEnabled(true);
            createUserIfNotFound(user);
        }
        createClinicaNotFound();
        createServiciosNotFound();
        alreadySetup = true;
    }
 
    @Transactional
    private Permission createPermissionIfNotFound(String name) {
  
    	Permission privilege = permissionRepository.findByName(name);
        if (privilege == null) {
            privilege = new Permission(name);
            privilege = permissionRepository.save(privilege);
        }
        return privilege;
    } 
 
    @Transactional
    private Role createRoleIfNotFound(String name, String details) {
        Role role = roleRepository.findByName(name);
        if (role == null) {
            role = new Role(name,details);
            role = roleRepository.save(role);
        }
        return role;
    }
    
    @Transactional
    private User createUserIfNotFound(User user) {
        User ur = userRepository.findByUsername(user.getUsername());
        if (ur == null) {
            user = userRepository.save(user);
        }
        return user;
    }

    @Transactional
    private void createClinicaNotFound(){
        Clinica clinica = clinicaService.getClinicaByCodigo("C1");
        if( clinica==null){
            clinica = new Clinica("sici","C1",true);
            clinicaService.addClinica(clinica);
        }
    }

    @Transactional
    private void createServiciosNotFound(){
        Clinica clinica = clinicaService.getClinicaByCodigo("C1");
        List<ServiciosOfrecidos> serviciosOfrecidosList = servicioService.findAll();
        ServiciosOfrecidos serviciosOfrecidos=null;
        if(serviciosOfrecidosList.isEmpty()){
            serviciosOfrecidos = new ServiciosOfrecidos("Consulta médica",clinica);
            servicioService.addServicio(serviciosOfrecidos);
            serviciosOfrecidos = new ServiciosOfrecidos("Prescripción de medicamentos",clinica);
            servicioService.addServicio(serviciosOfrecidos);
            serviciosOfrecidos = new ServiciosOfrecidos("Tratamientos",clinica);
            servicioService.addServicio(serviciosOfrecidos);
            serviciosOfrecidos = new ServiciosOfrecidos("Venta de otros servicios",clinica);
            servicioService.addServicio(serviciosOfrecidos);
        }
    }
}