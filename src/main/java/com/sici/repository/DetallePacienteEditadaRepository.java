package com.sici.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sici.entity.DetallePacienteEditada;

@Repository
public interface DetallePacienteEditadaRepository extends JpaRepository<DetallePacienteEditada, Serializable> {

}
