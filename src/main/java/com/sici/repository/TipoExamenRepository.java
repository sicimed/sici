package com.sici.repository;

import com.sici.entity.TipoExamen;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
@Repository("tipoExamenRepository")
public interface TipoExamenRepository extends JpaRepository<TipoExamen, Serializable> {

    public TipoExamen findById(Long id);
}
