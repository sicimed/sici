package com.sici.repository;

import com.sici.entity.MotivoConsulta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.List;

@Repository("motivoConsultaRepository")
public interface MotivoConsultaRepository extends JpaRepository<MotivoConsulta, Serializable> {

    public MotivoConsulta findById(Long id);

    public List<MotivoConsulta> findByEstado(boolean tipo);

}
