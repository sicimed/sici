package com.sici.repository;

import com.sici.entity.ServiciosOfrecidos;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("servicioClinicaRepository")
public interface ServiciosClinicaRepository extends JpaRepository<ServiciosOfrecidos,Long> {

}
