package com.sici.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sici.entity.AsignacionUsuario;

@Repository("asignacionUsuarioRepository")
public interface AsignacionUsuarioRepository extends JpaRepository<AsignacionUsuario, Serializable>
{
	public abstract AsignacionUsuario findById(Long id);
}
