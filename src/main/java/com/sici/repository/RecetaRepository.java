package com.sici.repository;

import com.sici.entity.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.List;

@Repository("recetaRepository")
 public interface RecetaRepository extends JpaRepository<Receta, Serializable>{

	  @Query("SELECT t FROM Receta t WHERE t.estado=?1")
	    List<Receta> findByEstado(boolean tipo);
	  
	  @Query("SELECT t FROM  Receta t WHERE t.consulta=?1 and t.estado=true")
	  public Receta findByConsulta(Consulta consulta);
	  
 
}
