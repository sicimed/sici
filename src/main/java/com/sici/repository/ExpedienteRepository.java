package com.sici.repository;

import com.sici.entity.Expediente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

@Repository("expedienteRepository")
public interface ExpedienteRepository extends JpaRepository<Expediente, Serializable> {
        public Expediente findById(Long id);
}
