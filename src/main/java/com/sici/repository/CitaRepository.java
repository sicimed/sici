package com.sici.repository;

import com.sici.entity.Citas;
import com.sici.entity.Expediente;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

@Repository("citaRepository")
public interface CitaRepository extends JpaRepository<Citas, Serializable> {
	
	@Query("from Citas c where c.expediente = :expediente and c.proceso = 'EJECUTADA'")
	public List<Citas> findByExpediente(@Param("expediente")Expediente expediente);
	
    //public List<Citas> findByEstado(boolean tipo);
    List<Citas> findByProcesoAndEstado(String tipo,boolean estado);
    public Citas findById(Long id);
    public List<Citas> findByExpedienteOrderByExpedienteDesc(Expediente expediente);
    @Query("from Citas c where c.proceso = :estadoCita and  c.fecha_consulta between :fechaInicio and :fechaFin order by c.fecha_consulta ASC")
    public List<Citas> getAllBetweenDates(@Param("fechaInicio")LocalDate fechaInicio, @Param("fechaFin") LocalDate fechaFin, @Param("estadoCita")String estadoCita);

    @Query("from Citas c where c.fecha_consulta > :fechaActual and c.proceso = 'ESPERA'")
    public List<Citas> findAllByFecha_consulta(@Param("fechaActual")LocalDate fechaActual);
    
    @Query("from Citas c where c.fecha_consulta = :fechaActual and c.proceso = 'ESPERA'")
    public List<Citas> findAllByFecha_consultaActual(@Param("fechaActual")LocalDate fechaActual);

//    @Query("select c.fecha_consulta, count(c) from Citas c where c.proceso = :estadoCita and c.fecha_consulta between  :fechaInicio and :fechaFin group by c.fecha_consulta")
//    public List<Integer> contadorCitas(@Param("fechaInicio")LocalDate fechaInicio, @Param("fechaFin") LocalDate fechaFin, @Param("estadoCita") String estadoCita);

   // @Query("select count(c) from Citas c where c.proceso = :estadoCita and c.fecha_consulta between  :fechaInicio and :fechaFin and group by c.fecha_consulta and order by c.fecha_consulta ASC")
   @Query(value = "select count(c) from citas as c where c.estado_cita = 'EJECUTADA' and c.fecha_consulta between ?1 and ?2 group by c.fecha_consulta order by c.fecha_consulta ASC",nativeQuery = true)
   public List<Integer> contadorCitas(LocalDate fechaInicio,LocalDate fechaFin, String estadoCita);

   //@Query(value = "select c.fecha_consulta, count(c) as cantidad from citas as c where c.estado_cita = 'EJECUTADA' and c.fecha_consulta between ?1 and ?2 group by c.fecha_consulta order by c.fecha_consulta ASC",nativeQuery = true)
   @Query("select new map(c.fecha_consulta as fecha,count(c) as cantidad) from Citas as c where c.proceso = 'EJECUTADA' and c.fecha_consulta BETWEEN ?1 AND ?2 GROUP BY c.fecha_consulta ORDER BY c.fecha_consulta ASC")
   public List<?> contadorCitasDia(LocalDate fechaInicio,LocalDate fechaFin);
}
