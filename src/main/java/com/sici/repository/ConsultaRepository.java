package com.sici.repository;

import com.sici.entity.Consulta;
import org.springframework.data.jpa.repository.JpaRepository;

import java.io.Serializable;

public interface ConsultaRepository extends JpaRepository<Consulta, Serializable> {
    public Consulta findById(Long id);
    

}
