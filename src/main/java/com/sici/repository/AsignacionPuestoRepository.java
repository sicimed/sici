package com.sici.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sici.entity.AsignacionPuesto;


@Repository("asignacionPuestoRepository")
public interface AsignacionPuestoRepository extends JpaRepository<AsignacionPuesto, Serializable>
{
	public abstract AsignacionPuesto findById(Long id);
}
