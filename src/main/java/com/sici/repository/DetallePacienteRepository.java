package com.sici.repository;

import com.sici.entity.DetallePaciente;
import com.sici.entity.Paciente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.List;

@Repository("detallePacienteRepository")
public interface DetallePacienteRepository extends JpaRepository<DetallePaciente, Serializable> {
    public DetallePaciente findById(Long id);

    @Query(value="SELECT * FROM detalle_paciente t WHERE t.paciente_id= ?1 ORDER BY t.created_at DESC LIMIT 1",nativeQuery = true)
    public DetallePaciente findLatest(Paciente paciente);

    public List<DetallePaciente> findByPacienteOrderByPacienteDesc(Paciente paciente);
}
