package com.sici.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.sici.entity.Role;
import com.sici.entity.User;

@Repository("userRepository")
public interface UserRepository extends JpaRepository<User, Long> {
	
	public abstract User findByUsername(String username);
	public abstract Set<User> findByRoles(Set<Role> roles);
	@Query(value = "select count(u)>0 from public.users as u where u.username = ?1",
			nativeQuery = true)
	public boolean exists(String name);
}
