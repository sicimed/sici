package com.sici.repository;

import com.sici.entity.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

@Repository("marcaRepository")
public interface MarcaRepository extends JpaRepository<Marca, Serializable>{

 
}
