package com.sici.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.sici.entity.ConsultaEditada;

@Repository
public interface ConsultaEditadaRespository extends JpaRepository<ConsultaEditada, Serializable>{
	
	public ConsultaEditada findById(Long id);
	
	@Query("from ConsultaEditada c where c.idConsulta = :id")
	public List<ConsultaEditada> findByIdConsulta(@Param("id")Long id);

}
