package com.sici.repository;

import com.sici.entity.Persona;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.List;

@Repository("personaRepository")
public interface PersonaRepository extends JpaRepository<Persona, Serializable>{

    @Query("SELECT t FROM Persona t WHERE t.estado=?1 and t.tipo = 'paciente'")
    List<Persona> findByEstado(boolean tipo);
    
 
}
