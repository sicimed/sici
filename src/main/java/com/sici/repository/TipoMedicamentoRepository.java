package com.sici.repository;

import com.sici.entity.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

@Repository("tipomedicamentoRepository")
public interface TipoMedicamentoRepository extends JpaRepository<TipoMedicamento, Serializable>{


 
}
