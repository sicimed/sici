package com.sici.repository;

import com.sici.entity.Existencia;

import com.sici.entity.Producto;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.List;

@Repository("existenciaRepository")
public interface ExistenciaRepository extends JpaRepository<Existencia, Serializable> {
        public Existencia findById(Long id);
        @Query(value = "SELECT e.* FROM Existencia e WHERE e.producto_id= ?1 AND e.lote = ?2 LIMIT 1",nativeQuery = true)
        public Existencia findByProductoAndLote(Long id,String lote);
        public abstract List<Existencia> findByProducto(Producto prod);
        
        //    @Query("SELECT e FROM Existencia e WHERE e.producto_id=?1 AND e.lote = ?2 ")
        //		List<Existencia> findByProductoAndLote(int id_prod, String Lote);
}
