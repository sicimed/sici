package com.sici.repository;

import com.sici.entity.Movimiento;
import com.sici.entity.Producto;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

@Repository("movimientoRepository")
public interface MovimientoRepository extends JpaRepository<Movimiento, Serializable> {
        public abstract Movimiento findById(Long id);
        public abstract List<Movimiento> findByProducto(Producto producto);
        //@Query("FROM Movimiento mov " + 
        //		"WHERE mov.producto_id = :idProd  Order by mov.fecha DESC")
        //public List<Movimiento> findByProducto(Producto producto);
        @Query("FROM Movimiento mov " + 
        		"WHERE mov.fecha >=  :fini Order by mov.fecha DESC")
        public List<Movimiento> findByFechaAfter(@Param("fini") LocalDate fini);
        @Query( "FROM Movimiento mov " + 
        		"WHERE mov.fecha <= :fend Order by mov.fecha DESC")
        public List<Movimiento> findByFechaBefore(@Param("fend") LocalDate fend);
        @Query("FROM Movimiento mov " + 
        		"WHERE mov.fecha BETWEEN :fini AND :fend Order by mov.fecha DESC")
        public List<Movimiento> findByFechaAfterAndFechaBefore(@Param("fini") LocalDate fini,@Param("fend")  LocalDate fend);
        @Query("FROM Movimiento as mov " + 
        		"WHERE mov.producto = :idProd AND " + 
        		"mov.fecha >= :fini Order by mov.fecha DESC")
        public List<Movimiento> findByProductoAndFechaAfter(@Param("idProd") Producto idProd, @Param("fini")  LocalDate fini);
        @Query("FROM Movimiento mov " + 
        		"WHERE mov.producto = :idProd AND " + 
        		"mov.fecha <= :fend Order by mov.fecha DESC")
        public List<Movimiento> findByProductoAndFechaBefore(@Param("idProd") Producto idProd, @Param("fend") LocalDate fend);
        @Query("FROM Movimiento mov WHERE mov.producto = :idProd AND mov.fecha >= :fini AND mov.fecha <= :fend Order by mov.fecha DESC")
        public List<Movimiento> findByProductoAndFechaAfterAndFechaBefore(@Param("idProd") Producto idProd, @Param("fini") LocalDate fini, @Param("fend")  LocalDate fend);
        
        //@Query("SELECT e FROM Existencia e WHERE e.producto_id=?1 AND e.fecha >= ?fini AND e.fecha <= ?3")
        //List<Producto> findByProductoAndDateRange(long idprod, LocalDate fini, LocalDate fend);
        // public abstract List<Movimiento> findByProductoAndFechaAfterAndFechaBefore(@Param("idProd") long idProd,@Param("fini") String fini, @Param("fend") String fend);
}
