package com.sici.repository;

import com.sici.entity.Expediente;
import com.sici.entity.HistorialMedico;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

@Repository("historialMedicoRepository")
public interface HistorialMedicoRepository extends JpaRepository<HistorialMedico, Serializable> {

//    @Query("SELECT t from HistorialMedico t where t.expediente= :expedienteId")
//    public HistorialMedico findByExpediente(@Param("expedienteId") Long id);

    public HistorialMedico findByExpediente(Expediente expediente);

}
