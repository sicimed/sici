package com.sici.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.sici.entity.Role;
import com.sici.entity.User;

@Repository("roleRepository")
public interface RoleRepository extends JpaRepository<Role, Long> {
	
	public abstract Role findByName(String name);
	public abstract Role findById(long id);
	public abstract Set<Role> findByUsers(Set<User> users);
	public abstract Set<Role> findByUsersNot(Set<User> users);
	@Query(value = "SELECT DISTINCT r FROM public.roles AS r inner join public.user_roles AS ur on ur.roles_id = r.id "
			+ "EXCEPT ( SELECT rl FROM public.user_roles AS urs  inner join public.users AS us on us.username = ?1 inner join public.roles AS rl on rl.id = urs.roles_id );",
			nativeQuery = true)
	public Set<Role> findByRolesNotSet(String username);	
	
	
}
