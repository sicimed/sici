package com.sici.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sici.entity.Puesto;

@Repository("puestoRepository")
public interface PuestoRepository extends JpaRepository<Puesto, Serializable>

{

}
