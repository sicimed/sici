package com.sici.repository;

import com.sici.entity.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

@Repository("ingresoTempRepository")
public interface IngresoTempRepository extends JpaRepository<IngresoTemp, Serializable>{
	public abstract IngresoTemp findById(Long id);
	public abstract void deleteById(Long id);
}
