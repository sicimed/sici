package com.sici.repository;

import com.sici.entity.Paciente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

@Repository("pacienteRepository")
public interface PacienteRepository extends JpaRepository<Paciente, Serializable> {

}
