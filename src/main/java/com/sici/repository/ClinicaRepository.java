package com.sici.repository;

import com.sici.entity.Clinica;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

@Repository("clinicaRepository")
public interface ClinicaRepository extends JpaRepository<Clinica, Serializable> {

    @Query("SELECT t FROM Clinica t WHERE t.codigo= :codigo")
    Clinica findClinicaByCodigo(@Param("codigo") String codigo);
}
