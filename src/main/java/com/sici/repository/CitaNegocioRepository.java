package com.sici.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.sici.entity.CitasNegocio;

@Repository("citaNegocioRepository")
public interface CitaNegocioRepository extends CrudRepository<CitasNegocio, Serializable> {

//	public Optional<CitasNegocio> findById(Long id);
	public List<CitasNegocio> findByEstado(boolean estado);
}
