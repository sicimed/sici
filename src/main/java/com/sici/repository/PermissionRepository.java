package com.sici.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sici.entity.Permission;
import com.sici.entity.Role;

@Repository("permissionRepository")
public interface PermissionRepository extends JpaRepository<Permission, Long> {
	
	public abstract Permission findByName(String name);
	public abstract Set<Permission> findByRoles(Set<Role> roles);
	public abstract Set<Permission> findByRolesNot(Set<Role> roles);
	public abstract Set<Permission> findByRolesIn(Set<Role> roles);
	public abstract long count();
}
