package com.sici.repository;

import com.sici.entity.DetalleIngresoTemp;
import com.sici.entity.Ingreso;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.List;

@Repository("detalleIngresoTempRepository")
public interface DetalleIngresoTempRepository extends JpaRepository<DetalleIngresoTemp, Serializable> {
    public DetalleIngresoTemp findById(Long id);
	@Query(value = "delete from public.detalle_ingreso_temp where id_ingreso = ?1", nativeQuery = true)
	@Modifying
	public int deleteByIngresos(Long idIngreso);
    public List<DetalleIngresoTemp> findByIngresosOrderById(Ingreso ingreso);
}
