package com.sici.repository;

import com.sici.entity.Resource;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ResourceRepository extends JpaRepository<Resource,Long> {
    @Query("select r from Resource r where r.Id= :id")
    Resource findOne(@Param("id") Long resource);
    Resource findByName(String name);
}
