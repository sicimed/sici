package com.sici.repository;

import com.sici.entity.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.List;

@Repository("productoRepository")
 public interface ProductoRepository extends JpaRepository<Producto, Serializable>{

    @Query("SELECT t FROM Producto t WHERE t.estado=?1")
    List<Producto> findByEstado(boolean tipo);
    
	public abstract Producto findById(Long id);
    @Query("FROM Producto p WHERE p.inventario <= p.limStock AND p.inventario > 0")
    public List<Producto> findByLimStockAndInventario();

 
}
