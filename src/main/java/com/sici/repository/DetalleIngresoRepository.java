package com.sici.repository;

import com.sici.entity.DetalleIngreso;
import com.sici.entity.Ingreso;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.List;

@Repository("detalleIngresoRepository")
public interface DetalleIngresoRepository extends JpaRepository<DetalleIngreso, Serializable> {
    public DetalleIngreso findById(Long id);

//    @Query("SELECT t FROM DetallePaciente t WHERE t.paciente=: paciente")
//    public DetallePaciente findLatest(@Param("paciente")Paciente paciente);

    public List<DetalleIngreso> findByIngresosOrderById(Ingreso ingreso);
}
