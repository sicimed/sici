package com.sici.repository;

import com.sici.entity.Evento;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import java.time.LocalDateTime;
import java.util.List;

public interface EventRepository extends JpaRepository<Evento,Long> {
    @Query("from Evento e where not(e.fin < :from or e.start > :to)")
    public List<Evento> findBetween(@Param("from") @DateTimeFormat(iso=ISO.DATE_TIME) LocalDateTime start, @Param("to") @DateTimeFormat(iso=ISO.DATE_TIME) LocalDateTime end);

    @Query("select e from Evento e where e.id= :id")
    Evento findOne(@Param("id") Long id);
}
