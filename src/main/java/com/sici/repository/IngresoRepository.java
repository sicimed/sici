package com.sici.repository;

import com.sici.entity.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

@Repository("ingresoRepository")
public interface IngresoRepository extends JpaRepository<Ingreso, Serializable>{
	public abstract Ingreso findById(Long id);
}
