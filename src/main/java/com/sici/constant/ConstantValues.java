package com.sici.constant;

public class ConstantValues {
	/* LISTA DE ENTIDADES REGISTRADAS QUE NECESITAN PERMISOS */
    public static final String ENTITIES[] = {
    	/* A*/	"ASIGNACION_PUESTO", "ASIGNACION_USUARIO",		
    	/* C*/	"CITAS",				"CITA_NEGOCIO",			"CLINICA",			"CONSULTA",    
    	/* D*/	"DETALLE_INGRESO",		"DETALLE_PACIENTE",		"DETALLE_VENTA",		
    	/* E*/	"EMPLEADO",				"EVENTO",				"EXAMENES_DETALLE",	"EXISTENCIA",	"EXPEDIENTE",
    	/* F*/	
    	/* H*/	"HISTORIAL_MEDICO",		
    	/* I*/	"INGRESO",
    	/* M*/	"MARCA",				"MOTIVO_DE_CONSULTA",	"MOVIMIENTO",
    	/* L*/	"LOG",
    	/* P*/	"PACIENTE",				"PERMISSION",			"PERSONA",			"PRODUCTO", 	"PROVEEDOR",	"PUESTO",				
    	/* R*/	"RECETA",				"ROLE",				
    	/* S*/	"SERVICIOS_OFRECIDOS",	
    	/* T*/	"TIPO_EMPLEADO", 		"TIPO_EXAMEN",			"TIPO_MEDICAMENTO",	
    	/* U*/	"USER",
    	/* V*/  "VENTA", "CITA_NEGOCIO"
    			};
    
    public static final String DEFAULT_USERS[] = {"admin","user"}; //las contraseñas serán las mismas
    
    public static final String TYPE_PERMISSIONS[] = {"READ_","WRITE_","DELETE_"};
    
    public static final String ROLE_PREFIX = "ROLE_";
    
    public static final String PERMISS_PREFIX = "";
    
    public static final String ADMIN_ROLE[] = {"ADMIN","Administrador del sistema"};
    
    public static final String GUESS_ROLE[] = {"GUESS","Invitado del sistema"};
    
}
