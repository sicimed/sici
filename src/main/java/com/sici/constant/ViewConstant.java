package com.sici.constant;

public class ViewConstant {
    //General

    //pacientes
    public static final String PACIENTE_FORM = "crud_paciente/form_register";
    public static final String PACIENTE_FORM_EDIT = "crud_paciente/form_edit";
    public static final String INDEX = "index/index";
    public static final String LIST_PATIENT="redirect:/pacientes";
    
    //User templates
    public static final String USER_FORM = "crud_user/user_form";
    public static final String UPDATE_FORM = "crud_user/update_form";
    public static final String SHOW_USER_FORM = "crud_user/show_form";
    public static final String USER_LIST = "crud_user/list_users";
    public static final String INDEX_USER = "index/indexUsuarios";

    //Role templates
    public static final String ROLE_FORM = "crud_role/role_form";
    public static final String ROLE_UPDATE_FORM = "crud_role/role_update_form";
    public static final String SHOW_ROLE_FORM = "crud_role/show_form";
    public static final String ROLE_LIST = "crud_role/list_roles";
    public static final String ROLE_DELETE = "crud_role/delete_role";

    //citas
    public static final String CITA_FORM_CREATE ="cita/form_create";
    public static final String CITA_FORM_DELETE = "cita/form_delete";
    public static final String CITA_LIST_PENDIENTES = "cita/list_citas_pendientes";
    public static final String CITA_LIST_REALIZADAS = "cita/list_cita_realizadas";
    public static final String CITA_FORM_EDIT  = "cita/form_edit";
    public static final String CITA_SHOW = "cita/show_cita";
    public static final String CITA_CANCEL="cita/form_cancel";
    public static final String CITA_CONTADOR  = "cita/contador_citas";

    //consultas
    public static final String CONSULTA_FORM = "consulta/form_create";
    public static final String CONSULTA_FORM_EDIT = "consulta/form_edit";
    public static final String CONSULTA_LIST = "consulta/list_consultas";
    public static final String CONSULTA_VIEW = "consulta/view_consulta";
    public static final String ESTADISTICA_VIEW = "consulta/estadisticas";
    public static final String SHOW_MOTIVOS_CONSULTA="consulta/show_motivos_consulta";
    public static final String SHOW_DIAGNOSTICO_CONSULTA="consulta/show_diagnostico_consulta";
    public static final String CONSULTA_LIST_EDIT = "consulta/list_consultas_edit";
    public static final String CONSULTA_RESUMEN_PERIODO="consulta/show_consulta_resumen_periodo";
    //historial medico

    //motivo consulta
    public static final String MOTIVO_FORM_CREATE ="motivo_consulta/form_create";
    public static final String MOTIVO_FORM_EDIT ="motivo_consulta/form_edit";
    public static final String MOTIVO_LIST ="motivo_consulta/list_motivos";

    //tipo examenes
    public static final String TIPO_EXAMEN_CREATE = "tipo_examen/form_create";
    public static final String TIPO_EXAMEN_EDIT = "tipo_examen/form_edit";
    public static final String LIST_TIPO_EXAMEN = "tipo_examen/list_tipo_examen";
    public static final String TIPO_EXAMEN_DELETE = "tipo_examen/form_delete";

    //Producto
    public static final String LIST_PRODUCTS = "crud_producto/productos";
    public static final String ALERT_STATUS_PRODUCTS ="crud_producto/form_status_alert";
    public static final String STATUS_PRODUCTS ="crud_producto/form_status";
    public static final String PRODUCTS="redirect:/producto/form_status";
    public static final String DELETE_PRODUCTS="crud_producto/form_delete";
    public static final String LIST_STATUS_CHANGE ="redirect:/productos";
    public static final String ADD_PRODUCTS = "crud_producto/form_create";
    public static final String LIST_PRODUCTS_ADD ="redirect:/productos";
    public static final String SHOW_PRODUCTS ="crud_producto/form_show";
    public static final String UPDATE_PRODUCTS ="crud_producto/form_update";
    public static final String PRODUCTS_FORM_EDIT = "crud_producto/form_edit";
    public static final String INDEX_PRODUCTS = "index/indexProducto";
    public static final String ESTADISTICAS = "crud_producto/stockmedicamento";
   
    //Existencia
    public static final String LIST_EXISTENCES = "crud_existencia/existencias";
    public static final String ADD_EXISTENCES = "crud_existencia/form_create";    
    public static final String SHOW_EXISTENCES ="crud_existencia/form_show";
    public static final String EXISTENCES_FORM_EDIT = "crud_existencia/form_edit";
    
    //Ingreso de productos
    public static final String LIST_ADDINGS = "crud_ingreso/ingresos";
    public static final String ADD_ADDINGS = "crud_ingreso/form_ingreso";    
    public static final String SHOW_ADDINGS ="crud_ingreso/ingreso_show";
    public static final String LIST_ADDINGS_TEMP = "crud_ingreso/ingresos_temp";
    public static final String SHOW_ADDINGS_TEMP = "crud_ingreso/ingreso_show_temp";
    public static final String ADDINGS_FORM_EDIT = "crud_ingreso/form_edit_ingreso";
    public static final String DELETE_ADDING = "crud_ingreso/delete_form";
    
    //Tipo de medicamentos
    public static final String LIST_TIPOS_MEDICAMENTO = "crud_tipo_medicamento/tipo_medicamentos";
    public static final String DELETE_TIPO_MEDICAMENTO="crud_tipo_medicamento/form_delete";
    public static final String LIST_TIPOMEDICAMENTO ="redirect:/tiposmedicamento";
    public static final String SHOW_TIPOS_MEDICAMENTO ="crud_tipo_medicamento/form_show";
    public static final String TIPOMEDICAMENTO_FORM_EDIT = "crud_tipo_medicamento/form_edit";


    //Expedientes
    public static final String LIST_EXPEDIENTES = "expediente_clinico/list_expediente_clinicos";
    public static final String SHOW_EXPEDIENTE = "expediente_clinico/show_expediente";
    public static final String LIST_CITAS_EXPEDIENTE="expediente_clinico/list_historial_medico";
    public static final String SHOW_HISTORIAL_MEDICO ="expediente_clinico/show_historial_medico";


    
    //Proveedores
    public static final String LIST_PROVEEDORES = "crud_proveedor/proveedores";
    public static final String ALERT_STATUS_PROVEEDORES ="crud_proveedor/form_status_alert";
    public static final String STATUS_PROVEEDORES ="crud_proveedor/form_status";
    public static final String PROVEEDORES="redirect:/proveedor/proveedor/form_status";
    public static final String DELETE_PROVEEDORES="crud_proveedor/form_delete";
    public static final String LIST_CHANGE_STATUS ="redirect:/proveedor/proveedores";
    public static final String ADD_PROVEEDORES = "crud_proveedor/form_create";
    public static final String LIST_PROVEEDORES_ADD ="redirect:/proveedor/proveedores";
    public static final String SHOW_PROVEEDORES ="crud_proveedor/form_show";
    public static final String PROVEEDORES_FORM_EDIT = "crud_proveedor/form_edit";
    public static final String INDEX_PROVEEDORES = "index/indexProveedor";
    
    //Marca
    public static final String LIST_MARCA = "crud_marca/marcas";
    public static final String DELETE_MARCA="crud_marca/form_delete";
    public static final String LIST_MARCAS ="redirect:/marca/marcas";
    public static final String SHOW_MARCA ="crud_marca/form_show";
    public static final String MARCA_FORM_EDIT = "crud_marca/form_edit";
    
    //Receta
    public static final String LIST_RECETA = "receta/recetas";
    public static final String DELETE_RECETAS="receta/form_delete";


    //Servicios clinica
    public static final String SERVICIO_CLINICA_CREATE="crud_servicios/form_create";
    public static final String LIST_SERVICIOS = "crud_servicios/list_servicios";
    public static final String SERVICIO_CLINICA_EDIT="crud_servicios/form_edit";
    public static final String SERVICIO_CLINICA_DELETE="crud_servicios/form_delete";
    
    //Citas negocio
    public static final String LIST_CITAS_NEGOCIO="crud_citas_negocio/list_citas";
    public static final String CREATE_UPDATE_CITAS_NEGOCIO="crud_citas_negocio/form_create_update";
    public static final String VIEW_CITAS_NEGOCIO="crud_citas_negocio/show_cita";
    

}
 
