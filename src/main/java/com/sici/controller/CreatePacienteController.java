package com.sici.controller;

import com.sici.constant.ViewConstant;
import com.sici.entity.Persona;
import com.sici.serviceImp.PacienteService;
import com.sici.serviceImp.PersonaService;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;



@Controller
@RequestMapping("/paciente")
public class CreatePacienteController {

    @Autowired
    @Qualifier("personaServiceImp")
    private PersonaService personaService;

    @Autowired
    @Qualifier("pacienteServiceImp")
    private PacienteService pacienteService;

    public String userAuthenticate(){
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return user.getUsername();
    }

    @GetMapping("/")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('WRITE_PACIENTE'))")    
    public String cancelRegister(Model model){
        model.addAttribute("username",userAuthenticate());
    	return ViewConstant.LIST_PATIENT;}

    @GetMapping("/form_register")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('WRITE_PACIENTE'))")   
    public String formRegister(Model model){
        model.addAttribute("username",userAuthenticate());
        DateFunction dateFunction = new DateFunction();
        model.addAttribute("date_today",dateFunction.convertDateToSpanish(dateFunction.dateToday()));
        model.addAttribute("persona",new Persona());
        model.addAttribute("result",model.asMap().get("result"));
        //System.out.println(model.addAttribute("result",model.asMap().get("result")));
        return ViewConstant.PACIENTE_FORM ;
    }

    @PostMapping("/add_paciente")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('WRITE_PACIENTE'))")   
    public String addPaciente(@Valid Persona persona, BindingResult result, Model model, RedirectAttributes rm){
        if (result.hasErrors()){
            java.util.Date utilDate = new java.util.Date();
            java.sql.Timestamp date_today = new java.sql.Timestamp(utilDate.getTime());
            model.addAttribute("date_today",date_today);
            return ViewConstant.PACIENTE_FORM;
        }
      //  Persona persona2 = personaService.addPersona(persona);
        int value=0;
        Persona persona2;
        String mensaje;
        try {
            persona2 = personaService.addPersona(persona);
            value = 1;
        }catch (ObjectNotFoundException e){
            persona2 = null;
        }

        if (null!= persona2 ){
           // System.out.println(rm.addAttribute("result",value));
            rm.addFlashAttribute("result",value);
            mensaje = "Paciente ingresado con exito";
        }else{
           // System.out.println(rm.addAttribute("result",value));
            rm.addFlashAttribute("result",value);
            mensaje = "Paciente no ingresado";
        }
        rm.addFlashAttribute("mensaje",mensaje);
        return "redirect:/pacientes";
    }


}
