package com.sici.controller;

import com.sici.constant.ViewConstant;
import com.sici.entity.*;
import com.sici.model.*;
import com.sici.reports.ConsultaPdfView;
import com.sici.reports.RecetaPdfView;
import com.sici.repository.*;
import com.sici.serviceImp.*;

import javassist.expr.NewArray;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.InputStreamResource;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.util.ResourceUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@Controller
@RequestMapping("/receta")

public class RecetaController {

	private ConsultaService consultaService;
	private RecetaService recetaService;

	@Autowired
	@Qualifier("recetaRepository")
	private RecetaRepository recetaRepository;

	@Autowired
	public void setRecetaService(RecetaService recetaService) {
		this.recetaService = recetaService;
	}

	@Autowired
	public void setConsultaService(ConsultaService consultaService) {
		this.consultaService = consultaService;
	}
	
	 @Autowired
	    @Qualifier("jdbcTemplate")
	    private JdbcTemplate jdbcTemplate;

	/*
	 * -Nombre de la función: GuardarReceta -Objetivo: Guardar producto -Fecha de
	 * creación: -Autor: Elizabeth Rodríguez -Fecha de última modificación -Autor de
	 * última modificación: Elizabeth Rodríguez
	 */

	/**
	 * New receta
	 *
	 * @param model
	 * @return
	 */
	@RequestMapping("/form_create/{id}")
	@PreAuthorize("isAuthenticated() and (hasAnyRole('ROLE_ADMIN') or hasAnyAuthority('WRITE_RECETA'))")
	public String nuevaReceta(@PathVariable Long id, Model model) throws Exception {
		System.out.println("1");

		model.addAttribute("consultas", consultaService.getConsultaById(id));

		model.addAttribute("receta", new Receta());
		// model.addAttribute("receta", recetaService.getRecetaById(id));
		System.out.println("Llenar formulario");
		return "receta/form_create";
	}

	/**
	 * Save receta
	 *
	 * @param model
	 * @return
	 */

	@PostMapping("/recetaGuardada/{id}")
	@PreAuthorize("isAuthenticated() and (hasAnyRole('ROLE_ADMIN') or hasAnyAuthority('READ_RECETA'))")
	public String recetaSave(@PathVariable Long id, Receta receta, Model model, RedirectAttributes rm)
			throws Exception {
		Consulta consulta = consultaService.getConsultaById(id);
		System.out.println("idconsulta obtenido");
		receta.setConsulta(consulta);
		Receta receta1 = recetaService.addReceta(receta);

		System.out.println("Consulta Guardada");
		return "redirect:/receta/view/"+receta1.getId();
	}

	/**
	 * Listado de recetas
	 *
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/recetas", method = RequestMethod.GET)
	@PreAuthorize("isAuthenticated() and (hasAnyRole('ROLE_ADMIN') or hasAnyAuthority('READ_RECETA'))")
	public String listadoRecetas(Model model) {

		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		model.addAttribute("username", user.getUsername());
		model.addAttribute("result", model.asMap().get("result_delete"));
		model.addAttribute("recetas", recetaService.listAllRecetasActivas());
		System.out.println("Listado de Recetas");
		return ViewConstant.LIST_RECETA;
	}

	/**
	 * Receta imprimir
	 *
	 * @param model
	 * @return
	 * @throws IOException
	 */

	@RequestMapping("/imprimir/{id}")
	@PostMapping
	@PreAuthorize("isAuthenticated() and (hasAnyRole('ROLE_ADMIN') or hasAnyAuthority('READ_RECETA'))")
	public void imprimirReceta(Receta id, HttpServletResponse response)
			throws SQLException, JRException, ClassNotFoundException, IOException {
  
		Map<String, Object> parameters = new HashMap<>();
		parameters.put("id", id.getId());
		Connection conexion = jdbcTemplate.getDataSource().getConnection();
		File file = ResourceUtils.getFile("classpath:templates/receta/imprimirReceta.jrxml");
		JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath());
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, conexion);
		response.setContentType("application/pdf");
		response.setHeader("Content-Disposition", "inline; filename=receta.pdf");
		final OutputStream outStream = response.getOutputStream();
		JasperExportManager.exportReportToPdfStream(jasperPrint, outStream);
		response.getOutputStream().flush();
		response.getOutputStream().close();
		System.out.println("Receta Impresa");
	}
	
	//RECETA IMPRIMIR
		@RequestMapping(value = "/pdfreceta/{id}",method = RequestMethod.GET,produces = MediaType.APPLICATION_PDF_VALUE)
	    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAnyAuthority('READ_RECETA'))")
	    public ResponseEntity<InputStreamResource> recetaReport(@PathVariable Long id) throws Exception{
	        Receta receta = null;
	        DateFunction dateFunction = new DateFunction();
	        String nombreReporte=null;
	        System.out.println("ID DE RECETA EN PDF"+id);
	        try {
	            receta = recetaService.getRecetaById(id);
	            if(receta==null){
	                return ResponseEntity.notFound().build();
	            }
	            nombreReporte = "inline; filename=Receta_"+receta.getConsulta().getCita().getExpediente().getCodigo()+"_"+dateFunction.convertDateToSpanish(new Date())+".pdf";       
	        }catch (DataAccessException e){
	            receta=null;
	        }
	        HttpHeaders headers = new HttpHeaders();
	        headers.add("Content-Disposition","inline; filename=Receta.pdf");
	        ByteArrayInputStream bis = RecetaPdfView.recetaReport(receta);
	        System.out.println("Receta generada");
	        return ResponseEntity.ok().headers(headers).contentType(MediaType.APPLICATION_PDF).body(new InputStreamResource(bis));
	    }

	/**
	 * Borrar receta
	 *
	 * @param id
	 * @param model
	 * @return
	 */

	@RequestMapping("/deleteForm/{id}")
	@PreAuthorize("isAuthenticated() and (hasAnyRole('ROLE_ADMIN') or hasAnyAuthority('DELETE_RECETA'))")
	public String ConfirmProductoDelete(@PathVariable Long id, Model model) {

		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		model.addAttribute("username", user.getUsername());
		model.addAttribute("receta", recetaService.getRecetaById(id));
		System.out.println("Preguntando si está seguro de realizar el cambio");
		return ViewConstant.DELETE_RECETAS;
	}

	@RequestMapping("/delete/{id}")
	@PreAuthorize("isAuthenticated() and (hasAnyRole('ROLE_ADMIN') or hasAnyAuthority('READ_RECETA'))")
	public String desactivarReceta(@PathVariable Long id, Model model, RedirectAttributes rm) {

		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		model.addAttribute("username", user.getUsername());
		Receta deleteReceta = recetaService.deleteReceta(recetaService.getRecetaById(id));

		if (null != deleteReceta) {
			rm.addFlashAttribute("result_delete", 1);
		} else {
			rm.addFlashAttribute("result_delete", 0);
		}
		System.out.println("Receta desactivada");
		return ViewConstant.LIST_RECETA;
	}

	/**
	 * Editar receta
	 *
	 * @param id
	 * @param model
	 * @return
	 */

	@RequestMapping("/edit/{id}")
	@PreAuthorize("isAuthenticated() and (hasAnyRole('ROLE_ADMIN') or hasAnyAuthority('WRITE_RECETA'))")
	public String editForm(@PathVariable Long id, Model model) {

		model.addAttribute("receta", recetaService.getRecetaById(id));

		System.out.println("Editar Receta");
		return "receta/form_edit";
	}

	@RequestMapping(value = "/actualizarReceta", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated() and (hasAnyRole('ROLE_ADMIN') or hasAnyAuthority('WRITE_RECETA'))")
	public String almacenar(Model model, Receta r, Long id) {
		System.out.println("Receta Editada 1");
		Receta receta = recetaService.getRecetaById(id);
		receta.setDetalle(r.getDetalle());
		receta.setRecomendacion(r.getRecomendacion());
		receta.setOtros(r.getOtros());
		receta.setEstado(true);
		System.out.println("Receta Editada 2");
		Receta re = recetaService.editReceta(recetaService.getRecetaById(id));
		if (re != null) {
			System.out.println("Receta Null");
		}
		System.out.println("Receta Editada");

		return "redirect:/receta/view/"+re.getId();
	}
	
	@RequestMapping("/view/{id}")
	public String viewReceta(@PathVariable Long id, Model model) {

		model.addAttribute("receta", recetaService.getRecetaById(id));

		System.out.println("Receta ver");
		return "receta/receta_show";
	}

}