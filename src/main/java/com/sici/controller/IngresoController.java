package com.sici.controller;

import com.sici.constant.ViewConstant;
import com.sici.entity.*;
import com.sici.repository.*;
import com.sici.serviceImp.*;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;


@Controller
@RequestMapping("/")


public class IngresoController {
	
	   private ProductoService productoService;
	   private IngresoService ingresoService;
	    
	   @Autowired
	    @Qualifier("ingresoTempRepository")
	    private IngresoTempRepository ingresoTempRepository;
	   
	   @Autowired
	    @Qualifier("productoRepository")
	    private ProductoRepository productoRepository;
	   
	   @Autowired
	    @Qualifier("movimientoRepository")
	    private MovimientoRepository movimientoRepository;

	   @Autowired
	    @Qualifier("ingresoRepository")
	    private IngresoRepository ingresoRepository;
	   
	    @Autowired
	    public void setProductoService(ProductoService productoService) {
	        this.productoService = productoService;
	    }
	    
	    @Autowired
	    public void setMarcaService(IngresoService ingresoService) {
	        this.ingresoService = ingresoService;
	    }
	    
		private static final Log LOG = LogFactory.getLog(UserController.class);
     
     /* 
     -Nombre de la función: newIngreso
     -Objetivo: Registrar ingreso de producto
     -Fecha de creación: 27/12/2019
     -Autor: Héctor Martínez
     -Fecha de última modificación 
     -Autor de última modificación: Héctor Martínez  
       */

     /**
      * View medicamento
      *
      * @param id
      * @param model
      * @return
      */ 
     @RequestMapping("/producto/ingresos/list/temp")
     @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('READ_INGRESO'))")
     public String listTempIngresos(Model model) {
    	User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();   	
    	List<IngresoTemp> listIngresos = ingresoService.listIngresoTemp();    	
    	LOG.info("METHOD: listTempIngresos() --PARAMS: ");    	
    	model.addAttribute("username", user.getUsername());
        model.addAttribute("ingresos", listIngresos);      
        return ViewConstant.LIST_ADDINGS_TEMP;
     }
 
     /* 
     -Nombre de la función: newIngreso
     -Objetivo: Registrar ingreso de producto
     -Fecha de creación: 27/12/2019
     -Autor: Héctor Martínez
     -Fecha de última modificación 
     -Autor de última modificación: Héctor Martínez  
       */

     /**
      * View medicamento
      *
      * @param id
      * @param model
      * @return
      */ 
     @RequestMapping("producto/ingreso")
     @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('WRITE_INGRESO'))")
     public String newIngreso(Model model) {
     	System.out.println("2");
    	User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();    	
    	LOG.info("METHOD: newIngreso() --PARAMS: ");
    	model.addAttribute("username",user.getUsername());
        model.addAttribute("ingreso", new Ingreso());
        model.addAttribute("productos", productoService.listAllProductosActivos());         
        System.out.println("Llenar formulario de ingreso");
        return ViewConstant.ADD_ADDINGS;
     }
     
     /**
      * View medicamento
      *
      * @param id
      * @param model
      * @return
      */
      @RequestMapping("producto/ingreso/show/{id}")
      @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('READ_INGRESO'))")
      public String showIngreso(@PathVariable Long id, Model model) {  	
     	User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();     	
     	LOG.info("METHOD: showIngreso(() --PARAMS: id = "+id);     	
     	model.addAttribute("username",user.getUsername());     	
        model.addAttribute("ingreso", ingresoRepository.findById(id)); 		
        return ViewConstant.SHOW_ADDINGS;
     }
      
      /* 
      -Nombre de la función: listIngresos
      -Objetivo: Mostrar listado de ingresos de productos
      -Fecha de creación: 27/12/2019
      -Autor: Héctor Martínez
      -Fecha de última modificación 
      -Autor de última modificación: Héctor Martínez  
        */

      /**
       * List Ingresos
       *
       * @param id
       * @param model
       * @return
       */
      @RequestMapping("producto/ingresos")
      @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('READ_INGRESO'))")
      public String listIngresos( Model model)   {
    	  User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();     	
    	  LOG.info("METHOD: listIngresos(() --PARAMS: ");      	
    	  model.addAttribute("username",user.getUsername());   
    	  model.addAttribute("ingresos", ingresoRepository.findAll());         
    	  return ViewConstant.LIST_ADDINGS;
      }
      
      /**
       * View medicamento
       *
       * @param id
       * @param model
       * @return
       */
       @GetMapping("producto/ingreso/show/temp/{id}")
       @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('READ_INGRESO'))")
       public String showIngresoTemp(@PathVariable Long id, Model model) {
    	   User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();        	
    	   IngresoTemp ingreso = new IngresoTemp();   	
    	   try {
    		   ingreso = ingresoTempRepository.findById(id);
        	   System.out.println("METHOD: showIngreso(() --INGRESO = "+ingreso.toString());
    	   }catch(Exception e) {
    		   e.printStackTrace();   		
    	   }    	

    	   LOG.info("METHOD: showIngreso(() --PARAMS: id = "+id);     	
    	   model.addAttribute("username",user.getUsername());   
    	   model.addAttribute("idTemp", 0);
    	   model.addAttribute("ingreso", ingreso);
    	   return ViewConstant.SHOW_ADDINGS_TEMP;
      }
      
      @PostMapping("/ingreso/save_temp")
      @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('WRITE_INGRESO'))")
      public String saveIngresoTemp(@ModelAttribute("ingreso") IngresoTemp ingreso, Model model) {
    	  IngresoTemp ingreso_ = ingresoTempRepository.findById(ingreso.getId());    	  
    	  if(ingreso_ != null) { System.out.print("\n\n Contenido de ingreso temp a guardar: \n"+ingreso_+"\n");}  		    	  
    	  LOG.info("METHOD: saveIngresoTemp(() --PARAMS: ");  
    	  System.out.print("Detallles: \n"+ingreso_.getDetalleIngresoList().toString());
    	  ingresoService.addIngreso(ingresoService.convertToIngreso(ingreso_), ingresoService.convertToDetalleIngreso(ingreso_.getDetalleIngresoList()));
    	  ingresoService.dropIngresoTemp(ingreso_.getId());  	  
    	  return "redirect:/producto/ingresos";
      }
      
      @GetMapping("producto/ingreso/edit/temp/{id}")
      @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('WRITE_INGRESO'))")
      public String editIngresoTemp(@PathVariable Long id, Model model) {
     	User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
     	model.addAttribute("username",user.getUsername());
     	try {
           model.addAttribute("ingreso", ingresoTempRepository.findById(id));
     	}catch(Exception e) {
     		e.printStackTrace();
     		model.addAttribute("ingreso", new IngresoTemp());
     	}
       model.addAttribute("productos", productoService.listAllProductosActivos());   
       return ViewConstant.ADDINGS_FORM_EDIT;
     }
      
      @RequestMapping("ingreso/deleteForm/{id}")
      @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('DELETE_INGRESO'))")
      public String ConfirmAddingDelete(@PathVariable Long id, Model model){
          User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
          model.addAttribute("username",user.getUsername());
          model.addAttribute("ingreso", ingresoService.getIngresoTemp(id));          
          return ViewConstant.DELETE_ADDING;
      }
      
      @RequestMapping("ingreso/delete/{id}")
      @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('DELETE_INGRESO'))")
      public String deleteProducto(@PathVariable Long id, Model model, RedirectAttributes rm){  	  
      	ingresoService.dropIngresoTemp(id);
        rm.addFlashAttribute("result_delete",1);      
        return "redirect:/producto/ingresos/list/temp";
      }
}


	    
	    
	    
	    
	    