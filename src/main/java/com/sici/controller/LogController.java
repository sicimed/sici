package com.sici.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.sici.serviceImp.LogService;

@Controller
@RequestMapping("/")
public class LogController {

	@Autowired
	@Qualifier("logServiceImp")
	private LogService logServiceImp;

	public String userAuthenticate() {
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return user.getUsername();
	}

	@RequestMapping(value = "/logs", method = RequestMethod.GET)
	@PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('READ_LOG'))")
	public String listLogs(Model model) {

		model.addAttribute("username", userAuthenticate());
		model.addAttribute("result", model.asMap().get("result_delete"));
		model.addAttribute("mensaje", model.asMap().get("mensaje"));
		model.addAttribute("logs", logServiceImp.retrieveAllLogs());

		return "log/logs";
	}

	@RequestMapping("/log/{id}")
	@PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('READ_LOG'))")
	public String editForm(@PathVariable Long id, Model model) {
		model.addAttribute("username", userAuthenticate());
		model.addAttribute("log", logServiceImp.getById(id));
		return "log/log_show";
	}
}
