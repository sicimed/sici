package com.sici.controller;

import com.sici.constant.ViewConstant;
import com.sici.entity.MotivoConsulta;
import com.sici.serviceImp.MotivoConsultaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
@SessionAttributes("motivo")
@RequestMapping("/motivos_consulta")
public class MotivoConsultaController {
    DateFunction dateFunction = new DateFunction();
    @Autowired
    @Qualifier("motivoConsultaService")
    private MotivoConsultaService motivoConsultaService;

    public String userAuthenticate(){
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return user.getUsername();
    }

    @GetMapping("/form_create")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('WRITE_MOTIVO_DE_CONSULTA'))")
    public String addRegister(Model model) {
        model.addAttribute("username",userAuthenticate());
        model.addAttribute("date_today", dateFunction.convertDateToSpanish(dateFunction.dateToday()));
        model.addAttribute("consulta", new MotivoConsulta());
        return ViewConstant.MOTIVO_FORM_CREATE;
    }

    @PostMapping("/add_motivo_consulta")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('WRITE_MOTIVO_DE_CONSULTA'))")
    public String addRegister(@Valid MotivoConsulta motivoConsulta, Model model, RedirectAttributes rm) {
        motivoConsulta.setEstado(true);
        motivoConsultaService.addMotivo(motivoConsulta);

        return "redirect:/motivos_consulta/list_motivo_consulta";
    }

    @GetMapping("/list_motivo_consulta")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('READ_MOTIVO_DE_CONSULTA'))")
    public String listMotivoConsulta(Model model) throws Exception {
        model.addAttribute("username",userAuthenticate());
        model.addAttribute("list_motivo", motivoConsultaService.listMotivoConsulta());
        return ViewConstant.MOTIVO_LIST;
    }

    @GetMapping("/edit_motivo/{id}")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('WRITE_MOTIVO_DE_CONSULTA'))")
    public String editMotivo(@PathVariable Long id, Model model) {
        model.addAttribute("username",userAuthenticate());
        model.addAttribute("motivo", motivoConsultaService.getMotivacionId(id));

        return ViewConstant.MOTIVO_FORM_EDIT;
    }

    @PostMapping("/edit_motivo/{id}")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('WRITE_MOTIVO_DE_CONSULTA'))")
    public String editMotivo(@Valid MotivoConsulta motivo, BindingResult result, @PathVariable Long id, Model model, RedirectAttributes rm, SessionStatus sessionStatus) {
        if (result.hasErrors()) {
            return ViewConstant.MOTIVO_FORM_EDIT;
        }
        System.out.println("ID DE MOTIVO DE CONSULTA------------------" + id);
        MotivoConsulta motivoConsulta = motivoConsultaService.getMotivacionId(id);
        motivoConsulta.setNombre(motivo.getNombre());
        motivoConsulta.setEstado(motivo.isEstado());
        motivoConsultaService.addMotivo(motivoConsulta);
        sessionStatus.setComplete();
        return "redirect:/motivos_consulta/list_motivo_consulta";
    }
}

