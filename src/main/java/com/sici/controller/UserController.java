package com.sici.controller;

import org.springframework.core.SpringVersion;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.sici.constant.ViewConstant;
import com.sici.repository.RoleRepository;
import com.sici.repository.UserRepository;
import com.sici.serviceImp.RoleService;
import com.sici.serviceImp.UserService;

@Controller
@RequestMapping("/users")
public class UserController {

    @Autowired
    @Qualifier("userService")
	private UserService userService;

    @Autowired
    @Qualifier("roleService")
	private RoleService roleService;
    
    @Autowired
    @Qualifier("roleRepository")
	private RoleRepository roleRepository;
    
    @Autowired
    @Qualifier("userRepository")
	private UserRepository userRepository;
    
	private static final Log LOG = LogFactory.getLog(UserController.class);
    
    @RequestMapping("")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('READ_USER'))")
    public String userList(Model model,
    		@RequestParam(name = "error", required = false) String error,
			@RequestParam(name = "logout", required = false) String logout) {
    	
    	User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	model.addAttribute("username",user.getUsername());
    	model.addAttribute("spring","version: " + SpringVersion.getVersion());
        model.addAttribute("date_today", new Date());
        model.addAttribute("users",userService.listUsers());   	
		LOG.info("METHOD: showLoginForm() --PARAMS: error ="+error+", logout: "+logout);
		model.addAttribute("error", error);
		model.addAttribute("logout", logout);        
    	return ViewConstant.USER_LIST;
    }
    
    
    @GetMapping("/add_user")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('WRITE_USER'))")
    public String formRegister(Model model,
    		@RequestParam(name = "error", required = false) String error,
			@RequestParam(name = "logout", required = false) String logout) {
    	
    	User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	model.addAttribute("username",user.getUsername());    	
        model.addAttribute("date_today",new  Date());
        model.addAttribute("userModel",new com.sici.entity.User());
        model.addAttribute("roles",roleRepository.findAll());     
		LOG.info("METHOD: showLoginForm() --PARAMS: error ="+error+", logout: "+logout);
		model.addAttribute("error", error);
		model.addAttribute("logout", logout);
		model.addAttribute("result", 2);
		model.addAttribute("update", 0);
        return ViewConstant.USER_FORM ;
    }
    
    @PostMapping("/add_user")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('WRITE_USER'))")
    public String addUser(@ModelAttribute(name="userModel") com.sici.entity.User userModel, Model model,
    		@RequestParam(name = "error", required = false) String error,
			@RequestParam(name = "logout", required = false) String logout) {    	
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String urlResult = "";       
        boolean result = userService.registerUser(userModel,0);
		
		if(result) {
			error = "El usuario:  "+userModel.getUsername()+" ha sido registrado existosamente.";
			model.addAttribute("result", 1);
	        model.addAttribute("userModel",userService.getUser(userModel.getUsername()));
	        urlResult = "redirect:/users";
		}else {
			error = "El usuario no ha sido registrado. El nombre de usuario pertenece a otro usuario existente.";
	        model.addAttribute("result",0);
	        model.addAttribute("userModel",userModel);
	        urlResult = ViewConstant.USER_FORM;
		}		
        model.addAttribute("date_today",new  Date());
        model.addAttribute("roles",roleRepository.findAll());
        model.addAttribute("username",user.getUsername());        
		LOG.info("METHOD: showLoginForm() --PARAMS: error ="+error+", logout: "+logout);
		model.addAttribute("error", error);
		model.addAttribute("logout", logout);
		model.addAttribute("update", 0);	
		return urlResult;
    }

    @GetMapping("/show_user/{username}")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('READ_USER'))")
    public String showUser(@PathVariable String username, Model model,
    		@RequestParam(name = "error", required = false) String error,
			@RequestParam(name = "logout", required = false) String logout) {
    	
    	User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	model.addAttribute("username",user.getUsername());       
		LOG.info("METHOD: showLoginForm() --PARAMS: error ="+error+", logout: "+logout);
        model.addAttribute("date_today",new  Date());
        model.addAttribute("userModel",userService.getUser(username)); 
        return ViewConstant.SHOW_USER_FORM ;
    }

    @GetMapping("/edit_user/{username}")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('WRITE_USER'))")
    public String editForm(@PathVariable String username, Model model,
    		@RequestParam(name = "error", required = false) String error,
			@RequestParam(name = "logout", required = false) String logout) {
    	
    	User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	model.addAttribute("username",user.getUsername());
    	
        try {
        	com.sici.entity.User usr = userService.getUser(username);

            model.addAttribute("userModel",usr);
            model.addAttribute("result",2);
            
    		LOG.info("METHOD: editForm() Roles --PARAMS: error ="+error+", logout: "+logout);

        }catch(Exception e) {
        	LOG.info("METHOD: editForm() Roles --PARAMS: error ="+e.getMessage()+", logout: "+logout);
            model.addAttribute("userModel",new com.sici.entity.User());
            model.addAttribute("result",0);
        }
        model.addAttribute("date_today",new  Date());
		model.addAttribute("error", error);
		model.addAttribute("logout", logout);
        model.addAttribute("roles",roleRepository.findAll());
		model.addAttribute("update", 1);
        return ViewConstant.UPDATE_FORM ;
    }
    
    @PostMapping("/edit_user/{username}")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('WRITE_USER'))")
    public String updateUser(@PathVariable String username,  Model model,
    		@ModelAttribute(name="userModel") com.sici.entity.User userModel, 
    		@RequestParam(name = "error", required = false) String error,
			@RequestParam(name = "logout", required = false) String logout) {
        boolean result = userService.registerUser(userModel,1);
    	String ruta;
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	model.addAttribute("username",user.getUsername());
		LOG.info("METHOD: showLoginForm() --PARAMS: error ="+error+", logout: "+logout);
		
		if(result) {
			ruta = "redirect:/users";
		}else {
			error = "El usuario no ha sido registrado. El nombre de usuario pertenece a otro usuario existente.";
	        model.addAttribute("roles",roleRepository.findAll());
	        model.addAttribute("result",0);
	        ruta = ViewConstant.UPDATE_FORM;
		}
        model.addAttribute("date_today",new  Date());
		model.addAttribute("error", error);
		model.addAttribute("logout", logout);		
		return ruta;
    }
    
    @GetMapping("/delete_user/{username}")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('DELETE_USER'))")
    public String deleteUser(@PathVariable String username, Model model,
    		@RequestParam(name = "error", required = false) String error,
			@RequestParam(name = "logout", required = false) String logout)  {
    	User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	model.addAttribute("username",user.getUsername());    	
    	if(!username.trim().equals("admin") && !username.trim().equals("user") && !username.equals(user.getUsername())) {
    		userService.deleteUser(username);	
    	}   	
		LOG.info("METHOD: showLoginForm() --PARAMS: error ="+error+", logout: "+logout);
		model.addAttribute("error", error);
		model.addAttribute("logout", logout);	
        return "redirect:/users";

    }
    
    
}
