package com.sici.controller;

import com.sici.entity.Persona;
import com.sici.serviceImp.PersonaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/")
public class DeletePacienteController {
    @Autowired
    @Qualifier("personaServiceImp")
    private PersonaService personaService;

    public void setPersonaService(PersonaService personaService) {
        this.personaService = personaService;
    }
    /**
     * Delete paciente
     *
     * @param id
     * @param model
     * @return
     */

    public String userAuthenticate(){
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return user.getUsername();
    }
   
    @RequestMapping("paciente/deleteForm/{id}")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('DELETE_PACIENTE'))")
    public String redirectPacienteDelete(@PathVariable Long id, Model model){
        model.addAttribute("username",userAuthenticate());
        model.addAttribute("persona", personaService.getPacienteById(id));
        return "crud_paciente/form_delete";
    }

    @RequestMapping("paciente/delete/{id}")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('DELETE_PACIENTE'))")
    public String deletePaciente(@PathVariable Long id, Model model, RedirectAttributes rm){
        model.addAttribute("username",userAuthenticate());
       //System.out.println("Impresion delete:"+id);
       //System.out.println("ver paciente"+personaService.getPacienteById(id));
       Persona deletePerson = personaService.deletePersona(personaService.getPacienteById(id));

       if(null != deletePerson){
           rm.addFlashAttribute("result",1);
       }else{
           rm.addFlashAttribute("result",0);
       }

        return "redirect:/pacientes";
    }
}
