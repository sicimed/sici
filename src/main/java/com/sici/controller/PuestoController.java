package com.sici.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.sici.entity.Puesto;
import com.sici.repository.PuestoRepository;

@Controller
@RequestMapping("/")
public class PuestoController 
{
    @Autowired
    @Qualifier("puestoRepository")
    private PuestoRepository puestoRepository;
    
	@RequestMapping (value = "/agregar_puesto", method = RequestMethod.GET)
	 @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAnyAuthority('WRITE_PUESTO'))")
	public String agregarPuesto(Model model) 
	{
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();//siempre va para obtener el usuario que ha iniciado sesión
    	model.addAttribute("username",user.getUsername());//siempre va para obtener el usuario que ha iniciado sesión
    	model.addAttribute("puesto",new Puesto());
    	
		return "crud_puesto/agregarPuesto";
	}
	
	
	@RequestMapping (value = "/guardar_puesto", method = RequestMethod.POST)
	@PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAnyAuthority('WRITE_PUESTO'))")
	public String guardarPuesto(@Valid Puesto puesto, BindingResult result,Model model) 
	{
		puesto.setCreatedAt(new java.util.Date());
		puesto.setUpdatedAt(new java.util.Date());
	
		Puesto p = this.puestoRepository.save(puesto);
				
		if(p!=null) 
		{
			model.addAttribute("result", 1);
			model.addAttribute("puesto",new Puesto());
		}
		else 
		{
			model.addAttribute("result", 0);
		}
		
		return "crud_puesto/agregarPuesto";
	}
    
	@RequestMapping (value = "/editar_Puesto", method = RequestMethod.POST)
	 @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAnyAuthority('WRITE_PUESTO'))")
	public String editarPuesto(@Valid Puesto puesto, BindingResult result,Model model) 
	{
		puesto.setCreatedAt(new java.util.Date());
		puesto.setUpdatedAt(new java.util.Date());
	
		Puesto p = this.puestoRepository.save(puesto);
				
		if(p!=null) 
		{
			model.addAttribute("result", 1);
			model.addAttribute("puesto",new Puesto());
		}
		else 
		{
			model.addAttribute("result", 0);
		}
		
		return "crud_puesto/editarPuesto";
	}
	
    
    
    @RequestMapping (value = "/listPuesto", method = RequestMethod.GET)
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAnyAuthority('READ_PUESTO'))")
	public String listPuesto(Model model) 
	{
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();//siempre va para obtener el usuario que ha iniciado sesión
    	model.addAttribute("username",user.getUsername());//siempre va para obtener el usuario que ha iniciado sesión
    	
    	Iterable<Puesto> puestos = puestoRepository.findAll(); // obtiene el listado de los puestos
    	
    	model.addAttribute("puestos", puestos);
    	
    
    	
    	
		return "crud_puesto/listPuesto";
	}
    
	@PostMapping (value = "/delete_puesto/{id}")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAnyAuthority('DELETE_PUESTO'))")
    public String redirectEmpleadoForm(@RequestParam Long id, Model model, RedirectAttributes rm){
    	User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	model.addAttribute("username",user.getUsername());
       Puesto deletePuesto= puestoRepository.findById(id).get();
       try {
           puestoRepository.delete(deletePuesto);
           deletePuesto = null;
       }catch(Exception e) {
    	   e.printStackTrace();
       }

       if(null == deletePuesto){
           model.addAttribute("result",1);
       }else{
    	   model.addAttribute("result",0);
       }
       model.addAttribute("puesto",deletePuesto);
        return "crud_puesto/form_delete";//"redirect:/listPuesto/";
    }
    
    @GetMapping("/delete_puesto/{id}")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAnyAuthority('DELETE_PUESTO'))")
    public String redirectEmpleadoDelete(@PathVariable Long id, Model model){
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        model.addAttribute("username",user.getUsername());
        Optional<Puesto> puesto = puestoRepository.findById(id);
        model.addAttribute("puesto", puesto.isPresent()?puesto.get():null);

        return "crud_puesto/form_delete";
    }
    
    
	@RequestMapping (value = "/editar_puesto/{id}", method = RequestMethod.GET)
	 @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAnyAuthority('WRITE_PUESTO'))")
	public String editarPuesto(@PathVariable Long id, Model model) 
	{
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();//siempre va para obtener el usuario que ha iniciado sesión
    	model.addAttribute("username",user.getUsername());//siempre va para obtener el usuario que ha iniciado sesión
    	
    	Optional<Puesto> puesto = this.puestoRepository.findById(id);
    	
    	model.addAttribute("puesto",puesto);
    	
		return "crud_puesto/editarPuesto";
	}
    

}
