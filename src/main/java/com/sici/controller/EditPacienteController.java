package com.sici.controller;

import javax.validation.Valid;

import com.sici.constant.ViewConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.sici.entity.*;
import com.sici.serviceImp.*;


@Controller
@SessionAttributes("persona")
@RequestMapping("/")
public class EditPacienteController {

    @Autowired
    @Qualifier("personaServiceImp")
    private PersonaService personaService;
    public String userAuthenticate(){
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return user.getUsername();
    }

    @RequestMapping("paciente/edit/{id}")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('WRITE_PACIENTE'))")
    public String editForm(@PathVariable Long id, Model model) {
        model.addAttribute("username",userAuthenticate());
        model.addAttribute("persona", personaService.getPacienteById(id));
       // System.out.println("Editar Paciente");
        return ViewConstant.PACIENTE_FORM_EDIT;
    }

    @RequestMapping(value = "paciente/save/", method = RequestMethod.POST)
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('WRITE_PACIENTE'))")
    public String editPersona(@Valid Persona persona, BindingResult result, Model model, RedirectAttributes rm, SessionStatus status) {
        if (result.hasErrors()) {
            return ViewConstant.PACIENTE_FORM_EDIT;
        }
        Persona per = personaService.editPersona(persona);
        if (per != null) {
            rm.addFlashAttribute("result", 1);
        } else {
            rm.addFlashAttribute("result", 0);
        }
        //System.out.println("Paciente Editado");

        status.setComplete();
        return "redirect:/paciente/" + per.getId();

    }
}
