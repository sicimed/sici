package com.sici.controller;

import com.sici.constant.ViewConstant;
import com.sici.serviceImp.ServicioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("/")
public class HelloWordController {

	@Autowired
	private ServicioService servicioService;

	@GetMapping("/")
	public String helloWorld(Model model) {
    	User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	model.addAttribute("username",user.getUsername());
		model.addAttribute("servicios",servicioService.findAll());
		model.addAttribute("hecho", model.asMap().get("hecho"));
		System.out.println("hellowcontroller----");
		System.out.println( model.asMap().get("hecho"));
		return ViewConstant.INDEX;
	}
	
	@GetMapping("/producto")
	public String inicioProductos(Model model) throws Exception {
    	User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	model.addAttribute("username",user.getUsername());
		model.addAttribute("servicios",servicioService.findAll());
		return ViewConstant.INDEX_PRODUCTS;
	}
	
	@GetMapping("/usuario")
	public String inicioUsuarios(Model model) {
    	User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	model.addAttribute("username",user.getUsername());
		model.addAttribute("servicios",servicioService.findAll());
		return ViewConstant.INDEX_USER;
	}

}
