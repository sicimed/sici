package com.sici.controller;

import com.sici.entity.Empleado;
import com.sici.entity.Persona;
import com.sici.repository.EmpleadoRepository;
import com.sici.repository.PersonaRepository;
import com.sici.serviceImp.ClinicaService;
import com.sici.serviceImp.EmpleadoServiceImp;
import com.sici.serviceImp.PersonaService;

import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
@RequestMapping("/empleados")
public class EmpleadoController {
    DateFunction dateFunction = new DateFunction();
    @Autowired
    @Qualifier("empleadoService")
    private EmpleadoServiceImp empleadoService;
    
    @Autowired
    @Qualifier("empleadoRepository")
    private EmpleadoRepository empleadoRepository;
    
    @Autowired
    @Qualifier("personaServiceImp")
    private PersonaService personaService;
    
    @Autowired
    @Qualifier("personaRepository")
    private PersonaRepository personaRepository;
    
    @Autowired
    @Qualifier("clinicaServiceImp")
    private ClinicaService clinicaService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('READ_EMPLEADO'))")
    public String listEmpleados(Model model) {

        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        model.addAttribute("username", user.getUsername());
        model.addAttribute("result", model.asMap().get("result"));
        model.addAttribute("mensaje",model.asMap().get("mensaje"));

        model.addAttribute("empleados", empleadoRepository.findAll());
        //System.out.println("Listado de empleados");
        return "crud_empleado/empleados";
    }

    @GetMapping("/form_register")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('WRITE_EMPLEADO'))")
    public String formRegister(Model model){
    	User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	model.addAttribute("username",user.getUsername());
        model.addAttribute("date_today",dateFunction.convertDateToSpanish(dateFunction.dateToday()));
        model.addAttribute("persona",new Persona());
        model.addAttribute("result",model.asMap().get("result"));
        //System.out.println(model.addAttribute("result",model.asMap().get("result")));
        return "crud_empleado/form_register" ;
    }
    @PostMapping("/add_empleado")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('WRITE_EMPLEADO'))")
    public String addEmpleado(@Valid Persona persona, BindingResult result, Model model, RedirectAttributes rm){
        if (result.hasErrors()){
            model.addAttribute("date_today",dateFunction.convertDateToSpanish(dateFunction.dateToday()));
            return "crud_empleado/form_register";
        }
      //  Persona persona2 = personaService.addPersona(persona);
    	User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	model.addAttribute("username",user.getUsername());
        int value=0;
        Persona persona2;
        String mensaje;
        Empleado empleado ;
        
        try {
        	persona.setTipo("empleado");
            persona2 = personaRepository.save(persona);
            
            empleado = new Empleado();
            empleado.setPersona(persona2);
            empleado.setClinica(clinicaService.getClinicaByCodigo("C1"));
            empleado = empleadoRepository.save(empleado);
            
            value = 1;
        }catch (ObjectNotFoundException e){
            persona2 = null;
        }             
        
        if (null!= persona2 ){
            //System.out.println(rm.addAttribute("result",value));
            rm.addFlashAttribute("result",value);
            mensaje = "Empleado ingresado con exito";
        }else{
        	value=0;
            //System.out.println(rm.addAttribute("result",value));
            rm.addFlashAttribute("result",value);
            mensaje = "Empleado no ingresado con exito";
        }
        rm.addFlashAttribute("mensaje",mensaje);
        return "redirect:/empleados/";
    }
    
    @RequestMapping("show/{id}")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('READ_EMPLEADO'))")
    public String showPersona(@PathVariable Long id, Model model) {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        model.addAttribute("username", user.getUsername());

        model.addAttribute("empleado", empleadoRepository.findById(id).get());
        
        if (model.asMap().get("result_edit") != null) {
            model.addAttribute("result", model.asMap().get("result_edit"));
        } else {
            model.addAttribute("result", 2);
        }

        return "crud_empleado/form_show";
    }
    
    @GetMapping("deleteForm/{id}")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('DELETE_EMPLEADO'))")
    public String redirectEmpleadoDelete(@PathVariable Long id, Model model){
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        model.addAttribute("username",user.getUsername());

        model.addAttribute("empleado", empleadoRepository.findById(id).get());

        return "crud_empleado/form_delete";
    }

    @PostMapping("deleteForm/{id}")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('DELETE_EMPLEADO'))")
    public String redirectEmpleadoForm(@RequestParam Long id, Model model, RedirectAttributes rm){
    	User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	model.addAttribute("username",user.getUsername());

       Empleado deleteEmpleado= empleadoRepository.findById(id).get();

       try {
           empleadoRepository.delete(deleteEmpleado);
          // deleteEmpleado = null;
       }catch(Exception e) {
    	   e.printStackTrace();
       }
       if(null == deleteEmpleado){
           model.addAttribute("result",1);
       }else{
    	   model.addAttribute("result",0);
       }
       model.addAttribute("empleado",deleteEmpleado);
        return "crud_empleado/form_delete";
    }
    
    @GetMapping("edit/{id}")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('WRITE_EMPLEADO'))")
    public String editForm(@PathVariable Long id, Model model) {
        model.addAttribute("persona", empleadoRepository.findById(id).get().getPersona());

        //System.out.println("Editar Empleado");
        return "crud_empleado/form_edit";
    }

    @RequestMapping(value = "edit/{id}", method = RequestMethod.POST)
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('WRITE_EMPLEADO'))")
    public String editEmpleado( @ModelAttribute(name="persona") @Valid Persona persona,@PathVariable Long id, BindingResult result, Model model, RedirectAttributes rm, SessionStatus status) {
        if (result.hasErrors()) {
            return "crud_empleado/form_edit";
        }
        persona.setId(id);
        Persona per = personaRepository.save(persona);
        if (per != null) {
            rm.addFlashAttribute("result_edit", 1);
        } else {
            rm.addFlashAttribute("result_edit", 0);
        }
        System.out.println("Empleado Editado");

        status.setComplete();
        return "redirect:/empleados/";

    }
    
}