package com.sici.controller;

import com.sici.constant.ViewConstant;
import com.sici.entity.*;
import com.sici.serviceImp.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/")


/* 
-Nombre de la función: listTipoMedicamento
-Objetivo: Mostrar el listado de tipo de medicamentos activos
-Fecha de creación: 
-Autor: Elizabeth Rodríguez
-Fecha de última modificación 
-Autor de última modificación: Elizabeth Rodríguez   
  */

public class TipoMedicamentoController {
	
	 private TipoMedicamentoService tipomedicamentoService;
	   


	 @Autowired
	 public void setTipoMedicamentoService(TipoMedicamentoService tipomedicamentoService) {
	 this.tipomedicamentoService = tipomedicamentoService;
	    }
	 

	  /**
     * Listado de tipos de medicamento
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/tiposmedicamento", method = RequestMethod.GET)
    @PreAuthorize("isAuthenticated() and (hasAnyRole('ROLE_ADMIN') or hasAnyAuthority('READ_TIPO_MEDICAMENTO'))")
    public String listTipoMedicamento(Model model) {
    	
    	User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	model.addAttribute("username",user.getUsername());
    	model.addAttribute("result",model.asMap().get("result_delete"));
        model.addAttribute("tiposmedicamento", tipomedicamentoService.listAllTiposMedicamento());
        System.out.println("Listado de Tipos de medicamento");
        return ViewConstant.LIST_TIPOS_MEDICAMENTO;
    }
    
    /* 
    -Nombre de la función: DeleteTipoMedicamento
    -Objetivo: Eliminar el tipo de medicamento
    -Fecha de creación: 
    -Autor: Elizabeth Rodríguez
    -Fecha de última modificación 
    -Autor de última modificación: Elizabeth Rodríguez   
      */
    
    /**
     * Desactivar tipo_medicamento
     *
     * @param id
     * @param model
     * @return
     */
   
    @RequestMapping("tipomedicamento/deleteForm/{id}")
    @PreAuthorize("isAuthenticated() and (hasAnyRole('ROLE_ADMIN') or hasAnyAuthority('DELETE_TIPO_MEDICAMENTO'))")
    public String ConfirmMedicamentoDelete(@PathVariable Long id, Model model){

        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        model.addAttribute("username",user.getUsername());
        model.addAttribute("tipomedicamento", tipomedicamentoService.getTipoMedicamentoById(id));
        System.out.println("Preguntando si está seguro de realizar el cambio");
        return ViewConstant.DELETE_TIPO_MEDICAMENTO;
    }
    /**
     * Delete tipo_medicamento
     *
     * @param id
     * @param model
     * @return
     */
  
    @RequestMapping("tipomedicamento/delete/{id}")
    @PreAuthorize("isAuthenticated() and (hasAnyRole('ROLE_ADMIN') or hasAnyAuthority('DELETE_TIPO_MEDICAMENTO'))")
    public String deleteTipoMedicamento(@PathVariable Long id, Model model, RedirectAttributes rm){

    	User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	model.addAttribute("username",user.getUsername());

    	tipomedicamentoService.deleteTipoMedicamento(id);
       System.out.println("Tipo de medicamento eliminado");
        return ViewConstant.LIST_TIPOMEDICAMENTO;
    }
    
    /* 
    -Nombre de la función: saveTipoMedicamento
    -Objetivo: Registrar el tipo de medicamento
    -Fecha de creación: 
    -Autor: Elizabeth Rodríguez
    -Fecha de última modificación 
    -Autor de última modificación: Elizabeth Rodríguez   
      */
    
    /**
     * New tipomedicamento.
     *
     * @param model
     * @return
     */
    @RequestMapping("tipomedicamento/new")
    @PreAuthorize("isAuthenticated() and (hasAnyRole('ROLE_ADMIN') or hasAnyAuthority('WRITE_TIPO_MEDICAMENTO'))")
    public String newTipoMedicamento(Model model) {
        model.addAttribute("tipomedicamento", new TipoMedicamento());
        System.out.println("Llenar formulario");
        return "crud_tipo_medicamento/form_create";
    }

    /**
     * Save tipo de medicamento 
     *
     * @param tipomedicamento
     * @return
     */
    @RequestMapping(value = "/guardar", method = RequestMethod.POST)
    @PreAuthorize("isAuthenticated() and (hasAnyRole('ROLE_ADMIN') or hasAnyAuthority('WRITE_TIPO_MEDICAMENTO'))")
    public String saveTipoMedicamento(TipoMedicamento tipomedicamento) {
        tipomedicamentoService.saveTipoMedicamento(tipomedicamento);
        System.out.println("Tipo de medicamento guardado");
        return ViewConstant.LIST_TIPOMEDICAMENTO;
    }
    
    /* 
    -Nombre de la función: showTipoMedicamento
    -Objetivo: Ver el tipo de medicamento
    -Fecha de creación: 
    -Autor: Elizabeth Rodríguez
    -Fecha de última modificación 
    -Autor de última modificación: Elizabeth Rodríguez   
      */

    /**
     * View tipo de medicamento
     *
     * @param id
     * @param model
     * @return
     */
     @RequestMapping("tipomedicamento/{id}")
     @PreAuthorize("isAuthenticated() and (hasAnyRole('ROLE_ADMIN') or hasAnyAuthority('READ_TIPO_MEDICAMENTO'))")
     public String showTipoMedicamento(@PathVariable Long id, Model model) {
    	
    	User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	model.addAttribute("username",user.getUsername());
        model.addAttribute("tipomedicamento", tipomedicamentoService.getTipoMedicamentoById(id));
		System.out.println(tipomedicamentoService.getTipoMedicamentoById(id));
        if(model.asMap().get("result_edit")!= null){
			model.addAttribute("result",model.asMap().get("result_edit"));
		}else{
			model.addAttribute("result",2);
		}
        return ViewConstant.SHOW_TIPOS_MEDICAMENTO;
    }
	 
    

}