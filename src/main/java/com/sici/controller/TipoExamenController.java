package com.sici.controller;

import com.sici.constant.ViewConstant;
import com.sici.entity.TipoExamen;
import com.sici.serviceImp.TipoExamenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
@RequestMapping("/tipo_examen")
public class TipoExamenController {

    @Autowired
    @Qualifier("tipoExamenService")
    private TipoExamenService tipoExamenService;
    public String userAuthenticate(){
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return user.getUsername();
    }
    @GetMapping("/form_create")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAnyAuthority('WRITE_TIPO_EXAMEN'))")
    public String addTipoExamen(Model model){
        model.addAttribute("username",userAuthenticate());
        DateFunction dateFunction = new DateFunction();
        model.addAttribute("date_today", dateFunction.convertDateToSpanish(dateFunction.dateToday()));
        model.addAttribute("tipoExamen",new TipoExamen());

        return ViewConstant.TIPO_EXAMEN_CREATE;
    }


    @PostMapping("/add_tipo_examen")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAnyAuthority('WRITE_TIPO_EXAMEN'))")
    public String addTipoExamen(@Valid TipoExamen tipoExamen, BindingResult result, Model model){
        tipoExamen.setEstado(true);
        tipoExamenService.addTipoExamen(tipoExamen);
        return "redirect:/tipo_examen/list_tipo_examen";
    }

    @GetMapping("/edit_texamen/{id}")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAnyAuthority('WRITE_TIPO_EXAMEN'))")
    public String editTipoExamen(@PathVariable Long id, Model model){
        model.addAttribute("username",userAuthenticate());
        model.addAttribute("tipo_examen",tipoExamenService.getTipoExamenId(id));
        return ViewConstant.TIPO_EXAMEN_EDIT;
    }

    @PostMapping("/edit_texamen/{id}")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAnyAuthority('WRITE_TIPO_EXAMEN'))")
    public String editTipoExamen(@Valid TipoExamen tipoExamen,BindingResult result, Model model, @PathVariable Long id){
        TipoExamen tipoExamen1 = tipoExamenService.getTipoExamenId(id);
        tipoExamen1.setNombre(tipoExamen.getNombre());
        tipoExamen1.setEstado(tipoExamen.isEstado());
        tipoExamenService.addTipoExamen(tipoExamen1);

        return "redirect:/tipo_examen/list_tipo_examen";
    }

    @GetMapping("/list_tipo_examen")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAnyAuthority('READ_TIPO_EXAMEN'))")
    public String listTipoExamen(Model model) throws Exception{
        model.addAttribute("username",userAuthenticate());
        model.addAttribute("list_texamen",tipoExamenService.listTipoExamen());
        model.addAttribute("result",model.asMap().get("result_edit"));
        model.addAttribute("mensaje",model.asMap().get("mensaje"));
        return ViewConstant.LIST_TIPO_EXAMEN;
    }

    @GetMapping("/delete/{id}")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAnyAuthority('DELETE_TIPO_EXAMEN'))")
    public String deleteTipoExamen(@PathVariable Long id,Model model){
        model.addAttribute("username",userAuthenticate());
        model.addAttribute("tipoExamen",tipoExamenService.getTipoExamenId(id));
        return ViewConstant.TIPO_EXAMEN_DELETE;
    }

    @RequestMapping("/delete/save/{id}")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAnyAuthority('DELETE_TIPO_EXAMEN'))")
    public String deleteTipoExamen(@PathVariable Long id, Model model, RedirectAttributes rm){
        String mensaje;
        int bandera;
        if (tipoExamenService.deleteTipoExamen(id) == 1) {
            bandera=1;
            mensaje = "TIpo de examen eliminado con exito";
        } else {
            bandera=0;
            mensaje = "Tipo de examen no pudo ser eliminado";
        }

        rm.addFlashAttribute("result_edit", bandera);
        rm.addFlashAttribute("mensaje",mensaje);

        return "redirect:/tipo_examen/list_tipo_examen";
    }




}
