package com.sici.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.sici.constant.ViewConstant;
import com.sici.entity.Role;
import com.sici.entity.Permission;
import com.sici.entity.Puesto;
import com.sici.repository.PermissionRepository;
import com.sici.serviceImp.RoleService;

@Controller
@RequestMapping("/roles")
public class RoleController {

    @Autowired
    @Qualifier("roleService")
	private RoleService roleService;
    
    @Autowired
    @Qualifier("permissionRepository")
	private PermissionRepository permissionRepository;
	
	private static final Log LOG = LogFactory.getLog(UserController.class);
    
    @RequestMapping("")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAnyAuthority('READ_ROLE'))")
    public String roleList(Model model,
    		@RequestParam(name = "error", required = false) String error,
			@RequestParam(name = "logout", required = false) String logout) {
    	
    	User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	model.addAttribute("username",user.getUsername());
    	
        Date date_today = new Date();
        model.addAttribute("date_today",date_today);
        model.addAttribute("roles",roleService.listRoles());

		LOG.info("METHOD: showLoginForm() --PARAMS: error ="+error+", logout: "+logout);
		model.addAttribute("error", error);
		model.addAttribute("logout", logout);
    	return ViewConstant.ROLE_LIST;
    }
    
    
    @GetMapping("/add_role")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAnyAuthority('WRITE_ROLE'))")
    public String formRegister(Model model){
    	
    	User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	model.addAttribute("username",user.getUsername());
    	
        Date date_today = new Date();
        model.addAttribute("date_today",date_today);
        model.addAttribute("roleModel",new Role());
        model.addAttribute("result", -1); 
        model.addAttribute("permissions",permissionRepository.findAll()); 
        
        return ViewConstant.ROLE_FORM ;
    }
    
    @PostMapping("/add_role")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAnyAuthority('WRITE_ROLE'))")
    public String addRole(@ModelAttribute(name="roleModel") Role roleModel, Model model){
    	
    	User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Date date_today = new Date();
    	
    	try {
    		roleService.registerRole(roleModel);
        	model.addAttribute("result", 1); 
    	}catch(Exception e) {
    		model.addAttribute("result", 0); 
    	}
    	
    	model.addAttribute("username",user.getUsername());
        model.addAttribute("date_today",date_today);
        model.addAttribute("roleModel",new Role());
        model.addAttribute("permissions",permissionRepository.findAll()); 

    	return ViewConstant.ROLE_FORM ;
    }

    @GetMapping("/show_role/{id}")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAnyAuthority('READ_ROLE'))")
    public String showRole(@PathVariable long id, Model model) {
    	
    	User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	Role role = new Role();
    	
        try {
    		role = roleService.getRole(id);
    		if(role == null) {
    			model.addAttribute("result",0);
    			model.addAttribute("error", " Rol no encontrado.");
    		}else {
    			model.addAttribute("result",-1);
    		}
    	}catch(Exception e) {
    		model.addAttribute("error", e.getMessage());
    		model.addAttribute("result",0);
    	}
        
    	List<Permission> targetList = new ArrayList<>(role.getPermissions());
    	Date date_today = new Date();
    	String lista  = "";
    	
    	try {
        	for(Permission pe : targetList) {
        		lista+=pe.toString();
        	}
    	}catch(Exception e) {
    		lista=e.getMessage();
    	}

    	model.addAttribute("username",user.getUsername());
        model.addAttribute("date_today",date_today);
        model.addAttribute("roleModel",role);
        model.addAttribute("lista",lista);
        model.addAttribute("permissions",targetList);
        
        return ViewConstant.SHOW_ROLE_FORM ;
    }

    @GetMapping("/edit_role/{id}")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAnyAuthority('WRITE_ROLE'))")
    public String editForm(@PathVariable Long id, Model model,
    		@RequestParam(name = "error", required = false) String error,
			@RequestParam(name = "logout", required = false) String logout) {
    	
    	User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	Date date_today = new Date();
    	Role role = new Role();
    	String activos = "";
    	
        try {
    		role = roleService.getRole(id);
    		if(role == null) {
    			model.addAttribute("result",0);
    			model.addAttribute("error", " Rol no encontrado.");
    		}else {
    			model.addAttribute("result",-1);
    		}
    	}catch(Exception e) {
    		model.addAttribute("error", e.getMessage());
    		model.addAttribute("result",0);
    	}
        
        Set<Role> setRole = new HashSet<Role>();
        setRole.add(role);
    	//String[] activos = new String[role.getPermissions().size()];
    	for(Permission p: role.getPermissions()) {
    		activos+= p.getId().toString()+',';
    	}
        LOG.info("METHOD: editForm() --PARAMS: error ="+error+", logout: "+logout);
    	
        //model.addAttribute("permissions",permissionRepository.findByRolesNot(setRole)); 
        model.addAttribute("permissions",permissionRepository.findAll()); 
        model.addAttribute("username",user.getUsername());
        model.addAttribute("date_today",date_today);
        model.addAttribute("roleModel",role);
        model.addAttribute("logout", logout);
        model.addAttribute("activos",activos);
        //model.addAttribute("cant",activos.length);
        
        return ViewConstant.ROLE_UPDATE_FORM;
    }
    
    @PostMapping("/edit_role/{id}")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAnyAuthority('WRITE_ROLE'))")
    public String updateRole(@ModelAttribute(name="roleModel") Role roleModel,@PathVariable Long id, Model model,
    		@RequestParam(name = "error", required = false) String error,
			@RequestParam(name = "logout", required = false) String logout){
        
    	User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Set<Role> setRole = new HashSet<Role>();
        setRole.add(roleModel);
    	boolean result;
        Date date_today = new Date();
        
    	try {
    		result = roleService.registerRole(roleModel,1);
    		if(result) {
    			model.addAttribute("result",1);
    		}else {
    			model.addAttribute("result",0);
    			model.addAttribute("error", "");
    		}
    	}catch(Exception e) {
    		model.addAttribute("error", e.getMessage());
    		model.addAttribute("result",0);
    	}
    	
    	LOG.info("METHOD: updateRole() --PARAMS: error ="+error+", logout: "+logout);
    	
        model.addAttribute("permissions",permissionRepository.findAll()); 
    	model.addAttribute("username",user.getUsername());    	
        model.addAttribute("date_today",date_today);
        model.addAttribute("roleModel",roleModel);
		model.addAttribute("logout", logout);
		
		return ViewConstant.ROLE_UPDATE_FORM;
    }
    
    @GetMapping("/delete_role/{id}")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAnyAuthority('DELETE_ROLE'))")
    public String getDeleteRole(@PathVariable Long id, Model model) {
    	User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	Role role = new Role();
    	
        try {
    		role = roleService.getRole(id);
    		if(role == null) {
    			model.addAttribute("result",0);
    			model.addAttribute("error", " Rol no encontrado.");
    		}else {
    			model.addAttribute("result",-1);
    		}
    	}catch(Exception e) {
    		model.addAttribute("error", e.getMessage());
    		model.addAttribute("result",0);
    	}
    	
    	model.addAttribute("username",user.getUsername());
    	model.addAttribute("role",role);
    	
        return ViewConstant.ROLE_DELETE;

    }
    
    @PostMapping("/delete_role/{id}")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAnyAuthority('DELETE_ROLE'))")
    public String postDeleteRole(@RequestParam Long id, Model model) {

    	User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	model.addAttribute("username",user.getUsername());
       Role deleteRol= roleService.getRole(id);
       try {
       		roleService.deleteRole(id);
           deleteRol = null;
       }catch(Exception e) {
    	   e.printStackTrace();
       }

       if(null == deleteRol){
           model.addAttribute("result",1);
       }else{
    	   model.addAttribute("result",0);
       }
       model.addAttribute("role",deleteRol);
       return ViewConstant.ROLE_DELETE;

    }
    
}
