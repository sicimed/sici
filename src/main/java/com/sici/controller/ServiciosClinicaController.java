package com.sici.controller;

import com.sici.constant.ViewConstant;
import com.sici.entity.ServiciosOfrecidos;
import com.sici.serviceImp.ClinicaServiceImp;
import com.sici.serviceImp.ServicioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
@RequestMapping("/servicios_clinica")
public class ServiciosClinicaController {

    @Autowired
    @Qualifier("servicioClinicaImp")
    private ServicioService servicioService;

    @Autowired
    private ClinicaServiceImp clinicaServiceImp;
    public String userAuthenticate(){
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return user.getUsername();
    }
    @GetMapping("/form_servicio")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('WRITE_SERVICIOS_OFRECIDOS'))")
    public String createServicio(Model model){
        model.addAttribute("username",userAuthenticate());
        model.addAttribute("servicio", new ServiciosOfrecidos());
        return ViewConstant.SERVICIO_CLINICA_CREATE;
    }

    @PostMapping("/form_servicio/save")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('WRITE_SERVICIOS_OFRECIDOS'))")
    public String saveServicio(@Valid ServiciosOfrecidos serviciosOfrecidos, BindingResult result, Model model, RedirectAttributes rm){
        serviciosOfrecidos.setClinica(clinicaServiceImp.getClinicaByCodigo("C1"));
        servicioService.addServicio(serviciosOfrecidos);

        return "redirect:/servicios_clinica/list_servicios";
    }

    @GetMapping("/list_servicios")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('READ_SERVICIOS_OFRECIDOS'))")
    public String listServicios(Model model){
        model.addAttribute("username",userAuthenticate());
        model.addAttribute("list_servicios",servicioService.findAll());
        return ViewConstant.LIST_SERVICIOS;
    }

    @GetMapping("/form_edit/{id}")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('WRITE_SERVICIOS_OFRECIDOS'))")
    public String editService(@PathVariable Long id, Model model){
        model.addAttribute("username",userAuthenticate());
        model.addAttribute("servicio",servicioService.findById(id));
        return ViewConstant.SERVICIO_CLINICA_EDIT;
    }

    @PostMapping("/form_edit/save/{id}")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('WRITE_SERVICIOS_OFRECIDOS'))")
    public String editSaveService(@Valid ServiciosOfrecidos serviciosOfrecidos,BindingResult bindingResult, @PathVariable Long id,Model model,RedirectAttributes rm){
        ServiciosOfrecidos serviciosOfrecidos1 = servicioService.findById(id);
        serviciosOfrecidos1.setNombre(serviciosOfrecidos.getNombre());
        servicioService.addServicio(serviciosOfrecidos1);

        return "redirect:/servicios_clinica/list_servicios";
    }

    @GetMapping("/form_delete/{id}")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('DELETE_SERVICIOS_OFRECIDOS'))")
    public String formDeleteService(@PathVariable Long id,Model model){
        model.addAttribute("username",userAuthenticate());
        ServiciosOfrecidos serviciosOfrecidos1 = servicioService.findById(id);
        model.addAttribute("servicio",serviciosOfrecidos1);

        return ViewConstant.SERVICIO_CLINICA_DELETE;
    }

    @GetMapping("/delete/servicio/{id}")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('DELETE_SERVICIOS_OFRECIDOS'))")
    public String deleteService(@PathVariable Long id,Model model,RedirectAttributes rm){
        model.addAttribute("username",userAuthenticate());
        servicioService.deleteById(id);
        return "redirect:/servicios_clinica/list_servicios";
    }


}
