package com.sici.controller;

import com.sici.constant.ViewConstant;
import com.sici.entity.*;
import com.sici.repository.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class ExistenciaController {

	   @Autowired
	    @Qualifier("existenciaRepository")
	    private ExistenciaRepository existenciaRepository;
	   
	   @Autowired
	    @Qualifier("productoRepository")
	    private ProductoRepository productoRepository;
	   
	   @Autowired
	    @Qualifier("movimientoRepository")
	    private MovimientoRepository movimientoRepository;

	   @Autowired
	    @Qualifier("ingresoRepository")
	    private IngresoRepository ingresoRepository;
	  
	    /* 
	    -Nombre de la función: Listado de productos
	    -Objetivo: Mostrar el listado de productos activos
	    -Fecha de creación: 
	    -Autor: Elizabeth Rodríguez
	    -Fecha de última modificación 
	    -Autor de última modificación: Elizabeth Rodríguez   
	      */

	
	  /**
     * Listado de productos
     *
     * @param model
     * @return
     */
    @RequestMapping("existencias")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('READ_EXISTENCIA'))")
    public String listExistencias(Model model) {    	
    	User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	model.addAttribute("username",user.getUsername());
    	
        model.addAttribute("listEx", existenciaRepository.findAll());

        return ViewConstant.LIST_EXISTENCES;
    }
    
    @RequestMapping("existencia/create")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('WRITE_EXISTENCIA'))")
    public String form_create(Model model) {    	
    	User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	model.addAttribute("username",user.getUsername());
    	
        model.addAttribute("ex", new Existencia());
        model.addAttribute("productos", productoRepository.findAll());
       
        System.out.println("Listado de Existencias");
        return ViewConstant.ADD_EXISTENCES;
    }	    
}


	    
	    
	    
	    
	    