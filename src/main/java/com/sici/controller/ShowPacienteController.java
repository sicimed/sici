package com.sici.controller;

import com.sici.serviceImp.PersonaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
//Se puede usar en los metodos o en clases, puedo usar and y permitAll()
@PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('READ_PACIENTE'))")
public class ShowPacienteController {
    @Autowired
    @Qualifier("personaServiceImp")
    private PersonaService personaService;
    public String userAuthenticate(){
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return user.getUsername();
    }
    @RequestMapping("paciente/{id}")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('READ_PACIENTE'))")
    public String showPersona(@PathVariable Long id, Model model) {
        model.addAttribute("username",userAuthenticate());
       // System.out.println("ID PARA ESTE PACIENTE ES:" + id);
        model.addAttribute("persona", personaService.getPacienteById(id));
        System.out.println(personaService.getPacienteById(id));
        if (model.asMap().get("result_edit") != null) {
            model.addAttribute("result", model.asMap().get("result"));
        } else {
            model.addAttribute("result", 2);
        }

        return "crud_paciente/form_show";
    }


}