package com.sici.controller;

import com.sici.constant.ViewConstant;
import com.sici.entity.*;
import com.sici.serviceImp.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


@Controller
@RequestMapping("/marca")


public class MarcaController {
	
	 private MarcaService marcaService;
	   


	 @Autowired
	 public void setMarcaService(MarcaService marcaService) {
	 this.marcaService = marcaService;
	    }

	 /* 
	 -Nombre de la función: listMarca
	 -Objetivo: Mostrar el listado de marcas
	 -Fecha de creación: 
	 -Autor: Elizabeth Rodríguez
	 -Fecha de última modificación 
	 -Autor de última modificación: Elizabeth Rodríguez   
	   */

	  /**
     * Listado de tipos de marca
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/marcas", method = RequestMethod.GET)
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('READ_MARCA'))")
    public String listMarca(Model model) {
    	
    	User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	model.addAttribute("username",user.getUsername());
    	model.addAttribute("result",model.asMap().get("result_delete"));
        model.addAttribute("marcas", marcaService.listAllMarca());
        System.out.println("Listado de Tipos de marca");
        return ViewConstant.LIST_MARCA;
    }
    
    /* 
    -Nombre de la función: DeleteMarca
    -Objetivo: Eliminar el tipo de medicamento
    -Fecha de creación: 
    -Autor: Elizabeth Rodríguez
    -Fecha de última modificación 
    -Autor de última modificación: Elizabeth Rodríguez   
      */
    
    /**
     * Desactivar marca
     *
     * @param id
     * @param model
     * @return
     */
   
    @RequestMapping("marca/deleteForm/{id}")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('DELETE_MARCA'))")
    public String ConfirmMarcaDelete(@PathVariable Long id, Model model){

        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        model.addAttribute("username",user.getUsername());
        model.addAttribute("marca", marcaService.getMarcaById(id));
        System.out.println("Preguntando si está seguro de realizar el cambio");
        return ViewConstant.DELETE_MARCA;
    }
    /**
     * Delete tipo_medicamento
     *
     * @param id
     * @param model
     * @return
     */
  
    @RequestMapping("marca/delete/{id}")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('DELETE_MARCA'))")
    public String DeleteMarca(@PathVariable Long id, Model model, RedirectAttributes rm){

    	User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	model.addAttribute("username",user.getUsername());
        marcaService.deleteMarca(id);
       System.out.println("Marca eliminada");
        return ViewConstant.LIST_MARCAS;
    }
    
   
    /* 
    -Nombre de la función: showMarca
    -Objetivo: Ver marca
    -Fecha de creación: 
    -Autor: Elizabeth Rodríguez
    -Fecha de última modificación 
    -Autor de última modificación: Elizabeth Rodríguez   
      */

    /**
     * View marca
     *
     * @param id
     * @param model
     * @return
     */
     @RequestMapping("marca/{id}")
     @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('READ_MARCA'))")
     public String showMarca(@PathVariable Long id, Model model) {
    	
    	User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	model.addAttribute("username",user.getUsername());
    	model.addAttribute("marca", marcaService.getMarcaById(id));
    	System.out.println(marcaService.getMarcaById(id));
        if(model.asMap().get("result_edit")!= null){
			model.addAttribute("result",model.asMap().get("result_edit"));
		}else{
			model.addAttribute("result",2);
		}
        return ViewConstant.SHOW_MARCA;
    }
     
     /* 
     -Nombre de la función: Guardar marca
     -Objetivo: Guardar proveedor
     -Fecha de creación: 
     -Autor: Elizabeth Rodríguez
     -Fecha de última modificación 
     -Autor de última modificación: Elizabeth Rodríguez   
       */
     
     
     /**
      * New proveedor.
      *
      * @param model
      * @return
      */
     @RequestMapping("/new")
     @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('WRITE_MARCA'))")
     public String newMarca(Model model) {
     	System.out.println("1");
         model.addAttribute("marca", new Marca());
         System.out.println("Llenar formulario");
         return "crud_marca/form_create";
     }

     /**
      * Save proveedor
      *
      * @param proveedor
      * @return
      */
     @PostMapping("/add_marca")
     @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('WRITE_MARCA'))")
     public String saveMarca(Marca marca, Model model) {
         marcaService.saveMarca(marca);
         System.out.println("Marca guardada");
         return "redirect:/marca/marcas";
     }
     
   


	 
    

}