package com.sici.controller;

import com.sici.entity.AsignacionPuesto;
import com.sici.repository.AsignacionPuestoRepository;
import com.sici.repository.ClinicaRepository;
import com.sici.repository.EmpleadoRepository;
import com.sici.repository.PuestoRepository;

import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
@RequestMapping("/asignacionpuesto")
public class AsignacionPuestoController {
    
    @Autowired
    @Qualifier("empleadoRepository")
    private EmpleadoRepository empleadoRepository;

    @Autowired
    @Qualifier("puestoRepository")
    private PuestoRepository puestoRepository;
    
    @Autowired
    @Qualifier("asignacionPuestoRepository")
    private AsignacionPuestoRepository asignacionPuestoRepository;
    
    @Autowired
    @Qualifier("clinicaRepository")
    private ClinicaRepository clinicaRepository;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('READ_ASIGNACION_PUESTO'))")
    public String listAsignaciones(Model model) {

        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        model.addAttribute("username", user.getUsername());
        model.addAttribute("result", model.asMap().get("result_delete"));
        model.addAttribute("mensaje",model.asMap().get("mensaje"));

        model.addAttribute("asignaciones", asignacionPuestoRepository.findAll());
        //System.out.println("Listado de Asignaciones de puestos");
        return "crud_asignar_puesto/asignaciones";
    }

    @GetMapping("/form_register")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('WRITE_ASIGNACION_PUESTO'))")
    public String formRegister(Model model){
    	
    	User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	model.addAttribute("username",user.getUsername());

       //Timestamp date_today = new Timestamp();
        //Date date_today = new Date();
        java.util.Date utilDate = new java.util.Date();
        java.sql.Timestamp date_today = new java.sql.Timestamp(utilDate.getTime());
        model.addAttribute("date_today",date_today);
        model.addAttribute("asignacion",new AsignacionPuesto());
        model.addAttribute("empleados",empleadoRepository.findAll());
        model.addAttribute("puestos",puestoRepository.findAll());
        model.addAttribute("result",model.asMap().get("result"));
        System.out.println(model.addAttribute("result",model.asMap().get("result")));
        return "crud_asignar_puesto/form_register" ;
    }
    @PostMapping("/add_asignacion") 
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('WRITE_ASIGNACION_PUESTO'))")
    public String addAsignacion( @Valid AsignacionPuesto asignacion,BindingResult result,Model model, RedirectAttributes rm){
       
    	if (result.hasErrors()){
        	User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        	model.addAttribute("username",user.getUsername());

           //Timestamp date_today = new Timestamp();
            //Date date_today = new Date();
            java.util.Date utilDate = new java.util.Date();
            java.sql.Timestamp date_today = new java.sql.Timestamp(utilDate.getTime());
            model.addAttribute("date_today",date_today);
            model.addAttribute("asignacion",new AsignacionPuesto());
            model.addAttribute("empleados",empleadoRepository.findAll());
            model.addAttribute("puestos",puestoRepository.findAll());
            model.addAttribute("result",model.asMap().get("result"));
            System.out.println(model.addAttribute("result",model.asMap().get("result")));
            return "crud_asignar_puesto/form_register" ;
        }
    	User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	model.addAttribute("username",user.getUsername());
        int value=0;
        AsignacionPuesto asignacion2;
        String mensaje;
        
        try {
        	asignacion2 = asignacionPuestoRepository.save(asignacion);
            
        }catch (ObjectNotFoundException e){
        	asignacion2  = null;
        }             
        
        if (null!= asignacion2 ){
        	value = 1;
            System.out.println(rm.addAttribute("result",value));
            rm.addFlashAttribute("result_delete",value);
            mensaje = "Asignacion ingresada con exito";
        }else{
            System.out.println(rm.addAttribute("result",value));
            rm.addFlashAttribute("result_delete",value);
            mensaje = "Asignacion ingresada fallida";
        }
        rm.addFlashAttribute("mensaje",mensaje);
        return "redirect:/asignacionpuesto/";
    }
    
    @RequestMapping("show/{id}")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('READ_ASIGNACION_PUESTO'))")
    public String showAsignacion(@PathVariable Long id, Model model) {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        model.addAttribute("username", user.getUsername());

        model.addAttribute("asignacion", asignacionPuestoRepository.findById(id));
        
        if (model.asMap().get("result_edit") != null) {
            model.addAttribute("result", model.asMap().get("result_edit"));
        } else {
            model.addAttribute("result", 2);
        }

        return "crud_asignar_puesto/form_show";
    }
    
    @RequestMapping("deleteForm/{id}")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('DELETE_ASIGNACION_PUESTO'))")
    public String redirectAsignacionDelete(@PathVariable Long id, Model model){
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        model.addAttribute("username",user.getUsername());

        model.addAttribute("asignacion", asignacionPuestoRepository.findById(id));

        return "crud_asignar_puesto/form_delete";
    }

    @RequestMapping("delete/{id}")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('DELETE_ASIGNACION_PUESTO'))")
    public String redirectAsignacionForm(@PathVariable Long id, Model model, RedirectAttributes rm){
    	User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	model.addAttribute("username",user.getUsername());

       AsignacionPuesto deleteAsignacion= asignacionPuestoRepository.findById(id);
       asignacionPuestoRepository.delete(deleteAsignacion);

       if(null != deleteAsignacion){
           rm.addFlashAttribute("result_delete",1);
       }else{
           rm.addFlashAttribute("result_delete",0);
       }

        return "redirect:/asignacionpuesto/";
    }
    
    @RequestMapping("edit/{id}")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('WRITE_ASIGNACION_PUESTO'))")
    public String editForm(@PathVariable Long id, Model model) {
        model.addAttribute("asignacion", asignacionPuestoRepository.findById(id));
        
    	User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	model.addAttribute("username",user.getUsername());

       //Timestamp date_today = new Timestamp();
        //Date date_today = new Date();
        java.util.Date utilDate = new java.util.Date();
        java.sql.Timestamp date_today = new java.sql.Timestamp(utilDate.getTime());
        model.addAttribute("date_today",date_today);
        model.addAttribute("empleados",empleadoRepository.findAll());
        model.addAttribute("puestos",puestoRepository.findAll());
        model.addAttribute("result",model.asMap().get("result"));
        System.out.println(model.addAttribute("result",model.asMap().get("result")));
        
        System.out.println("Editar Asignacion");
        return "crud_asignar_puesto/form_edit";
    }

    @RequestMapping(value = "save/", method = RequestMethod.POST)
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('WRITE_ASIGNACION_PUESTO'))")
    public String almacenar(@Valid AsignacionPuesto asignacion, BindingResult result, Model model, RedirectAttributes rm, SessionStatus status) {
        if (result.hasErrors()) {
            return "crud_asignar_puesto/form_edit";
        }
        AsignacionPuesto asignacion2 = asignacionPuestoRepository.save(asignacion);
        if (asignacion2 != null) {
            rm.addFlashAttribute("result_edit", 1);
        } else {
            rm.addFlashAttribute("result_edit", 0);
        }
        
        System.out.println("Asignacion editada");

        status.setComplete();
        return "redirect:/asignacionpuesto/";
    }
    
}