package com.sici.controller;

import com.lowagie.text.pdf.PdfWriter;
import com.sici.constant.ViewConstant;
import com.sici.entity.*;
import com.sici.reports.ConsultaPdfChartView;
import com.sici.reports.ConsultaPdfView;
import com.sici.reports.ConsultaResumenPdfView;
import com.sici.reports.DiagnosticoPdfView;
import com.sici.model.ModelDate;
import com.sici.reports.MotivoPdfView;
import com.sici.serviceImp.CitaService;
import com.sici.serviceImp.ConsultaEditadaService;
import com.sici.serviceImp.ConsultaService;
import com.sici.serviceImp.ExpedienteService;
import com.sici.serviceImp.MotivoConsultaService;
import com.sici.serviceImp.RecetaService;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.InputStreamResource;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.io.ByteArrayInputStream;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.*;

@Controller
@RequestMapping("/consulta")
public class ConsultaController {

    @Autowired
    @Qualifier("expedienteService")
    private ExpedienteService expedienteService;

    @Autowired
    @Qualifier("motivoConsultaService")
    private MotivoConsultaService motivoConsultaService;

    @Autowired
    @Qualifier("consultaService")
    private ConsultaService consultaService;

    @Autowired
    @Qualifier("citaServiceImp")
    private CitaService citaService;
    
    @Autowired
    @Qualifier("recetaService")
    private RecetaService recetaService;
    
    @Autowired
    private ConsultaEditadaService consultaEditadaService;

    public String userAuthenticate(){
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return user.getUsername();
    }

    public int ageBaby(Citas cita){
        LocalDate birthday = cita.getExpediente().getPaciente().getPersona().getFechaNacimiento();
        LocalDate today = LocalDate.now();
        Period period = Period.between(birthday,today);
        //System.out.println("periodo"+period);
        //System.out.println("periodo anio"+period.getYears());
        int age=0;
        if(period.getYears()>=2) {
                age = 1;
        }
        return age;
    }

    @GetMapping("/form_create/{id}")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('WRITE_CONSULTA'))")
    public String addConsulta(@PathVariable Long id, Model model){
        model.addAttribute("username",userAuthenticate());
        Citas cita = citaService.getCitaById(id);
        int age = ageBaby(cita);
        System.out.println("fecha baby:"+age);
        DateFunction date_today = new DateFunction();
        model.addAttribute("date_today",date_today.convertDateToSpanish(date_today.dateToday()));
        model.addAttribute("baby",age);
        model.addAttribute("citaPaciente",cita);
        model.addAttribute("citaNueva",new Citas());
        model.addAttribute("motivos_consulta",motivoConsultaService.listMotivoConsultaTrue());
        model.addAttribute("consulta",new Consulta());
        model.addAttribute("citasWait",citaService.listAllCitasPendiente());
        return ViewConstant.CONSULTA_FORM;
    }

    @PostMapping("/form_create/{id}")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('WRITE_USER'))")
    public String addConsulta(@Valid Consulta consulta, BindingResult result, @PathVariable Long id, Model model,RedirectAttributes rm) throws Exception{
        Citas cita = citaService.getCitaById(id);
        Citas citaNueva = new Citas();
        citaNueva.setExpediente(cita.getExpediente());
        citaNueva.setFecha_consulta(consulta.getCita().getFecha_consulta());
        citaNueva.setHora(consulta.getCita().getHora());
        citaNueva.setTipoHora(consulta.getCita().getTipoHora());
        citaNueva = citaService.addCita(citaNueva);
        Consulta consulta1 = consultaService.addConsulta(consulta,id);
        if (consulta1!=null) {
			rm.addFlashAttribute("mensaje","Consulta creada con exíto");
		}else {
			rm.addFlashAttribute("mensaje", "Consulta no pudo ser agregada");
		}
        return "redirect:/consulta/view_consulta/"+consulta1.getId();
    }

    @GetMapping("/list_consultas")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('READ_CONSULTA'))")
    public String listConsultas(Model model){
        model.addAttribute("username",userAuthenticate());
        model.addAttribute("result",model.asMap().get("result"));
        model.addAttribute("mensaje",model.asMap().get("mensaje"));
        model.addAttribute("list_consultas",consultaService.listConsultas());
        return ViewConstant.CONSULTA_LIST;
    }

    @GetMapping("/view_consulta/{id}")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('READ_CONSULTA'))")
    public String viewConsulta(@PathVariable Long id,Model model){
        model.addAttribute("username",userAuthenticate());
        Consulta consulta = consultaService.getConsultaById(id);
        model.addAttribute("consulta",consulta);
        Receta receta = recetaService.getRecetaByConsulta(consulta);
        int banderaReceta=0;
        if(receta!=null) {
        	banderaReceta=1;
        	model.addAttribute("receta", receta);
        }
        int bandera = 1;
        model.addAttribute("bandera", bandera);
        model.addAttribute("banderaReceta", banderaReceta);
        model.addAttribute("mensaje",model.asMap().get("mensajeReturn"));
        return ViewConstant.CONSULTA_VIEW;
    }

    @GetMapping("/edit_consulta/{id}")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('WRITE_CONSULTA'))")
    public String editConsulta(@PathVariable Long id, Model model){
        model.addAttribute("username",userAuthenticate());
        Consulta consulta = consultaService.getConsultaById(id);
        Citas citas = citaService.getCitaById(consulta.getCita().getId());
        int age = ageBaby(citas);
        model.addAttribute("baby",age);
        model.addAttribute("cita",citas);
        model.addAttribute("consulta",consulta);
        model.addAttribute("motivos_consulta",motivoConsultaService.listMotivoConsultaTrue());
        return ViewConstant.CONSULTA_FORM_EDIT;
    }

    @PostMapping("/edit_consulta/{id_consulta}/{id_cita}")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('WRITE_CONSULTA'))")
    public String editConsulta(Consulta consulta,@PathVariable("id_consulta") Long id_consulta,@PathVariable("id_cita") Long id_cita, Model model, RedirectAttributes rm){
        Consulta consulta1 = consultaService.editConsulta(consulta,id_consulta,id_cita);
        String mensaje;
        int resultado=0;
        if (consulta1 != null) {
        	resultado=1;
            mensaje = "Consulta editada con exito";
        } else {
            mensaje = "Consulta no pudo ser editada";
        }
         rm.addFlashAttribute("result", resultado);
        rm.addFlashAttribute("mensaje",mensaje);
        return "redirect:/consulta/list_consultas";
    }

    @GetMapping("/form_estadistica")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('READ_CONSULTA'))")
    public String showEstadistica(Model model){
    	System.out.println("ENTRO A METODO");
        model.addAttribute("username",userAuthenticate());
        model.addAttribute("modelDate",new ModelDate());
        model.addAttribute("surveyMap2",model.asMap().get("surveyMap"));
        return ViewConstant.ESTADISTICA_VIEW;
    }

    @GetMapping("/pdf_estadistica")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('READ_CONSULTA'))")
    public ResponseEntity<InputStreamResource> pdfEstadistica(Model model) throws Exception{

    	ModelDate modelDate = new ModelDate();
    	LocalDate d1,d2;
		d2 = LocalDate.now();
		d1 = d2.minusDays(6);
		modelDate.setDateStart(d1);
		modelDate.setDatEnd(d2);
		
    	SimpleDateFormat formato = new SimpleDateFormat("yyyy/MM/dd");
    	formato.applyPattern("dd/MM/yyyy");
    	
    	String fechaInicio = formato.format(Date.from(modelDate.getDateStart().atStartOfDay(ZoneId.systemDefault()).toInstant()));
    	String fechaFin = formato.format(Date.from(modelDate.getDatEnd().atStartOfDay(ZoneId.systemDefault()).toInstant()));
    	String periodo = "Desde:"+fechaInicio+" - "+" Hasta:"+fechaFin;

        List<Citas> citas = citaService.listCitasBetweenDates2(modelDate.getDateStart(),modelDate.getDatEnd());

        List<Integer> cantidad=citaService.countCitas(modelDate.getDateStart(),modelDate.getDatEnd());

        int posicion=0;
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        for (int i=0;i<cantidad.size();i++){
            dataset.setValue(cantidad.get(i),"CONSULTA",formato.format(Date.from(citas.get(cantidad.get(i)+posicion-1).getFecha_consulta().atStartOfDay(ZoneId.systemDefault()).toInstant())));
            posicion=cantidad.get(i);
        }
        JFreeChart chart = ChartFactory.createBarChart("Consultas registradas:"+periodo,"Fechas","Cantidad de consultas",
                dataset, PlotOrientation.VERTICAL,false,true,false);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename=consultaestadistica.pdf");

        ByteArrayInputStream bis = ConsultaPdfChartView.consultaReportChart(chart);
        return ResponseEntity.ok().headers(headers).contentType(MediaType.APPLICATION_PDF).body(new InputStreamResource(bis));

    }

    @GetMapping("/cancel_consulta/{id}")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('WRITE_CONSULTA'))")
    public String cancelConsulta(@PathVariable Long id, Model model,RedirectAttributes rm){
        model.addAttribute("username",userAuthenticate());
        String mensaje;
        int resultado =0;
        if (consultaService.cancelConsulta(id) != null) {
            rm.addFlashAttribute("result", 1);
            resultado =1;
            mensaje = "Consulta cancelada con exito";
        } else {
            mensaje = "Consulta no pudo ser cancelada";
        }
        rm.addFlashAttribute("result", resultado);
        rm.addFlashAttribute("mensaje",mensaje);
        return "redirect:/consulta/list_consultas";
    }

    //Uso de una variable global
    ModelDate modelDate2 = new ModelDate();
    //Fin de variable global

    @GetMapping("/show_motivos_consulta")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('READ_CONSULTA'))")
    public String showMotivosConsulta(Model model){
        model.addAttribute("username",userAuthenticate());
        model.addAttribute("modelDate",new ModelDate());
        model.addAttribute("citas",model.asMap().get("citas_result"));
        model.addAttribute("fechas",model.asMap().get("fechas"));
        model.addAttribute("fechas_vacias",model.asMap().get("fechas_vacias"));
        return ViewConstant.SHOW_MOTIVOS_CONSULTA;
    }

    @PostMapping("/list_motivos")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('READ_CONSULTA'))")
    public String listMotivos(ModelDate modelDate, Model model, RedirectAttributes rm){
        model.addAttribute("username",userAuthenticate());
        
        if(modelDate.getDateStart()==null || modelDate.getDatEnd()==null) {
        	rm.addFlashAttribute("fechas_vacias", "Fechas no pueden ser vacias");
    		return "redirect:/consulta/show_motivos_consulta";
    	}
    	
        if(modelDate.getDateStart().isAfter(modelDate.getDatEnd())) {
       	 rm.addFlashAttribute("fechas_vacias", "Fecha inicial no puede ser mayor que Fecha final");
     		return "redirect:/cita/show_motivos_consulta";
        }
        
        List<Citas> citas;
        modelDate2.setDateStart(modelDate.getDateStart());
        modelDate2.setDatEnd(modelDate.getDatEnd());
        try {
            citas=citaService.listCitasBetweenDates2(modelDate.getDateStart(), modelDate.getDatEnd());
            rm.addFlashAttribute("citas_result", citas);
            rm.addFlashAttribute("fechas",modelDate);
        } catch (DataAccessException e) {
            citas = null;
            rm.addFlashAttribute("citas_result", 0);
        }
        return "redirect:/consulta/show_motivos_consulta";
    }

    @RequestMapping(value = "/pdfmotivo", method = RequestMethod.GET,produces = MediaType.APPLICATION_PDF_VALUE)
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('READ_CONSULTA'))")
    public ResponseEntity<InputStreamResource> motivoReport() throws Exception {
        List<Citas> citas;
        try {
            citas=citaService.listCitasBetweenDates2(modelDate2.getDateStart(), modelDate2.getDatEnd());
        } catch (DataAccessException e) {
            citas = null;
        }
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename=motivoreport.pdf");

        ByteArrayInputStream bis = MotivoPdfView.motivoReport(citas);
        return ResponseEntity.ok().headers(headers).contentType(MediaType.APPLICATION_PDF).body(new InputStreamResource(bis));
    }

    @GetMapping("/show_diagnostico_consulta")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('READ_CONSULTA'))")
    public String showDiagnosticoConsulta(Model model){
        model.addAttribute("username",userAuthenticate());
        model.addAttribute("modelDate",new ModelDate());
        model.addAttribute("citas",model.asMap().get("citas_result"));
        model.addAttribute("fechas",model.asMap().get("fechas"));
        model.addAttribute("fechas_vacias",model.asMap().get("fechas_vacias"));
        return ViewConstant.SHOW_DIAGNOSTICO_CONSULTA;
    }

    @PostMapping("/list_diagnostico")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('READ_CONSULTA'))")
    public String listDiagnostico(ModelDate modelDate, Model model, RedirectAttributes rm){
    	model.addAttribute("username",userAuthenticate());
        
        if(modelDate.getDateStart()==null || modelDate.getDatEnd()==null) {
        	rm.addFlashAttribute("fechas_vacias", "Fechas no pueden ser vacias");
    		return "redirect:/consulta/show_diagnostico_consulta";
    	}
    	
        if(modelDate.getDateStart().isAfter(modelDate.getDatEnd())) {
          	 rm.addFlashAttribute("fechas_vacias", "Fecha inicial no puede ser mayor que Fecha final");
        		return "redirect:/cita/show_diagnostico_consulta";
           }
        
        List<Citas> citas;
        modelDate2.setDateStart(modelDate.getDateStart());
        modelDate2.setDatEnd(modelDate.getDatEnd());
        try {
            citas=citaService.listCitasBetweenDates2(modelDate.getDateStart(), modelDate.getDatEnd());
            rm.addFlashAttribute("citas_result", citas);
            rm.addFlashAttribute("fechas",modelDate);
        } catch (DataAccessException e) {
            citas = null;
            rm.addFlashAttribute("citas_result", 0);
        }

        return "redirect:/consulta/show_diagnostico_consulta";
    }

    @RequestMapping(value = "/pdfdiagnostico", method = RequestMethod.GET,produces = MediaType.APPLICATION_PDF_VALUE)
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('READ_CONSULTA'))")
    public ResponseEntity<InputStreamResource> diagnosticoReport() throws Exception {
        List<Citas> citas;
        try {
            citas=citaService.listCitasBetweenDates2(modelDate2.getDateStart(), modelDate2.getDatEnd());
        } catch (DataAccessException e) {
            citas = null;
        }
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename=diagnosticoreport.pdf");

        ByteArrayInputStream bis = DiagnosticoPdfView.diagnosticReport(citas);
        return ResponseEntity.ok().headers(headers).contentType(MediaType.APPLICATION_PDF).body(new InputStreamResource(bis));
    }

    @RequestMapping(value = "/pdf_consulta/{id}",method = RequestMethod.GET,produces = MediaType.APPLICATION_PDF_VALUE)
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('READ_CONSULTA'))")
    public ResponseEntity<InputStreamResource> consultaReport(@PathVariable Long id) throws Exception{
        Consulta consulta=null;
        DateFunction dateFunction = new DateFunction();
        String nombreReporte=null;
        try {
            consulta = consultaService.getConsultaById(id);
            if(consulta==null){
                return ResponseEntity.notFound().build();
            }
            nombreReporte = "inline; filename=Consulta_"+consulta.getCita().getExpediente().getCodigo()+"_"+dateFunction.convertDateToSpanish(new Date())+".pdf";
        }catch (DataAccessException e){
            consulta=null;
        }
        
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition","inline; filename=Consulta.pdf");
        ByteArrayInputStream bis = ConsultaPdfView.consultaReport(consulta);
        return ResponseEntity.ok().headers(headers).contentType(MediaType.APPLICATION_PDF).body(new InputStreamResource(bis));
    }

    @GetMapping("/list_consulta_editadas/{id}")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('READ_CONSULTA'))")
    public String listConsultasEditada(@PathVariable Long id,Model model){
    	model.addAttribute("username",userAuthenticate());
    	model.addAttribute("list_consultas", consultaEditadaService.listConsultas(id));
    	return ViewConstant.CONSULTA_LIST_EDIT;
    }
    
    @GetMapping("/show_consulta_edit/{id}")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('WRITE_CONSULTA'))")
    public String viewConsultaEdit(@PathVariable Long id,Model model) {
    	model.addAttribute("consulta", consultaEditadaService.findById(id));
    	 int bandera = 0;
         model.addAttribute("bandera", bandera);
    	return ViewConstant.CONSULTA_VIEW;
    }
    
    @GetMapping("/show_consulta_resumen_periodo")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('READ_CONSULTA'))")
    public String showConsultaResumen(Model model){
        model.addAttribute("username",userAuthenticate());
        model.addAttribute("modelDate",new ModelDate());
        model.addAttribute("citas",model.asMap().get("citas_result"));
        model.addAttribute("fechas",model.asMap().get("fechas"));
        model.addAttribute("fechas_vacias",model.asMap().get("fechas_vacias"));
        return ViewConstant.CONSULTA_RESUMEN_PERIODO;
    }
    
    @PostMapping("/list_consulta_resumen")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('READ_CONSULTA'))")
    public String listConsultaResumen(ModelDate modelDate, Model model, RedirectAttributes rm){
    	model.addAttribute("username",userAuthenticate());
        if(modelDate.getDateStart()==null || modelDate.getDatEnd()==null) {
        	rm.addFlashAttribute("fechas_vacias", "Fechas no pueden ser vacias");
    		return "redirect:/consulta/show_consulta_resumen_periodo";
    	}
    	
        if(modelDate.getDateStart().isAfter(modelDate.getDatEnd())) {
       	 rm.addFlashAttribute("fechas_vacias", "Fecha inicial no puede ser mayor que Fecha final");
     		return "redirect:/consulta/show_consulta_resumen_periodo";
        }
        
        List<Citas> citas;
        modelDate2.setDateStart(modelDate.getDateStart());
        modelDate2.setDatEnd(modelDate.getDatEnd());
        try {
            citas=citaService.listCitasBetweenDates2(modelDate.getDateStart(), modelDate.getDatEnd());
            for (Citas citas2 : citas) {
				System.out.println(citas2.getExpediente().getCodigo());
			}
            
            rm.addFlashAttribute("citas_result", citas);
            rm.addFlashAttribute("fechas",modelDate);
        } catch (DataAccessException e) {
            citas = null;
            rm.addFlashAttribute("citas_result", 0);
        }
        return "redirect:/consulta/show_consulta_resumen_periodo";
    }
    
    @RequestMapping(value = "/pdfconsultaresumenperiodo", method = RequestMethod.GET,produces = MediaType.APPLICATION_PDF_VALUE)
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('READ_CONSULTA'))")
    public ResponseEntity<InputStreamResource> consultaResumenReport() throws Exception {
        List<Citas> citas;
        try {
            citas=citaService.listCitasBetweenDates2(modelDate2.getDateStart(), modelDate2.getDatEnd());
            
        } catch (DataAccessException e) {
            citas = null;
        }
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename=ConsultaResumenReport.pdf");

        ByteArrayInputStream bis = ConsultaResumenPdfView.consultaResumenReport(citas,modelDate2);
        return ResponseEntity.ok().headers(headers).contentType(MediaType.APPLICATION_PDF).body(new InputStreamResource(bis));
    }

    
}
