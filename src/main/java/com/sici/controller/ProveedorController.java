package com.sici.controller;

import com.sici.constant.ViewConstant;
import com.sici.entity.*;
import com.sici.serviceImp.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


@Controller
@SessionAttributes("p")
@RequestMapping("/proveedor")


public class ProveedorController {
	
	   private ProveedorService proveedorService;
	   
	    @Autowired
	    public void setproveedorService(ProveedorService proveedorService) {
	        this.proveedorService = proveedorService;
	    }
	    

	
	  /**
     * List proveedores
     *
     * @param model
     * @return
     */

	    @RequestMapping(value = "/proveedores", method = RequestMethod.GET)
	    @PreAuthorize("isAuthenticated() and (hasAnyRole('ROLE_ADMIN') or hasAnyAuthority('READ_PROVEEDOR'))")
    public String listProveedores(Model model) {
    	
    	User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	model.addAttribute("username",user.getUsername());
    	model.addAttribute("result",model.asMap().get("result_delete"));
        model.addAttribute("proveedores", proveedorService.listAllProveedores());
        System.out.println("Listado de proveedores");
        return ViewConstant.LIST_PROVEEDORES;
    }
	

    /* 
    -Nombre de la función: proveedorDelete
    -Objetivo: Desactivar proveedor
    -Fecha de creación: 
    -Autor: Elizabeth Rodríguez
    -Fecha de última modificación 
    -Autor de última modificación: Elizabeth Rodríguez   
      */
    
    /**
     * Desactivar proveedor
     *
     * @param id
     * @param model
     * @return
     */
   
    @RequestMapping("proveedor/deleteForm/{id}")
    @PreAuthorize("isAuthenticated() and (hasAnyRole('ROLE_ADMIN') or hasAnyAuthority('DELETE_PROVEEDOR'))")
    public String ConfirmproveedorDelete(@PathVariable Long id, Model model){

        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        model.addAttribute("username",user.getUsername());
        model.addAttribute("proveedor", proveedorService.getProveedorById(id));
        System.out.println("Preguntando si está seguro de realizar el cambio");
        return ViewConstant.DELETE_PROVEEDORES;
    }
    /**
     * Delete proveedor
     *
     * @param id
     * @param model
     * @return
     */

    @RequestMapping("proveedor/delete/{id}")
    @PreAuthorize("isAuthenticated() and (hasAnyRole('ROLE_ADMIN') or hasAnyAuthority('DELETE_PROVEEDOR'))")
    public String deleteProveedor(@PathVariable Long id, Model model, RedirectAttributes rm){

    	User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	model.addAttribute("username",user.getUsername());
        proveedorService.deleteProveedor(id);
       System.out.println("Proveedor eliminada");
        return ViewConstant.LIST_PROVEEDORES_ADD;
    }
    
    /* 
    -Nombre de la función: showproveedores
    -Objetivo: Ver el medicamento
    -Fecha de creación: 
    -Autor: Elizabeth Rodríguez
    -Fecha de última modificación 
    -Autor de última modificación: Elizabeth Rodríguez   
      */

    /**
     * View proveedor
     *
     * @param id
     * @param model
     * @return
     */
 
     @RequestMapping("proveedor/{id}")
     @PreAuthorize("isAuthenticated() and (hasAnyRole('ROLE_ADMIN') or hasAnyAuthority('READ_PROVEEDOR'))")
     public String showProveedor(@PathVariable Long id, Model model) {
    	
    	User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	model.addAttribute("username",user.getUsername());
    	model.addAttribute("proveedor", proveedorService.getProveedorById(id));
    	System.out.println(proveedorService.getProveedorById(id));
        if(model.asMap().get("result_edit")!= null){
			model.addAttribute("result",model.asMap().get("result_edit"));
		}else{
			model.addAttribute("result",2);
		}
        return ViewConstant.SHOW_PROVEEDORES;
    }
    

     
    /* 
    -Nombre de la función: proveedorSave
    -Objetivo: Guardar proveedor
    -Fecha de creación: 
    -Autor: Elizabeth Rodríguez
    -Fecha de última modificación 
    -Autor de última modificación: Elizabeth Rodríguez   
      */
    
    
    /**
     * New proveedor.
     *
     * @param model
     * @return
     */
    @RequestMapping("/new")
    @PreAuthorize("isAuthenticated() and (hasAnyRole('ROLE_ADMIN') or hasAnyAuthority('WRITE_PROVEEDOR'))")
    public String newProveedores(Model model) {
    	System.out.println("1");
        model.addAttribute("proveedor", new Proveedor());
        System.out.println("Llenar formulario");
        return "crud_proveedor/form_create";
    }

    /**
     * Save proveedor
     *
     * @param proveedor
     * @return
     */
    @PostMapping("/add_proveedor")
    @PreAuthorize("isAuthenticated() and (hasAnyRole('ROLE_ADMIN') or hasAnyAuthority('WRITE_PROVEEDOR'))")
    public String saveProveedores(Proveedor proveedor, Model model) {
        proveedorService.saveProveedor(proveedor);
        System.out.println("Tipo de proveedor guardado");
        return "redirect:/proveedor/proveedores";
    }
    
  


}