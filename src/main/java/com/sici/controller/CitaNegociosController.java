package com.sici.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.sici.constant.ViewConstant;
import com.sici.entity.CitasNegocio;
import com.sici.serviceImp.CitaNegocioServiceImp;
import com.sici.serviceImp.ClinicaService;

@Controller
@RequestMapping("/cita_negocio")
public class CitaNegociosController {
	DateFunction today = new DateFunction();

	@Autowired
	private CitaNegocioServiceImp citaService;
	
    @Autowired
    @Qualifier("clinicaServiceImp")
    private ClinicaService clinicaService;

	public String userAuthenticate() {
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return user.getUsername();
	}

	@GetMapping("/list")
	@PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('READ_CITA_NEGOCIO'))")
	public String listCitaNegocios(Model model) {
		model.addAttribute("username", userAuthenticate());
		model.addAttribute("citas", citaService.list());
		model.addAttribute("mensaje", model.asMap().get("mensaje"));
		model.addAttribute("result", model.asMap().get("result"));
		return ViewConstant.LIST_CITAS_NEGOCIO;
	}

	@GetMapping("/form_create_update/{id}/{tipo}")
	@PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('WRITE_CITA_NEGOCIO'))")
	public String formCreate(@PathVariable Long id, @PathVariable String tipo, Model model) {
		model.addAttribute("username", userAuthenticate());
		model.addAttribute("date_today", today.convertDateToSpanish(today.dateToday()));
		if (!tipo.equals("crear")) {
			//System.out.println("se esta editando");
			model.addAttribute("cita", citaService.getById(id));
			model.addAttribute("crear", false);
			model.addAttribute("citaId", id);
		} else {
			//System.out.println("SE ESTA CREANDO");
			model.addAttribute("cita", new CitasNegocio());
			model.addAttribute("crear", true);
			model.addAttribute("citaId", -1);
		}
		model.addAttribute("citas", citaService.list());

		return ViewConstant.CREATE_UPDATE_CITAS_NEGOCIO;
	}

	/*
	 * @PostMapping("/form_save")
	 * 
	 * @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('WRITE_CITA_NEGOCIO'))"
	 * ) public String saveCita(CitasNegocio cita, Model model, RedirectAttributes
	 * rm) { CitasNegocio citaSave = null; String mensaje = ""; citaSave =
	 * citaService.save(cita); if (citaSave != null) { mensaje =
	 * "Cita agregada con exíto"; } else { mensaje = "Cita no pudo ser agregada"; }
	 * rm.addFlashAttribute("mensajeReturn", mensaje); return
	 * "redirect:/cita_negocio/list";
	 * 
	 * }
	 */

	@PostMapping("/form_save/{id}")
	@PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('WRITE_CITA_NEGOCIO'))")
	public String saveCitaUpdate(@PathVariable Long id, CitasNegocio cita, Model model, RedirectAttributes rm) {
		CitasNegocio citaUpdate = null, citaSave = null;
		String mensaje = "",tipo="";
		if (id < 0) {
			cita.setClinica(clinicaService.getClinicaByCodigo("C1"));
			cita.setProceso("PENDIENTE");
			citaSave = citaService.save(cita);
			tipo="creada";
			
		} else {
			citaUpdate = citaService.getById(id);
			citaUpdate.setFecha(cita.getFecha());
			citaUpdate.setHora(cita.getHora());
			citaUpdate.setTipoHora(cita.getTipoHora());
			citaUpdate.setNombre(cita.getNombre());
			citaUpdate.setDescripcion(cita.getDescripcion());
			//citaUpdate.setProceso(cita.getProceso());
			citaSave = citaService.save(citaUpdate);
			tipo="editada";
			//System.out.println("ACTUALIZO");
		}

		if (citaSave != null) {
			mensaje = "Cita " +tipo+ " con exíto";
			rm.addFlashAttribute("result", 1);
		} else {
			mensaje = "Cita no pudo ser "+tipo;
			rm.addFlashAttribute("result", 0);
		}
		rm.addFlashAttribute("mensaje", mensaje);
		return "redirect:/cita_negocio/list";

	}

	@GetMapping("/show/{id}/{tipo}")
	@PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('READ_CITA_NEGOCIO'))")
	public String showCita(@PathVariable Long id, @PathVariable String tipo, Model model) {
		model.addAttribute("username", userAuthenticate());
		model.addAttribute("cita", citaService.getById(id));
		if (tipo.equals("view")) {
			model.addAttribute("ver", true);
		} else {
			model.addAttribute("ver", false);
		}

		return ViewConstant.VIEW_CITAS_NEGOCIO;
	}

	@GetMapping("/status/{id}")
	@PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('READ_CITA_NEGOCIO'))")
	public String statusCita(@PathVariable Long id, Model model, RedirectAttributes rm) {
		model.addAttribute("username", userAuthenticate());
		CitasNegocio cita = null, citaUpdate=null;
		String mensaje = null;
		cita = citaService.getById(id);
		
		if(cita.getProceso().equals("EJECUTADA")) {
			cita.setProceso("PENDIENTE");
		} else {
			cita.setProceso("EJECUTADA");
		}
		
		citaUpdate = citaService.save(cita);
		if (citaUpdate!=null) {
			mensaje ="Estado de cita modificado exitosamente";
			rm.addFlashAttribute("result", 1);
		}else {
			mensaje ="Estado de cita no pudo ser modificado";
			rm.addFlashAttribute("result", 0);
		}
		rm.addFlashAttribute("mensaje", mensaje);
		return "redirect:/cita_negocio/list";
	}

	@GetMapping("/delete/{id}")
	@PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('DELETE_CITA_NEGOCIO'))")
	public String deleteCita(@PathVariable Long id, Model model, RedirectAttributes rm) {
		model.addAttribute("username", userAuthenticate());
		CitasNegocio citaUpdate = null, citaSave = null;
		citaUpdate = citaService.getById(id);
		String mensaje = "";
		if (citaUpdate != null) {
			citaUpdate.setEstado(false);
			citaSave = citaService.save(citaUpdate);
		}
		if (citaSave != null) {
			mensaje = "Cita eliminada con exíto";
			rm.addFlashAttribute("result", 1);
		} else {
			mensaje = "Cita no pudo ser eliminada";
			rm.addFlashAttribute("result", 0);
		}
		rm.addFlashAttribute("mensaje", mensaje);
		return ViewConstant.LIST_CITAS_NEGOCIO;
	}

}
