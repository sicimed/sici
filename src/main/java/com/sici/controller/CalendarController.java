//package com.sici.controller;
//
//import com.fasterxml.jackson.databind.annotation.JsonSerialize;
//import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
//import com.sici.entity.Evento;
//import com.sici.entity.Resource;
//import com.sici.repository.EventRepository;
//import com.sici.repository.ResourceRepository;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.format.annotation.DateTimeFormat;
//import org.springframework.web.bind.annotation.*;
//import org.springframework.format.annotation.DateTimeFormat.ISO;
//
//import javax.transaction.Transactional;
//import java.time.LocalDateTime;
//import java.util.List;
//
//@RestController
//public class CalendarController {
//    @Autowired
//    EventRepository er;
//
//    @Autowired
//    ResourceRepository rr;
//
//    @RequestMapping("/api")
//    @ResponseBody
//    String home() {
//        return "Welcome!";
//    }
//
//    @RequestMapping("/api/resources")
//    Iterable<Resource> resources() {
//        return rr.findAll();
//    }
//
//    @GetMapping("/api/events")
//    @JsonSerialize(using = LocalDateTimeSerializer.class)
//    Iterable<Evento> events(@RequestParam("start") @DateTimeFormat(iso = ISO.DATE_TIME) LocalDateTime start, @RequestParam("end") @DateTimeFormat(iso = ISO.DATE_TIME) LocalDateTime end) {
//
//        System.out.println(start);
//        System.out.println(end);
//        System.out.println("eventos:"+er.findBetween(start,end));
//        return er.findBetween(start, end);
//    }
//
//    @PostMapping("/api/events/create")
//    @JsonSerialize(using = LocalDateTimeSerializer.class)
//    @Transactional
//    Evento createEvent(@RequestBody EventCreateParams params) {
//
//        Resource r = null;
//        List<Resource> r2=null;
//
//        if (params.resource == null) {
//           // r = rr.findById(params.resource);
//           // r = rr.findOne(1L);
//            r = rr.findByName("Recurso 1");
//        }
//
//        Evento e = new Evento();
//        e.setInicio(params.start);
//        e.setFin(params.end);
//        e.setTexto(params.text);
//        e.setResource(r);
//
//        er.save(e);
//
//        return e;
//    }
//
//    @PostMapping("/api/events/move")
//    @JsonSerialize(using = LocalDateTimeSerializer.class)
//    @Transactional
//    Evento moveEvent(@RequestBody EventMoveParams params) {
//
//        Evento e = er.findOne(params.id);
//
//        Resource r = null;
//
//        if (params.resource == null) {
//            // r = rr.findById(params.resource);
//            // r = rr.findOne(1L);
//            r = rr.findByName("Recurso 1");
//        }
//
//        e.setInicio(params.start);
//        e.setFin(params.end);
//        e.setResource(r);
//
//        er.save(e);
//
//        return e;
//    }
//
//    @PostMapping("/api/events/setColor")
//    @JsonSerialize(using = LocalDateTimeSerializer.class)
//    @Transactional
//    Evento setColor(@RequestBody SetColorParams params) {
//
//        Evento e = er.findOne(params.id);
//        e.setColor(params.color);
//        er.save(e);
//
//        return e;
//    }
//    //delete
//    @PostMapping("/api/events/delete")
//    @JsonSerialize(using = LocalDateTimeSerializer.class)
//    @Transactional
//    EventDeleteResponse deleteEvent(@RequestBody EventDeleteParams params) {
//
//       // er.delete(params.id);
//        er.deleteById(params.id);
//        return new EventDeleteResponse() {{
//            message = "Deleted";
//        }};
//    }
//
//    public static class EventDeleteParams {
//        public Long id;
//    }
//
//    public static class EventDeleteResponse {
//        public String message;
//    }
//    //
//    public static class EventCreateParams {
//        public LocalDateTime start;
//        public LocalDateTime end;
//        public String text;
//        public Long resource;
//    }
//
//    public static class EventMoveParams {
//        public Long id;
//        public LocalDateTime start;
//        public LocalDateTime end;
//        public Long resource;
//    }
//
//    public static class SetColorParams {
//        public Long id;
//        public String color;
//    }
//
//
//
//
//}
