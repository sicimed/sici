package com.sici.controller;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

public class DateFunction {

    public Date dateToday(){

        java.util.Date utilDate = new java.util.Date();
        java.sql.Timestamp date_today = new java.sql.Timestamp(utilDate.getTime());

        return date_today;
    }

    public String convertDateToSpanish(Date date){
        Locale spanishLocale=new Locale("es", "ES");
        ZoneId defaultZoneId = ZoneId.systemDefault();
        Instant instant = date.toInstant();
        LocalDate localDate = instant.atZone(defaultZoneId).toLocalDate();

        return localDate.format(DateTimeFormatter.ofPattern("EEEE, dd MMMM, yyyy",spanishLocale));
    }

    public String convertDateToSpanish(LocalDate date){
        Locale spanishLocale=new Locale("es", "ES");
        return date.format(DateTimeFormatter.ofPattern("EEEE, dd MMMM, yyyy",spanishLocale));
    }
}
