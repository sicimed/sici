package com.sici.controller;

import com.sici.serviceImp.ExpedienteService;
import com.sici.serviceImp.PersonaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/")

public class StatusPacienteController {
    @Autowired
    @Qualifier("personaServiceImp")
    private PersonaService personaService;
    
    @Autowired
    @Qualifier("expedienteService")
    private ExpedienteService expedienteService;
    
    public String userAuthenticate(){
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return user.getUsername();
    }
    @RequestMapping(value = "/paciente/form_status", method = RequestMethod.GET)
    @PreAuthorize("isAuthenticated() and (hasAnyRole('ROLE_ADMIN') or hasAnyAuthority('READ_PACIENTE'))")
    public String listPacientes(Model model) {
        model.addAttribute("username",userAuthenticate());
        model.addAttribute("result", model.asMap().get("result_delete"));
       // model.addAttribute("personas", personaService.listAllPacientes());
        model.addAttribute("expedientes", expedienteService.findAll());
        System.out.println("Listado de Pacientes");
        return "crud_paciente/form_status";
    }


}