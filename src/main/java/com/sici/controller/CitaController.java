package com.sici.controller;

import com.sici.constant.ViewConstant;
import com.sici.entity.Citas;
import com.sici.entity.Persona;
import com.sici.model.CitaCountModel;
import com.sici.model.ModelDate;
import com.sici.reports.CantidadCitasPeriodoPdfView;
import com.sici.serviceImp.CitaService;
import com.sici.serviceImp.PersonaService;

import java.io.ByteArrayInputStream;
import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.InputStreamResource;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


@Controller
@RequestMapping("/cita")
public class CitaController {
    DateFunction today = new DateFunction();

    @Autowired
    @Qualifier("personaServiceImp")
    private PersonaService personaService;

    @Autowired
    @Qualifier("citaServiceImp")
    private CitaService citaService;

    public String userAuthenticate(){
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return user.getUsername();
    }

    @GetMapping("/form_create/{id}")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('WRITE_CITAS'))")
    public String formCreate(@PathVariable Long id, Model model) throws Exception{
        model.addAttribute("username",userAuthenticate());
        model.addAttribute("date_today",today.convertDateToSpanish(today.dateToday()));
        model.addAttribute("persona",personaService.getPacienteById(id));
        model.addAttribute("citasWait",citaService.listAllCitasPendiente());
        model.addAttribute("cita",new Citas());
        return ViewConstant.CITA_FORM_CREATE;
    }

    @PostMapping("/citas_pendientes/{id}")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('WRITE_CITAS'))")
    public String createCita(@PathVariable Long id, Citas cita, Model model, RedirectAttributes rm) throws Exception{
        Persona persona = personaService.getPacienteById(id);
        cita.setExpediente(persona.getPaciente().getExpediente());
        Citas cita1 = citaService.addCita(cita);
        String mensaje = "";
        int resultado =0;
        if (cita1!=null) {
			mensaje="Cita agregada con exíto";
			resultado=1;
		}else {
			mensaje="Cita no pudo ser agregada";
		}
        rm.addFlashAttribute("mensajeReturn", mensaje);
        rm.addFlashAttribute("result",resultado);
        return "redirect:/cita/show_cita/"+cita1.getId();
    }

    @GetMapping("/show_cita/{id}")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('READ_CITAS'))")
    public String citaShow(@PathVariable Long id, Model model) throws Exception{
        model.addAttribute("username",userAuthenticate());
        model.addAttribute("cita",citaService.getCitaById(id));
        model.addAttribute("mensaje", model.asMap().get("mensajeReturn"));
        return ViewConstant.CITA_SHOW;
    }

    @GetMapping("/list_citas_pendientes")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('READ_CITAS'))")
    public String listCitasWait(Model model) throws Exception{
        model.addAttribute("username",userAuthenticate());
        model.addAttribute("citasWait",citaService.listAllCitasPendiente());
        model.addAttribute("result",model.asMap().get("result"));
        model.addAttribute("mensaje",model.asMap().get("mensaje"));
        return ViewConstant.CITA_LIST_PENDIENTES;
    }

    @GetMapping("/list_citas_ejecutadas")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('READ_CITAS'))")
    public String listCitasExecuted(Model model) throws Exception{
        model.addAttribute("username",userAuthenticate());
        model.addAttribute("citasExecuted",citaService.listAllCitasEjecutada());

        return ViewConstant.CITA_LIST_REALIZADAS;
    }

    @GetMapping("/form_edit/{id}")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('WRITE_CITAS'))")
    public String editCita(@PathVariable Long id,Model model){
        model.addAttribute("username",userAuthenticate());
        Citas citas = citaService.getCitaById(id);
        model.addAttribute("citaId",id);
        model.addAttribute("codigo",citas);
        model.addAttribute("cita",citas);
        model.addAttribute("citasWait",citaService.listAllCitasPendiente());
        return ViewConstant.CITA_FORM_EDIT;
    }

    @PostMapping("form_edit/save/{id}")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('WRITE_CITAS'))")
    public String editCita(Citas citas, BindingResult result, @PathVariable Long id,RedirectAttributes rm){
        if(result.hasErrors()){
            return ViewConstant.CITA_FORM_EDIT;
        }
        Citas citas1 = citaService.editCitas(citas,id);
        String mensaje;
        int resultado =0;
        if (citas1 != null) {
            resultado=1;
            mensaje = "Cita editada con exito";
        } else {
            mensaje = "Cita no pudo ser editada";
        }
         rm.addFlashAttribute("result", resultado);
        rm.addFlashAttribute("mensaje",mensaje);
        return "redirect:/cita/list_citas_pendientes";
    }

    @GetMapping("/delete/{id}")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('DELETE_CITAS'))")
    public String deleteCita(Model model,@PathVariable Long id){
        model.addAttribute("username",userAuthenticate());
        model.addAttribute("cita",citaService.getCitaById(id));
        return ViewConstant.CITA_FORM_DELETE;
    }

    @RequestMapping("/delete/save/{id}")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('DELETE_CITAS'))")
    public String deleteCita(Model model,@PathVariable Long id,RedirectAttributes rm){
        model.addAttribute("username",userAuthenticate());
        Citas citas1 = citaService.deleteCita(id);
        String mensaje;
        int resultado=0;
        if (citas1 != null) {
        	resultado=1;
            mensaje = "Cita eliminada con exito";
        } else {
            mensaje = "Cita no pudo ser eliminada";
        }
        rm.addFlashAttribute("result", resultado);
        rm.addFlashAttribute("mensaje",mensaje);
        return "redirect:/cita/list_citas_pendientes";
    }

    @GetMapping("/cance_cita/{id}")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('WRITE_CITAS'))")
    public String cancelCita(@PathVariable Long id,Model model){
        model.addAttribute("username",userAuthenticate());
        model.addAttribute("cita",citaService.getCitaById(id));
        return ViewConstant.CITA_CANCEL;
    }

    @RequestMapping("/cancel_cita/save/{id}")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('WRITE_CITAS'))")
    public String cancelCita(@PathVariable Long id,Model model,RedirectAttributes rm){
        model.addAttribute("username",userAuthenticate());
        Citas citas1 = citaService.getCitaById(id);
        String mensaje;
        int resultado=0;
        if(null!=citaService.changeStatusCita(citas1,"CANCELADA")){
            resultado=1;
            mensaje = "Cita cancelada con exito";
        } else {
            mensaje = "Cita no pudo ser cancelada";
        }
        rm.addFlashAttribute("result", resultado);
        rm.addFlashAttribute("mensaje",mensaje);
        return "redirect:/cita/list_citas_pendientes";
    }

    @RequestMapping("/cancel_reprogramar/save/{id_cita}/{id_paciente}")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('WRITE_CITAS'))")
    public String reprogramarCita(@PathVariable Long id_cita,@PathVariable Long id_paciente,Model model,RedirectAttributes rm){
        Citas citas1 = citaService.getCitaById(id_cita);
        String mensaje;
        int resultado=0;
        if(null!=citaService.changeStatusCita(citas1,"CANCELADA")){
            resultado=1;
            mensaje = "Cita cancelada con exito";
        } else {
            mensaje = "Cita no pudo ser cancelada";
        }
        rm.addFlashAttribute("result_edit", resultado);
        rm.addFlashAttribute("mensaje",mensaje);
        return "redirect:/cita/form_create/"+id_paciente;
    }
    //--------------------------------------------------------------------------------------
    
    @GetMapping("/contador/citas_diarias")
    @PreAuthorize("isAuthenticated()")
    public String countCitas(Model model) {
    	 model.addAttribute("username",userAuthenticate());
    	 model.addAttribute("modelDate",new ModelDate());
    	 model.addAttribute("citas", model.asMap().get("citas_result"));
    	 model.addAttribute("fechas_vacias", model.asMap().get("fechas_error"));
    	 return ViewConstant.CITA_CONTADOR;
    }
    
    //Uso de una variable global
    ModelDate modelDate2 = new ModelDate();
    //Fin de variable global
    
    @PostMapping("/contador/citas_diarias")
    @PreAuthorize("isAuthenticated()")
    public String calculoCitas(ModelDate modelDate, Model model,RedirectAttributes rm) {
    	 model.addAttribute("username",userAuthenticate());
         
         if(modelDate.getDateStart()==null || modelDate.getDatEnd()==null) {
         	rm.addFlashAttribute("fechas_error", "Fechas no pueden ser vacias");
     		return "redirect:/cita/contador/citas_diarias";
     	}
         
         if(modelDate.getDateStart().isAfter(modelDate.getDatEnd())) {
        	 rm.addFlashAttribute("fechas_error", "Fecha inicial no puede ser mayor que Fecha final");
      		return "redirect:/cita/contador/citas_diarias";
         }
         
         List<?> citasDia;
         modelDate2.setDateStart(modelDate.getDateStart());
         modelDate2.setDatEnd(modelDate.getDatEnd());
         
         try {
			citasDia = citaService.countCitasDay(modelDate.getDateStart(), modelDate.getDatEnd());
			rm.addFlashAttribute("citas_result", citasDia);
            rm.addFlashAttribute("fechas",modelDate);
         } catch (DataAccessException e) {
			citasDia = null;
			 rm.addFlashAttribute("citas_result", 0);
		}
         
    	return "redirect:/cita/contador/citas_diarias";
    }
    
    @RequestMapping(value = "/pdfcantidadcitas", method = RequestMethod.GET,produces = MediaType.APPLICATION_PDF_VALUE)
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<InputStreamResource> cantidadCitasReport() throws Exception{
    	List<?> citas;
    	
    	try {
			citas = citaService.countCitasDay(modelDate2.getDateStart(), modelDate2.getDatEnd());
			System.out.println(citas);
		} catch (DataAccessException e) {
			citas = null;
		}
    	HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename=CantidadCitasReport.pdf");
        
        ByteArrayInputStream bis = CantidadCitasPeriodoPdfView.cantidadCitasReport(citas, modelDate2);
        System.out.println("recetas cantidasd generadas");
        return ResponseEntity.ok().headers(headers).contentType(MediaType.APPLICATION_PDF).body(new InputStreamResource(bis));
        
    } 
    
}
