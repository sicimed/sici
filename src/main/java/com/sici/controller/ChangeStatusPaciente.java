package com.sici.controller;

import com.sici.entity.Persona;
import com.sici.serviceImp.PersonaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/")
public class ChangeStatusPaciente {

    private PersonaService personaService;
    private Persona persona;
    @Autowired
    public void setPersonaService(PersonaService personaService) {
        this.personaService = personaService;
    }

    public String userAuthenticate(){
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return user.getUsername();
    }

    @RequestMapping("paciente/statusForm/{id}")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('WRITE_PACIENTE'))")
    public String changeStatus(@PathVariable Long id, Model model){
        model.addAttribute("username",userAuthenticate());
        model.addAttribute("persona", personaService.getPacienteById(id));

        return "crud_paciente/form_status_alert";
    }


    @RequestMapping("paciente/status/{id}")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('WRITE_PACIENTE'))")
    public String changeStatusForm(@PathVariable Long id, Model model, RedirectAttributes rm){
        model.addAttribute("username",userAuthenticate());
       Persona changeStatus = personaService.changeStatusPersona(personaService.getPacienteById(id));


       if(null != changeStatus){
           rm.addFlashAttribute("result_change",1);
       }else{
           rm.addFlashAttribute("result_change",0);
       }
       System.out.println("Estado modificado");
       return "redirect:/paciente/form_status";
    }
}
