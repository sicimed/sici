package com.sici.controller;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Font;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfWriter;
import com.sici.constant.ViewConstant;
import com.sici.entity.*;

import com.sici.reports.ProductosDesactivadosPdfView;
import com.sici.reports.ProductosExistenciaBajaPdfView;
import com.sici.reports.ProductosPdfView;
import com.sici.reports.RecetaPdfView;

import com.sici.reports.DiagnosticoPdfView;
import com.sici.reports.KardexPdfView;

import com.sici.repository.*;
import com.sici.restcontroller.ProductFilter;
import com.sici.serviceImp.*;

import net.sf.jasperreports.engine.JRException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.InputStreamResource;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.jdbc.core.JdbcTemplate;


import java.io.ByteArrayInputStream;

import java.awt.Color;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Controller
@RequestMapping("/")


public class ProductosController {
	
	   private ProductoService productoService;
	   private TipoMedicamentoService tipomedicamentoService;
	   private ProveedorService proveedorService;
	   private MarcaService marcaService;
	   private MovimientoService movimientoService;

	   
	   @Autowired
	    @Qualifier("productoRepository")
	    private ProductoRepository productoRepository;
	   
	   @Autowired
	    @Qualifier("movimientoRepository")
	    private MovimientoRepository movimientoRepository;

	   @Autowired
	    @Qualifier("ingresoRepository")
	    private IngresoRepository ingresoRepository;
	   
	    @Autowired
	    public void setProductoService(ProductoService productoService) {
	        this.productoService = productoService;
	    }
	    
	    @Autowired
	    public void setTipoMedicamentoService(TipoMedicamentoService tipomedicamentoService) {
	        this.tipomedicamentoService = tipomedicamentoService;
	    }
	    
	    @Autowired
	    public void setProveedorService(ProveedorService proveedorService) {
	        this.proveedorService = proveedorService;
	    }
	    
	    @Autowired
	    public void setMarcaService(MarcaService marcaService) {
	        this.marcaService = marcaService;
	    }
	    
	    @Autowired
	    public void setMovimientoService(MovimientoService movimientoService) {
	        this.movimientoService = movimientoService;
	    }	
	    
	    @Autowired
	    @Qualifier("jdbcTemplate")
	    private JdbcTemplate jdbcTemplate;
	  
	    /* 
	    -Nombre de la función: Listado de productos
	    -Objetivo: Mostrar el listado de productos activos
	    -Fecha de creación: 
	    -Autor: Elizabeth Rodríguez
	    -Fecha de última modificación 
	    -Autor de última modificación: Elizabeth Rodríguez   
	      */

	
	  /**
     * Listado de productos
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/productos", method = RequestMethod.GET)
    @PreAuthorize("isAuthenticated() and (hasAnyRole('ROLE_ADMIN','ROLE_GUESS') or hasAuthority('READ_PRODUCTO'))")
    public String listProducts(Model model) {
    	
    	User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	model.addAttribute("username",user.getUsername());
    	model.addAttribute("result",model.asMap().get("result_delete"));
        model.addAttribute("productos", productoService.listAllProductosActivos());
        System.out.println("Listado de Productos");
        return ViewConstant.LIST_PRODUCTS;
    }
    
    /* 
    -Nombre de la función: changeStatus
    -Objetivo: Cambiar estado de productos
    -Fecha de creación: 
    -Autor: Elizabeth Rodríguez
    -Fecha de última modificación 
    -Autor de última modificación: Elizabeth Rodríguez   
      */

    
    /**
     * Cambiar estado de productos
     *
     * @param model
     * @return
     */
    
	
    @RequestMapping(value = "/producto/form_status", method = RequestMethod.GET)
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('READ_PRODUCTO'))")
    public String listProductos(Model model) {
    	
    	User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	model.addAttribute("username",user.getUsername());
    	model.addAttribute("result",model.asMap().get("result_delete"));
        model.addAttribute("productos", productoService.listAllProductos());
        model.addAttribute("tipos", tipomedicamentoService.listAllTiposMedicamento());
        System.out.println("Listado de todos los Productos ");
        return ViewConstant.STATUS_PRODUCTS;
    }
    
    @RequestMapping("producto/statusForm/{id}")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('WRITE_PRODUCTO'))")
    public String changeStatus(@PathVariable Long id, Model model){
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        model.addAttribute("username",user.getUsername());
        model.addAttribute("producto", productoService.getProductoById(id));
        System.out.println("Preguntando si está seguro de realizar el cambio");
        return ViewConstant.ALERT_STATUS_PRODUCTS;
    }

    @RequestMapping("producto/status/{id}")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('WRITE_PRODUCTO'))")
    public String changeStatusForm(@PathVariable Long id, Model model, RedirectAttributes rm){

    User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    model.addAttribute("username",user.getUsername());

    Producto changeStatus = productoService.changeStatusProducto(productoService.getProductoById(id));

       if(null != changeStatus){
           rm.addFlashAttribute("result_change",1);
       }else{
           rm.addFlashAttribute("result_change",0);
       }
       System.out.println("Estado de producto modificado");
       return ViewConstant.PRODUCTS;
    }
    
    /* 
    -Nombre de la función: ProductoDelete
    -Objetivo: Desactivar producto
    -Fecha de creación: 
    -Autor: Elizabeth Rodríguez
    -Fecha de última modificación 
    -Autor de última modificación: Elizabeth Rodríguez   
      */
    
    /**
     * Desactivar producto
     *
     * @param id
     * @param model
     * @return
     */
   
    @GetMapping("producto/deleteForm/{id}")
    @PreAuthorize("isAuthenticated() and (hasAnyRole('ROLE_ADMIN','ROLE_GUESS') or hasAuthority('DELETE_PRODUCTO'))")
    public String ConfirmProductoDelete(@PathVariable Long id, Model model){

        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        model.addAttribute("username",user.getUsername());
        model.addAttribute("producto", productoService.getProductoById(id));
        System.out.println("Preguntando si está seguro de realizar el cambio");
        return ViewConstant.DELETE_PRODUCTS;
    }
    /**
     * Delete producto
     *
     * @param id
     * @param model
     * @return
     */
  
    @RequestMapping("producto/delete/{id}")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('DELETE_PRODUCTO'))")
    public String deleteProducto(@PathVariable Long id, Model model, RedirectAttributes rm){

    	User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	model.addAttribute("username",user.getUsername());
       Producto deleteProducto = productoService.deleteProducto(productoService.getProductoById(id));

       if(null != deleteProducto){
           rm.addFlashAttribute("result_delete",1);
       }else{
           rm.addFlashAttribute("result_delete",0);
       }
       System.out.println("Producto desactivado");
        return ViewConstant.LIST_STATUS_CHANGE;
    }
    
    /* 
    -Nombre de la función: ProductoSave
    -Objetivo: Guardar producto
    -Fecha de creación: 
    -Autor: Elizabeth Rodríguez
    -Fecha de última modificación 
    -Autor de última modificación: Elizabeth Rodríguez   
      */
    
    
    /**
     * New producto.
     *
     * @param model
     * @return
     */
    @RequestMapping("producto/form_create")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('WRITE_PRODUCTO'))")
    public String saveProductos(Model model) {
    	System.out.println("1");
    	
        model.addAttribute("producto", new Producto());
        
        model.addAttribute("tiposmedicamento", tipomedicamentoService.listAllTiposMedicamento());
        model.addAttribute("marca", marcaService.listAllMarca());
        model.addAttribute("proveedor", proveedorService.listAllProveedores());
        
        System.out.println("Llenar formulario");
        return "crud_producto/form_create";
    }

    /**
     * Save tipo de medicamento 
     *
     * @param tipomedicamento
     * @return
     */
    @RequestMapping(value = "/guardado", method = RequestMethod.POST)
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('WRITE_PRODUCTO'))")
    public String saveProductos(Producto producto, Model model) {
    	model.addAttribute("tiposmedicamento", tipomedicamentoService.listAllTiposMedicamento());
    	model.addAttribute("marca", marcaService.listAllMarca());
    	model.addAttribute("proveedor", proveedorService.listAllProveedores());
        productoService.addProducto(producto);
        System.out.println("Tipo de producto guardado");
        return ViewConstant.LIST_PRODUCTS_ADD;
    }
    
    @RequestMapping("producto/update/{id}")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('WRITE_PRODUCTO'))")
    public String updateProducto(@PathVariable Long id, Model model) {
   	
   	User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
   	model.addAttribute("username",user.getUsername());
       model.addAttribute("producto", productoService.getProductoById(id));
       model.addAttribute("tiposmedicamento", tipomedicamentoService.listAllTiposMedicamento());
       model.addAttribute("marca", marcaService.listAllMarca());
       model.addAttribute("proveedor", proveedorService.listAllProveedores());
		System.out.println("Editar producto");
       if(model.asMap().get("result_edit")!= null){
			model.addAttribute("result",model.asMap().get("result_edit"));
		}else{
			model.addAttribute("result",2);
		}
       return ViewConstant.UPDATE_PRODUCTS;
   }
    
    @RequestMapping(value = "/producto/update", method = RequestMethod.POST)
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('WRITE_PRODUCTO'))")
    public String updateProductos(Producto producto, Model model) {
    	if(producto.getId()!= null && producto.getId() > 0) {
    		//Set success procesure
            productoService.editProducto(producto);
            System.out.println("Tipo de producto guardado");
    	}
        return ViewConstant.LIST_PRODUCTS_ADD;
    }
    
    /* 
    -Nombre de la función: showProducto
    -Objetivo: Ver el medicamento
    -Fecha de creación: 
    -Autor: Elizabeth Rodríguez
    -Fecha de última modificación 
    -Autor de última modificación: Elizabeth Rodríguez   
      */

    /**
     * View medicamento
     *
     * @param id
     * @param model
     * @return
     */
     @RequestMapping("producto/{id}")
     @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('READ_PRODUCTO'))")
     public String showProducto(@PathVariable Long id, Model model) {
    	
    	User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	model.addAttribute("username",user.getUsername());
        model.addAttribute("producto", productoService.getProductoById(id));
		System.out.println("Ver producto");
        if(model.asMap().get("result_edit")!= null){
			model.addAttribute("result",model.asMap().get("result_edit"));
		}else{
			model.addAttribute("result",2);
		}
        return ViewConstant.SHOW_PRODUCTS;
    }
     
     /* 
     -Nombre de la función: formKardex
     -Objetivo: Registrar ingreso de producto
     -Fecha de creación: 27/12/2019
     -Autor: Héctor Martínez
     -Fecha de última modificación 
     -Autor de última modificación: Héctor Martínez  
       */

     /**
      * View medicamento
      *
      * @param id
      * @param model
      * @return
      */
     @RequestMapping("producto/kardex")
     @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAuthority('READ_PRODUCTO'))")
     public String queryKardex( Model model)   {
         model.addAttribute("productos", productoService.listAllProductosActivos());
         model.addAttribute("productFilter", new ProductFilter());
         
         return "crud_producto/kardex";
     }

 	//@RequestMapping("producto_rest/kardex/report") 
 	public void exportReport(HttpServletResponse response, @RequestParam("idProd") long idProd,@RequestParam("fini_") String fini_,@RequestParam("fend_") String fend_) throws JRException, IOException, SQLException, ClassNotFoundException  {
 	 	// Validar datos de entrada en ProductFilter
 	    List<Movimiento> list = new ArrayList<Movimiento>();
 	    LocalDate fini;
 	    LocalDate fend;
 	    	
 	    System.out.print("\n\n Contenido de parametros: \n idProd: "+idProd+"\n fini: "+fini_+"\n fend: "+fend_+"\n");
 	    	
 		try {
 			fini = fini_ != ""? LocalDate.parse(fini_): null;  
 			fend = fend_ != ""? LocalDate.parse(fend_): null;   
 		} catch (Exception e1) {
 			// TODO Auto-generated catch block
 			e1.printStackTrace();
 			fini = null;
 			fend = null;
 		}
    	 
        try {
        	
	    	list = movimientoService.filter(idProd,fini,fend);	
	    	
        	if(!list.isEmpty()) {

    	        File file = ResourceUtils.getFile("classpath:templates/crud_producto/ReportKardex.jrxml");
    	        JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath());
    	 		JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(list);
    	 		Map<String,Object> parameters = new HashMap<String, Object>();
    	 		
    	 		Producto prod = productoRepository.findById(idProd);
    	 		parameters.put("idProd", idProd);
    	 		parameters.put("nombreProd", prod.getNombre());
    	 		parameters.put("activoProd", prod.getEstado().toString());
    	 		parameters.put("invProd", prod.getInventario());
    	 		parameters.put("stoklimProd", prod.getLimStock());
    	 		parameters.put("costoProm", prod.getCostoProm());
    	 		parameters.put("dateStart", fini);
    	 		parameters.put("dateEnd", fend);
    	 		
    	        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);
    	        response.setContentType("application/pdf");
    	        response.setHeader("Content-Disposition", "inline; filename=kardex.pdf");
    			JasperExportManager.exportReportToPdfStream(jasperPrint, response.getOutputStream());
    			response.getOutputStream().flush();
    			response.getOutputStream().close();        		
        	}
        }catch(Exception e) {
        	e.printStackTrace();
        }

 	}
	    

 	 @RequestMapping("producto_rest/kardex/report")
    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAnyAuthority('READ_PRODUCTO','READ_MOVIMIENTO'))")
 	public ResponseEntity<InputStreamResource> exportReportTest(HttpServletResponse response, @RequestParam("idProd") long idProd,@RequestParam("fini_") String fini_,@RequestParam("fend_") String fend_) {
 		 ByteArrayOutputStream out = new ByteArrayOutputStream(); 
 		 // Validar datos de entrada en ProductFilter
  	    List<Movimiento> list = new ArrayList<Movimiento>();
  	    LocalDate fini;
  	    LocalDate fend;
  	    	
  	    System.out.print("\n\n Contenido de parametros: \n idProd: "+idProd+"\n fini: "+fini_+"\n fend: "+fend_+"\n");
  	    	
  		try {
  			fini = fini_ != ""? LocalDate.parse(fini_): null;  
  			fend = fend_ != ""? LocalDate.parse(fend_): null;   
  		} catch (Exception e1) {
  			// TODO Auto-generated catch block
  			e1.printStackTrace();
  			fini = null;
  			fend = null;
  		}    	 
         try {
         	
 	    	list = movimientoService.filter(idProd,fini,fend);	
 	    	
         	if(!list.isEmpty()) {

  			   Document doc = new Document();
  			   //PdfWriter writer = PdfWriter.getInstance(doc, new FileOutputStream(DEST));
  			   //setting font family, color
  			   Font font = new Font(Font.HELVETICA, 16, Font.BOLDITALIC, Color.RED);
  			   Paragraph para = new Paragraph("Hello! This PDF is created using openPDF", font);
  			   PdfWriter.getInstance(doc, out);
  			   doc.open();
  			   doc.add(para);
  			   doc.close();     		
         	}
         }catch(Exception e) {
         	e.printStackTrace();
         } 		
 		  		  
          HttpHeaders headers = new HttpHeaders();
          headers.add("Content-Disposition", "inline; filename=kardexreport.pdf");
          ByteArrayInputStream bis = KardexPdfView.kardexReport(list);
 		 return ResponseEntity.ok().headers(headers).contentType(MediaType.APPLICATION_PDF).body(new InputStreamResource(bis));
 	 }
 	 
     @RequestMapping("/producto/bajoinventario")
	    @PostMapping
	    @PreAuthorize("isAuthenticated() and (hasAnyRole('ROLE_ADMIN','ROLE_GUESS') or hasAuthority('READ_PRODUCTO'))")
	    public void Report(HttpServletResponse response) throws JRException, IOException, SQLException, ClassNotFoundException {
	      
    	  System.out.println("Inventario bajo");
    	  Connection conexion = jdbcTemplate.getDataSource().getConnection();
	    
	        String path = ResourceUtils.getURL("classpath:templates/crud_producto/Estadisticas.jrxml").getFile();
	        JasperReport jasperReport = JasperCompileManager.compileReport(path);
	        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, null, conexion);
	        response.setContentType("application/pdf");
	        response.setHeader("Content-Disposition", "inline; filename=productos.pdf");
	    	final OutputStream outStream = response.getOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint, outStream);
			response.getOutputStream().flush();
			response.getOutputStream().close();
     }
	    
	
	    /**
	     * Reporte de Producto
	     *
	     * @param mode
	     * @return
	     * @throws IOException 
	     */
	    
	    @RequestMapping("/producto/reporte")
	    @PostMapping
	    @PreAuthorize("isAuthenticated() and (hasAnyRole('ROLE_ADMIN','ROLE_GUESS') or hasAuthority('READ_PRODUCTO'))")
	    public void ReporteInventario(HttpServletResponse response) throws SQLException,JRException, ClassNotFoundException, IOException{

	        System.out.println("inventario ");
	        Connection conexion = jdbcTemplate.getDataSource().getConnection();

	        String path = ResourceUtils.getURL("classpath:templates/crud_producto/reporte.jrxml").getFile();

	        JasperReport jasperReport = JasperCompileManager.compileReport(path);
	        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, null, conexion);
	        response.setContentType("application/pdf");
	        response.setHeader("Content-Disposition", "inline; filename=productos.pdf");
	    	final OutputStream outStream = response.getOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint, outStream);
			response.getOutputStream().flush();
			response.getOutputStream().close();

	    }
	    
		//PRODUCTO IMPRIMIR
		@RequestMapping(value = "/producto/pdf_producto",method = RequestMethod.GET,produces = MediaType.APPLICATION_PDF_VALUE)
	    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAnyAuthority('READ_PRODUCTO'))")
	    public ResponseEntity<InputStreamResource> productoReport() throws Exception{
			List<Producto> productos;
			System.out.println("inventario ");
	        DateFunction dateFunction = new DateFunction();
	        String nombreReporte=null;
	        System.out.println("inventario2 ");
	        try {
	        	productos = (List<Producto>) productoService.listAllProductosActivos();
	        	if(productos == null) {
	        		return ResponseEntity.notFound().build();
	        	}
	        	nombreReporte = "inline; filename=Producto_"+dateFunction.convertDateToSpanish(new Date())+".pdf";       
	        	System.out.println("3 ");
	        }catch (DataAccessException e){
	            productos=null;
	        }
	        
	        HttpHeaders headers = new HttpHeaders();
	        headers.add("Content-Disposition","inline; filename=Productos.pdf");
	        ByteArrayInputStream bis = ProductosPdfView.productosReport(productos);
	       
	        System.out.println("Productos generados");
	        return ResponseEntity.ok().headers(headers).contentType(MediaType.APPLICATION_PDF).body(new InputStreamResource(bis));
	    }
		
		//PRODUCTO DESACTIVADO IMPRIMIR
				@RequestMapping(value = "/producto/producto_desactivado",method = RequestMethod.GET,produces = MediaType.APPLICATION_PDF_VALUE)
			    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAnyAuthority('READ_PRODUCTO'))")
			    public ResponseEntity<InputStreamResource> productoDesactivadoReport() throws Exception{
					List<Producto> productos;
					System.out.println("producto");
			        DateFunction dateFunction = new DateFunction();
			        String nombreReporte=null;

			        try {
			        	productos = (List<Producto>) productoService.listAllProductosDesactivados();
			        	if(productos == null) {
			        		return ResponseEntity.notFound().build();
			        	}
			        	nombreReporte = "inline; filename=Producto_"+dateFunction.convertDateToSpanish(new Date())+".pdf";       

			        }catch (DataAccessException e){
			            productos=null;
			        }
			        
			        HttpHeaders headers = new HttpHeaders();
			        headers.add("Content-Disposition","inline; filename=ProductosDesactivado.pdf");
			        ByteArrayInputStream bis = ProductosDesactivadosPdfView.productosDReport(productos);
			       
			        System.out.println("Productos desactivado");
			        return ResponseEntity.ok().headers(headers).contentType(MediaType.APPLICATION_PDF).body(new InputStreamResource(bis));
			    }
				
				
				//PRODUCTO CON EXISTENCIA BAJA
				@RequestMapping(value = "/producto/producto_existenciabaja",method = RequestMethod.GET,produces = MediaType.APPLICATION_PDF_VALUE)
			    @PreAuthorize("isAuthenticated() and (hasRole('ROLE_ADMIN') or hasAnyAuthority('READ_PRODUCTO'))")
			    public ResponseEntity<InputStreamResource> productoExistenciaBajaReport() throws Exception{
					List<Producto> productos;
					System.out.println("existencia baja ");
			        DateFunction dateFunction = new DateFunction();
			        String nombreReporte=null;

			        try {
			        	productos = (List<Producto>) productoService.listAllProductosExistenciaBaja();
			        	if(productos == null) {
			        		return ResponseEntity.notFound().build();
			        	}
			        	nombreReporte = "inline; filename=ProductoBajaExistencia_"+dateFunction.convertDateToSpanish(new Date())+".pdf";       
			        	System.out.println("3 ");
			        }catch (DataAccessException e){
			            productos=null;
			        }
			        
			        HttpHeaders headers = new HttpHeaders();
			        headers.add("Content-Disposition","inline; filename=ProductosExistenciaBaja.pdf");
			        ByteArrayInputStream bis = ProductosExistenciaBajaPdfView.productosBJReport(productos);
			       
			        System.out.println("Productos con existencia baja");
			        return ResponseEntity.ok().headers(headers).contentType(MediaType.APPLICATION_PDF).body(new InputStreamResource(bis));
			    }	
	    
	    /**
	     * Reporte de Productos Desactivados
	     *
	     * @param mode
	     * @return
	     * @throws IOException 
	     */
	    
	    @RequestMapping("/producto/noexistencia")
	    @PostMapping
	    @PreAuthorize("isAuthenticated() and (hasAnyRole('ROLE_ADMIN','ROLE_GUESS') or hasAuthority('READ_PRODUCTO'))")
	    public void ReporteInventarioNoExistencia(HttpServletResponse response) throws SQLException,JRException, ClassNotFoundException, IOException{

	        System.out.println("inventario ");
	        Connection conexion = jdbcTemplate.getDataSource().getConnection();

	        String path = ResourceUtils.getURL("classpath:templates/crud_producto/productos-desactivados.jrxml").getFile();

	        JasperReport jasperReport = JasperCompileManager.compileReport(path);
	        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, null, conexion);
	        response.setContentType("application/pdf");
	        response.setHeader("Content-Disposition", "inline; filename=productos-noexistentes.pdf");
	    	final OutputStream outStream = response.getOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint, outStream);
			response.getOutputStream().flush();
			response.getOutputStream().close();

	    }
	    
	    
	    
	    
	
	    
	    
}


	    
	    
	    
	    
	    