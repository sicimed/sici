package com.sici.controller;

import com.sici.serviceImp.ClinicaService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.sici.serviceImp.UserService;


@Controller
public class LoginController {

	@Autowired
	@Qualifier("clinicaServiceImp")
	private ClinicaService clinicaService;

    @Autowired
    @Qualifier("userService")
	private UserService userService;
	
	private static final Log LOG = LogFactory.getLog(LoginController.class);
	
	@GetMapping("/login")
	public String showLoginForm( Model model,
			@RequestParam(name = "error", required = false) String error,
			@RequestParam(name = "logout", required = false) String logout){
		
		LOG.info("METHOD: showLoginForm() --PARAMS: error ="+error+", logout: "+logout);
		model.addAttribute("error", error);
		model.addAttribute("logout", logout);
		LOG.info("Running to login view");

		return "login"; //login auth-normal-sign-in
	}
	
	@GetMapping({"/loginsuccess"})
	public String loginCheck() {
		LOG.info("METHOD: loginCheck()");
		LOG.info("Running to patients view");
		return "redirect:/";
	}
	@RequestMapping("/403")
	public String accessDenied( Model model,
	@RequestParam(name = "error", required = false) String error,
	@RequestParam(name = "logout", required = false) String logout){
		
    	User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	model.addAttribute("username",user.getUsername());
    	
		LOG.info("METHOD: accessDenied() --PARAMS: error ="+error+", logout: "+logout);
		model.addAttribute("error", error);
		model.addAttribute("logout", logout);
		LOG.info("Running to 403 view");
		return "error/403";
	}
	
}
